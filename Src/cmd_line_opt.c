/*
*      ____                         __ __
*     / __ \ ____ __ ___   __  __  / // /
*    / /_/ // __// // _ \ /  \/ / / // /
*    \____//_/  /_/ \___//_/\__/ /_//_/
*    
*    Based on Pluto 3.
*
*    Please refer to COPYING in Pluto's root directory.
*
*   Modification: Orional code based on PLUTO3.0
*   1) PS(08/06/10): Change log file name to orion2.
*
*/

#include "orion2.h"

/* ********************************************************** */
void GET_OPT (int argc, char *argv[], char *ini_file, 
              Cmd_Line *cmd)
/*
 *
 * PURPOSE
 *
 *   Parse command line options for ORION2.
 *   Available command lines are:
 *
 *
 *    -makegrid       run the code until the grid is
 *                    generated and stop. No calculation
 *                    is done;
 *    -i name         use name as initialization file, instead
 *                    of orion2.ini (default);
 *    -restart [n]    restart the code from last written 
 *                    file is n is not specified; otherwise
 *                    restart from file n;
 *    -ext            assign initial conditions from external file(s).
 *    -jet            follow jet evolution by integrating only up to the jet position.
 *    -maxsteps n     run the code for n steps and then stop;
 *    -no-write       run the code without saving files to disk;
 *    -no-x1par       do not parallelize along the x-direction (on parallel architectures only)
 *    -no-x2par       do not parallelize along the y-direction (on parallel architectures only)
 *    -no-x3par       do not parallelize along the z-direction (on parallel architectures only)
 *    -ext            assign initial conditions from external files
 *    --help          print a help screen with possible
 *                    command line options.
 *
 ************************************************************ */
{
  int i,j;

/* --------------------------------------------------------------------
                 Parse Command Line Options
   -------------------------------------------------------------------- */

  cmd->restart  = NO;
  cmd->maxsteps = 0;
  cmd->write    = YES;
  cmd->makegrid = NO; 
  cmd->ext      = NO;
  cmd->jet      = NO;

  for (i = 1; i < argc ; i++){

    if (!strcmp(argv[i],"-ext")) {

      cmd->ext = YES;

    } else if (!strcmp(argv[i],"-jet")) {

      cmd->jet = YES;

    } else if (!strcmp(argv[i],"-makegrid")) {

      cmd->makegrid = YES;

    }else if (!strcmp(argv[i],"-i")) {

      sprintf (ini_file,"%s",argv[++i]);
    
    }else  if (!strcmp(argv[i],"-restart")) {

     /* --------------------------------------------- 
          default restart is last written file (-1)
        --------------------------------------------- */

      cmd->restart = -1; 

      if ((++i) < argc){
        cmd->restart = atoi(argv[i]);

        /* -------------------------------------
            if a non-numerical character is
            encountered, atoi() returns 0 and
            cmd->restart should again be set to -1
           --------------------------------------- */

        if (cmd->restart == 0) {
          i--;
          cmd->restart = -1;
        }
      }

    }else if (!strcmp(argv[i],"-maxsteps")){

      if ((++i) >= argc){
        printf (" ! You must specify -maxsteps nn\n");
        QUIT_ORION2(1);
      }else{
        cmd->maxsteps = atoi(argv[i]);
        if (cmd->maxsteps == 0) {
          printf (" ! You must specify -maxsteps nn, with nn > 0 \n");
          QUIT_ORION2(0)
        }
      }

    }else if (!strcmp(argv[i],"-no-write")) {
      cmd->write = NO;
    }else if (!strcmp(argv[i],"-no-x1par")) {
      cmd->parallel_dim[IDIR] = NO;
    }else if (!strcmp(argv[i],"-no-x2par")) {
      cmd->parallel_dim[JDIR] = NO;
    }else if (!strcmp(argv[i],"-no-x3par")) {
      cmd->parallel_dim[KDIR] = NO;
    }else if (!strcmp(argv[i],"--help")){
      printf ("Usage: orion2 [OPTIONS]\n\n");
      printf (" -ext           assign initial conditions from external file(s);\n");
      printf (" -i <name>      use <name> as initialization file,\n");
      printf ("                instead of orion2.ini;\n");
      printf (" -jet           save computational time by excluding from integration\n");
      printf ("                regions of zero pressure gradient.\n");
      printf ("                This option is specifically designed for jets propagating\n");
      printf ("                in the positive x2-direction. In parallel mode, automatically\n");
      printf ("                disable domain decomposition in the x2-direction;\n");
      printf (" -makegrid      generate grid only;\n");
      printf (" -maxsteps n    stop calculations after n steps;\n");
      printf (" -no-write      do not write data to disk;\n");
      printf (" -no-x1par      do not perform domain decomposition along \n");
      printf ("                the x1-direction (on parallel architectures only);\n");
      printf (" -no-x2par      do not perform domain decomposition along \n");
      printf ("                the x2-direction (on parallel architectures only);\n");
      printf (" -no-x3par      do not perform domain decomposition along \n");
      printf ("                the x3-direction (on parallel architectures only);\n");
      printf (" -restart [n]   do not assign initial condition,\n");
      printf ("                but restart from file n instead. If n\n");
      printf ("                is not given last written file is assumed;\n");

      QUIT_ORION2(0);
    }else{
      print (" > Unknown options '%s'\n",argv[i]);
      QUIT_ORION2(1);
    }
  }

/* -- disable domain decomposition in the 
      x2 direction if -jet has been given  -- */

  if (cmd->jet) cmd->parallel_dim[JDIR] = NO;

#if PRINT_TO_FILE == YES
#ifdef PARALLEL 
  if (prank == 0){
#endif
  if (cmd->restart == 0){
    orion2_log_file = fopen("orion2.log","w");
    fprintf(orion2_log_file,"\n");
    fclose(orion2_log_file);
  }else{
    orion2_log_file = fopen("orion2.log","aw");
    fprintf(orion2_log_file,"\n");
    fclose(orion2_log_file);
  } 
#ifdef PARALLEL 
  }
#endif
   
#endif
}




