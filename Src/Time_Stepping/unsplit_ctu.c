#include"orion2.h"

#ifdef STAGGERED_MHD
 #if TIME_STEPPING == CHARACTERISTIC_TRACING
  #define CTU_MHD_SOURCE YES
 #elif TIME_STEPPING == HANCOCK && PRIMITIVE_HANCOCK == YES
  #define CTU_MHD_SOURCE YES
 #else
  #define CTU_MHD_SOURCE NO
 #endif
#else
 #define CTU_MHD_SOURCE NO
#endif

static void ADD_CTU_MHD_SOURCE (real **, real **, real **,
                                real *, int, int, Grid *);

/* ************************************************************ */
void UNSPLIT (const Data *d, Riemann_Solver *RIEMANN, 
              Time_Step *Dts, Grid *grid)

/*
 * 
 * PURPOSE:
 *
 *   Unsplit Corner transport upwind method (CTU). It consists of
 *
 *  1) a predictor step:
 *
 *     - start from cell center values of V and compute
 *       V+ and V- at n+1/2 using either HANCOCK or
 *       CHARACTERISTIC_TRACING:
 *
 *         V+ = V + dV/2 - dt/2*A*dV/dx + dt/2*S
 *         V- = V - dV/2 - dt/2*A*dV/dx + dt/2*S
 *
 *       Store the resulting states by converting V+ and V- 
 *       in U+ and U-.
 *       Normal (n) and trasnverse (t) indexes for this step 
 *       should span
 *  
 *         n in [nbeg - 1, nend + 1]
 *         t in [tbeg - 1, tend + 1]  for cell-centered schemes
 *
 *         n in [nbeg - 2, nend + 2]
 *         t in [tbeg - 2, tend + 2]  for staggered MHD
 *          
 *
 *     - Solve Riemann problems between V+(i) and V-(i+1) and
 *       store the flux difference in RHS.
 *
 *     - Compute cell and time-centered values with 
 *
 *       UH = U + dt/2*RHS  in 
 *
 *        n in [nbeg, nend]
 *        t in [tbeg, tend]      for cell-centered schemes
 *
 *        n in [nbeg-1, nend+1]
 *        t in [tbeg-1, tend+1]  for staggered MHD
 *  
 *
 *  2) Corrector:
 *
 *     - correct left and right states UP and UM computed
 *       in step 1) by adding the transverse RHS contribution:
 *       
 *         UP += dt/2*RHS(t)
 *         UM += dt/2*RHS(t)
 * 
 *       This step should cover  
 *       
 *         [nbeg-1, nend + 1], [tbeg, tend] for cell-cent
 *
 *         [nbeg-2, nend + 2], [tbeg-1, tend+1] for stag. MHD
 *
 *  3) Solve a normal Riemann problem with left and right states
 *     UP and UM, get RHS and evolve to final stage:
 *
 *         U(n+1) = U(n) + dt*RHS
 *  
 *
 *    Reference papers: 
 *  
 *
 * A)  "The Piecewise Parabolic Method for Multidimensional 
 *      Relativistic Fluid Dynamics" 
 *
 *      Mignone, A.; Plewa, T.; Bodo, G. 
 *      ApJS (2005), 160..199M 
 * 
 * B) "An unsplit Godunov method for ideal MHD via constrained transport 
 *     Gardiner & Stone
 *     JCP (2005), 205, 509
 *
 *    This integrator performs an integration in the 
 *    ghost boundary zones, in order to recover appropriate 
 *    information to build the transverse predictors.
 *
 *
 *
 *  Last Modified
 *
 *     Sept 10 2008 by A. Mignone (mignone@to.astro.it)
 *     
 *************************************************************** */
{
  int ii, jj, kk, nv, in;
  int errp, errm;
  int *i, *j, *k;
  real dt2;
  Index indx;
  Data_Arr  UP, UM;
  static char *flagm, *flagp;
  static Data_Arr UMX, UPX;  
  static Data_Arr UMY, UPY;  
  static Data_Arr UMZ, UPZ;

  static Data_Arr UU, dU;
  static State_1D state;
  static real *dtdVx2, *dtdVy2, *dtdVz2;
  real *dtdV2, *du;
  real **u, **rhs, **up, **um;
  #ifdef STAGGERED_MHD
   real ***bx, ***by, ***bz;
  #endif
  
/* -----------------------------------------------------------------
               Check algorithm compatibilities
   ----------------------------------------------------------------- */

  #if INCLUDE_PARABOLIC_FLUX == YES && ARTIFICIAL_VISCOSITY == NO
   print1 ("! Parabolic Terms are 1st order in CTU at the moment\n");
   print1 ("! Uncomment this line if you wish to test them.\n");
   QUIT_ORION2(1);
  #endif
 
  #if !(GEOMETRY == CARTESIAN || GEOMETRY == CYLINDRICAL)
   print1 ("! CTU only works in cartesian or cylindrical coordinates\n");
   QUIT_ORION2(1);
  #endif     
   
/*  ---------------------------------------------------------------------------
                   Allocate static memory areas   
    ---------------------------------------------------------------------------  */

  if (UU == NULL){

    dtdVx2 = Array_1D(NMAX_POINT, double);
    dtdVy2 = Array_1D(NMAX_POINT, double);
    dtdVz2 = Array_1D(NMAX_POINT, double);

    Make_State (&state);

    flagp = Array_1D(NMAX_POINT, char);
    flagm = Array_1D(NMAX_POINT, char);

    UU  = Array_4D(NZ_TOT, NY_TOT, NX_TOT, NVAR, double);
    dU  = Array_4D(NZ_TOT, NY_TOT, NX_TOT, NVAR, double);
   
  /* ------------------------------------------
      corner-coupled multidimensional arrays 
      are ordered in memory following the same 
      convention adopted when sweeping along 
      the coordinate directions, i.e., 

         (z,y,x)->(z,x,y)->(y,x,z).

      This allows 1-D arrays to conveniently
      point at the fastest running indexes 
      of the respective multi-D ones.
     ----------------------------------------- */  
        
    UMX = Array_4D(NZ_TOT, NY_TOT, NX_TOT, NVAR, double);
    UPX = Array_4D(NZ_TOT, NY_TOT, NX_TOT, NVAR, double);

    UMY = Array_4D(NZ_TOT, NX_TOT, NY_TOT, NVAR, double);
    UPY = Array_4D(NZ_TOT, NX_TOT, NY_TOT, NVAR, double);

    #if DIMENSIONS == 3
     UMZ  = Array_4D(NY_TOT, NX_TOT, NZ_TOT, NVAR, double);
     UPZ  = Array_4D(NY_TOT, NX_TOT, NZ_TOT, NVAR, double);
    #endif
    
  }

/* -- save pointers -- */

  u   = state.u;
  rhs = state.rhs;

/*  memset (dU[0][0][0], 0.0, NX_TOT*NY_TOT*NZ_TOT*NVAR*sizeof(double));  */

/* ----------------------------------------------------
           SET BOUNDARY CONDITIONS  
   ---------------------------------------------------- */

  ISTEP = 1;
  BOUNDARY (d, ALLDIR, grid);

  dt2 = 0.5*delta_t;

  #ifdef STAGGERED_MHD
   D_EXPAND(bx = d->Vs[BXs];  ,
            by = d->Vs[BYs];  ,
            bz = d->Vs[BZs];)
  #endif

  D_EXPAND(
   ITOT_LOOP(ii) dtdVx2[ii] = dt2/grid[IDIR].dV[ii]; ,
   JTOT_LOOP(jj) dtdVy2[jj] = dt2/grid[JDIR].dV[jj]; ,
   KTOT_LOOP(kk) dtdVz2[kk] = dt2/grid[KDIR].dV[kk];
  )

  #if INTERPOLATION == LINEAR_MULTID 
   MULTID_LIMITER (d, grid);
  #endif
  #if SHOCK_FLATTENING == MULTID
   FIND_SHOCK (d, grid);
  #endif

/* ----------------------------------------------------
     1. Compute Normal predictors and
        solve normal Riemann problems. 
        Store computations in UP, UM, RHS (X,Y,Z)
   ---------------------------------------------------- */
 
  for (DIR = 0; DIR < DIMENSIONS; DIR++){

    if      (DIR == IDIR) {UP = UPX; UM = UMX; dtdV2 = dtdVx2;}
    else if (DIR == JDIR) {UP = UPY; UM = UMY; dtdV2 = dtdVy2;}
    else if (DIR == KDIR) {UP = UPZ; UM = UMZ; dtdV2 = dtdVz2;}

    SET_INDEXES (&indx, &state, grid);
    TRANSVERSE_LOOP(indx,in,i,j,k){  

    /* ---------------------------------------------
        save computational time by having state.up 
        and state.um pointing at the fastest 
        running indexes of UP and UM. 
        Also, during the x-sweep we initialize UU 
        and dU by changing the memory address of 
        state.u and state.rhs. 
       --------------------------------------------- */
        
      state.up = UP[indx.t2][indx.t1]; state.uL = state.up;
      state.um = UM[indx.t2][indx.t1]; state.uR = state.um + 1;

      if (DIR == IDIR) {
        state.u   = UU[*k][*j]; 
        state.rhs = dU[*k][*j];
        for (nv = NVAR; nv--;  )
          state.rhs[indx.beg-1][nv] = state.rhs[indx.end+1][nv] = 0.0;
      }else{
        state.u   = u;        
        state.rhs = rhs;       
      }

    /* ---- get a 1-D array of primitive quantities ---- */

      for (in = 0; in < indx.ntot; in++) {
        for (nv = NVAR; nv--;  ) state.v[in][nv] = d->Vc[nv][*k][*j][*i];
        #ifdef STAGGERED_MHD
         state.bn[in] = d->Vs[DIR][*k][*j][*i];
        #endif
      }

      CHECK_NAN (state.v, 0, indx.ntot - 1, 0);
      PRIMTOCON (state.v, state.u, 0, indx.ntot - 1);
      STATES  (&state, indx.beg - 1, indx.end + 1, delta_t, grid);
      RIEMANN (&state, indx.beg - 1, indx.end, Dts->cmax + DIR, grid);
      #ifdef STAGGERED_MHD
       EMF_PUT (&state, indx.beg - 1, indx.end, grid);
      #endif
      GET_RHS (&state, indx.beg, indx.end, dt2, grid);

      #if CTU_MHD_SOURCE == YES
       ADD_CTU_MHD_SOURCE (state.v, state.up, state.um,
                           dtdV2, indx.beg - 1, indx.end + 1, grid);
      #endif

   /* --------------------------------------------------
        At this point we have at disposal the normal 
        predictors U^_\pm.
        To save memory, we compute corner coupled
        states by first subtracting the normal contribution
        and then by adding the total time increment.
        For example, in the x-direction, we do

         U^{n+1/2}_\pm = U^_\pm - RX + (RX + RY + RZ)

        where the first subtraction is done here while 
        dU = (RX + RY + RZ) is the total time increment 
        which will be added later (step 2 below).
        In a 2-D domain dU contains the following 
        (X,Y,Z) contributions:

             X 
         +--------+
         |        |
        Y|   XY   |Y
         |        |
         +--------+
             -X-
      -------------------------------------------------- */

      if (DIR == IDIR){
        for (in = indx.beg; in <= indx.end; in++) {
          du = state.rhs[in];
          for (nv = NVAR; nv--; ){
            state.up[in][nv] -= du[nv];
            state.um[in][nv] -= du[nv];
          }
        }
      }else{
        for (in = indx.beg; in <= indx.end; in++) {
          du = state.rhs[in];
          for (nv = NVAR; nv--; ){
            dU[*k][*j][*i][nv] += du[nv];
            state.up[in][nv]   -= du[nv];
            state.um[in][nv]   -= du[nv];
          }
        }
      }

    } /* -- end loop on transverse directions -- */
  } /* -- end loop on dimensions -- */

/* ----------------------------------------------------
     2. Corner coupled states 
   ---------------------------------------------------- */

  /* --------------------------------------
      Correct + and - states in the 
      x1-direction by adding contributions 
      from the transverse directions
     -------------------------------------- */

  DIR = IDIR;
  SET_INDEXES (&indx, &state, grid);
  TRANSVERSE_LOOP(indx,in,i,j,k){  
    up = UPX[indx.t2][indx.t1];
    um = UMX[indx.t2][indx.t1];
    for (in = indx.beg - 1; in <= indx.end + 1; in++) {
      du = dU[*k][*j][*i];
      for (nv = NVAR; nv--; ){
        up[in][nv] += du[nv];
        um[in][nv] += du[nv];
      }
    }
  }

  /* --------------------------------------
      Correct + and - states in the 
      x2-direction by adding contributions 
      from the transverse directions
     -------------------------------------- */

  DIR = JDIR;
  SET_INDEXES (&indx, &state, grid);
  TRANSVERSE_LOOP(indx,in,i,j,k){  
    up = UPY[indx.t2][indx.t1];
    um = UMY[indx.t2][indx.t1];
    for (in = indx.beg - 1; in <= indx.end + 1; in++) {
      du = dU[*k][*j][*i];
      for (nv = NVAR; nv--; ){
        up[in][nv] += du[nv];
        um[in][nv] += du[nv];
      }
    }
  }

  #if DIMENSIONS == 3

  /* --------------------------------------
      Correct + and - states in the 
      x3-direction by adding contributions 
      from the transverse directions
     -------------------------------------- */

   DIR = KDIR;
   SET_INDEXES (&indx, &state, grid);
   TRANSVERSE_LOOP(indx,in,i,j,k){  
     up = UPZ[indx.t2][indx.t1];
     um = UMZ[indx.t2][indx.t1];
     for (in = indx.beg - 1; in <= indx.end + 1; in++) {
       du = dU[*k][*j][*i];
       for (nv = NVAR; nv--; ){
         up[in][nv] += du[nv];
         um[in][nv] += du[nv];
       }
     }
   }

  #endif

  #ifdef STAGGERED_MHD     /* ------------------------------
                                advance of staggered 
                                magnetic field of dt/2  
                              ------------------------------ */
   FCT_UPDATE (ISTEP, d, grid);
  #endif

/*   ---------------------------------------------------- 
        3. Compute FINAL RHS      
     ---------------------------------------------------- */

  ISTEP = 2;
  for (DIR = 0; DIR < DIMENSIONS; DIR++){

    if      (DIR == IDIR) {UP = UPX; UM = UMX;}
    else if (DIR == JDIR) {UP = UPY; UM = UMY;}
    else if (DIR == KDIR) {UP = UPZ; UM = UMZ;}    

    SET_INDEXES (&indx, &state, grid);
    TRANSVERSE_LOOP(indx,in,i,j,k){  

    /* --------------------------------------------------------------
         Convert conservative corner-coupled states to primitive 
       -------------------------------------------------------------- */
       
      state.up = UP[indx.t2][indx.t1]; state.uL = state.up;
      state.um = UM[indx.t2][indx.t1]; state.uR = state.um + 1;

      #ifdef STAGGERED_MHD
      for (in = indx.beg - 2; in <= indx.end + 1; in++){
        state.uL[in][B1] = state.uR[in][B1] = d->Vs[BXs + DIR][*k][*j][*i];
      }
      #endif

      errm = !CONTOPRIM (state.um, state.vm, indx.beg - 1, indx.end + 1, flagm);
      errp = !CONTOPRIM (state.up, state.vp, indx.beg - 1, indx.end + 1, flagp);
/*
      if (errm || errp){
        WARNING(print ("! Corner coupled states not physical: reverting to 1st order\n");)
        for (in = indx.beg - 1; in <= indx.end + 1; in++){
          if (flag[indx.t2][indx.t1][in]){
           !!!! PUT STAGGERED FIELD !!!!!
            for (nv = 0; nv < NVAR; nv++) {
              state.vm[in][nv] = state.vp[in][nv] = state.v[in][nv];
              state.um[in][nv] = state.up[in][nv] = state.u[in][nv];
            }
          }
        }
      }
*/

    /* -------------------------------------------------------
        compute time centered, cell centered state. 
	Useful for source terms like gravity, 
	curvilinear terms and Powell's 8wave 
       ------------------------------------------------------- */
       
      if (DIR == IDIR){
        for (in = indx.beg-1; in <= indx.end+1; in++) { /* +1 or -1 needed for STAGGERED_MHD */
        for (nv = NVAR; nv--;   ) {
          dU[*k][*j][*i][nv] += UU[*k][*j][*i][nv];
          state.uh[in][nv] = dU[*k][*j][*i][nv];
        }}
      }else{
        for (in = indx.beg; in <= indx.end; in++) {
        for (nv = NVAR; nv--;   ) {
          state.uh[in][nv] = dU[*k][*j][*i][nv];
        }}
      }
      CONTOPRIM (state.uh, state.vh, indx.beg, indx.end, state.flag);

    /* ----  source term computation ---- */

      RIEMANN (&state, indx.beg - 1, indx.end, Dts->cmax + DIR, grid);
      #ifdef STAGGERED_MHD
       EMF_PUT (&state, indx.beg - 1, indx.end, grid);
      #endif
      #if SAVE_VEC_POT == YES
       VEC_POT (d, NULL, &state, grid);
      #endif

      GET_RHS (&state, indx.beg, indx.end, delta_t, grid);

      for (in = indx.beg; in <= indx.end; in++) {
      for (nv = NVAR; nv--;  ) {
        UU[*k][*j][*i][nv] += state.rhs[in][nv];
      }}           
    }
  }

  #ifdef STAGGERED_MHD

   #if CT_EMF_AVERAGE != ARITHMETIC

   DIR = IDIR;
   SET_INDEXES (&indx, &state, grid);
   for (kk = KBEG - KOFFSET; kk <= KEND + KOFFSET; kk++){ NZ_PT = &kk;
   for (jj = JBEG - JOFFSET; jj <= JEND + JOFFSET; jj++){ NY_PT = &jj;
     CONTOPRIM (dU[kk][jj], state.v, IBEG - 1, IEND + 1, state.flag);
     for (ii = IBEG - 1; ii <= IEND + 1; ii++){
     for (nv = 0; nv < NVAR; nv++) {
       d->Vc[nv][kk][jj][ii] = state.v[ii][nv];
     }} 
   }}
/*
DIR = IDIR; SET_INDEXES (&indx, grid);
for (kk = KBEG-KOFFSET; kk <= KEND+KOFFSET; kk++){
for (jj = JBEG-1; jj <= JEND+1; jj++){
 CONTOPRIM (UH[kk][jj], state.v, IBEG-1, IEND+1, state.flag);
 for (ii = IBEG-1; ii <= IEND+1; ii++){
 for (nv = NVAR; nv--;  ) {
   d->Vc[nv][kk][jj][ii] = state.v[ii][nv];
 }}
}}
*/
   #endif
   FCT_UPDATE (ISTEP, d, grid);
   AVERAGE_MAGNETIC_FIELD (d->Vs, UU, grid);
  #endif

/* ----------------------------------------------
           convert to primitive 
   ---------------------------------------------- */

  DIR = IDIR;
  SET_INDEXES (&indx, &state, grid);
  for (kk = KBEG; kk <= KEND; kk++){ NZ_PT = &kk;
  for (jj = JBEG; jj <= JEND; jj++){ NY_PT = &jj;
    CONTOPRIM (UU[kk][jj], state.v, IBEG, IEND, state.flag);
    for (ii = IBEG; ii <= IEND; ii++){
    for (nv = NVAR; nv--;  ) {
      d->Vc[nv][kk][jj][ii] = state.v[ii][nv];
    }} 
  }}
}

#if CTU_MHD_SOURCE == YES
/* ********************************************************* */
void ADD_CTU_MHD_SOURCE (real **v, real **up, real **um,
                         real *dtdV, int beg, int end, Grid *grid)
/*
 *
 * PURPOSE
 *
 *   Add source terms to conservative left and 
 *   right states obtained from the primitive form 
 *   of the equations. The source terms are:
 *
 *
 *     m  += dt/2 *  B  * dbx/dx
 *     Bt += dt/2 * vt  * dbx/dx   (t = transverse component)
 *     E  += dt/2 * v*B * dbx/dx
 *
 *   These terms are NOT accounted for when the primitive 
 *   form of the equations is used (see 
 *   Gardiner & Stone JCP (2005), Crockett et al. 
 *   JCP(2005)). This is true for both the Charactheristic 
 *   Tracing AND the primitive Hancock scheme when the 
 *   constrained transport is used, since the resulting 
 *   system is 7x7. To better understand this, you can
 *   consider the stationary solution rho = p = 1, v = 0
 *   and Bx = x, By = -y. If these terms were not included
 *   the code would generate spurious velocities.
 *
 *********************************************************** */
{
  int    i;
  real   scrh, *dx, *A;
  static real *db;

  if (db == NULL) db = Array_1D(NMAX_POINT, double);

/* ----------------------------------------
              comput db/dx
   ---------------------------------------- */

  #if GEOMETRY == CARTESIAN
   for (i = beg; i <= end; i++){
     db[i] = dtdV[i]*(up[i][B1] - um[i][B1]); 
   }
  #elif GEOMETRY == CYLINDRICAL
   if (DIR == IDIR){
     A = grid[IDIR].A;
     for (i = beg; i <= end; i++){
       db[i] = dtdV[i]*(up[i][B1]*A[i] - um[i][B1]*A[i - 1]);
     }
   }else{
     for (i = beg; i <= end; i++){
       db[i] = dtdV[i]*(up[i][B1] - um[i][B1]); 
     }
   }
  #else
   print1 (" ! CTU-MHD does not work in this geometry\n");
   QUIT_ORION2(1);
  #endif

/* --------------------------------------------
         Add source terms
   -------------------------------------------- */

  for (i = beg; i <= end; i++){
    
    EXPAND( up[i][MX] += v[i][BX]*db[i];
            um[i][MX] += v[i][BX]*db[i];   ,
            up[i][MY] += v[i][BY]*db[i];
            um[i][MY] += v[i][BY]*db[i];   ,
            up[i][MZ] += v[i][BZ]*db[i];
            um[i][MZ] += v[i][BZ]*db[i]; ) 

    EXPAND(                                ,
            up[i][B2] += v[i][V2]*db[i]; 
            um[i][B2] += v[i][V2]*db[i];   ,
            up[i][B3] += v[i][V3]*db[i]; 
            um[i][B3] += v[i][V3]*db[i];)

    #if EOS != ISOTHERMAL
     scrh = EXPAND(   v[i][VX]*v[i][BX]  , 
                    + v[i][VY]*v[i][BY]  , 
                    + v[i][VZ]*v[i][BZ]);
     up[i][EN] += scrh*db[i];
     um[i][EN] += scrh*db[i];
    #endif

  }

}
#endif

