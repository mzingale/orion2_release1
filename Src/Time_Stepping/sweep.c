#include "orion2.h"

/* ------------------------------------------------
     Need to define the extra array Vres ? 
   ------------------------------------------------ */

#ifndef FARGO_SCHEME 
 #define FARGO_SCHEME NO 
#endif

#if RESISTIVE_MHD == EXPLICIT
 #define BOUND_DIR ALLDIR
#else
 #define BOUND_DIR (ALLDIR)
#endif

#if FARGO_SCHEME == NO
/* ************************************************************** */
void SWEEP (const Data *d, Riemann_Solver *RIEMANN, 
            Time_Step *Dts, Grid *grid)
/*
 *
 *
 * PURPOSE: 
 *     
 *   Handle x1, x2 and x3 sweep integrators
 *
 *
 * ARGUMENTS:
 *
 *  UU(in/out):  solution vector in terms of conservative
 *               quantities
 * 
 *
 *
 *    
 **************************************************************** */
{
  int  ii, jj, kk;
  int  *i, *j, *k;
  int  in, nv, beg, end;
  Index indx;
  static Data_Arr UU;
  static State_1D state;
  static real one_third = 1.0/3.0;
  #if INCLUDE_PARABOLIC_FLUX == YES
   static Data_Arr Vres;
  #endif

/* -----------------------------------------------------------------
               Check algorithm compatibilities
   ----------------------------------------------------------------- */

  #ifdef STAGGERED_MHD
   print1 ("! Flux-CT works with unsplit integrators only\n");
   QUIT_ORION2(1);
  #endif

/* -----------------------------------------------------------------
                      Allocate memory
   ----------------------------------------------------------------- */

  if (state.rhs == NULL){
    Make_State (&state);
    #ifndef SINGLE_STEP
     UU = Array_4D(NZ_TOT, NY_TOT, NX_TOT, NVAR, double);
    #endif
    #if INCLUDE_PARABOLIC_FLUX == YES
     Vres = Array_4D(NVAR, NZ_TOT, NY_TOT, NX_TOT, double);
    #endif
  }

  /*  --------------------------------
       Set normal and tangent indexes  
      -------------------------------- */

  ISTEP = 1;
  SET_INDEXES (&indx, &state, grid);
  beg = grid[DIR].lbeg;
  end = grid[DIR].lend;

/* ----------------------------------------------------
                   STEP I   (PREDICTOR)
   ---------------------------------------------------- */
  
  BOUNDARY (d, BOUND_DIR, grid);
  #if INTERPOLATION == LINEAR_MULTID 
   MULTID_LIMITER (d, grid);
  #endif
  #if SHOCK_FLATTENING == MULTID
   FIND_SHOCK (d, grid);
  #endif

  #if INCLUDE_PARABOLIC_FLUX == YES
   for (nv = 0; nv < NVAR; nv++){
   for (kk = 0; kk < NZ_TOT; kk++){ 
   for (jj = 0; jj < NY_TOT; jj++){
   for (ii = 0; ii < NX_TOT; ii++){
      Vres[nv][kk][jj][ii] = d->Vc[nv][kk][jj][ii];
   }}}}
  #endif

  TRANSVERSE_LOOP(indx,in,i,j,k){  

    for (in = 0; in < indx.ntot; in++) {
    for (nv = NVAR; nv--;  ) {
      state.v[in][nv] = d->Vc[nv][*k][*j][*i];
    }}

    CHECK_NAN (state.v, 0, indx.ntot - 1, 0);
    PRIMTOCON (state.v, state.u, 0, indx.ntot - 1);

    #ifndef SINGLE_STEP
     for (in = indx.beg - 1; in <= indx.end + 1; in++) {
     for (nv = NVAR; nv--;  ) {
       UU[*k][*j][*i][nv] = state.u[in][nv]; 
     }}
    #endif

    STATES  (&state, indx.beg - 1, indx.end + 1, delta_t, grid);
    RIEMANN (&state, indx.beg - 1, indx.end, Dts->cmax + DIR, grid);
    #if INCLUDE_PARABOLIC_FLUX == YES  /* !! will be first order in time 
                                             for single step algorithm   !! */
     PARABOLIC_FLUX (Vres, &state, indx.beg - 1, indx.end, Dts, grid);
    #endif
    #if SAVE_VEC_POT == YES
     VEC_POT (d, NULL, &state, grid);
    #endif

    GET_RHS (&state, beg, end, delta_t, grid);

    for (in = beg; in <= end; in++) {
    for (nv = NVAR; nv--;  ) {
      state.u[in][nv] += state.rhs[in][nv];
    }}

    CONTOPRIM (state.u, state.v, beg, end, state.flag);

    for (in = beg; in <= end; in++) {
    for (nv = NVAR; nv--;  ) {
      d->Vc[nv][*k][*j][*i] = state.v[in][nv];
    }}
  }

/* ----------------------------------------------------
                   STEP II  (or CORRECTOR)
   ---------------------------------------------------- */

#if (TIME_STEPPING == RK2) || (TIME_STEPPING == RK3)

  ISTEP = 2;
  BOUNDARY (d, BOUND_DIR, grid);
  #if INTERPOLATION == LINEAR_MULTID 
   MULTID_LIMITER (d, grid);
  #endif

  #if INCLUDE_PARABOLIC_FLUX == YES
   for (nv = 0; nv < NVAR; nv++){
   for (kk = 0; kk <= KEND + grid[KDIR].nghost; kk++){ 
   for (jj = 0; jj <= JEND + grid[JDIR].nghost; jj++){
   for (ii = 0; ii <= IEND + grid[IDIR].nghost; ii++){
      Vres[nv][kk][jj][ii] = d->Vc[nv][kk][jj][ii];
   }}}}
  #endif

  TRANSVERSE_LOOP(indx,in,i,j,k){  

    for (in = 0; in < indx.ntot; in++) {
    for (nv = NVAR; nv--;  ) {
      state.v[in][nv] = d->Vc[nv][*k][*j][*i];
    }}

    PRIMTOCON (state.v, state.u, 0, indx.ntot - 1);

    STATES  (&state, indx.beg - 1, indx.end + 1, delta_t, grid);
    RIEMANN (&state, indx.beg - 1, indx.end, Dts->cmax + DIR, grid);
    #if INCLUDE_PARABOLIC_FLUX == YES
     PARABOLIC_FLUX (Vres, &state, indx.beg - 1, indx.end, Dts, grid);
    #endif
    #if SAVE_VEC_POT == YES
     VEC_POT (d, NULL, &state, grid);
    #endif

    GET_RHS (&state, beg, end, delta_t, grid);

    for (in = beg; in <= end; in++) {
    for (nv = NVAR; nv--;  ) {
      #if TIME_STEPPING == RK2
       state.u[in][nv] = 0.5*(UU[*k][*j][*i][nv] + state.u[in][nv] + state.rhs[in][nv]);
      #elif TIME_STEPPING == RK3    
       state.u[in][nv] = 0.25*(3.0*UU[*k][*j][*i][nv] + state.u[in][nv] + state.rhs[in][nv]);
      #endif
    }}

    CONTOPRIM (state.u, state.v, beg, end, state.flag);

    for (in = beg; in <= end; in++) {
    for (nv = NVAR; nv--;  ) {
      d->Vc[nv][*k][*j][*i] = state.v[in][nv];
    }}

  }

#endif

/* ----------------------------------------------------
                   STEP III  (or CORRECTOR)
   ---------------------------------------------------- */

#if TIME_STEPPING == RK3 

  ISTEP = 3;
  BOUNDARY (d, BOUND_DIR, grid);
  #if INTERPOLATION == LINEAR_MULTID 
   MULTID_LIMITER (d, grid);
  #endif

  #if INCLUDE_PARABOLIC_FLUX == YES
   for (nv = 0; nv < NVAR; nv++){
   for (kk = 0; kk <= KEND + grid[KDIR].nghost; kk++){ 
   for (jj = 0; jj <= JEND + grid[JDIR].nghost; jj++){
   for (ii = 0; ii <= IEND + grid[IDIR].nghost; ii++){
      Vres[nv][kk][jj][ii] = d->Vc[nv][kk][jj][ii];
   }}}}
  #endif

  TRANSVERSE_LOOP(indx,in,i,j,k){  

    for (in = 0; in < indx.ntot; in++) {
    for (nv = NVAR; nv--;  ) {
      state.v[in][nv] = d->Vc[nv][*k][*j][*i];
    }}
      
    PRIMTOCON (state.v, state.u, 0, indx.ntot - 1);
    STATES  (&state, indx.beg - 1, indx.end + 1, delta_t, grid);
    RIEMANN (&state, indx.beg - 1, indx.end, Dts->cmax + DIR, grid);
    #if INCLUDE_PARABOLIC_FLUX == YES
     PARABOLIC_FLUX (Vres, &state, indx.beg - 1, indx.end, Dts, grid);
    #endif
    #if SAVE_VEC_POT == YES
     VEC_POT (d, NULL, &state, grid);
    #endif

    GET_RHS (&state, beg, end, delta_t, grid);

    for (in = beg; in <= end; in++) {
    for (nv = NVAR; nv--;  ) {
      state.u[in][nv] = one_third*(UU[*k][*j][*i][nv] + 
                        2.0*(state.u[in][nv] + state.rhs[in][nv]));
    }}    

    CONTOPRIM (state.u, state.v, beg, end, state.flag);

    for (in = beg; in <= end; in++) {
    for (nv = NVAR; nv--;  ) {
       d->Vc[nv][*k][*j][*i] = state.v[in][nv];
    }}

  }
  
#endif

}

 #else 

/* ************************************************************** */
void SWEEP (const Data *d, Riemann_Solver *RIEMANN, 
            Time_Step *Dts, Grid *grid)
/*
 *
 *
 *  Sweep routine for the FARGO scheme
 *  Works in polar coordinates. 
 * 
 *
 *
 *    
 **************************************************************** */
{
  int  ii, jj, kk;
  int  *i, *j, *k;
  Index indx;
  int  in, nv;
  
  static Data_Arr UU;
  static State_1D state;
  static real one_third = 1.0/3.0;
  
  int    l[4096], jnew;  
  real vmean[4096], phi, tau, r, vphi_0[4096], dphi, cmax_y;
  real scrh;
  static real **v0;

  if (state.rhs == NULL){

    v0 = Array_2D(NMAX_POINT, NVAR, double);
    Make_State (&state);

    #ifndef SINGLE_STEP
     UU = Array_4D(NZ + 2*grid[KDIR].nghost, 
                   NY + 2*grid[JDIR].nghost, 
                   NX + 2*grid[IDIR].nghost, NVAR, double);
    #endif
  }

  #if PHYSICS != HD
   print1("! FARGO scheme works for HD module only\n");
   QUIT_ORION2(1);
  #endif

/* ----------------------------------------------------
                   STEP I   (PREDICTOR)
   ---------------------------------------------------- */
  
  ISTEP = 1;
  SET_INDEXES (&indx, &state, grid);
  BOUNDARY (d, BOUND_DIR, grid);

  if (DIR == JDIR) cmax_y = 0.0;

  TRANSVERSE_LOOP(indx,in,i,j,k){  

    if (DIR == JDIR) {
    
    /* -------------------------------
        compute the average v_\phi
        over the ring at a given r
       ------------------------------- */
       
      vmean[indx.t1] = 0.0;
      for (in = indx.beg; in <= indx.end; in++) {
        vmean[indx.t1] += d->Vc[VY][*k][*j][*i]; 
      }
      vmean[indx.t1] /= (real)(NY);
      r           = grid[0].x[indx.t1];
      dphi        = grid[1].dx[4];
      tau         = vmean[indx.t1]/r*delta_t/dphi;
      
      l[indx.t1] = (int)(tau + 0.5);
      vphi_0[indx.t1] = l[indx.t1]*r*dphi/delta_t;

    /* -------------------------------
         define residual velocity 
       ------------------------------- */

      for (in = 0; in < indx.ntot; in++) {
        d->Vc[VY][*k][*j][*i] -= vphi_0[indx.t1]; 
      }
    }    

    for (in = 0; in < indx.ntot; in++) {
    for (nv = NVAR; nv--;  ) {
      state.v[in][nv] = d->Vc[nv][*k][*j][*i];
    }}

    PRIMTOCON   (state.v, state.u, 0, indx.ntot - 1);

    #ifndef SINGLE_STEP
     for (in = indx.beg - 1; in <= indx.end + 1; in++) {
     for (nv = NVAR; nv--;  ) {
       UU[*k][*j][*i][nv] = state.u[in][nv]; 
     }}
    #endif
    
    STATES  (&state, indx.beg - 1, indx.end + 1, delta_t, grid);
    RIEMANN (&state, indx.beg - 1, indx.end, Dts->cmax + DIR, grid);

//    #if ARTIFICIAL_VISCOSITY == YES
//     VISC_FLUX   (&state, indx.beg - 1, indx.end, grid);
//    #endif
    
    GET_RHS (&state, indx.beg, indx.end, delta_t, grid);

    for (in = indx.beg; in <= indx.end; in++) {
    for (nv = NVAR; nv--;  ) {
      state.u[in][nv] += state.rhs[in][nv];
    }}
 
    CONTOPRIM (state.u, state.v, indx.beg, indx.end, state.flag);

    if (DIR == JDIR){
      for (in = indx.beg; in <= indx.end ; in++) {
        scrh   = sqrt(gmm*state.v[in][PR]/state.v[in][DN]);
        scrh  += fabs(state.v[in][VY] + vphi_0[indx.t1] - vmean[indx.t1]);
        cmax_y = dmax(cmax_y, scrh); 
      }
    }

    for (in = indx.beg; in <= indx.end; in++) {
    for (nv = NVAR; nv--;  ) {
      d->Vc[nv][*k][*j][*i] = state.v[in][nv];
    }}
  }

/* ----------------------------------------------------
                   STEP II  (or CORRECTOR)
   ---------------------------------------------------- */

#if (TIME_STEPPING == RK2) || (TIME_STEPPING == RK3)

  ISTEP = 2;
  SET_INDEXES (&indx, &state, grid);
  BOUNDARY (d, DIR, grid);

  TRANSVERSE_LOOP(indx,in,i,j,k){  

    for (in = 0; in < indx.ntot; in++) {
    for (nv = NVAR; nv--;  ) {
      state.v[in][nv] = d->Vc[nv][*k][*j][*i];
    }}
      
    PRIMTOCON (state.v, state.u, 0, indx.ntot - 1);
    STATES  (&state, indx.beg - 1, indx.end + 1, delta_t, grid);
    RIEMANN (&state, indx.beg - 1, indx.end, Dts->cmax + DIR, grid);

//    #if ARTIFICIAL_VISCOSITY == YES
//     VISC_FLUX   (&state, indx.beg - 1, indx.end, grid);
//    #endif

    GET_RHS (&state, indx.beg, indx.end, delta_t, grid);

    for (in = indx.beg; in <= indx.end; in++) {
    for (nv = NVAR; nv--;  ) {
      #if TIME_STEPPING == RK2
       state.u[in][nv] = 0.5*(UU[*k][*j][*i][nv] + state.u[in][nv] + state.rhs[in][nv]);
      #elif TIME_STEPPING == RK3    
       state.u[in][nv] = 0.25*(3.0*UU[*k][*j][*i][nv] + state.u[in][nv] + state.rhs[in][nv]);
      #endif
    }}

    CONTOPRIM (state.u, state.v, indx.beg, indx.end, state.flag);

    if (DIR == JDIR){
      for (in = indx.beg; in <= indx.end ; in++) {
        scrh   = sqrt(gmm*state.v[in][PR]/state.v[in][DN]);
        scrh  += fabs(state.v[in][VY] + vphi_0[indx.t1] - vmean[indx.t1]);
        cmax_y = dmax(cmax_y, scrh); 
      }
    }
    #if TIME_STEPPING == RK2
     if (DIR == IDIR){

       for (in = indx.beg; in <= indx.end; in++) {
       for (nv = NVAR; nv--;  ) {
         d->Vc[nv][*k][*j][*i] = state.v[in][nv];
       }}
     }else if (DIR == JDIR){

/* ------------------------------------
    transform back to inertial frame
    when nsweep = 2
   ------------------------------------ */

    /* ----  define angular velocity in inertial frame  ---- */
    
      for (in = indx.beg ; in <= indx.end; in++) {
        state.v[in][VY] += vphi_0[indx.t1]; 
      }
      
    /* ----  shift solution  ---- */

      for (nv = 0; nv < NVAR; nv++) {
      for (in = indx.beg ; in <= indx.end; in++) {
        jnew = in + l[indx.t1] - grid[1].nghost;
        jnew = jnew%(NY) + grid[1].nghost;

if (jnew < indx.beg || jnew > indx.end){
 print (" ! index jnew out of range in sweep_fargo.c\n");
 print (" ! in: %d   lt: %d \n",in, l[indx.t1]);
 QUIT_ORION2(1);
}

        v0[jnew][nv] = state.v[in][nv]; 
      }}

    /* ---- update solution ---- */
    
      for (nv = 0; nv < NVAR; nv++) {
      for (in = indx.beg ; in <= indx.end; in++) {
        d->Vc[nv][*k][*j][*i] = v0[in][nv];
      }}
    }
#else

    for (in = indx.beg; in <= indx.end; in++) {
    for (nv = NVAR; nv--;  ) {
      d->Vc[nv][*k][*j][*i] = state.v[in][nv];
    }}

#endif
  }

#endif

/* ----------------------------------------------------
                   STEP III  (or CORRECTOR)
   ---------------------------------------------------- */

#if TIME_STEPPING == RK3 

  ISTEP = 3;
  SET_INDEXES (&indx, &state, grid);
  BOUNDARY (d, DIR, grid);

  TRANSVERSE_LOOP(indx,in,i,j,k){  

    for (in = 0; in < indx.ntot; in++) {
    for (nv = NVAR; nv--;  ) {
      state.v[in][nv] = d->Vc[nv][*k][*j][*i];
    }}
      
    PRIMTOCON (state.v, state.u, 0, indx.ntot - 1);
    STATES  (&state, indx.beg - 1, indx.end + 1, delta_t, grid);
    RIEMANN (&state, indx.beg - 1, indx.end, Dts->cmax + DIR, grid);

//    #if ARTIFICIAL_VISCOSITY == YES
//     VISC_FLUX   (&state, indx.beg - 1, indx.end, grid);
//    #endif

    GET_RHS (&state, indx.beg, indx.end, delta_t, grid);

    for (in = indx.beg; in <= indx.end; in++) {
    for (nv = NVAR; nv--;  ) {
      state.u[in][nv] = one_third*(UU[*k][*j][*i][nv] + 
                        2.0*(state.u[in][nv] + state.rhs[in][nv]));
    }}    

    CONTOPRIM (state.u, state.v, indx.beg, indx.end, state.flag);

    if (DIR == JDIR){
      for (in = indx.beg; in <= indx.end; in++) {
        scrh   = sqrt(gmm*state.v[in][PR]/state.v[in][DN]);
        scrh  += fabs(state.v[in][VY] + vphi_0[indx.t1] - vmean[indx.t1]);
        cmax_y = dmax(cmax_y, scrh); 
      }
    }

    if (DIR == IDIR){

      for (in = indx.beg; in <= indx.end; in++) {
      for (nv = NVAR; nv--;  ) {
        d->Vc[nv][*k][*j][*i] = state.v[in][nv];
      }}
    }else if (DIR == JDIR){

/* ------------------------------------
    transform back to inertial frame
    when nsweep = 2
   ------------------------------------ */

    /* ----  define angular velocity in inertial frame  ---- */
    
      for (in = indx.beg ; in <= indx.end; in++) {
        state.v[in][VY] += vphi_0[indx.t1]; 
      }
      
    /* ----  shift solution  ---- */

      for (nv = 0; nv < NVAR; nv++) {
      for (in = indx.beg ; in <= indx.end; in++) {
        jnew = in + l[indx.t1] - grid[1].nghost;
        jnew = jnew%(NY) + grid[1].nghost;
if (jnew < indx.beg || jnew > indx.end){
 print (" ! index jnew out of range in sweep_fargo.c\n");
 print (" ! in: %d   lt: %d \n",in, l[indx.t1]);
 SHOW(state.v,in);
 QUIT_ORION2(1);
}
        v0[jnew][nv] = state.v[in][nv]; 
      }}

    /* ---- update solution ---- */
    
      for (nv = 0; nv < NVAR; nv++) {
      for (in = indx.beg ; in <= indx.end; in++) {
        d->Vc[nv][*k][*j][*i] = v0[in][nv];
      }}
    }

  }
  
#endif

  if (DIR == JDIR) Dts->cmax[JDIR] = cmax_y;
}

#endif
