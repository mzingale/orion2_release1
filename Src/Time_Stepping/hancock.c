#include "orion2.h"

/* ---------------------------------------------------------------
    when PRIMITIVE_HANCOCK  YES, use the primitive Hancock step 
    when PRIMITIVE_HANCOCK  NO, use the conservative Hancock step 
   --------------------------------------------------------------- */

#if PRIMITIVE_HANCOCK == YES
/* ****************************************************************** */
void STATES (const State_1D *state, int beg, int end, real dt, Grid *grid)
/*
 *
 * PURPOSE
 *
 *   make a predictor step using the Hancock Primitive-Variable 
 *   Predictor;
 *
 * ARGUMENTS
 *
 *  u0 (IN/OUT): on input, is the cell-centered conservative 
 *               value at    t  = t0
 *  v0 (IN/OUT): on input, is the cell-centered primitive 
 *               value at    t  = t0
 *  vL (IN/OUT): left edge interpolated value at t = t0;
 *               on output, provides the predicted interface value 
 *               at t = t + dt/2
 *  vR (IN/OUT): same thing as vL, but for the right edge interface value
 *                
 *  grid (IN)  : array of grids   
 *
 *
 * REFERENCE: 
 * 
 *              "Riemann Solvers and Numerical Methods for
 *               Fluid Dynamics", E.F. Toro
 *
 * LAST_MODIFIED
 *
 *   August, 17/2005, written by Andrea Mignone  (mignone@to.astro.it)
 *
 ********************************************************************** */
{
  int    nv, i;
  real scrh, dt_2, ch2;
  real Adv[NVAR], dv[NVAR];
  real *vp, *vm, *vc;
  static real **src, *a2, *h, *d_dl;

  if (src == NULL){
    src  = Array_2D(NMAX_POINT, NVAR, double);
    h    = Array_1D(NMAX_POINT, double);
    a2   = Array_1D(NMAX_POINT, double);
    d_dl = Array_1D(NMAX_POINT, double);
  }

/* -------------------------------------------------------
        compute geometric factor 1/(h*dx)
   ------------------------------------------------------- */

  #if GEOMETRY == CARTESIAN || GEOMETRY == CYLINDRICAL

   for (i = beg; i <= end; i++) {
     d_dl[i] = 1.0/grid[DIR].dx[i];
   }

  #elif GEOMETRY == SPHERICAL || GEOMETRY == POLAR

   if (DIR == IDIR){
     for (i = beg; i <= end; i++) {
       d_dl[i] = 1.0/grid[DIR].dx[i];
     }
   }else if (DIR == JDIR){
     for (i = beg; i <= end; i++) {
       d_dl[i] = 1.0/(grid[IDIR].x[*NX_PT]*grid[JDIR].dx[i]);
     }
   }

  #endif
    
  RECONSTRUCT  (state, grid); 
  SOUND_SPEED2 (state->v, a2, h, beg, end);
  PRIM_SOURCE  (state, beg, end, a2, h, src, grid);

/*  Get left and right states for predictor  */

  dt_2 = 0.5*dt;
  for (i = beg; i <= end; i++) {

    vc = state->v[i];
    vm = state->vm[i];
    vp = state->vp[i];

    for (nv = NVAR; nv--;  ) dv[nv] = vp[nv] - vm[nv];

    PRIM_RHS (vc, dv, a2[i], h[i], Adv);
    for (nv = NVAR; nv--;  ) {
      scrh = dt_2*(d_dl[i]*Adv[nv] - src[i][nv]);
      vp[nv] -= scrh;
      vm[nv] -= scrh;
    }
    
  }

  CHECK_PRIM_STATES (state->vm, state->vp, state->v, beg, end);

/* -----------------------------------
     evolve center value by dt/2 
   ----------------------------------- */

  for (i = beg; i <= end; i++) {
    vc = state->vh[i];
    vp = state->vp[i];
    vm = state->vm[i];
    for (nv = 0; nv < NVAR; nv++) {
      vc[nv] = 0.5*(vp[nv] + vm[nv]);
    }
  }

  PRIMTOCON (state->vh, state->uh, beg, end);
  PRIMTOCON (state->vp, state->up, beg, end);
  PRIMTOCON (state->vm, state->um, beg, end);
}

#else

/* ****************************************************************** */
void STATES (const State_1D *state, int beg, int end, real dt, Grid *grid)
/*
 *
 *
 ******************************************************************** */
#define CHAR_UPWIND NO
 {
  int    nv, i, ifail;
  real scrh, scrhp, scrhm, dtdx, dtdV;
  real *A, *dx, *dV, r;
  real *vp, *vm, *rhs;
  static real **fp, **fm;
  static real *pp, *pm;
  static real *lambda_max, *lambda_min;
  static real *MTsrc_33, *MTsrc_31, *OTsrc_31;

static real *sp, *sm, *s;


  #if GEOMETRY != CARTESIAN && GEOMETRY != CYLINDRICAL
   print1 ("! Hancock does not work in this geometry \n");
   QUIT_ORION2(1);
  #endif
  
  if (fp == NULL){
    fp   = Array_2D(NMAX_POINT, NVAR, double);
    fm   = Array_2D(NMAX_POINT, NVAR, double);
    pp   = Array_1D(NMAX_POINT, double);
    pm   = Array_1D(NMAX_POINT, double);
    lambda_max = Array_1D(NMAX_POINT, double);
    lambda_min = Array_1D(NMAX_POINT, double);
    MTsrc_33 = Array_1D(NMAX_POINT, double);
    MTsrc_31 = Array_1D(NMAX_POINT, double);
    OTsrc_31 = Array_1D(NMAX_POINT, double);
    sp = Array_1D(NMAX_POINT, double);
    sm = Array_1D(NMAX_POINT, double);
    s  = Array_1D(NMAX_POINT, double);
  }
  
/* --------------------------------
         make fluxes  
   -------------------------------- */

  RECONSTRUCT  (state, grid); 
  PRIMTOCON (state->vp, state->up, beg, end);
  PRIMTOCON (state->vm, state->um, beg, end);
  #if PHYSICS == MHD
   FLUX(state->up, state->vp, NULL, fp, pp, beg, end);
   FLUX(state->um, state->vm, NULL, fm, pm, beg, end);
  #else 
   FLUX(state->up, state->vp, fp, pp, beg, end);
   FLUX(state->um, state->vm, fm, pm, beg, end);
  #endif
  dx = grid[DIR].dx; A = grid[DIR].A; dV = grid[DIR].dV;

/* ---------------------------------------------------
          Compute passive scalar fluxes
   --------------------------------------------------- */

  #if NFLX != NVAR
   for (i = beg; i <= end; i++){
     vp  = state->vp[i]; vm  = state->vm[i];
     #if NSCL > 0
      for (nv = NFLX; nv < (NFLX + NSCL); nv++){
        fp[i][nv] = fp[i][DN]*vp[nv];
        fm[i][nv] = fm[i][DN]*vm[nv];
      }
     #endif
   }
  #endif

  #if CHAR_UPWIND == YES  
   MAX_CH_SPEED (state->v, lambda_min, lambda_max, beg, end);
  #endif

/* ------------------------------------------------------
        Initialize r.h.s. with flux difference  
   ------------------------------------------------------ */

  dt = 0.5*dt; 
  for (i = beg; i <= end; i++) { 

    dtdx = dt/dx[i]; 
    vp  = state->vp[i]; 
    vm  = state->vm[i];
    rhs = state->rhs[i];
    #if GEOMETRY == CARTESIAN

     for (nv = NVAR; nv--;   ){
       rhs[nv] = -dtdx*(fp[i][nv] - fm[i][nv]);
     }

    #elif GEOMETRY == CYLINDRICAL

     dtdV = dt/dV[i]; 
     for (nv = NVAR; nv--;   ) {
       rhs[nv] = -dtdV*(A[i]*fp[i][nv] - A[i-1]*fm[i][nv]);
     }
     #ifdef PSI_GLM
      rhs[B1] = -dtdx*(vp[PSI_GLM] - vm[PSI_GLM]);
     #endif

    #endif

    rhs[M1] -= dtdx*(pp[i] - pm[i]);
  }

/* ------------------------------------------------
            Source terms: Powell
   ------------------------------------------------ */

  #if (PHYSICS == MHD || PHYSICS == RMHD) && (MHD_FORMULATION == EIGHT_WAVES)
   for (i = beg; i <= end; i++) {
     scrh = (  state->vp[i][B1]*A[i] 
             - state->vm[i][B1]*A[i-1])/dV[i];
     EXPAND(state->rhs[i][BX] -= dt*state->v[i][VX]*scrh;  ,
            state->rhs[i][BY] -= dt*state->v[i][VY]*scrh;  ,
            state->rhs[i][BZ] -= dt*state->v[i][VZ]*scrh;)
   }
  #endif
   
/* ------------------------------------------------
       Source terms: geometry
   ------------------------------------------------ */

  #if GEOMETRY == CYLINDRICAL && COMPONENTS == 3
   if (DIR == IDIR) {
     for (i = beg; i <= end; i++){
     for (nv = NVAR; nv--;  ) {
       state->uh[i][nv] = state->u[i][nv];
       state->vh[i][nv] = state->v[i][nv];
     }}
     MOM_SOURCE (state, beg, end, KDIR, KDIR, MTsrc_33, grid);
     MOM_SOURCE (state, beg, end, KDIR, IDIR, MTsrc_31, grid);
     #if PHYSICS == MHD || PHYSICS == RMHD
      OMEGA_SOURCE (state, beg, end, KDIR, IDIR, OTsrc_31, grid);
     #endif
     for (i = beg; i <= end; i++){
       EXPAND(state->rhs[i][iMR] += dt*MTsrc_33[i]*grid[IDIR].r_1[i];  ,
                                                                       ,
              state->rhs[i][iMPHI] -= dt*MTsrc_31[i]*grid[IDIR].r_1[i];)
       #if PHYSICS == MHD || PHYSICS == RMHD
        state->rhs[i][iBPHI] -= dt*OTsrc_31[i]*grid[IDIR].r_1[i];
       #endif
     }
   }
  #endif

/* ------------------------------------------------
           Source terms: gravity
   ------------------------------------------------ */

  #if INCLUDE_BODY_FORCE == EXPLICIT
   ADD_BODY_FORCE (state, dt, grid);
  #endif

/* ------------------------------------------------
        Update solution vector to get u(n+1/2)
   ------------------------------------------------ */

  for (i = beg; i <= end; i++) {
    for (nv = NVAR; nv--;  ) {
      state->uh[i][nv] = state->u[i][nv] + state->rhs[i][nv];
    }
  }

/* -------------------------------------------------
     check if U(x_i, n+1/2) has physical meaning.
     Revert to 1st order otherwise.
   ------------------------------------------------- */

  if (!CONTOPRIM (state->uh, state->vh, beg, end, state->flag)){
    for (i = beg; i <= end; i++){
      if (state->flag[i]){
        for (nv = NVAR; nv--;   ){
          state->uh[i][nv] = state->u[i][nv];
          state->vh[i][nv] = state->v[i][nv];
          state->vp[i][nv] = state->vm[i][nv] = state->v[i][nv];
        }
        #ifdef STAGGERED_MHD
         state->vp[i][B1] = state->bn[i];
         state->vm[i][B1] = state->bn[i-1];
        #endif
      }
    }
  }    

/* ----------------------------------------------------
        evolve interface values accordingly 
   ---------------------------------------------------- */

  for (i = beg; i <= end; i++) {
  for (nv = NVAR; nv--;    ){
    scrh = state->vh[i][nv] - state->v[i][nv]; /* -- approx dv/dt * dt -- */

    #if CHAR_UPWIND == YES
     if (lambda_min[i] > 0.0) {
       state->vp[i][nv] += scrh;
     }else if (lambda_max[i] < 0.0) {
       state->vm[i][nv] += scrh;
     }else{
      state->vp[i][nv] += scrh;
      state->vm[i][nv] += scrh;
     }  
    #else   
     state->vp[i][nv] += scrh;
     state->vm[i][nv] += scrh;
    #endif
  }}

  CHECK_PRIM_STATES (state->vm, state->vp, state->v, beg, end);

  PRIMTOCON (state->vp, state->up, beg, end);
  PRIMTOCON (state->vm, state->um, beg, end);

/* -- check LR states for magnetic field -- */
   	      
  #ifdef STAGGERED_MHD
   for (i = beg; i <= end-1; i++) {
     scrh = fabs(state->vL[i][B1] - state->vR[i][B1]);
     if (scrh > 1.e-12){
       printf ("! B1 not unique in hancock.c\n");
       SHOW(state->vL,i);
       SHOW(state->vR,i);
       QUIT_ORION2(1);
     }
   }
  #endif       
}
#undef CHAR_UPWIND

#endif

