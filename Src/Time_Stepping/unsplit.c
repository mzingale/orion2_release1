#include "orion2.h"

/* ************************************************************ */
void UNSPLIT (const Data *d, Riemann_Solver *RIEMANN, 
              Time_Step *Dts, Grid *grid)

/*
 *
 *
 * PURPOSE: 
 *     
 *   Handle x1, x2 and x3 sweep integrators
 *
 *
 * ARGUMENTS:
 *
 *
 *
 *    
 *************************************************************** */
{
  int  ii, jj, kk;
  int  *i, *j, *k;
  int  in, nv;
  
  static real  one_third = 1.0/3.0;
  static State_1D state;
  static Data_Arr UU, UU_1;
  real dt;
  Index indx;

/* ----------------------------------------------------
                   Allocate memory 
   ---------------------------------------------------- */

  if (state.rhs == NULL){
    Make_State (&state);
    UU   = Array_4D(NZ_TOT, NY_TOT, NX_TOT, NVAR, double);
    UU_1 = Array_4D(NZ_TOT, NY_TOT, NX_TOT, NVAR, double);
  }

/* ----------------------------------------------------
                   Step 1: predictor
   ---------------------------------------------------- */

  ISTEP = 1;  

  BOUNDARY (d, ALLDIR, grid);

  #if INTERPOLATION == LINEAR_MULTID 
   MULTID_LIMITER (d, grid);
  #endif
  #if SHOCK_FLATTENING == MULTID
   FIND_SHOCK (d, grid);
  #endif

  dt = delta_t;
  for (DIR = 0; DIR < DIMENSIONS; DIR++){
  
  /*  --------------------------------
       Set normal and tangent indexes  
      -------------------------------- */

    SET_INDEXES (&indx, &state, grid);
    TRANSVERSE_LOOP(indx,in,i,j,k){  

      for (in = 0; in < indx.ntot; in++) {
        for (nv = NVAR; nv--;  ) state.v[in][nv] = d->Vc[nv][*k][*j][*i];
        #ifdef STAGGERED_MHD
         state.bn[in] = d->Vs[DIR][*k][*j][*i];
        #endif
      }

      CHECK_NAN (state.v, 0, indx.ntot-1,0);
      PRIMTOCON (state.v, state.u, 0, indx.ntot - 1);
      STATES  (&state, indx.beg - 1, indx.end + 1, dt, grid); 
      RIEMANN (&state, indx.beg - 1, indx.end, Dts->cmax + DIR, grid);
      #ifdef STAGGERED_MHD
       EMF_PUT (&state, indx.beg - 1, indx.end, grid);
      #endif
      #if INCLUDE_PARABOLIC_FLUX == YES
       PARABOLIC_FLUX (d->Vc, &state, indx.beg - 1, indx.end, Dts, grid);
      #endif
      #if SAVE_VEC_POT == YES
       VEC_POT (d, NULL, &state, grid);
      #endif

      GET_RHS (&state, indx.beg, indx.end, dt, grid);

      if (DIR == IDIR){
        for (in = indx.beg; in <= indx.end; in++) {
        for (nv = NVAR; nv--;  ) {
          UU_1[*k][*j][*i][nv] = state.u[in][nv] + state.rhs[in][nv];
          UU  [*k][*j][*i][nv] = state.u[in][nv]; /* -- save initial value -- */
        }}
      }else{
        for (in = indx.beg; in <= indx.end; in++) {
        for (nv = NVAR; nv--;  ) {
          UU_1[*k][*j][*i][nv] += state.rhs[in][nv];
        }}
      }
    }
  }

  #ifdef STAGGERED_MHD
   FCT_UPDATE(1, d, grid);
   AVERAGE_MAGNETIC_FIELD (d->Vs, UU_1, grid);
  #endif

  /* -------------------------
       convert to primitive
     ------------------------- */
     
  DIR = IDIR;
  SET_INDEXES (&indx, &state, grid);
  for (kk = KBEG; kk <= KEND; kk++){ NZ_PT = &kk;
  for (jj = JBEG; jj <= JEND; jj++){ NY_PT = &jj;
    CONTOPRIM (UU_1[kk][jj], state.v, IBEG, IEND, state.flag);
    for (ii = IBEG; ii <= IEND; ii++){
    for (nv = 0; nv < NVAR; nv++) {
      d->Vc[nv][kk][jj][ii] = state.v[ii][nv];
    }} 
  }}
  
/* ----------------------------------------------------
                   STEP II  (or CORRECTOR)
   ---------------------------------------------------- */

  #if (TIME_STEPPING == RK2) || (TIME_STEPPING == RK3)

   ISTEP = 2;
   BOUNDARY (d, ALLDIR, grid);
   #if INTERPOLATION == LINEAR_MULTID 
    MULTID_LIMITER (d, grid);
   #endif
   #if SHOCK_FLATTENING == MULTID
    FIND_SHOCK (d, grid);
   #endif

   for (kk = KBEG; kk <= KEND; kk++){
   for (jj = JBEG; jj <= JEND; jj++){
   for (ii = IBEG; ii <= IEND; ii++){
   for (nv = NVAR; nv--;  ) {
     #if TIME_STEPPING == RK2 
      UU_1[kk][jj][ii][nv] = 0.5*(UU[kk][jj][ii][nv] + UU_1[kk][jj][ii][nv]);
     #elif TIME_STEPPING == RK3 
      UU_1[kk][jj][ii][nv] = 0.75*UU[kk][jj][ii][nv] + 0.25*UU_1[kk][jj][ii][nv];
     #endif
   }}}}

   #if TIME_STEPPING == RK2
    dt = 0.5*delta_t;
   #elif TIME_STEPPING == RK3
    dt = 0.25*delta_t;
   #endif

   for (DIR = 0; DIR < DIMENSIONS; DIR++){

  /* --------------------------------
      Set normal and tangent indexes  
     -------------------------------- */

     SET_INDEXES (&indx, &state, grid);
     TRANSVERSE_LOOP(indx,in,i,j,k){  

       for (in = 0; in < indx.ntot; in++) {
         for (nv = NVAR; nv--;  ) state.v[in][nv] = d->Vc[nv][*k][*j][*i];
         #ifdef STAGGERED_MHD
          state.bn[in] = d->Vs[DIR][*k][*j][*i];
         #endif
       }

       PRIMTOCON   (state.v, state.u, 0, indx.ntot - 1);
       STATES  (&state, indx.beg - 1, indx.end + 1, dt, grid);     
       RIEMANN (&state, indx.beg - 1, indx.end, Dts->cmax + DIR, grid);
       #ifdef STAGGERED_MHD
        EMF_PUT (&state, indx.beg - 1, indx.end, grid);
       #endif
       #if INCLUDE_PARABOLIC_FLUX == YES
        PARABOLIC_FLUX (d->Vc, &state, indx.beg - 1, indx.end, Dts, grid);
       #endif
       #if SAVE_VEC_POT == YES
        VEC_POT (d, NULL, &state, grid);
       #endif
       GET_RHS (&state, indx.beg, indx.end, dt, grid);
      
       for (in = indx.beg; in <= indx.end; in++) {
       for (nv = NVAR; nv--;  ) {
          UU_1[*k][*j][*i][nv] += state.rhs[in][nv];
       }}
     }
   }

   #ifdef STAGGERED_MHD
    FCT_UPDATE(2, d, grid);
    AVERAGE_MAGNETIC_FIELD (d->Vs, UU_1, grid);
   #endif

  /* -------------------------
       convert to primitive
     ------------------------- */
     
   DIR = IDIR;
   SET_INDEXES (&indx, &state, grid);
   for (kk = KBEG; kk <= KEND; kk++){ NZ_PT = &kk;
   for (jj = JBEG; jj <= JEND; jj++){ NY_PT = &jj;
     CONTOPRIM (UU_1[kk][jj], state.v, IBEG, IEND, state.flag);
     for (ii = IBEG; ii <= IEND; ii++){
     for (nv = 0; nv < NVAR; nv++) {
       d->Vc[nv][kk][jj][ii] = state.v[ii][nv];
     }} 
   }}

  #endif

/* ----------------------------------------------------
                  STEP   III   (or CORRECTOR)
   ---------------------------------------------------- */

  #if TIME_STEPPING == RK3

   ISTEP = 3;
   BOUNDARY (d, ALLDIR, grid);
   #if INTERPOLATION == LINEAR_MULTID 
    MULTID_LIMITER (d, grid);
   #endif

   #if SHOCK_FLATTENING == MULTID
    FIND_SHOCK (d, grid);
   #endif

   for (kk = KBEG; kk <= KEND; kk++){
   for (jj = JBEG; jj <= JEND; jj++){
   for (ii = IBEG; ii <= IEND; ii++){
   for (nv = NVAR; nv--;  ) {
     UU_1[kk][jj][ii][nv] = one_third*(    UU  [kk][jj][ii][nv] +
                                       2.0*UU_1[kk][jj][ii][nv]);
   }}}}

   dt = 2.0/3.0*delta_t;

   for (DIR = 0; DIR < DIMENSIONS; DIR++){

   /*  --------------------------------
        Set normal and tangent indexes  
       -------------------------------- */

     SET_INDEXES (&indx, &state, grid);
     TRANSVERSE_LOOP(indx,in,i,j,k){  

       for (in = 0; in < indx.ntot; in++) {
         for (nv = NVAR; nv--;  ) state.v[in][nv] = d->Vc[nv][*k][*j][*i];
         #ifdef STAGGERED_MHD
          state.bn[in] = d->Vs[DIR][*k][*j][*i];
         #endif
       }

       PRIMTOCON   (state.v, state.u, 0, indx.ntot - 1);
       STATES  (&state, indx.beg - 1, indx.end + 1, dt, grid);    
       RIEMANN (&state, indx.beg - 1, indx.end, Dts->cmax + DIR, grid);
       #ifdef STAGGERED_MHD
        EMF_PUT (&state, indx.beg - 1, indx.end, grid);
       #endif
       #if INCLUDE_PARABOLIC_FLUX == YES
        PARABOLIC_FLUX (d->Vc, &state, indx.beg - 1, indx.end, Dts, grid);
       #endif
       #if SAVE_VEC_POT == YES
        VEC_POT (d, NULL, &state, grid);
       #endif
       GET_RHS (&state, indx.beg, indx.end, dt, grid);

       for (in = indx.beg; in <= indx.end; in++) {
       for (nv = NVAR; nv--;  ) {
         UU_1[*k][*j][*i][nv] += state.rhs[in][nv];
       }}
     }
   }

   #ifdef STAGGERED_MHD
    FCT_UPDATE(3, d, grid);
    AVERAGE_MAGNETIC_FIELD (d->Vs, UU_1, grid);
   #endif

   /* -------------------------
        convert to primitive
      ------------------------- */
 
   DIR = IDIR;
   SET_INDEXES (&indx, &state, grid);
   for (kk = KBEG; kk <= KEND; kk++){ NZ_PT = &kk;
   for (jj = JBEG; jj <= JEND; jj++){ NY_PT = &jj;
     CONTOPRIM (UU_1[kk][jj], state.v, IBEG, IEND, state.flag);
     for (ii = IBEG; ii <= IEND; ii++){
     for (nv = 0; nv < NVAR; nv++) {
       d->Vc[nv][kk][jj][ii] = state.v[ii][nv];
     }} 
   }}

  #endif
  
}
