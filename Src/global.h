#ifdef PARALLEL
 int SZ;
 int SZ_float;
 int SZ_char;
 int SZ_Float_Vect;
 int SZ_rgb;
 int SZ_short;
#endif

int prank;

int NPROCESSORS;
int MAX_RIEM;

long int USED_MEMORY, NMAX_POINT, NSTEP;
long int IBEG, IEND, JBEG, JEND, KBEG, KEND;
long int NX, NY, NZ;
long int NX_TOT, NY_TOT, NZ_TOT;

int ISTEP;
int V1, V2, V3;
int M1, M2, M3;
int B1, B2, B3;

int DIR;
int *NX_PT, *NY_PT, *NZ_PT;

real UNIT_DENSITY     = 1.0;
real UNIT_LENGTH      = 1.0;
real UNIT_VELOCITY    = 1.0;
real T_CUT_COOL       = 50.0;
real FLOOR_TRAD       = 0.;
real FLOOR_TGAS       = 0.;
real CEIL_VA          = 1.e100;
real CEIL_TGAS        = 1.e100;
real MAX_COOLING_RATE = 0.1;
real SMALL_DN         = 1.e-12;
real SMALL_PR         = 1.e-12;
#if EOS != ISOTHERMAL
 real gmm = 5./3.;
#elif EOS == ISOTHERMAL
 real C_ISO = 1.0;
#endif
real glob_time, delta_t, CFL;
real MAX_MACH_NUMBER;

real DOM_XBEG[3], DOM_XEND[3];

/* aux is an array containing the user-defined
   parameters     */

real aux[32];

#ifdef PSI_GLM
 double MAXC=1.0;
#endif

#if PRINT_TO_FILE == YES
FILE *orion2_log_file;
#endif
