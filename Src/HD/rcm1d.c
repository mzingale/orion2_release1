#include "orion2.h"

#define MAX_ITER   20
#define small_p       1.e-9
#define small_rho     1.e-9

static void PFUN (real p, real *vL, real *vR, real *f, real *df);
static void FUN_LR (real p, real *v, real *fLR, real *dfLR);

/* ***************************************************************************** */
void RCM_STATE (const State_1D *state, real dxdt, Grid *grid)
/*
 *
 * NAME
 *
 *   RIEMANN
 *
 *
 * PURPOSE
 *
 *
 * LAST_MODIFIED
 *
 *   April 4th 2006, by Andrea Mignone  (mignone@to.astro.it)
 *
 *
 ******************************************************************************* */
{
  int  nv, k, i, beg, end;
  real pstar, ustar, dp, fp, dfp;
  real th, q, scrh, gmmp, gp1, gm1;
  real *vL, *vR;
  real fL, dfL, SL, STL, csL;
  real fR, dfR, SR, STR, csR;
  static real *s, **vs, **us, *cmax_loc;
  static int *shock;

  if (vs == NULL){
    vs       = Array_2D(NMAX_POINT, NVAR, double);
    us       = Array_2D(NMAX_POINT, NVAR, double);
    s        = Array_1D(NMAX_POINT, double);
    cmax_loc = Array_1D(NMAX_POINT, double);
    shock    = Array_1D(NMAX_POINT, int);
  }
 
  beg = grid[DIR].lbeg - 1;
  end = grid[DIR].lend;

  gm1  = gmm - 1.0;
  gp1  = gmm + 1.0;
  gmmp = gm1/gp1;

/* -- select zones for random sampling -- */

  for (i = beg; i <= end; i++) {
    scrh = state->v[i + 1][VX] - state->v[i - 1][VX];
    dp   = fabs(state->v[i + 1][PR] - state->v[i - 1][PR]);
    dp  /= dmin(state->v[i + 1][PR], state->v[i - 1][PR]);

    shock[i] = 1;
    if (scrh < 0.0 && dp > 1.0) shock[i] = 1; 
  }

/* -- Generate pseudo random number sequence -- */

  th   = VAN_DER_CORPUT (NSTEP, 2, 1);
  for (i = beg - 1; i <= end + 1; i++) {
    s[i] = (th - 0.5)*dxdt;
    for (nv = NVAR; nv--; ){
      state->vL[i][nv] = state->v[i][nv];
      state->vR[i][nv] = state->v[i + 1][nv];
    }
  }

  for (i = beg; i <= end; i++){

    vL = state->vL[i];
    vR = state->vR[i];

/*
s[i] = -0.5 + 1.0*(real)(i - beg)/(real)(end - beg);
s[i] /= 0.3;
vL[DN] = 1.0;
vL[VX] = 0.;
vL[PR] = 1.;

vR[DN] = 0.5;
vR[VX] = 0.5;
vR[PR] = 0.1;
*/    


  /* -- guess here -- */

    pstar = 0.5*(vL[PR] + vR[PR]);
    
    for (k = 0; k < MAX_ITER; k++){

      PFUN (pstar, vL, vR, &fp, &dfp);
      dp     = fp/dfp;
      pstar -= dp;

      if (fabs(dp) < 1.e-7*pstar) break;
      if (k == (MAX_ITER-1)){
        printf ("! Too many iterations in Rieman\n");
        QUIT_ORION2(1);
      }
    }

    FUN_LR (pstar, vL, &fL, &dfL);
    FUN_LR (pstar, vR, &fR, &dfR);

    ustar = 0.5*(vL[V1] + vR[V1] + fR - fL);

  /* -- sample solution -- */

    if (s[i] <= ustar){  /* -- left of CD -- */
      q   = pstar/vL[PR];
      csL = sqrt(gmm*vL[PR]/vL[DN]);

      if (q > 1.0) { /* -- left wave is a shock -- */

        scrh = gp1*q + gm1;          
        SL   = vL[V1] - csL*sqrt(0.5/gmm*scrh);
        if (s[i] < SL){
          for (nv = NVAR; nv--; ) vs[i][nv] = vL[nv];
        }else { 
          vs[i][DN] = vL[DN]*(q + gmmp)/(gmmp*q + 1.0);
          vs[i][V1] = ustar;
          vs[i][PR] = pstar;
        }

      }else{  /* -- left wave is a rarefaction -- */

        SL = vL[V1] - csL;
 
        if (s[i] < SL) {
          for (nv = NVAR; nv--; ) vs[i][nv] = vL[nv];
        }else { 
          vs[i][DN] = vL[DN]*pow(q, 1.0/gmm);
          vs[i][V1] = ustar;
          vs[i][PR] = pstar;
          STL = ustar - sqrt(gmm*pstar/vs[i][DN]);
          if (s[i] < STL){ /* -- sol inside rarefaction -- */
            scrh = 2.0 + gm1/csL*(vL[V1] - s[i]);
            vs[i][DN] = vL[DN]*pow(scrh/gp1, 2.0/gm1);
            vs[i][PR] = vL[PR]*pow(scrh/gp1, 2.0*gmm/gm1);
            vs[i][V1] = 2.0/gp1*(csL + 0.5*gm1*vL[V1] + s[i]);
          }
        }
      } 

    }else{  /* -- right of CD -- */

      q   = pstar/vR[PR];
      csR = sqrt(gmm*vR[PR]/vR[DN]);

      if (q > 1.0) { /* -- right wave is a shock -- */

        scrh = gp1*q + gm1;          
        SR   = vR[V1] + csR*sqrt(0.5/gmm*scrh);
        if (s[i] > SR){
          for (nv = NVAR; nv--; ) vs[i][nv] = vR[nv];
        }else { 
          vs[i][DN] = vR[DN]*(q + gmmp)/(gmmp*q + 1.0);
          vs[i][V1] = ustar;
          vs[i][PR] = pstar;
        }

      }else{  /* -- right wave is a rarefaction -- */

        SR = vR[V1] + csR;
 
        if (s[i] > SR) {
          for (nv = NVAR; nv--; ) vs[i][nv] = vR[nv];
        }else { 
          vs[i][DN] = vR[DN]*pow(q, 1.0/gmm);
          vs[i][V1] = ustar;
          vs[i][PR] = pstar;
          STR = ustar + sqrt(gmm*pstar/vs[i][DN]);
          if (s[i] > STR){ /* -- sol inside rarefaction -- */
            scrh = 2.0 - gm1/csR*(vR[V1] - s[i]);
            vs[i][DN] = vR[DN]*pow(scrh/gp1, 2.0/gm1);
            vs[i][PR] = vR[PR]*pow(scrh/gp1, 2.0*gmm/gm1);
            vs[i][V1] = 2.0/gp1*(-csR + 0.5*gm1*vR[V1] + s[i]);
          }
        }
      } 
    }   

/*printf ("%f  %f  %f  %f \n",s[i]*0.3,vs[i][DN],vs[i][VX],vs[i][PR]);  */

  }

/* -- compute fluxes -- */

/*
  MAX_CH_SPEED (vs, cmax_loc, grid, beg, end);
  for (i = beg; i <= end; i++) {
    *cmax = dmax(cmax_loc[i], *cmax);
  }
  FLUX(us, vs, state->flux, state->press, beg, end);
*/

/* ----------------------------------------------------------
        Compute RCM solution                
   ---------------------------------------------------------- */

PRIMTOCON (vs, us, beg, end);

  for (i = beg + 1; i <= end; i++){
    if (shock[i]){
      if (th <= 0.5) {
        for (nv = NVAR; nv--; ) state->u[i][nv] = us[i][nv];
/*      for (nv = NVAR; nv--; ) state->u[i][nv] = us[i-1][nv]; */
      }else{ 
        for (nv = NVAR; nv--; ) state->u[i][nv] = us[i - 1][nv];
/*      for (nv = NVAR; nv--; ) state->u[i][nv] = us[i][nv]; */
      }
    }
  }

}

/* ************************************************ */
void PFUN (real p, real *vL, real *vR, real *f, real *df)
/*
 *
 *
 *
 *
 ************************************************** */
{
  real fL , fR; 
  real dfL, dfR;
  
  FUN_LR (p, vL, &fL, &dfL);
  FUN_LR (p, vR, &fR, &dfR);

  *f  = fL  + fR + vR[V1] - vL[V1];
  *df = dfL + dfR; 
}

/* ************************************************ */
void FUN_LR (real p, real *v, real *fLR, real *dfLR)
/*
 *
 *
 *
 *
 ************************************************** */
{
  real A, B, scrh, cs;
  real q;

  if (p > v[PR]) {  /* -- (shock) -- */

    A = 2.0/(gmm + 1.0)/v[DN];
    B = (gmm - 1.0)/(gmm + 1.0)*v[PR];
    scrh  = A/(p + B);
    *fLR  = (p - v[PR])*sqrt(scrh);
    *dfLR = sqrt(scrh)*(1.0 - 0.5*(p - v[PR])/(B + p));

  }else{   /* -- (rarefaction) -- */

    cs = sqrt(gmm*v[PR]/v[DN]);
    q  = p/v[PR];
    scrh = pow(q, 0.5*(gmm - 1.0)/gmm) - 1.0;
    *fLR  = 2.0*cs/(gmm - 1.0)*scrh;
    scrh  = pow(q, -0.5*(gmm + 1.0)/gmm);
    *dfLR = 1.0/(v[DN]*cs)*scrh;
  }

}




