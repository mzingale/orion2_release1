/* ****************************************************************

     Set label, indexes and basic prototyping for the Hydro (HD)
     module.

     We make extra vector components, when not needed, point 
     to the last element (255) of the array stored by startup.c.  
   **************************************************************** */

enum {
 #if COMPONENTS == 1
  DN, MX, 
  #if EOS == IDEAL
   EN, PR=EN,
  #endif
  MY = 255, MZ = 255,
 #elif COMPONENTS == 2
  DN, MX, MY, 
  #if EOS == IDEAL
   EN, PR=EN,
  #endif
  MZ = 255,
 #elif COMPONENTS == 3
  DN, MX, MY, MZ,
  #if EOS == IDEAL
   EN, PR=EN,
  #endif
 #endif
 VX = MX, VY = MY, VZ = MZ
};


#define NFLX (COMPONENTS + (EOS == IDEAL ? 2:1))

#if EOS == ISOTHERMAL 
 #define C_ISO2 ((C_ISO)*(C_ISO))
#endif

/* *************************************************
     Now define more convenient and user-friendly 
     pointer labels for geometry setting      
   ************************************************* */

#if GEOMETRY == CYLINDRICAL 

 #define iVR    VX
 #define iVZ    VY
 #define iVPHI  VZ

 #define iMR    MX
 #define iMZ    MY
 #define iMPHI  MZ

#endif

#if GEOMETRY == POLAR 

 #define iVR    VX
 #define iVPHI  VY
 #define iVZ    VZ

 #define iMR    MX
 #define iMPHI  MY
 #define iMZ    MZ

#endif

#if GEOMETRY == SPHERICAL 

 #define iVR    VX
 #define iVTH   VY
 #define iVPHI  VZ

 #define iMR    MX
 #define iMTH   MY
 #define iMPHI  MZ

#endif

/* *************************************************
     Label the different waves in increasing order 
     following the number of vector components.
   ************************************************* */

enum KWAVES {
 KSOUNDM, KSOUNDP
 #if EOS == IDEAL 
  , KENTRP
 #endif
};

/* ***********************************************************
                   Prototyping goes here          
   *********************************************************** */

int  CONTOPRIM   (real **, real **, int, int, char *);
void EIGENVALUES (double *, double *);
void PRIM_EIGENVECTORS (double *, double, double, double *, double **, double **);
void CONS_EIGENVECTORS (double *, double *, double **, double **, double *);

void ENTHALPY  (real **, real *, int, int);
void FLUX      (real **, real **, real **, real *p, int, int);
void HLL_SPEED (real **, real **, real *, real *, int, int);
void MAX_CH_SPEED(real **, real *,  real *, int, int);
void PRIMTOCON   (real **, real **, int, int);
void PRIM_RHS    (real *, real *, real, real, real *);
void PRIM_SOURCE (const State_1D *, int, int, real *, real *, real **, Grid *);
                                                                                                                                                                         
void MOM_SOURCE (const State_1D *, int, int, double, Grid *);


Riemann_Solver TWO_SHOCK, LF_SOLVER, MUSTA_FLUX, 
               ROE_SOLVER, HLL_SOLVER, HLLC_SOLVER;
Riemann_Solver AUSMp;


void RCM_STATE (const State_1D *, real, Grid *);
