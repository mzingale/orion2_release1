#include"orion2.h"

/*   ****  switches, see below for description  ****  */

#define DAVIS_ESTIMATE     YES
#define EINFELDT_ESTIMATE  NO
#define ROE_ESTIMATE       NO

/* ***************************************************************************** */
void HLL_SPEED (real **vL, real **vR,  
                real *SL, real *SR, int beg, int end)
/* 
 *
 *
 * NAME
 *
 *   HLL_SPEED
 *
 *
 * PURPOSE
 *
 *   Compute leftmost (SL) and rightmost (SR) speed for the Riemann fan
 * 
 *
 * ARGUMENTS
 *
 *   vL (IN)       1-D array of left-edge primitive values at i+1/2
 *   vR (IN)       1-D array of right-edge primitive values at i+1/2
 *   grid (IN)     Array of grids
 *   cmax(OUT)     Array of maximum characteristic speeds in this direction
 *
 * SWITCHES
 *
 *    ROE_ESTIMATE (YES/NO), DAVIS_ESTIMATE (YES/NO). TVD_ESTIMATE (YES/NO)
 *    JAN_HLL (YES/NO) 
 *
 *    These switches set how the wave speed estimates are
 *    going to be computed. Only one can be set to 'YES', and
 *    the rest of them must be set to 'NO'  
 *
 *     ROE_ESTIMATE:    b_m = \min(0, \min(u_R - c_R, u_L - c_L, u_{roe} - c_{roe}))     
 *                      b_m = \min(0, \min(u_R + c_R, u_L + c_L, u_{roe} + c_{roe}))
 * 
 *                      where u_{roe} and c_{roe} are computed using Roe averages.
 *  
 *     DAVIS_ESTIMATE:  b_m = \min(0, \min(u_R - c_R, u_L - c_L))     
 *                      b_m = \min(0, \min(u_R + c_R, u_L + c_L))  
 *
 *
 *
 *
 *
 * LAST_MODIFIED
 *
 *   March, 31/2006, Andrea Mignone  (mignone@to.astro.it)
 *              
 *
 ******************************************************************************** */
{
  int    i;
  real   scrh, s, c;
  double aL2, aL, dL;
  double aR2, aR, dR;
  double dvx, dvy, dvz;
  double a_av, du, vx;
  static real *sl_min, *sl_max;
  static real *sr_min, *sr_max;

  if (sl_min == NULL){
    sl_min = Array_1D(NMAX_POINT, double);
    sl_max = Array_1D(NMAX_POINT, double);

    sr_min = Array_1D(NMAX_POINT, double);
    sr_max = Array_1D(NMAX_POINT, double);
  }

/* ----------------------------------------------
              DAVIS Estimate  
   ---------------------------------------------- */

  #if DAVIS_ESTIMATE == YES

   MAX_CH_SPEED (vL, sl_min, sl_max, beg, end);
   MAX_CH_SPEED (vR, sr_min, sr_max, beg, end);

   for (i = beg; i <= end; i++) {

     SL[i] = dmin(sl_min[i], sr_min[i]);
     SR[i] = dmax(sl_max[i], sr_max[i]);

     #if EOS == IDEAL
      aL2 = gmm*vL[i][PR]/vL[i][DN];
      aR2 = gmm*vR[i][PR]/vR[i][DN];
      aL  = sqrt(aL2);
      aR  = sqrt(aR2);
     #elif EOS == ISOTHERMAL
      aL2 = aR2 = C_ISO2;
      aL  = aR  = C_ISO;
     #endif

     scrh  = fabs(vL[i][V1]) + fabs(vR[i][V1]);    
     scrh /= aL + aR;

     MAX_MACH_NUMBER = dmax(scrh, MAX_MACH_NUMBER); 

   }

  #endif

/* ----------------------------------------------
        Einfeldt Estimate for wave speed 
   ---------------------------------------------- */

  #if EINFELDT_ESTIMATE == YES

   for (i = beg; i <= end; i++) {
     #if EOS == IDEAL
      aL   = sqrt(gmm*vL[i][PR]/vL[i][DN]);
      aR   = sqrt(gmm*vR[i][PR]/vR[i][DN]);
     #elif EOS == ISOTHERMAL
      aL = aR = C_ISO;
     #endif

     dL   = sqrt(vL[i][DN]);
     dR   = sqrt(vR[i][DN]);
     a_av = 0.5*dL*dR/( (dL + dR)*(dL + dR));
     du   = vR[i][V1] - vL[i][V1];
     scrh = (dR*aL*aL + dR*aR*aR)/(dL + dR);
     scrh += a_av*du*du;
      
     SL[i] = (dl*ql[V1] + dr*qr[V1])/(dl + dr);
  
     bmin = dmin(0.0, um[V1] - sqrt(scrh));
     bmax = dmax(0.0, um[V1] + sqrt(scrh));      
   }
  #endif

/* ----------------------------------------------
              Roe-like Estimate  
   ---------------------------------------------- */

  #if ROE_ESTIMATE  == YES
   
   for (i = beg; i <= end; i++) {

     #if EOS == IDEAL
      aL2 = gmm*vL[i][PR]/vL[i][DN];
      aR2 = gmm*vR[i][PR]/vR[i][DN];
      aL  = sqrt(aL2);
      aR  = sqrt(aR2);
     #elif EOS == ISOTHERMAL
      aL2 = aR2 = C_ISO2;
      aL  = aR  = C_ISO;
     #endif

     s = 1.0/(1.0 + sqrt(vR[i][DN]/vL[i][DN]));
     c = 1.0 - s;

     vx = s*vL[i][V1] + c*vR[i][V1];

/* ************************************************************
     The following definition of the averaged sound speed
     is equivalent to original formulation given by Roe;
     here we just simplify the expression without explicitly 
     computing the Enthalpy:                                 
   ************************************************************ */
   
     EXPAND(dvx = vL[i][VX] - vR[i][VX];  ,
            dvy = vL[i][VY] - vR[i][VY];  ,
            dvz = vL[i][VZ] - vR[i][VZ];)

     scrh = EXPAND(dvx*dvx, + dvy*dvy, + dvz*dvz);
  
     a_av = sqrt(s*aL2 + c*aR2 + 0.5*s*c*gmm1*scrh);

     SL[i] = dmin(vL[i][V1] - aL, vx - a_av);
     SR[i] = dmax(vR[i][V1] + aR, vx + a_av);

     scrh = (fabs(vL[i][V1]) + fabs(vR[i][V1]))/(aL + aR);
     MAX_MACH_NUMBER = dmax(scrh, MAX_MACH_NUMBER);

   }
  #endif
   
}
#undef DAVIS_ESTIMATE     
#undef EINFELDT_ESTIMATE  
#undef ROE_ESTIMATE       

