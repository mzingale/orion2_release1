#include"orion2.h"

/* **************************************************************************** */
void HLLC_SOLVER (const State_1D *state, int beg, int end, 
                  real *cmax, Grid *grid)
/*
 *
 *
 * PURPOSE
 *
 *   - Solve Riemann problem using a two-state HLLC solver
 * 
 *     Reference:   see the book by Toro  
 *   
 *   - On input, it takes left and right primitive state
 *     vectors state->vL and state->vR at zone edge i+1/2;
 *     On output, return flux and pressure vectors at the
 *     same interface.
 *
 *   - Also, compute maximum wave propagation speed (cmax) 
 *     for  explicit time step computation
 *
 * The isothermal version is essentially derived by the HLLD
 * Riemann solver for MHD by letting B -> 0.
 *  
 *
 * LAST_MODIFIED
 *
 *   March 23th 2012, by Andrea Mignone  (mignone@ph.unito.it)
 *
 ******************************************************************************* */
{
  int    nv, i;
  double scrh, vxr, vxl;
  double usL[NFLX], usR[NFLX], vs;
  double qL, qR, wL, wR;
  double *vL, *vR, *uL, *uR;
  #if EOS == ISOTHERMAL
   double rho, mx;
  #endif
  static real *pL, *pR, *SL, *SR;
  static real **fL, **fR;

/* -- Allocate memory -- */

  if (fL == NULL){
    fL = Array_2D(NMAX_POINT, NFLX, double);
    fR = Array_2D(NMAX_POINT, NFLX, double);

    pR = Array_1D(NMAX_POINT, double);
    pL = Array_1D(NMAX_POINT, double);
    SR = Array_1D(NMAX_POINT, double);
    SL = Array_1D(NMAX_POINT, double);
  }

  FLUX  (state->uL, state->vL, fL, pL, beg, end);
  FLUX  (state->uR, state->vR, fR, pR, beg, end);

  HLL_SPEED (state->vL, state->vR, SL, SR, beg, end);

  for (i = beg; i <= end; i++) {

    scrh = dmax(fabs(SL[i]), fabs(SR[i]));
    /*PS: Use old style cmax
    cmax[i]  = scrh;
    */
    *cmax  = dmax(*cmax, scrh);

    if (SL[i] > 0.0){
    
      for (nv = NFLX; nv--; ) state->flux[i][nv] = fL[i][nv];
      state->press[i] = pL[i];

    }else if (SR[i] < 0.0){

      for (nv = NFLX; nv--; ) state->flux[i][nv] = fR[i][nv];
      state->press[i] = pR[i];

    }else{

      vR = state->vR[i]; uR = state->uR[i];
      vL = state->vL[i]; uL = state->uL[i];

      vxr = vR[V1];
      vxl = vL[V1];
 
      #if SHOCK_FLATTENING == MULTID   

      /* ---------------------------------------------
         Switch to HLL in proximity of strong shocks.
        --------------------------------------------- */

       if (CHECK_ZONE(i, FLAG_HLL) || CHECK_ZONE(i+1, FLAG_HLL)){
         scrh  = 1.0/(SR[i] - SL[i]);
         for (nv = NFLX; nv--; ){
           state->flux[i][nv]  = SL[i]*SR[i]*(uR[nv] - uL[nv])
                              +  SR[i]*fL[i][nv] - SL[i]*fR[i][nv];
           state->flux[i][nv] *= scrh;
         }
         state->press[i] = (SR[i]*pL[i] - SL[i]*pR[i])*scrh;
         continue;
       }
      #endif

  /* ---------------------------------------
                   get u* 
     --------------------------------------- */    

      #if EOS == IDEAL
       qL = vL[PR] + uL[M1]*(vL[V1] - SL[i]);
       qR = vR[PR] + uR[M1]*(vR[V1] - SR[i]);

       wL = vL[DN]*(vL[V1] - SL[i]);
       wR = vR[DN]*(vR[V1] - SR[i]);

       vs = (qR - qL)/(wR - wL); /* wR - wL > 0 since SL < 0, SR > 0 */
/*
      vs = vR[PR] - vL[PR] + uL[M1]*(SL[i] - vxl) 
                           - uR[M1]*(SR[i] - vxr);
      vs /= vL[DN]*(SL[i] - vxl) - vR[DN]*(SR[i] - vxr);
*/

       usL[DN] = uL[DN]*(SL[i] - vxl)/(SL[i] - vs);
       usR[DN] = uR[DN]*(SR[i] - vxr)/(SR[i] - vs);
       EXPAND(usL[M1] = usL[DN]*vs;     usR[M1] = usR[DN]*vs;      ,
              usL[M2] = usL[DN]*vL[V2]; usR[M2] = usR[DN]*vR[V2];  ,
              usL[M3] = usL[DN]*vL[V3]; usR[M3] = usR[DN]*vR[V3];)
           
       usL[EN] = uL[EN]/vL[DN] + (vs - vxl)*(vs + vL[PR]/(vL[DN]*(SL[i] - vxl)));
       usR[EN] = uR[EN]/vR[DN] + (vs - vxr)*(vs + vR[PR]/(vR[DN]*(SR[i] - vxr)));

       usL[EN] *= usL[DN];
       usR[EN] *= usR[DN];
      #elif EOS == ISOTHERMAL
       scrh = 1.0/(SR[i] - SL[i]);
       rho  = (SR[i]*uR[DN] - SL[i]*uL[DN] - fR[i][DN] + fL[i][DN])*scrh;
       mx   = (SR[i]*uR[M1] - SL[i]*uL[M1] - fR[i][M1] + fL[i][M1])*scrh;
       
       usL[DN] = usR[DN] = rho;
       usL[M1] = usR[M1] = mx;
       vs  = (SR[i]*fL[i][DN] - SL[i]*fR[i][DN] + SR[i]*SL[i]*(uR[DN] - uL[DN]));
       vs *= scrh;
       vs /= rho;
       EXPAND(                                            ,
              usL[M2] = rho*vL[V2]; usR[M2] = rho*vR[V2]; ,
              usL[M3] = rho*vL[V3]; usR[M3] = rho*vR[V3];)
      #endif

        
 /* ---- verify consistency condition ------ */
 /*
      for (nv = 0; nv < NFLX; nv++) {
        scrh  = (vs - SL[i])*usL[nv] + (SR[i] - vs)*usR[nv];
        scrh -= SR[i]*uR[i][nv] - SL[i]*uL[i][nv] + 
                fl[i][nv] - fr[i][nv];
          if (fabs(scrh) > 1.e-9){
          printf (" Consistency condition violated\n");
          printf ("%d %d  %12.6e\n",i,nv, scrh);
        }
      }
 */          
/*  ----  Compute HLLC flux  ----  */

      if (vs >= 0.0){
        for (nv = NFLX; nv--;   ) {
          state->flux[i][nv] = fL[i][nv] + SL[i]*(usL[nv] - uL[nv]);
        }
        state->press[i] = pL[i];
      } else {
        for (nv = NFLX; nv--;   ) {
          state->flux[i][nv] = fR[i][nv] + SR[i]*(usR[nv] - uR[nv]);
        }
        state->press[i] = pR[i];
      }
    }
  } /* end loops on points */
}
