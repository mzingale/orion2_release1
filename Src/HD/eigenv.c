#include "orion2.h"

/* ******************************************************************** */
void MAX_CH_SPEED (double **v, double *cmin, double *cmax, int beg, int end)
/*
 *
 *
 * MAXIMUM EIGENSPEED
 *
 * Note: MAX_MACH_NUMBER is not computed here as for other
 *       modules. This would change all the log files in the
 *       Test_Prob/ directory.
 *
 *********************************************************************** */
{
  int    i;
  double a, local_mach;

  for (i = beg; i <= end; i++) {
    #if EOS == IDEAL
     a = sqrt(gmm*v[i][PR]/v[i][DN]);
    #elif EOS == ISOTHERMAL
     a = C_ISO;
    #endif
    local_mach = fabs(v[i][V1]/a);

/*    MAX_MACH_NUMBER = dmax (local_mach, MAX_MACH_NUMBER); */
/*    cmax[ii] = fabs(u[ii][V1]) + a; */
    cmin[i] = v[i][V1] - a;
    cmax[i] = v[i][V1] + a;
  }
}

/* ****************************************************************** */
void EIGENVALUES (double *v, double *lambda)
/*
 *
 *  Provide eigenvalues for the Euler equations.
 *
 ********************************************************************* */
{
  int    k;
  double cs;

  #if EOS == IDEAL
   cs = gmm*v[PR]/v[DN];
   cs = sqrt(cs);
  #elif EOS == ISOTHERMAL
   cs = C_ISO;
  #endif

  lambda[0] = v[V1] - cs;  
  lambda[1] = v[V1] + cs;  
  for (k = 2; k < NFLX; k++) lambda[k] = v[V1];
 
}

/* ****************************************************************** */
void  EIGENV (double *q, double cs2, double h, double *lambda,
                         double **LL, double **RR)
/*
 *
 *    PROVIDE EIGENVECTOR DECOMPOSITION FOR THE EQUATIONS IN
 *    PRIMITIVE FORM.
 *
 *
 *
 * INPUT:
 *
 *   q:    a veector of primitive quantities
 * cs2:    sound speed
 *   h:    enthalpy
 *  
 * OUTPUT:
 *
 *   lambda:   returns a vector containing the eigenvalues
 *   LL,RR :   return matrices containing the left and right 
 *             eigenvectors; the column of RR are the right 
 *             eigenvectors, while the row of LL are the left
 *             eigenvectors.
 * 
 *  NOTE: only non-zero components of LL and RR are computed; 
 *        RR and LL must be initialized to zero outside.
 *
 ********************************************************************* */
{
  int      i, j, k;
  double   rhocs, rho_cs, cs;
#if CHECK_EIGENVECTORS == YES
  double Aw1[NFLX], Aw0[NFLX], AA[NFLX][NFLX], a;
#endif

  #if EOS == IDEAL
   cs = sqrt(cs2);
  #elif EOS == ISOTHERMAL
   cs = C_ISO;
  #endif

  rhocs  = q[DN]*cs;
  rho_cs = q[DN]/cs;

/* ======================================================
                RIGHT EIGENVECTORS  
   ====================================================== */

/*       lambda = u - c       */

  lambda[0] = q[V1] - cs;

  RR[DN][0] =  0.5*rho_cs;
  RR[V1][0] = -0.5;
  #if EOS == IDEAL
   RR[PR][0] =  0.5*rhocs;
  #endif
     
/*       lambda = u + c       */

  lambda[1] = q[V1] + cs;

  RR[DN][1] = 0.5*rho_cs;
  RR[V1][1] = 0.5; 
  #if EOS == IDEAL
   RR[PR][1] = 0.5*rhocs;
  #endif

/*       lambda = u        */

  for (k = 2; k < NFLX; k++) lambda[k] = q[V1];
  #if EOS == IDEAL
   EXPAND(RR[DN][2] = 1.0;  ,
          RR[V2][3] = 1.0;  ,
          RR[V3][4] = 1.0;)
  #elif EOS == ISOTHERMAL
   EXPAND(                  ,
          RR[V2][2] = 1.0;  ,
          RR[V3][3] = 1.0;)
  #endif

/* ======================================================
                LEFT EIGENVECTORS  
   ====================================================== */

/*       lambda = u - c       */

  LL[0][V1] = -1.0; 
  #if EOS == IDEAL
   LL[0][PR] =  1.0/rhocs;
  #elif EOS == ISOTHERMAL
   LL[0][DN] =  1.0/rhocs;
  #endif

/*       lambda = u + c       */

  LL[1][V1] = 1.0; 
  #if EOS == IDEAL
   LL[1][PR] = 1.0/rhocs;
  #elif EOS == ISOTHERMAL
   LL[1][DN] = 1.0/rhocs;
  #endif

/*       lambda = u       */

  #if EOS == IDEAL
   EXPAND(LL[2][DN] = 1.0; ,
          LL[3][V2] = 1.0; ,
          LL[4][V3] = 1.0;)

   LL[2][PR] = -1.0/cs2;
  #elif EOS == ISOTHERMAL
   EXPAND(                 ,
          LL[2][V2] = 1.0; ,
          LL[3][V3] = 1.0;)
  #endif

/* ------------------------------------------------------------
    Verify eigenvectors consistency by

    1) checking that A = L.Lambda.R, where A is
       the Jacobian dF/dU
    2) verify orthonormality, L.R = R.L = I
   ------------------------------------------------------------ */

#if CHECK_EIGENVECTORS == YES
{
  static double **A, **ALR;
  double dA;

  if (A == NULL){
    A   = Array_2D(NFLX, NFLX, double);
    ALR = Array_2D(NFLX, NFLX, double);
  }

 /* --------------------------------------
     Construct the Jacobian analytically
    -------------------------------------- */

  for (i = 0; i < NFLX; i++){
  for (j = 0; j < NFLX; j++){
    A[i][j] = ALR[i][j] = 0.0;
  }}

  #if EOS == IDEAL
   A[DN][DN] = q[V1]    ; A[DN][V1] = q[DN];
   A[V1][V1] = q[V1]    ; A[V1][PR] = 1.0/q[DN];
   A[V2][V2] = q[V1]    ;  
   A[V3][V3] = q[V1]    ;  
   A[PR][V1] = gmm*q[PR]; A[PR][PR] =  q[V1];
  #elif EOS == ISOTHERMAL
   A[DN][DN] = q[V1]; A[DN][V1] = q[DN];
   A[V1][V1] = q[V1];
   A[V1][DN] = C_ISO2/q[DN];
   A[V2][V2] = q[V1];
   A[V3][V3] = q[V1];
  #endif

  for (i = 0; i < NFLX; i++){
  for (j = 0; j < NFLX; j++){
    ALR[i][j] = 0.0;
    for (k = 0; k < NFLX; k++) ALR[i][j] += RR[i][k]*lambda[k]*LL[k][j];
  }}

  for (i = 0; i < NFLX; i++){
  for (j = 0; j < NFLX; j++){
    dA = ALR[i][j] - A[i][j];
    if (fabs(dA) > 1.e-8){
      print ("! PRIM_EIGENVECTORS: eigenvectors not consistent\n");
      print ("! A[%d][%d] = %16.9e, ALR[%d][%d] = %16.9e\n",
                i,j, A[i][j], i,j,ALR[i][j]);
      print ("\n\n A = \n");   SHOWMATRIX(A, 1.e-8);
      print ("\n\n ALR = \n"); SHOWMATRIX(ALR, 1.e-8);
      QUIT_ORION2(1);
    }
  }}  

/* -- check orthornomality -- */

  for (i = 0; i < NFLX; i++){
  for (j = 0; j < NFLX; j++){
    a = 0.0;
    for (k = 0; k < NFLX; k++) a += LL[i][k]*RR[k][j];
    if ( (i == j && fabs(a-1.0) > 1.e-8) ||
         (i != j && fabs(a)>1.e-8) ) {
      print ("! PRIM_EIGENVECTORS: Eigenvectors not orthogonal\n");
      print ("!   i,j = %d, %d  %12.6e \n",i,j,a);
      print ("!   DIR: %d\n",DIR);
      QUIT_ORION2(1);
    }
  }}
}
#endif
}
/* ****************************************************************** */
void C_EIGENV (double *u, double *v, double **LL, double **RR,
                        double *lambda)
/*
 *
 *  Provide left and right eigenvectors of the conservative
 *  form of the equations.
 *
 *  Toro, pag. 107
 *
 ********************************************************************* */
{
  int    i, j, k, nv;
  double H, a, a2, gt_1, vmag2;

  #if EOS == IDEAL
   gt_1 = 1.0/(gmm - 1.0);
   a2   = gmm*v[PR]/v[DN];
   a    = sqrt(a2);
  #elif EOS == ISOTHERMAL
   a2 = C_ISO2;
   a  = C_ISO;
  #endif

  vmag2 = EXPAND(v[V1]*v[V1], + v[V2]*v[V2], + v[V3]*v[V3]);

/* -----------------------------------------------
    to compute H we use primitive variable since
    U and V may have been averaged in WENO_RF and
    are not the map of each other.
    Otherwise left and right eigenvectors wouldn't
    be orthonormal.
   ----------------------------------------------- */
/*
  H  = (u[EN] + v[PR])/v[DN];
*/
  #if EOS == IDEAL
   H = 0.5*vmag2 + a2*gt_1;
  #endif

/* ======================================================
                RIGHT EIGENVECTORS  
   ====================================================== */

/*       lambda = u - c       */

  k = 0;
  lambda[k] = v[V1] - a;

  RR[DN][k] = 1.0;
  EXPAND(RR[M1][k] = lambda[k];  ,
         RR[M2][k] = v[V2];      ,
         RR[M3][k] = v[V3]; )
  #if EOS == IDEAL
   RR[EN][k] = H - a*v[V1];
  #endif
     
/*       lambda = u + c       */
 
  k = 1;
  lambda[k] = v[V1] + a;

  RR[DN][k] = 1.0;
  EXPAND(RR[M1][k] = lambda[k];  ,
         RR[M2][k] = v[V2];      ,
         RR[M3][k] = v[V3];)
  #if EOS == IDEAL
   RR[EN][k] = H + a*v[V1];
  #endif

/*       lambda = u        */

  #if EOS == IDEAL
   k = 2;
   lambda[k] = v[V1];

   RR[DN][k] = 1.0;  
   EXPAND(RR[M1][k] = v[V1];  ,
          RR[M2][k] = v[V2];  ,
          RR[M3][k] = v[V3];)
   RR[EN][k] = 0.5*vmag2;  

   #if COMPONENTS > 1
    k = 3;
    lambda[k] = v[V1];
    RR[M2][k] = 1.0;  
    RR[EN][k] = v[V2];  
   #endif

   #if COMPONENTS == 3
    k = 4;
    lambda[k] = v[V1];
    RR[M3][k] = 1.0;  
    RR[EN][k] = v[V3];  
   #endif
  #elif EOS == ISOTHERMAL 
   EXPAND(                                      ,
          lambda[2] = v[V1]; RR[M2][2] = 1.0;   ,
          lambda[3] = v[V1]; RR[M3][3] = 1.0;)  
  #endif

/* ======================================================
                LEFT EIGENVECTORS  
   ====================================================== */

/*       lambda = u - c       */

  k = 0;
  #if EOS == IDEAL
   LL[k][DN] = H + a*gt_1*(v[V1] - a); 
   EXPAND(LL[k][M1] = -(v[V1] + a*gt_1); ,
          LL[k][M2] = -v[V2];            ,
          LL[k][M3] = -v[V3];)
   LL[k][EN] = 1.0;
  #elif EOS == ISOTHERMAL
   LL[k][DN] = 0.5*(1.0 + v[V1]/a);
   LL[k][M1] = -0.5/a; 
  #endif 

/*       lambda = u + c       */

  k = 1;
  #if EOS == IDEAL
   LL[k][DN] = H - a*gt_1*(v[V1] + a); 
   EXPAND(LL[k][M1] = -v[V1] + a*gt_1;  ,
          LL[k][M2] = -v[V2];         ,
          LL[k][M3] = -v[V3];)
    LL[k][EN] = 1.0;
  #elif EOS == ISOTHERMAL
   LL[k][DN] = 0.5*(1.0 - v[V1]/a);
   LL[k][M1] = 0.5/a; 
  #endif

/*       lambda = u       */

  #if EOS == IDEAL
   k = 2;
   LL[k][DN] = -2.0*H + 4.0*gt_1*a2; 
   EXPAND(LL[k][M1] = 2.0*v[V1];   ,
          LL[k][M2] = 2.0*v[V2];   ,
          LL[k][M3] = 2.0*v[V3];)
   LL[k][EN] = -2.0;

   #if COMPONENTS > 1
    k = 3;
    LL[k][DN] = -2.0*v[V2]*a2*gt_1; 
    LL[k][M2] = 2.0*a2*gt_1;    
    LL[k][EN] = 0.0;
   #endif

   #if COMPONENTS == 3
    k = 4; 
    LL[k][DN] = -2.0*v[V3]*a2*gt_1; 
    LL[k][M3] = 2.0*a2*gt_1;
   #endif

   for (k = 0; k < NFLX; k++){   /* normalization */
   for (nv = 0; nv < NFLX; nv++){
     LL[k][nv] *= (gmm - 1.0)/(2.0*a2);
   }}

  #elif EOS == ISOTHERMAL
   EXPAND(                                              ,
          k = 2; LL[k][DN] = -v[V2]; LL[k][V2] =  1.0;  ,
          k = 3; LL[k][DN] = -v[V3]; LL[k][V3] =  1.0; )
  #endif

/* -----------------------------------------
         Check eigenvectors consistency
   ----------------------------------------- */

#if CHECK_EIGENVECTORS == YES
{
  static double **A, **ALR;
  double dA, vel2, Bmag2, vB;

  if (A == NULL){
    A   = Array_2D(NFLX, NFLX, double);
    ALR = Array_2D(NFLX, NFLX, double);
  }

 /* --------------------------------------
     Construct the Jacobian analytically
    -------------------------------------- */

  for (i = 0; i < NFLX; i++){
  for (j = 0; j < NFLX; j++){
    A[i][j] = ALR[i][j] = 0.0;
  }}

  vel2  = v[V1]*v[V1] + v[V2]*v[V2] + v[V3]*v[V3];
  #if EOS == IDEAL
   A[DN][M1] = 1.0;

   A[M1][DN] = -v[V1]*v[V1] + (gmm-1.0)*vel2*0.5;  
   A[M1][M1] = (3.0 - gmm)*v[V1];
   A[M1][M2] = (1.0 - gmm)*v[V2];
   A[M1][M3] = (1.0 - gmm)*v[V3];
   A[M1][EN] = gmm - 1.0;

   A[M2][DN] = - v[V1]*v[V2];
   A[M2][M1] =   v[V2];
   A[M2][M2] =   v[V1];

   A[M3][DN] = - v[V1]*v[V3];
   A[M3][M1] =   v[V3];
   A[M3][M3] =   v[V1];

   A[EN][DN] =  0.5*(gmm - 1.0)*vel2*v[V1] - (u[EN] + v[PR])/v[DN]*v[V1];

   A[EN][M1] =  (1.0 - gmm)*v[V1]*v[V1] + (u[EN] + v[PR])/v[DN];

   A[EN][M2] = (1.0 - gmm)*v[V1]*v[V2];
   A[EN][M3] = (1.0 - gmm)*v[V1]*v[V3];

   A[EN][EN] = gmm*v[V1];
  #elif EOS == ISOTHERMAL
   A[DN][M1] = 1.0;

   A[M1][DN] = -v[V1]*v[V1] + C_ISO2;
   A[M1][M1] = 2.0*v[V1];

   A[M2][DN] = - v[V1]*v[V2];
   A[M2][M1] =   v[V2];
   A[M2][M2] =   v[V1];

   A[M3][DN] = - v[V1]*v[V3];
   A[M3][M1] =   v[V3];
   A[M3][M3] =   v[V1];
  #endif

  for (i = 0; i < NFLX; i++){
  for (j = 0; j < NFLX; j++){
    ALR[i][j] = 0.0;
    for (k = 0; k < NFLX; k++){
      ALR[i][j] += RR[i][k]*lambda[k]*LL[k][j];
    }
  }}

  for (i = 0; i < NFLX; i++){
  for (j = 0; j < NFLX; j++){
    if (fabs(ALR[i][j] - A[i][j]) > 1.e-6){
      print ("! CONS_EIGENVECTORS: eigenvectors not consistent\n");
      print ("! A[%d][%d] = %16.9e, ALR[%d][%d] = %16.9e\n",
                i,j, A[i][j], i,j,ALR[i][j]);
      print ("\n\n A   = \n"); SHOWMATRIX(A, 1.e-8);
      print ("\n\n ARL = \n"); SHOWMATRIX(ALR, 1.e-8);
      
      print ("\n\n RR   = \n"); SHOWMATRIX(RR, 1.e-8);
      print ("\n\n LL   = \n"); SHOWMATRIX(LL, 1.e-8);
      QUIT_ORION2(1);
    }
  }}

/* -- check orthornomality -- */

  for (i = 0; i < NFLX; i++){
  for (j = 0; j < NFLX; j++){
    dA = 0.0;
    for (k = 0; k < NFLX; k++) dA += LL[i][k]*RR[k][j];
    if ( (i == j && fabs(dA-1.0) > 1.e-8) || 
         (i != j && fabs(dA)>1.e-8) ) {
      print ("! CONS_EIGENVECTORS: Eigenvectors not orthogonal\n");
      print ("!   i,j = %d, %d  %12.6e \n",i,j,dA);
      print ("!   DIR: %d\n",DIR);
      QUIT_ORION2(1);
    }
  }}
}
#endif
}
/* ********************************************************** */
void LpPROJECT (double **L, double *v, double *w)
/*
 *
 *  Compute the matrix-vector product between the
 *  the L matrix (containing primitive left eigenvectors) 
 *  and the vector v. 
 *  For efficiency purpose, multiplication is done 
 *  explicitly, so that only nonzero entries
 *  of the left primitive eigenvectors are considered.
 *  
 *
 ************************************************************ */
{
  int nv;

  #if EOS == IDEAL
   w[0] = L[0][V1]*v[V1] + L[0][PR]*v[PR];
   w[1] = L[1][V1]*v[V1] + L[1][PR]*v[PR];
   EXPAND( w[2] = v[DN] + L[2][PR]*v[PR];  ,
           w[3] = v[V2];                   ,
           w[4] = v[V3];)
  #elif EOS == ISOTHERMAL
    w[0] = L[0][DN]*v[DN] + L[0][V1]*v[V1];
    w[1] = L[1][DN]*v[DN] + L[1][V1]*v[V1];
    EXPAND(                     ,
            w[2] = v[V2];       ,
            w[3] = v[V3];)
  #endif

  /* ----------------------------------------------- 
       For passive scalars, the characteristic 
       variable is equal to the primitive one, 
       since  l = r = (0,..., 1 , 0 ,....)
     ----------------------------------------------- */		   

  #if NFLX != NVAR
   for (nv = NFLX; nv < NVAR; nv++){
     w[nv] = v[nv];
   }
  #endif   
}
