#include "orion2.h"

/* *********************************************************************** */
void PRIM_RHS (real *w, real *dw, real cs2, real h, real *Adw)
/*
 *
 *  HD vector for the primitive equations
 *
 *
 * -----------------------------------------------------------
 *  Reference paper:
 *
 * "A solution adaptive upwind scheme for ideal MHD"
 *
 *     K. G. Powell, P. L. Roe, and T. J. Linde
 *  Journal of Computational Physics, 154, 284-309, (1999).
 * -----------------------------------------------------------
 *
 *  without the magnetic field, of course.
 *
 *********************************************************************** */
{
  int  nv;
  real u;

  u   = w[V1];
  
  Adw[DN] = u*dw[DN] + w[DN]*dw[V1];
  #if EOS == IDEAL
   EXPAND(Adw[V1] = u*dw[V1] + dw[PR]/w[DN];  ,
          Adw[V2] = u*dw[V2];                 ,
          Adw[V3] = u*dw[V3];)
   Adw[PR] = cs2*w[DN]*dw[V1] + u*dw[PR];
  #elif EOS == ISOTHERMAL
   EXPAND(Adw[V1] = u*dw[V1] + C_ISO2*dw[DN]/w[DN];  ,
          Adw[V2] = u*dw[V2];                        ,
          Adw[V3] = u*dw[V3];)
  #endif

 /* ---- scalars  ---- */
     			    
  for (nv = NFLX; nv < NVAR; nv++) Adw[nv] = u*dw[nv];
}

/* **************************************************************************  */
void PRIM_SOURCE (const State_1D *state, int beg, int end,
                  real *a2, real *h, real **src, Grid *grid)
/*
 *
 *
 *     Add source terms for primitive-variable-based 
 *     time evolutions;
 *     Source terms include:
 *
 *      - Geometrical sources
 *      - Gravity
 *
 *  Only include components along the same sweep
 *  directions. If 2.5 dim are used, the third
 *  component is included in the r-sweep.
 *  
 **************************************************************************** */
{
  int nv, i;
  real x1, x2, x3;
  real vth, vphi, dA_dV, scrh;
  static real **g;
  real *v, *u, r_inv, ct;

  if (g == NULL) g = Array_2D(NMAX_POINT, 3, double);

  for (i = beg; i <= end; i++){
  for (nv = NVAR; nv--;   ) {
    src[i][nv] = 0.0;
  }}

/* --------------------------------------------------
      1 - Geometrical Source Terms 
   -------------------------------------------------- */

  #if GEOMETRY == CYLINDRICAL

   if (DIR == IDIR) {
     for (i = beg; i <= end; i++){

       v = state->v[i];
       u = state->u[i];

       dA_dV = 1.0/grid[DIR].x[i];

       src[i][DN] = -u[M1]*dA_dV;
       #if EOS == IDEAL
        src[i][PR] = src[i][DN]*a2[i];
       #endif
      
       #if COMPONENTS == 3
        src[i][iVR]   =  v[iVPHI]*v[iVPHI]*dA_dV;   
        src[i][iVPHI] = -v[iVR]  *v[iVPHI]*dA_dV;
       #endif
   
     }
   }
 
  #elif GEOMETRY == POLAR

   if (DIR == IDIR) {
     for (i = beg; i <= end; i++){

       v = state->v[i];
       u = state->u[i];

       dA_dV = 1.0/grid[DIR].x[i];

       src[i][DN] = -u[M1]*dA_dV;
       #if EOS == IDEAL
        src[i][PR] = src[i][DN]*a2[i];
       #endif
       src[i][iVR] = v[iVPHI]*v[iVPHI]*dA_dV;   
   
     }

   }else if (DIR == JDIR) {

     dA_dV = 1.0/fabs(grid[IDIR].x[*NX_PT]);
     for (i = beg; i <= end; i++){
       v = state->v[i];
       src[i][iVPHI] = -v[iVR]*v[iVPHI]*dA_dV;      
     }
   }
   
  #elif GEOMETRY == SPHERICAL

   if (DIR == IDIR) {

     ct = 1.0/tan(grid[JDIR].x[*NY_PT]);
     for (i = beg; i <= end; i++){

       v = state->v[i];
       u = state->u[i];

       dA_dV = 2.0/grid[DIR].x[i];
       r_inv = 0.5*dA_dV;

       src[i][DN] = -u[M1]*dA_dV;
       #if EOS == IDEAL
        src[i][PR] = src[i][DN]*a2[i];
       #endif

       #if COMPONENTS >= 2
        vth = v[iVTH];
       #else 
        vth = 0.0;
       #endif

       #if COMPONENTS == 3
        vphi = v[iVPHI];
       #else 
        vphi = 0.0;
       #endif

       EXPAND(src[i][iVR]   = (vth*vth + vphi*vphi)*r_inv;     ,
              src[i][iVTH]  = 0.0;                             ,
              src[i][iVPHI] = -vphi*(v[iVR] + vth*ct)*r_inv;)
    } 

  }else if (DIR == JDIR) {

     r_inv = 1.0/grid[IDIR].x[*NX_PT];
     for (i = beg; i <= end; i++){

       v = state->v[i];
       u = state->u[i];

       ct    = 1.0/tan(grid[JDIR].x[i]);
       dA_dV = ct*r_inv;

       src[i][DN] = -u[M1]*dA_dV;
       #if EOS == IDEAL
        src[i][PR] = src[i][DN]*a2[i];
       #endif
      
       #if COMPONENTS == 3
        vphi = v[iVPHI];
       #else 
        vphi = 0.0;
       #endif

       EXPAND(src[i][iVR]   = 0.0;                                  ,
              src[i][iVTH]  = (-v[iVR]*v[iVTH] + vphi*vphi*ct)*r_inv;  ,
              src[i][iVPHI] = 0.0;)   
     }
   }
 
  #endif

/* -----------------------------------------------------------
               2 - Include Body Force
   ----------------------------------------------------------- */

  #if INCLUDE_BODY_FORCE == EXPLICIT
  {
    Force *f;
    f  = ACCELERATION (state->v, DIR, beg, end, grid);
    for (i = beg; i <= end; i++){
      src[i][V1] += f->a[i];
    }

    #if DIMENSIONS == 2 && COMPONENTS == 3
     if (DIR == JDIR){
       f = ACCELERATION (state->v, KDIR, beg, end, grid);
       for (i = beg; i <= end; i++) {
         src[i][VZ] += f->a[i];
       }
     } 
    #endif
  }
  #endif

/* -----------------------------------------------------------
               FARGO source terms
   ----------------------------------------------------------- */
  
  #ifdef FARGO
  #ifndef SHEARINGBOX
   double **wA, *dx, *dz;
   wA = FARGO_GET_VEL();
   if (DIR == IDIR){
     dx = grid[IDIR].dx;
     for (i = beg; i <= end; i++){
       v = state->v[i];
       src[i][VY] -= 0.5*v[VX]*(wA[*NZ_PT][i+1] - wA[*NZ_PT][i-1])/dx[i];
     }
   }else if (DIR == KDIR){
     dz = grid[KDIR].dx;
     for (i = beg; i <= end; i++){
       v = state->v[i];
       src[i][VY] -= 0.5*v[VZ]*(wA[i+1][*NX_PT] - wA[i-1][*NX_PT])/dz[i];
     }
   }
  #endif
  #endif
}
