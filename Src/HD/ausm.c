#include"orion2.h"

/* ********************************************************************** */
void AUSMp (const State_1D *state, int beg, int end, 
            real *cmax, Grid *grid)
/*
 *
 * PURPOSE
 *
 *   - Solve riemann problem for the Euler equations using the AUSM+ 
 *     scheme given in 
 *
 *  "A Sequel to AUSM: AUSM+"
 *  Liou, M.S., JCP (1996), 129, 364
 *
 * LAST_MODIFIED
 *
 *   July 17th 2006, by Andrea Mignone  (mignone@to.astro.it)
 *
 *
 ************************************************************************ */
{
#if EOS == IDEAL
  int  i, nv;
  real aL, ML, MpL, PpL, asL2, asL, atL;
  real aR, MR, MmR, PmR, asR2, asR, atR;
  real a, m, p, mp, mm;
  real *vL, *vR, *uL, *uR;
  real alpha = 3.0/16.0, beta = 0.125;
  static real  **fl, **fr, **ul, **ur;

  if (fl == NULL){
    fl = Array_2D(NMAX_POINT, NFLX, double);
    fr = Array_2D(NMAX_POINT, NFLX, double);
    ul = Array_2D(NMAX_POINT, NVAR, double);
    ur = Array_2D(NMAX_POINT, NVAR, double);
  }

  PRIMTOCON (state->vL, ul, beg, end);
  PRIMTOCON (state->vR, ur, beg, end);

  for (i = beg; i <= end; i++)  {

    vL = state->vL[i];
    vR = state->vR[i];

    uL = ul[i];
    uR = ur[i];

    aL = sqrt(gmm*vL[PR]/vL[DN]);
    aR = sqrt(gmm*vR[PR]/vR[DN]);

    asL2  = EXPAND(vL[VX]*vL[VX], + vL[VY]*vL[VY], + vL[VZ]*vL[VZ]);
    asL2  = aL*aL/(gmm - 1.0) + 0.5*asL2;
    asL2 *= 2.0*(gmm - 1.0)/(gmm + 1.0);

    asR2  = EXPAND(vR[VX]*vR[VX], + vR[VY]*vR[VY], + vR[VZ]*vR[VZ]);
    asR2  = aR*aR/(gmm - 1.0) + 0.5*asR2;
    asR2 *= 2.0*(gmm - 1.0)/(gmm + 1.0);

    asL = sqrt(asL2);
    asR = sqrt(asR2);

    atL = asL2/dmax(asL, fabs(vL[V1]));
    atR = asR2/dmax(asR, fabs(vR[V1]));

    a = dmin(atL, atR);
/*
    a = 0.5*(aL + aR);
*/
  /* --------------------------------------------
          define split Mach numbers  
          define pressure terms
     -------------------------------------------- */

    ML = vL[V1]/a;
    if ( fabs(ML) >= 1.0){
      MpL = 0.5*(ML  + fabs(ML));
      PpL = ML > 0.0 ? 1.0:0.0;
    }else{
      MpL = 0.25*(ML + 1.0)*(ML + 1.0) + beta*(ML*ML - 1.0)*(ML*ML - 1.0);
      PpL = 0.25*(ML + 1.0)*(ML + 1.0)*(2.0 - ML) + alpha*ML*(ML*ML - 1.0)*(ML*ML - 1.0);
    }

    MR = vR[V1]/a;
    if ( fabs(MR) >= 1.0){
      MmR = 0.5*(MR  - fabs(MR));
      PmR = MR > 0.0 ? 0.0:1.0;
    }else{
      MmR = - 0.25*(MR - 1.0)*(MR - 1.0) - beta*(MR*MR - 1.0)*(MR*MR - 1.0);
      PmR =   0.25*(MR - 1.0)*(MR - 1.0)*(2.0 + MR) - alpha*MR*(MR*MR - 1.0)*(MR*MR - 1.0);
    }

    m = MpL + MmR;

    mp = 0.5*(m + fabs(m));
    mm = 0.5*(m - fabs(m));

  /* -------------------------------------------------------------
                     Compute fluxes 
     ------------------------------------------------------------- */

    state->press[i] = PpL*vL[PR] + PmR*vR[PR];
    state->flux[i][DN]        = a*(mp*uL[DN] + mm*uR[DN]);
    EXPAND(state->flux[i][MX] = a*(mp*uL[MX] + mm*uR[MX]); ,
           state->flux[i][MY] = a*(mp*uL[MY] + mm*uR[MY]); ,
           state->flux[i][MZ] = a*(mp*uL[MZ] + mm*uR[MZ]); )
    state->flux[i][EN]        = a*(mp*(uL[EN] + vL[PR]) + mm*(uR[EN] + vR[PR]));
 
  /*  ----  get max eigenvalue  ----  */

    cmax[i] = dmax(fabs(vL[V1]) + aL, fabs(vR[V1]) + aR);

    MAX_MACH_NUMBER = dmax(fabs(ML), MAX_MACH_NUMBER);
    MAX_MACH_NUMBER = dmax(fabs(MR), MAX_MACH_NUMBER);

  }
#endif /* EOS == IDEAL */
}
