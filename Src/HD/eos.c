#include "orion2.h"

/* **************************************************************** */
void SOUND_SPEED2  (real *u[], real *cs2, real *h, 
                    int beg, int end)
/*
 *
 *    Define the square of the sound speed for different EOS
 *
 ****************************************************************** */
{
  int   i;
  real  a2, theta;

/*
  ENTHALPY(u, h, is, ie);
*/
  for (i = beg; i <= end; i++) {
    #if EOS == IDEAL
     cs2[i] = gmm*u[i][PR]/u[i][DN];
    #elif EOS == ISOTHERMAL
     cs2[i] = C_ISO2;
    #endif
  }
}


/* *************************************************************** */
void ENTHALPY (real **uprim, real *h, int beg, int end)
/*
 *
 *
 *
 ***************************************************************** */
{
  int i;
  double gmmr;

  #if EOS == IDEAL
   gmmr = gmm/(gmm - 1.0);
   for (i = beg; i <= end; i++){
     h[i] = gmmr*uprim[i][PR]/uprim[i][DN];
   }
  #elif EOS == ISOTHERMAL 
   print (" Enthalpy not defined for isothermal EoS\n");
   QUIT_ORION2(1);
  #endif
}
