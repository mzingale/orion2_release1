#include "orion2.h"

/* *********************************************************************  */
void MOM_SOURCE (const State_1D *state, int beg, int end,
                 double dt, Grid *grid)
/* 
 *
 * PURPOSE
 *
 *   add geometrical source terms to the momentum equation
 *   in curvilinear coordinates.
 *
 * ARGUMENTS
 *
 *   state   :  a pointer to a state structure
 *   beg, end:  initial and final points of the grid
 *   dt      :  time step
 *   grid    :  grid structure.
 * 
 *
 * LAST MODIFIED
 *
 *   Sept 5th, 2011 by A. Mignone (mignone@ph.unito.it)
 *
 *********************************************************************** */
{
  int  i;
  double *u, *v, **rhs;
  double dt_r, ct, r_1;
  #ifdef FARGO
   double vphi, **wA;
   wA = FARGO_GET_VEL();
  #endif

  rhs = state->rhs;
  if (DIR == IDIR){
    for (i = beg; i <= end; i++){
      v    = state->vh[i]; u = state->uh[i];
      r_1  = grid[IDIR].r_1[i];
/*       dt_r = dt*grid[IDIR].r_1[i]; log files seem to change with this */

      #if GEOMETRY == CYLINDRICAL && COMPONENTS == 3

        rhs[i][MX] += dt*u[iMPHI]*v[iVPHI]*r_1;   /* = dt * T_phi_phi/r */

      #elif GEOMETRY == POLAR && COMPONENTS >= 2    

       #ifdef FARGO 
        vphi = v[iVPHI] + wA[*NZ_PT][i];
        rhs[i][MX] += dt*u[DN]*vphi*vphi*r_1;
       #else
        rhs[i][MX] += dt*u[iMPHI]*v[iVPHI]*r_1;
       #endif

      #elif GEOMETRY == SPHERICAL   /* T_\theta_\theta + T_\phi_\phi */

       #ifdef FARGO
        vphi = v[iVPHI] + wA[*NY_PT][i];
        rhs[i][MX] += dt*(EXPAND(0.0, + u[MY]*v[VY], + u[DN]*vphi*vphi))*r_1;
       #else
        rhs[i][MX] += dt*(EXPAND(0.0, + u[MY]*v[VY], + u[MZ]*v[VZ]))*r_1;
       #endif

      #endif
    }  

  }else if (DIR == JDIR) {

    dt_r = dt*grid[IDIR].r_1[*NX_PT];
    r_1  = grid[IDIR].r_1[*NX_PT];
    for (i = beg; i <= end; i++){
      v = state->vh[i]; u = state->uh[i];
      ct = grid[JDIR].ct[i];
      #if GEOMETRY == SPHERICAL  /* -T_\theta_r + cot(theta)*T_\phi_\phi */

       #ifdef FARGO
        vphi = v[iVPHI] + wA[i][*NX_PT];
        rhs[i][MY] += dt*(EXPAND(0.0, - u[MY]*v[VX], + ct*u[DN]*vphi*vphi))*r_1;
       #else
        rhs[i][MY] += dt*(EXPAND(0.0, - u[MY]*v[VX], + ct*u[MZ]*v[VZ]))*r_1;
       #endif

      #endif
    }  
  }
}
