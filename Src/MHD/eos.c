#include "orion2.h"

/* **************************************************************** */
void SOUND_SPEED2  (real *u[], real *cs2, real *h, 
                    int is, int ie)
/*
 *
 *    Define the square of the sound speed for different EOS
 *
 ****************************************************************** */
{
  int  i;
/*
  ENTHALPY(u, h, is, ie);
*/
  for (i = is ; i <= ie ; i++) {
    #if EOS == IDEAL
     cs2[i] = gmm*u[i][PR]/u[i][DN];
    #elif EOS == ISOTHERMAL
     cs2[i] = C_ISO2;
    #endif
  }
}

/* *************************************************************** */
void ENTHALPY (real **uprim, real *h, int is, int ie)
/*
 *
 *
 *
 ***************************************************************** */
{
  int i;
  double gmmr;

  #if EOS == IDEAL

   gmmr = gmm/(gmm - 1.0);
   for (i = is; i <= ie; i++){
     h[i] = gmmr*uprim[i][PR]/uprim[i][DN];
   }

  #elif EOS == ISOTHERMAL

   print (" Enthalpy not defined in isothermal MHD\n");
   QUIT_ORION2(1);

  #endif
}

/* *************************************************************** */
void ENTROPY (real **v, real *s, int is, int ie)
/*
 *
 *
 *
 ***************************************************************** */
{
  int i;
  double rho;

  #if EOS == IDEAL

   for (i = is; i <= ie; i++){
     rho  = v[i][DN];
     s[i] = v[i][PR]/pow(rho,gmm);
   }

  #elif EOS == ISOTHERMAL

   print (" Entropy not defined in isothermal MHD\n");
   QUIT_ORION2(1);

  #endif
}

