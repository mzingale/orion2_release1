#include"orion2.h"

#if EOS == IDEAL
/* ***************************************************************************** */
void HLLC_SOLVER (const State_1D *state, int beg, int end, 
                  real *cmax, Grid *grid)
/* 
 *
 *
 * NAME
 *
 *   HLLC_SOLVER
 *
 *
 * PURPOSE
 *
 *  - Solve riemann problem for the MHD equations using the 
 *    two-state HLLC Riemann solver
 * 
 *     Reference:   "An HLLC RIemann Solver for MHD" 
 *                  S. Li, 2005, JCP 203, 344
 *
 *     However, our differ from Li's solver in the way
 *     transverse momenta are computed.
 * 
 *   - On input, it takes left and right primitive state
 *     vectors state->vL and state->vR at zone edge i+1/2;
 *     On output, return flux and pressure vectors at the
 *     same interface.
 *
 *   - Also, compute maximum wave propagation speed (cmax) 
 *     for  explicit time step computation
 *  
 *
 * LAST_MODIFIED
 *
 *   April 4th 2006, by Andrea Mignone  (mignone@to.astro.it)
 *
 *
 ******************************************************************************** */
{
  int   nv, i;
  real  scrh;
  real  pl, pr;
  real  vBl, usl[NFLX];
  real  vBr, usr[NFLX];

  real  Bxs, Bys, Bzs, ps, vBs;
  real  vxl, vxr, vxs, vys, vzs;
  real  Fhll[NFLX], alpha_l, alpha_r;
  real  **bgf, *vL, *vR, *uL, *uR, *SL, *SR;
  static real **fL, **fR, **Uhll;
  static double **VL, **VR, **UL, **UR;
  static real *pL, *pR;

  if (fL == NULL){
    fL = Array_2D(NMAX_POINT, NFLX, double);
    fR = Array_2D(NMAX_POINT, NFLX, double);
    Uhll = Array_2D(NMAX_POINT, NFLX, double);
    pL  = Array_1D(NMAX_POINT, double);
    pR  = Array_1D(NMAX_POINT, double);
    #ifdef GLM_MHD
     VL = Array_2D(NMAX_POINT, NVAR, double);
     VR = Array_2D(NMAX_POINT, NVAR, double);
     UL = Array_2D(NMAX_POINT, NVAR, double);
     UR = Array_2D(NMAX_POINT, NVAR, double);
    #endif
  }
  
  #if INCLUDE_BACKGROUND_FIELD == YES
   print (" ! Background field splitting not allowed with HLLC solver\n");
   QUIT_ORION2(1);
  #endif
  
  #ifdef GLM_MHD
   GLM_2X2_SOLVE (state, VL, VR, beg, end, grid);
   PRIMTOCON (VL, UL, beg, end);
   PRIMTOCON (VR, UR, beg, end);
  #else
   VL = state->vL; UL = state->uL;
   VR = state->vR; UR = state->uR;
  #endif

  FLUX  (UL, VL, bgf, fL, pL, beg, end);
  FLUX  (UR, VR, bgf, fR, pR, beg, end);

  /* ----------------------------------------
       get max and min signal velocities
     ---------------------------------------- */
             
  SL = state->SL; SR = state->SR;
  HLL_SPEED (VL, VR, bgf, SL, SR, beg, end);

  for (i = beg; i <= end; i++) {
    
    scrh  = dmax(fabs(SL[i]), fabs(SR[i]));
    *cmax = dmax(*cmax, scrh);

/* --------------------------------------------
              compute fluxes 
   -------------------------------------------- */

    if (SL[i] >= 0.0){
    
      for (nv = 0; nv < NFLX; nv++) {
        state->flux[i][nv] = fL[i][nv];
      }
      state->press[i] = pL[i];

    }else if (SR[i] <= 0.0){

      for (nv = 0; nv < NFLX; nv++) {
        state->flux[i][nv] = fR[i][nv];
      }
      state->press[i] = pR[i];

    }else{

      vL = VL[i]; uL = UL[i];
      vR = VR[i]; uR = UR[i];

  /* ----  define hll states  ----  */

      scrh = 1.0/(SR[i] - SL[i]);
      for (nv = 0; nv < NFLX; nv++){  
        Uhll[i][nv] =   SR[i]*uR[nv] - SL[i]*uL[nv] 
                      + fL[i][nv] - fR[i][nv];
        Uhll[i][nv] *= scrh;
  
        Fhll[nv]  = SL[i]*SR[i]*(uR[nv] - uL[nv])
                   + SR[i]*fL[i][nv] - SL[i]*fR[i][nv];
        Fhll[nv] *= scrh;
      }
      Uhll[i][M1] += (pL[i] - pR[i])*scrh;
      Fhll[M1] += (SR[i]*pL[i] - SL[i]*pR[i])*scrh;

      #if SHOCK_FLATTENING == MULTID   

      /* ---------------------------------------------
         Switch to HLL in proximity of strong shocks.
        --------------------------------------------- */

       if (CHECK_ZONE(i, FLAG_HLL) || CHECK_ZONE(i+1, FLAG_HLL)){
         for (nv = NFLX; nv--; ){
           state->flux[i][nv]  = SL[i]*SR[i]*(uR[nv] - uL[nv])
                              +  SR[i]*fL[i][nv] - SL[i]*fR[i][nv];
           state->flux[i][nv] *= scrh;
         }
         state->press[i] = (SR[i]*pL[i] - SL[i]*pR[i])*scrh;
         continue;
       }
      #endif

   /* ---- define total pressure, vB in left and right states ---- */

      pl = EXPAND(vL[BX]*vL[BX], + vL[BY]*vL[BY], + vL[BZ]*vL[BZ]);
      pr = EXPAND(vR[BX]*vR[BX], + vR[BY]*vR[BY], + vR[BZ]*vR[BZ]);

      pl = vL[PR] + 0.5*pl;  
      pr = vR[PR] + 0.5*pr;

      vBl = EXPAND(vL[VX]*vL[BX], + vL[VY]*vL[BY], + vL[VZ]*vL[BZ]);
      vBr = EXPAND(vR[VX]*vR[BX], + vR[VY]*vR[BY], + vR[VZ]*vR[BZ]);

      vxl = vL[V1]; 
      vxr = vR[V1];

   /* ----  magnetic field ---- */

      EXPAND(Bxs = Uhll[i][B1];  ,
             Bys = Uhll[i][B2];  ,
             Bzs = Uhll[i][B3];)

   /* ---- normal velocity vx  ----  */

      vxs = Uhll[i][M1]/Uhll[i][DN];
      ps  = Fhll[M1] + Bxs*Bxs - Fhll[DN]*vxs;
/*
        ps = vL[DN]*(SL[i] - vxl)*(vxs - vxl) + pl - vL[B1]*vL[B1] + Bxs*Bxs; 
*/
      vBs = EXPAND(Uhll[i][BX]*Uhll[i][MX], + 
                   Uhll[i][BY]*Uhll[i][MY], + 
                   Uhll[i][BZ]*Uhll[i][MZ]);

      vBs /= Uhll[i][DN];

      usl[DN] = uL[DN]*(SL[i] - vxl)/(SL[i] - vxs);
      usr[DN] = uR[DN]*(SR[i] - vxr)/(SR[i] - vxs);

      usl[EN] = (uL[EN]*(SL[i] - vxl) + 
                 ps*vxs - pl*vxl - Bxs*vBs + vL[B1]*vBl)/(SL[i] - vxs);
      usr[EN] = (uR[EN]*(SR[i] - vxr) + 
                 ps*vxs - pr*vxr - Bxs*vBs + vR[B1]*vBr)/(SR[i] - vxs);

      EXPAND(usl[M1] = usl[DN]*vxs;
             usr[M1] = usr[DN]*vxs;        ,

             usl[M2] = (uL[M2]*(SL[i] - vxl) - (Bxs*Bys - vL[B1]*vL[B2]))/(SL[i] - vxs);
             usr[M2] = (uR[M2]*(SR[i] - vxr) - (Bxs*Bys - vR[B1]*vR[B2]))/(SR[i] - vxs); ,

             usl[M3] = (uL[M3]*(SL[i] - vxl) - (Bxs*Bzs - vL[B1]*vL[B3]))/(SL[i] - vxs);
             usr[M3] = (uR[M3]*(SR[i] - vxr) - (Bxs*Bzs - vR[B1]*vR[B3]))/(SR[i] - vxs);)

      EXPAND(usl[B1] = usr[B1] = Bxs;   ,
             usl[B2] = usr[B2] = Bys;   ,
             usl[B3] = usr[B3] = Bzs;)

      #ifdef GLM_MHD
       usl[PSI_GLM] = usr[PSI_GLM] = vL[PSI_GLM];
      #endif

      if (vxs >= 0.0){
        for (nv = 0; nv < NFLX; nv++) {
          state->flux[i][nv] = fL[i][nv] + SL[i]*(usl[nv] - uL[nv]);
        }
        state->press[i] = pL[i];
      } else {
        for (nv = 0; nv < NFLX; nv++) {
          state->flux[i][nv] = fR[i][nv] + SR[i]*(usr[nv] - uR[nv]);
        }
        state->press[i] = pR[i];
      }
    }
  }

/* -----------------------------------------------------
               initialize source term
   ----------------------------------------------------- */

  #if MHD_FORMULATION == EIGHT_WAVES
/*
   ROE_DIVB_SOURCE (state, beg, end, grid);
*/
/*
   for (i = beg; i <= end; i++) {
     uR = state->uR[i]; uL = state->uL[i];
     scrh = 1.0 / (SR[i] - SL[i]);
     for (nv = 0; nv < NFLX; nv++) {
       Uhll[i][nv] = SR[i]*uR[nv] - SL[i]*uL[nv] +
                     fL[i][nv] - fR[i][nv];
       Uhll[i][nv] *= scrh;
     }
     Uhll[i][M1] += (pL[i] - pR[i])*scrh;
   }
*/
   HLL_DIVB_SOURCE (state, Uhll, beg + 1, end, grid);
  #endif
}
#elif EOS == ISOTHERMAL 

/* ************************************************************* */
void HLLC_SOLVER (const State_1D *state, int beg, int end, 
                  real *cmax, Grid *grid)
/*
 *
 *
 *
 *************************************************************** */
{
  print1 ("! HLLC solver not implemented for Isothermal EOS\n");
  print1 ("! Use hll or hlld instead.\n");
  QUIT_ORION2(1);
}

#endif /* end #if on EOS */


