/* ###############################################################

         SET LABELS FOR CONSERVATIVE, PRIMITIVE VARIABLES
         AND SOLVERS  FOR THE MHD MODULE

     Pressure and energy labels are the last ones.

   ############################################################### */

                 /*   ONE-COMPONENT VECTOR   */

#if COMPONENTS == 1

 #define  DN      0
 #define  VX      1
 #define  BX      2
 #define  PR      3

 #define  MX      VX
 #define  EN      PR

/*   Non used pointers are kept for convenience, although 
     they're never used, they point to the last element of 
     the array stored by startup.c                  */

 #define VY   255
 #define MY   255
 #define VZ   255
 #define MZ   255
 #define BY   255
 #define BZ   255

#endif

                     /*   TWO-COMPONENT VECTOR   */

#if COMPONENTS == 2

 #define  DN      0
 #define  VX      1
 #define  VY      2
 #define  BX      3
 #define  BY      4
 #define  PR      5

 #define  MX      VX
 #define  MY      VY
 #define  EN      PR

 #define VZ   255
 #define MZ   255
 #define BZ   255

#endif

                    /*   THREE-COMPONENT VECTOR   */

#if COMPONENTS == 3

 #define  DN      0
 #define  VX      1
 #define  VY      2
 #define  VZ      3
 #define  BX      4
 #define  BY      5
 #define  BZ      6
 #define  PR      7

 #define  MX      VX
 #define  MY      VY
 #define  MZ      VZ
 #define  EN      PR

#endif

/* -- add the PSI_GLM label if necessary -- */

#if MHD_FORMULATION == DIV_CLEANING 

 #if EOS == IDEAL
  #define PSI_GLM (2*COMPONENTS + 2)
 #elif EOS == ISOTHERMAL
  #define PSI_GLM (2*COMPONENTS + 1)
 #endif

#endif


#define NFLX (2*COMPONENTS + (EOS == IDEAL ? 2:1) + (MHD_FORMULATION == DIV_CLEANING))

/*
#define KFASTM 0 
#define KFASTP 1 
#define KENTRP (KFASTP + (EOS == IDEAL))
#define KDIVB  (KENTRP + (MHD_FORMULATION != DIV_CLEANING))
#define KSLOWM (KDIVB  + (COMPONENTS >= 2))
#define KSLOWP (KSLOWM + (COMPONENTS >= 2))
#define KALFVM (KSLOWP + (COMPONENTS == 3))
#define KALFVP (KALFVM + (COMPONENTS == 3))
#if MHD_FORMULATION == DIV_CLEANING
 #define KPSI_GLMM (KALFVP + 1)
 #define KPSI_GLMP (KPSI_GLMM  + 1)
#endif
*/

enum KWAVES {
 KFASTM, KFASTP

 #if EOS == IDEAL 
  , KENTRP
 #endif

 #if MHD_FORMULATION != DIV_CLEANING
  , KDIVB
 #endif

 #if COMPONENTS >= 2
  , KSLOWM, KSLOWP
  #if COMPONENTS == 3
   , KALFVM, KALFVP
  #endif
 #endif

 #if MHD_FORMULATION == DIV_CLEANING
  , KPSI_GLMM, KPSI_GLMP 
 #endif
};


#if EOS == IDEAL

/* -------------------
    labels for waves
   ------------------- */
/*
 #define KFASTM   0
 #define KFASTP   1
 #define KENTRP   2
 #define KDIVB    3
 #define KSLOWM   4
 #define KSLOWP   5
 #define KALFVM   6
 #define KALFVP   7

  #define KPSI_GLMP 3
  #if COMPONENTS == 2  
   #define KPSI_GLMM 6
  #elif COMPONENTS == 3
   #define KPSI_GLMM 8
  #endif
*/

#elif EOS == ISOTHERMAL

 #undef EN
 #undef PR
 #define C_ISO2 (C_ISO*C_ISO)

/* -------------------
    labels for waves
   ------------------- */
/*
 #define KFASTM   0
 #define KFASTP   1
 #define KDIVB    2
 #define KSLOWM   3
 #define KSLOWP   4
 #define KALFVM   5
 #define KALFVP   6
*/
#endif

/*   Vector potential: these labels are and MUST only
     be used in the STARTUP / INIT  functions;
     they're convenient in obtaining a discretization 
     that preserve divB since the beginning   */

#define   AX  (NVAR + 1)
#define   AY  (NVAR + 2)
#define   AZ  (NVAR + 3)



/*       Now define more convenient and user-friendly 
         pointer labels for geometry setting      */

#if GEOMETRY == CYLINDRICAL 

 #define iVR    VX
 #define iVZ    VY
 #define iVPHI  VZ

 #define iMR    MX
 #define iMZ    MY
 #define iMPHI  MZ

 #define iBR    BX
 #define iBZ    BY
 #define iBPHI  BZ

#endif

#if GEOMETRY == POLAR 

 #define iVR    VX
 #define iVPHI  VY
 #define iVZ    VZ

 #define iMR    MX
 #define iMPHI  MY
 #define iMZ    MZ

 #define iBR    BX
 #define iBPHI  BY
 #define iBZ    BZ

#endif

#if GEOMETRY == SPHERICAL 

 #define iVR     VX
 #define iVTH    VY
 #define iVPHI   VZ

 #define iMR    MX
 #define iMTH   MY
 #define iMPHI  MZ

 #define iBR    BX
 #define iBTH   BY
 #define iBPHI  BZ

#endif

/* ---------------------------------------------------------------------
                    Prototyping goes here          
   --------------------------------------------------------------------- */

void BACKGROUND_FIELD (real x1, real x2, real x3, real *B0);

#if INCLUDE_BACKGROUND_FIELD == YES
 real **GET_BACKGROUND_FIELD (int, int, int, Grid *);
#endif 

int  CONTOPRIM   (real **, real **, int , int, char *);
void EIGENV      (real *, real, real, real *, real **, real **);
void EIGENVALUES (double *, double *);
void C_EIGENV (real *u, real *v, real **Lc, real **Rc, real *lambda);

void ENTHALPY    (real **, real *, int, int );
void ENTROPY     (real **, real *, int, int );

void FLUX        (real **, real **, real **, real **, real *, int, int);
void HLL_SPEED (real **vL, real **vR, real **bgf, 
                real *SL, real *SR, int, int);
void MAX_CH_SPEED(real **, real *, real *, real **, int, int);
void PRIMTOCON   (real **, real **, int, int);
void PRIM_RHS    (real *, real *, real, real, real *);
void PRIM_SOURCE (const State_1D *, int, int, real *, real *,
                  real **, Grid *);
void SOUND_SPEED2  (real **, real *, real *, int, int);

void MOM_SOURCE   (const State_1D *, int, int, int, int, real *, Grid *);
void OMEGA_SOURCE (const State_1D *, int, int, int, int, real *, Grid *);


#if MHD_FORMULATION == EIGHT_WAVES

 void ROE_DIVB_SOURCE (const State_1D *, int, int, Grid *);
 void HLL_DIVB_SOURCE (const State_1D *, double **, int, int, Grid *);

#elif MHD_FORMULATION == DIV_CLEANING

 #include "MHD/GLM/glm.h"

#elif MHD_FORMULATION == FLUX_CT

 #include "MHD/FCT/fct.h"

#endif

Riemann_Solver HLL_SOLVER, HLLC_SOLVER, HLLD_SOLVER;
Riemann_Solver LF_SOLVER, MUSTA_FLUX, ROE_SOLVER;
Riemann_Solver WENO5_RF;

#if RESISTIVE_MHD != NO
 #include "Resistive_MHD/res.h"
#endif



