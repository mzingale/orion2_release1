#include"orion2.h"

#define VERIFY_CONSISTENCY_CONDITION NO

#if EOS == IDEAL
/* ***************************************************************************** */
void HLLD_SOLVER (const State_1D *state, int beg, int end, 
                  real *cmax, Grid *grid)
/* 
 *
 *
 * NAME
 *
 *   HLLD_SOLVER 
 *
 *
 * PURPOSE
 *
 *   Solve riemann problem for the MHD equations using the 
 *   four-state HLLD 
 * 
 *   Reference:    "A muLti-state HLL approximate Riemann Solver for 
 *                  ideal MHD", Miyoshi, T., Kusano, K., JCP 2005 
 *
 * ARGUMENTS
 *
 *   vL (IN)       1-D array of left-edge primitive values at i+1/2
 *   vR (IN)       1-D array of right-edge primitive values at i+1/2
 *   flux (OUT)    1-D array of numerical fluxes, not including pressuRe
 *   grid (IN)     Array of grids
 *   cmax(OUT)     Array of maximum characteristic speeds in this direction
 *
 * SWITCHES
 *
 *
 * LAST_MODIFIED
 *
 *   June 8, 2007 by Andrea Mignone  (mignone@to.astro.it)
 *              
 *
 ******************************************************************************** */
{
  int    nv, i;
  int    revert_to_hllc;
  real   scrh, Uhll[NFLX];

  real usL[NFLX], ussl[NFLX];
  real usR[NFLX], ussr[NFLX];
  
  real vsL, wsL, scrhL, S1L, sqrL, duL;
  real vsR, wsR, scrhR, S1R, sqrR, duR;
  real Bx, SM, sBx, pts;
  real vss, wss;
  real *vL, *vR, *uL, *uR, *SL, *SR;
  static real *ptL, *ptR;
  static real **fL, **fR;
  static double **VL, **VR, **UL, **UR;

  if (fL == NULL){
    fL  = Array_2D(NMAX_POINT, NFLX, double);
    fR  = Array_2D(NMAX_POINT, NFLX, double);

    ptR = Array_1D(NMAX_POINT, double);
    ptL = Array_1D(NMAX_POINT, double);
    #ifdef GLM_MHD
     VL = Array_2D(NMAX_POINT, NVAR, double);
     VR = Array_2D(NMAX_POINT, NVAR, double);
     UL = Array_2D(NMAX_POINT, NVAR, double);
     UR = Array_2D(NMAX_POINT, NVAR, double);
    #endif
  }

  #if INCLUDE_BACKGROUND_FIELD == YES
   print ("! Background field splitting not allowed with HLLD solver\n");
   QUIT_ORION2(1);
  #endif
  #if MHD_FORMULATION == EIGHT_WAVES
   print ("! hlld Riemann solver does not work with Powell's 8-wave\n");
   QUIT_ORION2(1);
  #endif
 
  #ifdef GLM_MHD
   GLM_2X2_SOLVE (state, VL, VR, beg, end, grid);
   PRIMTOCON (VL, UL, beg, end);
   PRIMTOCON (VR, UR, beg, end);
  #else
   VL = state->vL; UL = state->uL;
   VR = state->vR; UR = state->uR;
  #endif

  FLUX (UL, VL, NULL, fL, ptL, beg, end);
  FLUX (UR, VR, NULL, fR, ptR, beg, end);

  /* ----------------------------------------
       get max and min signal velocities
     ---------------------------------------- */
             
  SL = state->SL; SR = state->SR;
  HLL_SPEED (VL, VR, NULL, SL, SR, beg, end);

  for (i = beg; i <= end; i++) {
    
  /* ----------------------------------------
      get max propagation speed for dt comp.
     ---------------------------------------- */             

    scrh  = dmax(fabs(SL[i]), fabs(SR[i]));
    *cmax = dmax(*cmax, scrh);

    vL = VL[i]; uL = UL[i];
    vR = VR[i]; uR = UR[i];

/* ---------------------------------------------------------- 
                COMPUTE FLUXES and STATES
   ---------------------------------------------------------- */

    if (SL[i] >= 0.0){                     /*  ----  Region L  ---- */

      for (nv = NFLX; nv--; ) state->flux[i][nv] = fL[i][nv];
      state->press[i] = ptL[i];

    }else if (SR[i] <= 0.0) {              /*  ----  Region R  ---- */

      for (nv = NFLX; nv--; ) state->flux[i][nv] = fR[i][nv];
      state->press[i] = ptR[i];
 
    } else {

      #if SHOCK_FLATTENING == MULTID

      /* -- revert to HLL in proximity of strong shocks -- */

       if (CHECK_ZONE(i, FLAG_HLL) || CHECK_ZONE(i+1, FLAG_HLL)){
         scrh = 1.0/(SR[i] - SL[i]);
         for (nv = NFLX; nv--; ){
           state->flux[i][nv]  = SR[i]*SL[i]*(uR[nv] - uL[nv])
                              +  SR[i]*fL[i][nv] - SL[i]*fR[i][nv];
           state->flux[i][nv] *= scrh;
         }
         state->press[i] = (SR[i]*ptL[i] - SL[i]*ptR[i])*scrh;
         continue;
       }
      #endif

    /* ---------------------------
              Compute U*  
       --------------------------- */

      scrh = 1.0/(SR[i] - SL[i]);
      Bx   = (SR[i]*vR[B1] - SL[i]*vL[B1])*scrh; 
      sBx  = (Bx > 0.0 ? 1.0 : -1.0);

      duL  = SL[i] - vL[V1];
      duR  = SR[i] - vR[V1];

      scrh = 1.0/(duR*uR[DN] - duL*uL[DN]);
      SM   = (duR*uR[M1] - duL*uL[M1] - ptR[i] + ptL[i])*scrh;

      pts  = duR*uR[DN]*ptL[i] - duL*uL[DN]*ptR[i] + 
             vL[DN]*vR[DN]*duR*duL*(vR[V1]- vL[V1]);
      pts *= scrh;

      usL[DN] = uL[DN]*duL/(SL[i] - SM);
      usR[DN] = uR[DN]*duR/(SR[i] - SM);

      sqrL = sqrt(usL[DN]);
      sqrR = sqrt(usR[DN]);

      S1L = SM - fabs(Bx)/sqrL;
      S1R = SM + fabs(Bx)/sqrR;

    /* ---------------------------------------------
        When S1L -> SL or S1R -> SR a degeneracy 
        occurs. Although Miyoshi & Kusano say that 
        no jump exists, we don't think this is 
        actually true. Indeed, vy*, vz*, By*, Bz* 
        cannote be solved independently. 
        In this case we revert to the HLLC solver 
        of Li (2005), except for the term v.B in the
        * region, which we compute in our own way.
        Note, that by comparing the expressions of 
        Li (2005) and Miyoshi & Kusano (2005), the 
        only change involves a re-definition
        of By* and Bz* in terms of By(HLL), Bz(HLL).
       --------------------------------------------- */

      revert_to_hllc = 0;

      if ( (S1L - SL[i]) <  1.e-4*(SM - SL[i]) ) revert_to_hllc = 1;
      if ( (S1R - SR[i]) > -1.e-4*(SR[i] - SM) ) revert_to_hllc = 1;

      if (revert_to_hllc){

        scrh = 1.0/(SR[i] - SL[i]);
        for (nv = NFLX; nv--; ){  
          Uhll[nv]  = SR[i]*uR[nv] - SL[i]*uL[nv] + fL[i][nv] - fR[i][nv];
          Uhll[nv] *= scrh;
        }

        EXPAND(usL[B1] = usR[B1] = Uhll[B1];   ,
               usL[B2] = usR[B2] = Uhll[B2];   ,
               usL[B3] = usR[B3] = Uhll[B3];)
 
        S1L = S1R = SM; /* region ** should never be computed since */ 
                        /* fluxes are given in terms of UL* and UR* */

      }else{

        scrhL = (uL[DN]*duL*duL - Bx*Bx)/(uL[DN]*duL*(SL[i] - SM) - Bx*Bx);
        scrhR = (uR[DN]*duR*duR - Bx*Bx)/(uR[DN]*duR*(SR[i] - SM) - Bx*Bx);
 
        EXPAND(usL[B1]  = Bx;            ,
               usL[B2]  = uL[B2]*scrhL;  ,
               usL[B3]  = uL[B3]*scrhL;)           

        EXPAND(usR[B1]  = Bx;            ,
               usR[B2]  = uR[B2]*scrhR;  ,
               usR[B3]  = uR[B3]*scrhR;)           
      }

      scrhL = Bx/(uL[DN]*duL);
      scrhR = Bx/(uR[DN]*duR);

      EXPAND(                                          ,
             vsL = vL[V2] - scrhL*(usL[B2] - uL[B2]);
             vsR = vR[V2] - scrhR*(usR[B2] - uR[B2]);  ,

             wsL = vL[V3] - scrhL*(usL[B3] - uL[B3]);
             wsR = vR[V3] - scrhR*(usR[B3] - uR[B3]); )
         
      EXPAND(usL[M1] = usL[DN]*SM; 
             usR[M1] = usR[DN]*SM;   ,
    
             usL[M2] = usL[DN]*vsL;
             usR[M2] = usR[DN]*vsR;  ,

             usL[M3] = usL[DN]*wsL;
             usR[M3] = usR[DN]*wsR;)

      scrhL  = EXPAND(vL[V1]*Bx, + vL[V2]*uL[B2] , + vL[V3]*uL[B3]);
      scrhL -= EXPAND(    SM*Bx, +    vsL*usL[B2], +    wsL*usL[B3]);
     
      usL[EN]  = duL*uL[EN] - ptL[i]*vL[V1] + pts*SM + Bx*scrhL;
      usL[EN] /= SL[i] - SM;

      scrhR  = EXPAND(vR[V1]*Bx, + vR[V2]*uR[B2] , + vR[V3]*uR[B3]);
      scrhR -= EXPAND(    SM*Bx, +    vsR*usR[B2], +    wsR*usR[B3]);
     
      usR[EN]  = duR*uR[EN] - ptR[i]*vR[V1] + pts*SM + Bx*scrhR;
      usR[EN] /= SR[i] - SM;

      #ifdef GLM_MHD
       usL[PSI_GLM] = usR[PSI_GLM] = vL[PSI_GLM];
      #endif

  /* ------------------------------
         compute HLLD flux 
     ------------------------------ */

      if (S1L >= 0.0){       /*  ----  Region L*  ---- */

        for (nv = NFLX; nv--; ){
          state->flux[i][nv] = fL[i][nv] + SL[i]*(usL[nv] - uL[nv]);
        }
        state->press[i] = ptL[i];

      }else if (S1R <= 0.0) {    /*  ----  Region R*  ---- */
    
        for (nv = NFLX; nv--; ){
          state->flux[i][nv] = fR[i][nv] + SR[i]*(usR[nv] - uR[nv]);
        }
        state->press[i] = ptR[i];
         
      } else {   /* -- This state exists only if B_x != 0 -- */
      
  /* ---------------------------
           Compute U**
     --------------------------- */

        ussl[DN] = usL[DN];
        ussr[DN] = usR[DN];
 
        EXPAND(                           ,
       
               vss  = sqrL*vsL + sqrR*vsR + (usR[B2] - usL[B2])*sBx;       
               vss /= sqrL + sqrR;        ,
            
               wss  = sqrL*wsL + sqrR*wsR + (usR[B3] - usL[B3])*sBx;
               wss /= sqrL + sqrR;)
           
        EXPAND(ussl[M1] = ussl[DN]*SM;
               ussr[M1] = ussr[DN]*SM;    ,
     
               ussl[M2] = ussl[DN]*vss;
               ussr[M2] = ussr[DN]*vss;  ,
           
               ussl[M3] = ussl[DN]*wss;
               ussr[M3] = ussr[DN]*wss;)           
    
        EXPAND(ussl[B1] = ussr[B1] = Bx;   ,

               ussl[B2]  = sqrL*usR[B2] + sqrR*usL[B2] + sqrL*sqrR*(vsR - vsL)*sBx;
               ussl[B2] /= sqrL + sqrR;        
               ussr[B2]  = ussl[B2];        ,
           
               ussl[B3]  = sqrL*usR[B3] + sqrR*usL[B3] + sqrL*sqrR*(wsR - wsL)*sBx;
               ussl[B3] /= sqrL + sqrR;        
               ussr[B3]  = ussl[B3];)
          
        scrhL  = EXPAND(SM*Bx, +  vsL*usL [B2], +  wsL*usL [B3]);
        scrhL -= EXPAND(SM*Bx, +  vss*ussl[B2], +  wss*ussl[B3]);

        scrhR  = EXPAND(SM*Bx, +  vsR*usR [B2], +  wsR*usR [B3]);
        scrhR -= EXPAND(SM*Bx, +  vss*ussr[B2], +  wss*ussr[B3]);

        ussl[EN] = usL[EN] - sqrL*scrhL*sBx;
        ussr[EN] = usR[EN] + sqrR*scrhR*sBx;

        #ifdef GLM_MHD
         ussl[PSI_GLM] = ussr[PSI_GLM] = vL[PSI_GLM];
        #endif
    
  /* --------------------------------------
      verify consistency condition 
     -------------------------------------- */

/*
      for (nv = 0; nv < NFLX; nv++){
        scrh = (SR[i] - S1R)*usR[nv]  + (S1R - SM)*ussr[nv] +
               (SM - S1L)*ussl[nv] + (S1L - SL[i])*usL[nv] -
               SR[i]*UR[nv] + SL[i]*UL[nv] + fr[i][nv] - fl[i][nv];

        if (fabs(scrh) > 1.e-2){
          printf (" ! Consistency condition violated, pt %d, nv %d, %12.6e \n", 
                   i,nv,scrh);
          printf ("%f %f %f %f %f %f\n",UL[B1], usL[B1], ussl[B1],
                                   ussr[B1], usR[B1], UR[B1]);
          printf ("%f %f %f %f %f\n",SL[i], S1L, SM, S1R, SR[i]);
          printf ("%f %f  %d\n",fr[i][nv],fl[i][nv],B1);
          exit(1);
        }
      }
*/

        if (SM >= 0.0){           /*  ----  Region L**  ---- */

          for (nv = NFLX; nv--; ){
            state->flux[i][nv] = fL[i][nv] + S1L*(ussl[nv] - usL[nv])
                                           + SL[i]*(usL[nv]  - uL[nv]);
          }
          state->press[i] = ptL[i];
        }else{                   /*  ----  Region R**  ---- */

          for (nv = NFLX; nv--; ){
            state->flux[i][nv] = fR[i][nv] + S1R*(ussr[nv] - usR[nv])
                                           + SR[i]*(usR[nv]  - uR[nv]);
          }
          state->press[i] = ptR[i];
        }
      }
    } 
  }

}
#endif

#if EOS == ISOTHERMAL
/* ***************************************************************************** */
void HLLD_SOLVER (const State_1D *state, int beg, int end, 
                  real *cmax, Grid *grid)
/* 
 *
 *
 * NAME
 *
 *   HLLD_SOLVER 
 *
 *
 * PURPOSE
 *
 *   Solve riemann problem for the isothermal MHD equations using 
 *   the threee-state HLLD 
 * 
 *   Reference:    "A simple and accurate Riemann Solver for 
 *                  isothermal MHD"
 *                 A. Mignone, JCP (2005)
 *
 * ARGUMENTS
 *
 *
 * SWITCHES
 *
 *
 *
 * LAST_MODIFIED
 *
 *   June 8, 2007 by Andrea Mignone  (mignone@to.astro.it)
 *              
 *
 ******************************************************************************** */
{
  int  nv, i;
  int  revert_to_hll;
  real scrh;
  real usL[NFLX], *SL;
  real usR[NFLX], *SR;
  real usc[NFLX];
  
  real scrhL, S1L, duL;
  real scrhR, S1R, duR;
  real Bx, SM, sBx, rho, sqrho;

  real *vL, *vR, *uL, *uR;
  static real *ptL, *ptR;
  static real **fL, **fR;
  static double **VL, **VR, **UL, **UR;


  if (fL == NULL){
    fL  = Array_2D(NMAX_POINT, NFLX, double);
    fR  = Array_2D(NMAX_POINT, NFLX, double);

    ptR = Array_1D(NMAX_POINT, double);
    ptL = Array_1D(NMAX_POINT, double);
    #ifdef GLM_MHD
     VL = Array_2D(NMAX_POINT, NVAR, double);
     VR = Array_2D(NMAX_POINT, NVAR, double);
     UL = Array_2D(NMAX_POINT, NVAR, double);
     UR = Array_2D(NMAX_POINT, NVAR, double);
    #endif
  }

  #if INCLUDE_BACKGROUND_FIELD == YES
   print ("! Background field splitting not allowed with HLLD solver\n");
   QUIT_ORION2(1);
  #endif
  #if MHD_FORMULATION == EIGHT_WAVES
   print ("! hlld Riemann solver does not work with Powell\n");
   QUIT_ORION2(1);
  #endif

  #ifdef GLM_MHD
   GLM_2X2_SOLVE (state, VL, VR, beg, end, grid);
   PRIMTOCON (VL, UL, beg, end);
   PRIMTOCON (VR, UR, beg, end);
  #else
   VL = state->vL; UL = state->uL;
   VR = state->vR; UR = state->uR;
  #endif

  FLUX  (UL, VL, NULL, fL, ptL, beg, end);
  FLUX  (UR, VR, NULL, fR, ptR, beg, end);

  /* ----------------------------------------
        get max and min signal velocities
     ---------------------------------------- */
             
  SL = state->SL; SR = state->SR;
  HLL_SPEED (VL, VR, NULL, SL, SR, beg, end);

  for (i = beg; i <= end; i++) {
    
  /* ----------------------------------------
      get max propagation speed for dt comp.
     ---------------------------------------- */             

    scrh  = dmax(fabs(SL[i]), fabs(SR[i]));
    *cmax = dmax(*cmax, scrh);

    vL = VL[i]; uL = UL[i];
    vR = VR[i]; uR = UR[i];

/* ---------------------------------------------------------- 
                COMPUTE FLUXES and STATES
   ---------------------------------------------------------- */

    if (SL[i] >= 0.0){                     /*  ----  Region L  ---- */

      for (nv = NFLX; nv--; ) state->flux[i][nv] = fL[i][nv];
      state->press[i] = ptL[i];

    }else if (SR[i] <= 0.0) {              /*  ----  Region R   ---- */

      for (nv = NFLX; nv--; ) state->flux[i][nv] = fR[i][nv];
      state->press[i] = ptR[i];
 
    } else {

      scrh = 1.0/(SR[i] - SL[i]);
      duL = SL[i] - vL[V1];
      duR = SR[i] - vR[V1];

      Bx   = (SR[i]*vR[B1] - SL[i]*vL[B1])*scrh; 

      rho                = (uR[DN]*duR - uL[DN]*duL)*scrh;
      state->flux[i][DN] = (SL[i]*uR[DN]*duR - SR[i]*uL[DN]*duL)*scrh;
           
  /* ---------------------------
          compute S*
     --------------------------- */

      sqrho = sqrt(rho);

      SM  = state->flux[i][DN]/rho;
      S1L = SM - fabs(Bx)/sqrho;
      S1R = SM + fabs(Bx)/sqrho;

    /* ---------------------------------------------
        Prevent degeneracies when S1L -> SL or 
        S1R -> SR. Revert to HLL if necessary.
       --------------------------------------------- */

      revert_to_hll = 0;

      if ( (S1L - SL[i]) <  1.e-4*(SR[i] - SL[i]) ) revert_to_hll = 1;
      if ( (S1R - SR[i]) > -1.e-4*(SR[i] - SL[i]) ) revert_to_hll = 1;

      if (revert_to_hll){
        scrh = 1.0/(SR[i] - SL[i]);
        for (nv = NFLX; nv--; ){
          state->flux[i][nv] = SL[i]*SR[i]*(uR[nv] - uL[nv]) +
                               SR[i]*fL[i][nv] - SL[i]*fR[i][nv];
          state->flux[i][nv] *= scrh;
        }
        state->press[i] = (SR[i]*ptL[i] - SL[i]*ptR[i])*scrh;
        continue;
      }

      state->flux[i][M1] = (SR[i]*fL[i][M1] - SL[i]*fR[i][M1] 
                            + SR[i]*SL[i]*(uR[M1] - uL[M1]))*scrh;

      state->press[i]    = (SR[i]*ptL[i] - SL[i]*ptR[i])*scrh;
      #ifdef GLM_MHD
       state->flux[i][B1]      = fL[i][B1];
       state->flux[i][PSI_GLM] = fL[i][PSI_GLM];
      #else
       state->flux[i][B1] = SR[i]*SL[i]*(uR[B1] - uL[B1])*scrh;
      #endif

  /* ---------------------------
             Compute U*  
     --------------------------- */
       
      scrhL = 1.0/((SL[i] - S1L)*(SL[i] - S1R));
      scrhR = 1.0/((SR[i] - S1L)*(SR[i] - S1R));

      EXPAND(                                                       ,
             usL[M2] = rho*vL[V2] - Bx*uL[B2]*(SM - vL[V1])*scrhL;
             usR[M2] = rho*vR[V2] - Bx*uR[B2]*(SM - vR[V1])*scrhR;  ,

             usL[M3] = rho*vL[V3] - Bx*uL[B3]*(SM - vL[V1])*scrhL;
             usR[M3] = rho*vR[V3] - Bx*uR[B3]*(SM - vR[V1])*scrhR;)
         
      EXPAND(                                                      ,
             usL[B2] = uL[B2]/rho*(uL[DN]*duL*duL - Bx*Bx)*scrhL; 
             usR[B2] = uR[B2]/rho*(uR[DN]*duR*duR - Bx*Bx)*scrhR; ,

             usL[B3] = uL[B3]/rho*(uL[DN]*duL*duL - Bx*Bx)*scrhL;           
             usR[B3] = uR[B3]/rho*(uR[DN]*duR*duR - Bx*Bx)*scrhR;)           


      if (S1L >= 0.0){       /*  ----  Region L*  ---- */

        EXPAND(                                                       ,
          state->flux[i][M2] = fL[i][M2] + SL[i]*(usL[M2] - uL[M2]);  ,
          state->flux[i][M3] = fL[i][M3] + SL[i]*(usL[M3] - uL[M3]);  
        ) 
        EXPAND(                                                       ,
          state->flux[i][B2] = fL[i][B2] + SL[i]*(usL[B2] - uL[B2]);  ,
          state->flux[i][B3] = fL[i][B3] + SL[i]*(usL[B3] - uL[B3]);  
        ) 

      }else if (S1R <= 0.0) {    /*  ----  Region R*  ---- */
    
        EXPAND(                                                       ,
          state->flux[i][M2] = fR[i][M2] + SR[i]*(usR[M2] - uR[M2]);  ,
          state->flux[i][M3] = fR[i][M3] + SR[i]*(usR[M3] - uR[M3]);  
        ) 
        EXPAND(                                                       ,
          state->flux[i][B2] = fR[i][B2] + SR[i]*(usR[B2] - uR[B2]);  ,
          state->flux[i][B3] = fR[i][B3] + SR[i]*(usR[B3] - uR[B3]);  
        ) 
         
      } else {
                      
       /* ---------------------------
               Compute U** = Uc
          --------------------------- */

        sBx = (Bx > 0.0 ? 1.0 : -1.0);

        EXPAND(                                                                    ,
               usc[M2] = 0.5*(usR[M2] + usL[M2] + (usR[B2] - usL[B2])*sBx*sqrho);  ,     
               usc[M3] = 0.5*(usR[M3] + usL[M3] + (usR[B3] - usL[B3])*sBx*sqrho);)
           
        EXPAND(                                                                    ,
               usc[B2] = 0.5*(usR[B2] + usL[B2] + (usR[M2] - usL[M2])*sBx/sqrho);  ,
               usc[B3] = 0.5*(usR[B3] + usL[B3] + (usR[M3] - usL[M3])*sBx/sqrho);)


        EXPAND(                                               ,
               state->flux[i][M2] = usc[M2]*SM - Bx*usc[B2];  ,
               state->flux[i][M3] = usc[M3]*SM - Bx*usc[B3]; )
        EXPAND(                                                   ,
               state->flux[i][B2] = usc[B2]*SM - Bx*usc[M2]/rho;  ,
               state->flux[i][B3] = usc[B3]*SM - Bx*usc[M3]/rho;)

    /* --------------------------------------
          verify consistency condition 
       -------------------------------------- */

        #if VERIFY_CONSISTENCY_CONDITION == YES

         for (nv = NFLX; nv--; ){
           if (nv == DN || nv == M1 || nv == B1) continue;
           scrh = (S1L - SL[i])*usL[nv]  + (S1R - S1L)*usc[nv] +
                  (SR[i] - S1R)*usR[nv] -
                  SR[i]*uR[nv] + SL[i]*uL[nv] + fR[i][nv] - fL[i][nv];

           if (fabs(scrh) > 1.e-6){
             printf (" ! Consistency condition violated, pt %d, nv %d, %12.6e \n", 
                     i,nv,scrh);
             printf (" scrhL = %12.6e   scrhR = %12.6e\n",scrhL, scrhR);
             printf (" SL = %12.6e, S1L = %12.6e, S1R = %12.6e, SR = %12.6e\n",
                     SL[i],S1L,S1R, SR[i]);
             SHOW(state->vL,i);
             SHOW(state->vR,i);

             exit(1);
           }
         }
	 
        #endif	  
                 
      }
    }
  }

}
#endif /* end #if on EOS  */

#undef VERIFY_CONSISTENCY_CONDITION 
