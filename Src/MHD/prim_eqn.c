#include "orion2.h"

/* *********************************************************************** */
void PRIM_RHS (real *v, real *dv, real cs2, real h, real *Adv)
/*
 *
 *  PURPOSE:
 *
 *  Compute the matrix-vector multiplication Adv = A(v)*dv
 *  for the primitive form of the equations.
 *
 * -----------------------------------------------------------
 *  Reference paper(s):
 *
 * - "A solution adaptive upwind scheme for ideal MHD"
 *
 *     K. G. Powell, P. L. Roe, and T. J. Linde
 *     Journal of Computational Physics, 154, 284-309, (1999).
 *
 * - "An unsplit Godunov method for ideal MHD via constrained 
 *    transport"
 *
 *    T. Gardiner, J. Stone 
 *    Journal of Computational Physics, 205, 509-539, (2005).
 * -----------------------------------------------------------
 *
 ************************************************************************* */
{
  int nv;
  real tau, scrh;
  double ch2;

  tau = 1.0/v[DN];

  /*  ---------------------------------------------
           Adv[k]  Contains A[k][*]*dv[*]  
      ---------------------------------------------  */

  Adv[DN] = v[V1]*dv[DN] + v[DN]*dv[V1];
  scrh = EXPAND(0.0, + v[B2]*dv[B2], + v[B3]*dv[B3]);

  #if EOS == IDEAL
   Adv[V1] = v[V1]*dv[V1] + tau*(dv[PR] + scrh);
  #elif EOS == ISOTHERMAL
   Adv[V1] = v[V1]*dv[V1] + tau*(C_ISO2*dv[DN] + scrh);
  #endif

  EXPAND(                                              ,
         Adv[V2] = v[V1]*dv[V2] - tau*v[B1]*dv[B2];    ,
         Adv[V3] = v[V1]*dv[V3] - tau*v[B1]*dv[B3]; ) 

  #if MHD_FORMULATION == EIGHT_WAVES
   Adv[B1] = v[V1]*dv[B1];
  #elif MHD_FORMULATION == DIV_CLEANING 
   ch2 = MAXC*MAXC;
   Adv[B1]      = dv[PSI_GLM];             
   Adv[PSI_GLM] = dv[B1]*ch2; 
  #else
   Adv[B1] = 0.0;
  #endif
   
  /* ----------------------------------------------------------
      The following matrix is used in the 7-wave and 8-wave
      formulations. It is the same matrix being
      decomposed into right and left eigenvectors during 
      the Characteristic Tracing step.
      Note, however, that it DOES NOT include two additional 
      terms (-v2*dV[B1] for B2, -v3*dv[B1] for B3) that 
      are needed in the 7-wave form and are added using 
      source terms.
     ---------------------------------------------------------- */

  EXPAND(                                                        ,
         Adv[B2] = v[B2]*dv[V1] - v[B1]*dv[V2] + v[V1]*dv[B2];   ,
         Adv[B3] = v[B3]*dv[V1] - v[B1]*dv[V3] + v[V1]*dv[B3];)

  #if EOS == IDEAL
   Adv[PR] = gmm*v[PR]*dv[V1] + v[V1]*dv[PR];
  #endif

/*  -------------------------------------------------------------
                         Now Define Tracers
    ------------------------------------------------------------- */

  #if NFLX != NVAR
   for (nv = NFLX; nv < (NFLX + NSCL); nv++) Adv[nv] = v[V1]*dv[nv];
  #endif

}

/* **************************************************************************  */
void PRIM_SOURCE (const State_1D *state, int beg, int end, real *a2, 
                  real *h, real **src, Grid *grid)
/*
 *
 *
 *     Add source terms for primitive-base time marching method;
 *     Source terms include:
 *
 *      - Geometrical sources
 *      - Gravity
 *
 *  These function contains, among other things, all
 *  source terms coming from the divergence of a vector
 *  A in curvilinear geometry, that is:
 *
 *   div A = 1/V [ d_1 (A_1 V/h_1) + d_2 (A_2 V/h_2) + d_3 (A_3 V/h_3)] = 
 *         = (1/h_1 d_1 A_1 + ... ) + A_1 1/V d_1 (V/h_1) 
 *
 *  the last term (1/V d_1 (V/h_1))  being just dA/dV, defined 
 *  in setgrid 
 **************************************************************************** */
{
  int nv, i;
  real x1, x2, x3;
  real vth, vphi, dA_dV, scrh;
  real tau, db, bth, bphi;
  static real **g;
  real *v, *u, *A, *dV, r_inv, ct;
  double ch2;


  if (g == NULL) g = Array_2D(NMAX_POINT, 3, double);

  for (i = 0; i < NMAX_POINT; i++){
  for (nv = NVAR; nv--;   ) {
    src[i][nv] = 0.0;
  }}

  #ifdef PSI_GLM
   ch2 = MAXC*MAXC;
  #endif

  A  = grid[DIR].A;
  dV = grid[DIR].dV;


/* --------------------------------------------------
      1 - Geometrical Source Terms 
   -------------------------------------------------- */

  #if GEOMETRY == CYLINDRICAL

   if (DIR == IDIR) {
     for (i = beg; i <= end; i++){

       v = state->v[i];
       u = state->u[i];

       dA_dV = 1.0/grid[DIR].x[i];

       src[i][DN] = -u[M1]*dA_dV;
       #if EOS == IDEAL
        src[i][PR] = a2[i]*src[i][DN];
       #endif
      
    /*  ---------------------------------------
          the source term in the iBZ component 
          of magnetic field is = -Ephi/r;
        --------------------------------------- */
      
       #if COMPONENTS >= 2       
        src[i][iBZ] = (v[iBR]*v[iVZ] - v[iVR]*v[iBZ])*dA_dV; 
       #endif

       #if COMPONENTS == 3
        scrh = 1.0/v[DN];
        src[i][iVR]   = ( v[iVPHI]*v[iVPHI] - v[iBPHI]*v[iBPHI]*scrh)*dA_dV;
        src[i][iVPHI] = (-v[iVR]  *v[iVPHI] + v[iBR]  *v[iBPHI]*scrh)*dA_dV;
       #endif
       #ifdef PSI_GLM
        src[i][PSI_GLM] = -v[iBR]*dA_dV*ch2;
       #endif
     }
   }
 
  #elif GEOMETRY == POLAR

   if (DIR == IDIR) {
     for (i = beg; i <= end; i++){
                                                                                  
       v = state->v[i];
       u = state->u[i];

       dA_dV = 1.0/grid[DIR].x[i];
                                                                                  
       src[i][DN] = -u[M1]*dA_dV;
       #if EOS == IDEAL
        src[i][PR] = a2[i]*src[i][DN];
       #endif
                                                                                  
       scrh = 1.0/v[DN];
       src[i][iVR] = (v[iVPHI]*v[iVPHI] - v[iBPHI]*v[iBPHI]*scrh)*dA_dV;
                                                                                  
       #if COMPONENTS == 3
        src[i][iBZ] = (v[iVZ]*v[iBR] - v[iVR]*v[iBZ])*dA_dV;
       #endif
       #ifdef PSI_GLM
        src[i][PSI_GLM] = -v[iBR]*dA_dV*ch2;
       #endif

     }
                                                                                  
   }else if (DIR == JDIR) {
                                                                                  
     dA_dV = 1.0/grid[IDIR].x[*NX_PT];
     for (i = beg; i <= end; i++){
                                                                                  
       v = state->v[i];
       u = state->u[i];

       scrh = 1.0/v[DN];
       src[i][iVPHI] = (-v[iVR]*v[iVPHI] + v[iBR]*v[iBPHI]*scrh)*dA_dV;
/*       src[i][iBPHI] = ( v[iVZ]*v[iBR]   - v[iVR]*v[iBZ]  *scrh)*dA_dV;*/
                                                                                 
     }
   }
                                                                                  
  #elif GEOMETRY == SPHERICAL

   if (DIR == IDIR) {

     ct = 1.0/tan(grid[JDIR].x[*NY_PT]);
     for (i = beg; i <= end; i++){

       v = state->v[i];
       u = state->u[i];

       dA_dV = 2.0/fabs(grid[DIR].x[i]);
       r_inv = 0.5*dA_dV;

       src[i][DN] = scrh = -u[M1]*dA_dV; 
       #if EOS == IDEAL
        src[i][PR] = scrh*a2[i];
       #endif
      
       #if COMPONENTS >= 2
        vth = v[iVTH];
        bth = v[iBTH];
       #else 
        vth = bth = 0.0;
       #endif

       #if COMPONENTS == 3
        vphi = v[iVPHI];
        bphi = v[iBPHI];
       #else 
        vphi = bphi = 0.0;
       #endif

       EXPAND(
         src[i][iVR]   = (  vth*vth + vphi*vphi 
                          - bth*bth - bphi*bphi)*r_inv;      ,
         src[i][iVTH]   = 0.0;                               ,
         src[i][iVPHI]  = ( - vphi*(v[iVR] + vth*ct) 
	                    + bphi*(v[iBR] + bth*ct))*r_inv;
       )
       EXPAND(
         src[i][iBR]   = - (vth*v[iBR] - v[iVR]*bth)*ct*r_inv;  , 
         src[i][iBTH]  = 0.0;                                   ,
         src[i][iBPHI] = - (v[iVR]*bphi - vphi*v[iBR])*r_inv;
       )

       #ifdef PSI_GLM
        src[i][PSI_GLM] = -v[iBR]*dA_dV*ch2;
       #endif
     } 

   }else if (DIR == JDIR) {

     r_inv = 1.0/grid[IDIR].x[*NX_PT];
     for (i = beg; i <= end; i++){

       v = state->v[i];
       u = state->u[i];

       ct    = 1.0/tan(grid[JDIR].x[i]);
       dA_dV = ct*r_inv;

       src[i][DN] = scrh = -u[M1]*dA_dV;
       #if EOS == IDEAL
        src[i][PR] = scrh*a2[i];
       #endif
      
       #if COMPONENTS == 3
        vphi = v[iVPHI];
        bphi = v[iBPHI];
       #else 
        vphi = bphi = 0.0;
       #endif

       EXPAND(
         src[i][iVR]   = 0.0;                                       ,
         src[i][iVTH]  = ( - v[iVR]*v[iVTH] + vphi*vphi*ct
	                   + v[iBR]*v[iBTH] - bphi*bphi*ct)*r_inv;  ,
         src[i][iVPHI] = 0.0;
       )

       EXPAND(
         src[i][iBR]   = 0.0;                                        ,
         src[i][iBTH]  = - (v[iVR]*v[iBTH] - v[iVTH]*v[iBR])*r_inv;  ,
         src[i][iBPHI] = 0.0;
       )
       #ifdef PSI_GLM
        src[i][PSI_GLM] = -v[iBR]*dA_dV*ch2;
       #endif
   
     }
   }

  #endif

/* -----------------------------------------------------------
      2 - MHD, div.B related source terms  
   ----------------------------------------------------------- */

  #if MHD_FORMULATION == DIV_CLEANING
   #if GEOMETRY == POLAR || GEOMETRY == SPHERICAL
    print1 ("! Error: Div. Cleaning does not work in this configuration.\n");
    print1 ("!        Try RK integrator instead\n");
    QUIT_ORION2(1);
   #endif
   for (i = beg; i <= end; i++){
     v = state->v[i];

     tau = 1.0/v[DN]; 
     db  = 0.5*(  A[i]  *(state->v[i+1][B1] + state->v[i][B1]) 
                - A[i-1]*(state->v[i-1][B1] + state->v[i][B1]))/dV[i];
     #if MHD_FORMULATION == DIV_CLEANING && EGLM == NO		
      EXPAND(src[i][V1] += v[B1]*tau*db;  ,
             src[i][V2] += v[B2]*tau*db;  ,
             src[i][V3] += v[B3]*tau*db;)
     #endif
     EXPAND(                        ,
            src[i][B2] += v[V2]*db; ,
            src[i][B3] += v[V3]*db;)
     
     #if EOS == IDEAL
      scrh = EXPAND(v[V1]*v[B1], + v[V2]*v[B2], + v[V3]*v[B3]);
      src[i][PR] += (1.0 - gmm)*scrh*db;
      #if MHD_FORMULATION == DIV_CLEANING && EGLM == NO	
       scrh = 0.5*(state->v[i+1][PSI_GLM] - state->v[i-1][PSI_GLM])/grid[DIR].dx[i];
       src[i][PR] += (gmm - 1.0)*v[B1]*scrh;
      #endif        
     #endif
   }
  #endif

/* -----------------------------------------------------------
               3 - Include Body Force
   ----------------------------------------------------------- */


  #if INCLUDE_BODY_FORCE == EXPLICIT
  {
    Force *f;

    f  = ACCELERATION (state->v, DIR, beg, end, grid);
    for (i = beg; i <= end; i++){
      src[i][V1] += f->a[i];
    }

    #if DIMENSIONS == 2 && COMPONENTS == 3
     if (DIR == JDIR){
       f = ACCELERATION (state->v, KDIR, beg, end, grid);
       for (i = beg; i <= end; i++) {
         src[i][VZ] += f->a[i];
       }
     } 
    #endif
  }
  #endif
  
}


