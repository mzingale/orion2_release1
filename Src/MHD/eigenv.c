#include "orion2.h"

/* ************************************************************************** */
void MAX_CH_SPEED (real **v, real *cmin, real *cmax, real **bgf,
                   int beg, int end)

/*
 * 
 * PURPOSE:
 *
 *   Compute the maximum and minimum characteristic
 *   velocities for the MHD equation:
 *
 *    cmin= v - cf,   cmax = v + cf
 *
 *
 *********************************************************************** */
{
  int  i;
  real gpr, Bmag2, Btmag2;
  real cf;
  real b1, b2, b3;

  b1 = b2 = b3 = 0.0;
  for (i = beg; i <= end; i++) {

    #if EOS == IDEAL
     gpr = gmm*v[i][PR];
    #elif EOS == ISOTHERMAL
     gpr = C_ISO2*v[i][DN];
    #endif

/*    Get the total field     */

    EXPAND (b1 = v[i][B1];  ,
            b2 = v[i][B2];  ,
            b3 = v[i][B3];  )

    #if INCLUDE_BACKGROUND_FIELD == YES
     EXPAND (b1 += bgf[i][B1]; ,
             b2 += bgf[i][B2]; ,
             b3 += bgf[i][B3];)
    #endif
    Btmag2 = b2*b2 + b3*b3;
    Bmag2  = b1*b1 + Btmag2;

    cf = gpr - Bmag2;
    cf = gpr + Bmag2 + sqrt(cf*cf + 4.0*gpr*Btmag2);
    cf = sqrt(0.5*cf/v[i][DN]);
    cmin[i] = v[i][V1] - cf;
    cmax[i] = v[i][V1] + cf;
/*
    MAX_MACH_NUMBER = dmax(fabs (w[ii][V1] / sqrt(a2)), MAX_MACH_NUMBER);
*/
  }
}

/* *********************************************************************** */
void EIGENVALUES(double *q, double *lambda)
/*
 *
 *    Provide eigenvalues for the MHD equations
 *
 *********************************************************************** */
{
  int  k;
  real scrh0, scrh1, scrh2, scrh3, scrh4;
  real u, a, a2, ca2, cf2, cs2;
  real cs, ca, cf, b2, A2, At2;
  real tau;
  real sqrt_rho;

  u   = q[V1];
  tau = 1.0/q[DN];
  sqrt_rho = sqrt(q[DN]);

  #if EOS == IDEAL
   a2  = gmm*q[PR]/q[DN];
  #elif EOS == ISOTHERMAL
   a2 = C_ISO2;
  #endif

  scrh2 = q[B1]*q[B1];            /* > 0 */
  scrh3 = EXPAND(0.0, + q[B2]*q[B2],  + q[B3]*q[B3]);   /* > 0 */

  b2  = scrh2 + scrh3;     /*  >0             */
  ca2 = scrh2*tau;         /*  >0  if tau >0  */
  A2  = b2*tau;            /*  >0  ''   ''    */
  At2 = scrh3*tau;

  scrh1 = a2 - A2;
  scrh0 = sqrt(scrh1*scrh1 + 4.0*a2*At2);      /*   >0   */

/*   Now get fast and slow speeds   */
    
  cf2 = 0.5*(a2 + A2 + scrh0);
  cs2 = a2*ca2/cf2;

  cf = sqrt(cf2);
  cs = sqrt(cs2);
  ca = sqrt(ca2);
  a  = sqrt(a2);

  lambda[KFASTM] = u - cf;
  lambda[KFASTP] = u + cf;

  #if EOS == IDEAL 
   lambda[KENTRP] = u;
  #endif

  #ifndef GLM_MHD
   #if MHD_FORMULATION == EIGHT_WAVES
    lambda[KDIVB] = u;
   #else  
    lambda[KDIVB] = 0.0;
   #endif
  #endif

  #if COMPONENTS > 1
   lambda[KSLOWM] = u - cs;
   lambda[KSLOWP] = u + cs;
  #endif

  #if COMPONENTS == 3
   lambda[KALFVM] = u - ca;
   lambda[KALFVP] = u + ca;
  #endif

  #ifdef GLM_MHD
   lambda[KPSI_GLMM] = -Ch_GLM;
   lambda[KPSI_GLMP] =  Ch_GLM;
  #endif
}


/* *********************************************************************** */
void EIGENV(real *q, real a2, real h, real *lambda,
            real **LL, real **RR)
/*
 *
 *  PURPOSE
 *
 *    provide left and right eigenvectors and corresponding 
 *    eigenvalues for the PRIMITIVE form of the MHD equations 
 *    (adiabatic & isothermal cases).
 *    
 *    It is highly recommended that LL and RR be initialized to 
 *    zero *BEFORE* since only non-zero entries are treated here. 
 *
 *    Wave names and their order are defined as enumeration 
 *    constants in mod_defs.h. 
 *    Notice that, the characteristic decomposition may differ 
 *    depending on the way div.B is treated.
 * 
 *    Advection modes associated with passive scalars are simple cases 
 *    for which lambda = u (entropy mode) and l = r = (0, ... , 1, 0, ...). 
 *    For this reason they are NOT defined here and must be treated 
 *    seperately.
 *
 * ----------------------------------------------------------------
 *  Reference papers:
 *
 *  "Notes on the Eigensystem of Magnetohydrodynamics",
 *  P.L. Roe, D.S. Balsara,
 *  SIAM Journal on Applied Mathematics, 56, 57 (1996)
 *
 * "A solution adaptive upwind scheme for ideal MHD",
 *  K. G. Powell, P. L. Roe, and T. J. Linde,
 *  Journal of Computational Physics, 154, 284-309, (1999).
 *
 *  "ATHENA: A new code for astrophysical MHD",
 *  J. Stone, T. Gardiner, 
 *  ApJS, 178, 137 (2008)
 *
 *  --------------------------------------------------------------- 
 * 
 * The derivation of the isothermal eigenvectors follows the
 * consideration given in roe.c
 *
 * 
 * Last Modified:  Nov 16 2009 by A. Mignone (mignone@ph.unito.it)
 *
 *********************************************************************** */
#define  sqrt_1_2  (0.70710678118654752440)
{
  int  k;
  real scrh0, scrh1, scrh2, scrh3, scrh4;
  real u, a, ca2, cf2, cs2;
  real cs, ca, cf, b2, A2, At2;
  real tau, S;
  real alpha_f, alpha_s, beta_y, beta_z;
  real sqrt_rho;

  u   = q[V1];
  tau = 1.0/q[DN];
  sqrt_rho = sqrt(q[DN]);

  scrh2 = q[B1]*q[B1];                                 /*  Bx^2 */
  scrh3 = EXPAND(0.0, + q[B2]*q[B2],  + q[B3]*q[B3]);  /*  Bt^2 */

  b2  = scrh2 + scrh3;     /*  B^2 = Bx^2 + Bt^2 */
  ca2 = scrh2*tau;         /*  Bx^2/rho          */
  A2  = b2*tau;            /*  B^2/rho           */
  At2 = scrh3*tau;         /*  Bt^2/rho           */

  scrh1 = a2 - A2;
  scrh0 = sqrt(scrh1*scrh1 + 4.0*a2*At2);  /* sqrt( (g*p/rho-B^2/rho)^2 
                                                   + 4*g*p/rho*Bt^2/rho)  */
/* --  Now get fast and slow speeds -- */
    
  cf2 = 0.5*(a2 + A2 + scrh0);
  cs2 = a2*ca2/cf2;

  cf = sqrt(cf2);
  cs = sqrt(cs2);
  ca = sqrt(ca2);
  a  = sqrt(a2);

  if (cf == cs) {
    alpha_f = 1.0;
    alpha_s = 0.0;
  }else{
    scrh0   = 1.0/scrh0;
    alpha_f = (a2 - cs2)*scrh0;
    alpha_s = (cf2 - a2)*scrh0;

    alpha_f = dmax(0.0, alpha_f);
    alpha_s = dmax(0.0, alpha_s);

    alpha_f = sqrt(alpha_f);
    alpha_s = sqrt(alpha_s);
  }

  scrh0 = sqrt(scrh3);

  if (scrh0 > 1.e-9) {
    SELECT (                        , 
            beta_y = dsign(q[B2]);  ,
            beta_y = q[B2] / scrh0; 
            beta_z = q[B3] / scrh0;)
  } else {
    SELECT (                  , 
            beta_y = 1.0;     ,
            beta_z = beta_y = sqrt_1_2;)
  }

  S = (q[B1] >= 0.0 ? 1.0 : -1.0);

/*  ------------------------------------------------------------
     define primitive right and left eigenvectors (RR and LL),
     for all of the 8 waves;
     left eigenvectors for fast & slow waves can be defined
     in terms of right eigenvectors (see page 296)  
    ------------------------------------------------------------  */

  /* -------------------------
      FAST WAVE,  (u - c_f)
     -------------------------  */

  k = KFASTM;  
  lambda[k] = u - cf;
  scrh0 = alpha_s*cs*S;
  scrh1 = alpha_s*sqrt_rho*a;
  scrh2 = 0.5 / a2;
  scrh3 = scrh2*tau;

  RR[DN][k] = q[DN]*alpha_f;        
  EXPAND(RR[V1][k] = -cf*alpha_f;   ,
         RR[V2][k] = scrh0*beta_y;  ,
         RR[V3][k] = scrh0*beta_z;)
  EXPAND(                           , 
         RR[B2][k] = scrh1*beta_y;  ,
         RR[B3][k] = scrh1*beta_z;)

  #if EOS == IDEAL
   scrh4 = alpha_f*gmm*q[PR];
   RR[PR][k] = scrh4;
  #endif

  #if EOS == ISOTHERMAL
   LL[k][DN] = 0.5*alpha_f/q[DN];
  #endif
  EXPAND(LL[k][V1] = RR[V1][k]*scrh2; ,
         LL[k][V2] = RR[V2][k]*scrh2; ,
         LL[k][V3] = RR[V3][k]*scrh2;) 
  EXPAND(                             , 
         LL[k][B2] = RR[B2][k]*scrh3; ,
         LL[k][B3] = RR[B3][k]*scrh3;)
  #if EOS == IDEAL
   LL[k][PR] = alpha_f*scrh3;
  #endif

  /* -------------------------
      FAST WAVE,  (u + c_f)
     -------------------------  */

  k = KFASTP; 
  lambda[k] = u + cf;
  RR[DN][k] = RR[DN][KFASTM];
  EXPAND(RR[V1][k] = -RR[V1][KFASTM];  ,
         RR[V2][k] = -RR[V2][KFASTM];  ,
         RR[V3][k] = -RR[V3][KFASTM];)
  EXPAND(                             ,
         RR[B2][k] = RR[B2][KFASTM];  ,
         RR[B3][k] = RR[B3][KFASTM];)
  #if EOS == IDEAL
   RR[PR][k] = RR[PR][KFASTM];
  #endif

  #if EOS == ISOTHERMAL
   LL[k][DN] = LL[KFASTM][DN];
  #endif
  EXPAND(LL[k][V1] = -LL[KFASTM][V1];  ,
         LL[k][V2] = -LL[KFASTM][V2];  ,
         LL[k][V3] = -LL[KFASTM][V3];) 
  EXPAND(                              ,                         
         LL[k][B2] = LL[KFASTM][B2];  ,
         LL[k][B3] = LL[KFASTM][B3];)
  #if EOS == IDEAL
   LL[k][PR] = LL[KFASTM][PR]; 
  #endif

  /* -------------------------
      entropy wave,  (u) only
      in ideal MHD
     -------------------------  */

  #if EOS == IDEAL 
   k = KENTRP;
   lambda[k] = u;
   RR[DN][k] =   1.0;
   LL[k][DN] =   1.0; 
   LL[k][PR] = - 1.0/a2;
  #endif

  /* -------------------------
        magnetic flux, (u)
     -------------------------  */

  #ifndef GLM_MHD
   k = KDIVB;
   #if MHD_FORMULATION == EIGHT_WAVES
    lambda[k] = u;
    RR[B1][k] = 1.0;
    LL[k][B1] = 1.0;
   #else  
    lambda[k] = 0.0;
   #endif
  #endif

  #if COMPONENTS > 1

  /* -------------------------
      SLOW WAVE,  (u - c_s)
     -------------------------  */

   k = KSLOWM;
   lambda[k] = u - cs;
   scrh0 = alpha_f*cf*S;
   scrh1 = alpha_f*sqrt_rho*a;

   RR[DN][k] = q[DN]*alpha_s;
   EXPAND(RR[V1][k] = -cs*alpha_s;     ,
          RR[V2][k] = -scrh0*beta_y;   ,
          RR[V3][k] = -scrh0*beta_z;)
   EXPAND(                            ,
          RR[B2][k] = -scrh1*beta_y;  ,
          RR[B3][k] = -scrh1*beta_z;)
   #if EOS == IDEAL
    scrh4 = alpha_s*gmm*q[PR]; 
    RR[PR][k] = scrh4;
   #endif

   #if EOS == ISOTHERMAL
    LL[k][DN] = 0.5*alpha_s/q[DN];
   #endif
   EXPAND(LL[k][V1] = RR[V1][k]*scrh2; ,
          LL[k][V2] = RR[V2][k]*scrh2; ,
          LL[k][V3] = RR[V3][k]*scrh2;) 
   EXPAND(                             ,
          LL[k][B2] = RR[B2][k]*scrh3; ,
          LL[k][B3] = RR[B3][k]*scrh3;) 

   #if EOS == IDEAL
    LL[k][PR] = alpha_s*scrh3;
   #endif

  /* -------------------------
      SLOW WAVE,  (u + c_s)
     -------------------------  */

   k = KSLOWP;
   lambda[k] = u + cs;

   RR[DN][k] = RR[DN][KSLOWM];
   EXPAND(RR[V1][k] = -RR[V1][KSLOWM];   ,
          RR[V2][k] = -RR[V2][KSLOWM];   ,
          RR[V3][k] = -RR[V3][KSLOWM];)

   EXPAND(                             ,
          RR[B2][k] = RR[B2][KSLOWM];  ,
          RR[B3][k] = RR[B3][KSLOWM];)
   #if EOS == IDEAL
    RR[PR][k] = scrh4;
   #endif

   #if EOS == ISOTHERMAL
    LL[k][DN] = LL[KSLOWM][DN];
   #endif
   EXPAND(LL[k][V1] = -LL[KSLOWM][V1];   ,
          LL[k][V2] = -LL[KSLOWM][V2];   ,
          LL[k][V3] = -LL[KSLOWM][V3];) 
   EXPAND(                               ,
          LL[k][B2] = LL[KSLOWM][B2];   ,
          LL[k][B3] = LL[KSLOWM][B3];) 

   #if EOS == IDEAL
    LL[k][PR] = LL[KSLOWM][PR];
   #endif
  #endif

  #if COMPONENTS == 3

  /* -------------------------
      Alfven WAVE,  (u - c_a)
     -------------------------  */
   
   k = KALFVM;
   lambda[k] = u - ca;
   scrh2 = beta_y*sqrt_1_2;
   scrh3 = beta_z*sqrt_1_2;

   RR[V2][k] = -scrh3;  
   RR[V3][k] =  scrh2;
   RR[B2][k] = -scrh3*sqrt_rho*S;   
   RR[B3][k] =  scrh2*sqrt_rho*S;   

   LL[k][V2] = RR[V2][k]; 
   LL[k][V3] = RR[V3][k]; 
   LL[k][B2] = RR[B2][k]*tau;
   LL[k][B3] = RR[B3][k]*tau; 

  /* -------------------------
      Alfven WAVE,  (u + c_a)
      (-R, -L of the eigenv 
      defined by Gardiner Stone)
     -------------------------  */

   k = KALFVP;
   lambda[k] = u + ca;
   RR[V2][k] =   RR[V2][KALFVM]; 
   RR[V3][k] =   RR[V3][KALFVM]; 
   RR[B2][k] = - RR[B2][KALFVM]; 
   RR[B3][k] = - RR[B3][KALFVM]; 

   LL[k][V2] =   LL[KALFVM][V2];
   LL[k][V3] =   LL[KALFVM][V3];
   LL[k][B2] = - LL[KALFVM][B2];
   LL[k][B3] = - LL[KALFVM][B3];

  /* ------------------------------------
      HotFix: when B=0 in a 2D plane and 
      only Bz != 0, enforce zero jumps in
      B2. This is useful with CharTracing
      since 2 equal and opposite jumps 
      may accumulate due to round-off.
      It also perfectly consistent, since
      no (Bx,By) can be generated.  
     ------------------------------------ */

   #if DIMENSIONS <= 2
    if (q[B1] == 0 && q[B2] == 0.0) 
      for (k = 0; k < NFLX; k++) RR[B2][k] = 0.0;
   #endif

  #endif


  #ifdef GLM_MHD

  /* -------------------------
      GLM wave,  -Ch_GLM
     -------------------------  */

   k = KPSI_GLMM;
   lambda[k] = -Ch_GLM;
   RR[B1][k]      =  1.0;
   RR[PSI_GLM][k] = -Ch_GLM;

   LL[k][B1]      =  0.5;
   LL[k][PSI_GLM] = -0.5/Ch_GLM;

  /* -------------------------
      GLM wave,  +Ch_GLM
     -------------------------  */

   k = KPSI_GLMP;
   lambda[k] = Ch_GLM;
   RR[B1][k]      =  1.0;
   RR[PSI_GLM][k] =  Ch_GLM;

   LL[k][B1]      =  0.5;
   LL[k][PSI_GLM] =  0.5/Ch_GLM;

  #endif

/* -----------------------------------------
         Check eigenvectors consistency
   ----------------------------------------- */

#if CHECK_EIGENVECTORS == YES
{
  int  i, j, l;
  int  ka, kb;
  real A[NFLX][NFLX], ALR[NFLX][NFLX];
  real da;

 /* --------------------------------
      make an imaginary jump dq = 2q
    -------------------------------- */

  for (i = 0; i < NFLX; i++){
  for (j = 0; j < NFLX; j++){
    A[i][j] = ALR[i][j] = 0.0;
  }}

  #if EOS == IDEAL

   A[DN][DN] = q[V1]    ; A[DN][V1] = q[DN];
   A[V1][V1] = q[V1]    ; A[V1][B2] =  q[B2]*tau; A[V1][B3] = q[B3]*tau; A[V1][PR] = tau;
   A[V2][V2] = q[V1]    ; A[V2][B2] = -q[B1]*tau; 
   A[V3][V3] = q[V1]    ; A[V3][B3] = -q[B1]*tau; 
   A[B2][V1] = q[B2]    ; A[B2][V2] = -q[B1]; A[B2][B1] = -q[V2]; A[B2][B2] = q[V1];
   A[B3][V1] = q[B3]    ; A[B3][V3] = -q[B1]; A[B3][B1] = -q[V3]; A[B3][B3] = q[V1];
   A[PR][V1] = gmm*q[PR]; A[PR][PR] =  q[V1];

  #elif EOS == ISOTHERMAL

   A[DN][DN] = q[V1]    ; A[DN][V1] = q[DN];
   A[V1][V1] = q[V1]    ; A[V1][B2] =  q[B2]*tau; A[V1][B3] = q[B3]*tau; 
   A[V1][DN] = tau*C_ISO2;
   A[V2][V2] = q[V1]    ; A[V2][B2] = -q[B1]*tau; 
   A[V3][V3] = q[V1]    ; A[V3][B3] = -q[B1]*tau; 
   A[B2][V1] = q[B2]    ; A[B2][V2] = -q[B1]; A[B2][B1] = -q[V2]; A[B2][B2] = q[V1];
   A[B3][V1] = q[B3]    ; A[B3][V3] = -q[B1]; A[B3][B1] = -q[V3]; A[B3][B3] = q[V1];

  #endif

  #ifdef GLM_MHD
   A[B1][PSI_GLM] = 1.0;
   A[PSI_GLM][B1] = Ch_GLM*Ch_GLM;
  #endif

  for (i = 0; i < NFLX; i++){
  for (j = 0; j < NFLX; j++){
    ALR[i][j] = 0.0;
    for (k = 0; k < NFLX; k++){
      ALR[i][j] += RR[i][k]*lambda[k]*LL[k][j];
    }
  }}

  for (i = 0; i < NFLX; i++){
  for (j = 0; j < NFLX; j++){

  /* ------------------------------------------
     NOTE: if the standard 7-wave formulation
           is adopted, the column and the row 
           corresponding B(normal) do not
           exists. However, ORION2 uses a 
           primitive matrix with 8 entries 
           and the B(normal) column contains 
           two entries (-vy, -vz) which cannot
           be recovered using a 7x7 system.
     ------------------------------------------ */    

    if (j == B1) continue;

    da = ALR[i][j] - A[i][j];
    if (fabs(da) > 1.e-6){
      print ("! different in element %d, %d,  diff = %12.6e\n",i,j, da);
      print ("! A[%d][%d] = %16.9e, ALR[%d][%d] = %16.9e\n",
                i,j, A[i][j], i,j,ALR[i][j]);
      print ("\n\n A = \n");
      for (ka = 0; ka < NFLX; ka++){
        for (kb = 0; kb < NFLX; kb++){
          print ("%12.3e   ", fabs(A[ka][kb]) > 1.e-8 ? A[ka][kb]:0.0);
        }
        print("\n");
      }

      print ("\n\n ALR = \n");
      for (ka = 0; ka < NFLX; ka++){
        for (kb = 0; kb < NFLX; kb++){
          print ("%12.3e   ", fabs(ALR[ka][kb]) > 1.e-8 ? ALR[ka][kb]:0.0);
        }
        print("\n");
      }
      QUIT_ORION2(1);
    }
  }}  

/*  Check eigenvector orthonormality   */

  for (ka = 0; ka < NFLX; ka++){
    for (kb = 0; kb < NFLX; kb++){

      #if MHD_FORMULATION == NONE || MHD_FORMULATION == FLUX_CT
       if (ka == KDIVB || kb == KDIVB) continue;
      #endif
      a = 0.0;
      for (i = 0; i < NFLX; i++){
        a += LL[ka][i]*RR[i][kb];
      }
      if (ka == kb && fabs(a-1.0)>1.e-8) {
        print (" ! Eigenvectors not orthogonal!  %d  %d  %12.6e \n",ka,kb,a);
        print ("DIR: %d\n",DIR);
        exit(1);
      }
      if(ka != kb && fabs(a)>1.e-8) {
        print (" ! Eigenvectors not orthogonal (2) %d  %d  %12.6e !\n",ka,kb,a);
        print ("DIR: %d\n",DIR);
        exit(1);
      }
    }
  }
}
#endif
}
#undef sqrt_1_2

/* **************************************************************************** */
void C_EIGENV (real *u, real *v, real **Lc, real **Rc, real *lambda)
/*
 *
 * Provide conservative eigenvectors for MHD equations
 * Reference paper:
 *
 *  "A High-order WENO Finite Difference Scheme for the Equations
 *   of Ideal MHD"
 *  Jiang,Wu, JCP 150,561 (1999)
 *
 *  With the following corrections:
 *
 *  The components (By, Bz) of  L_{1,7}  page 571 should
 *  be +\sqrt{rho}a\alpha_s instead of -.
 *  Also, since Bx is not considered as a parameter, 
 *  one must also add a component in the fast and slow waves.
 *  This can be seen by forming the conservative
 *  eigenvectors from the primitive ones, see the paper
 *  from Powell, Roe Linde.
 *
 *  
 *  The Lc_{1,3,5,7}[Bx] components, according to Serna 09
 *  are -(1-gmm)*a_{f,s}*Bx and not (1-gmm)*a_{f,s}*Bx. Both are 
 *  orthonormal though. She is WRONG!
 *  -Petros-
 **************************************************************************** */
{
  int    nv, i,j,k;
  double rho;
  double vx, vy, vz;
  double Bx, By, Bz;
  double bx, by, bz;
  double beta_y, beta_z;
  double one_sqrho;
  double cf2, cs2, ca2, a2, v2;
  double cf, cs, ca, a;
  double alpha_f, alpha_s;
  double g1, g2, tau, Gf, Ga, Gs;
  double scrh0, scrh1, S, bt2, b2, Btmag;
  double one_gmm;
  double LBX = 0.0;  /* setting LBX to 1.0 will include Powell's */
                     /* eight wave. Set it to 0 to recover the   */ 
                     /* standard 7 wave formulation              */  

  rho = v[DN];

  EXPAND (vx = v[V1];  ,
          vy = v[V2];  ,
          vz = v[V3];)

  EXPAND (Bx = v[B1];  ,
          By = v[B2];  ,
          Bz = v[B3];)
  one_sqrho = 1.0/sqrt(rho);
  
  S   = (Bx >= 0.0 ? 1.0 : -1.0);
  #if EOS == IDEAL
   a2 = gmm*v[PR]/rho;
  #elif EOS == ISOTHERMAL
   a2 = C_ISO2;
  #endif
 
  EXPAND(bx = Bx*one_sqrho;  ,
         by = By*one_sqrho;  ,
         bz = Bz*one_sqrho;)
    
  bt2   = EXPAND(0.0  , + by*by, + bz*bz);
  b2    = bx*bx + bt2;
  Btmag = sqrt(bt2*rho);
  v2    = EXPAND(vx*vx , + vy*vy, + vz*vz);

/* ------------------------------------------------------------
    Compute fast and slow magnetosonic speeds.

    The following expression appearing in the definitions
    of the fast magnetosonic speed 
    
     (a^2 - b^2)^2 + 4*a^2*bt^2 = (a^2 + b^2)^2 - 4*a^2*bx^2

    is always positive and avoids round-off errors.
   ------------------------------------------------------------ */
        
  scrh0 = a2 - b2;
  ca2   = bx*bx;
  scrh0 = scrh0*scrh0 + 4.0*bt2*a2;    
  scrh0 = sqrt(scrh0);    

  cf2 = 0.5*(a2 + b2 + scrh0); 
  cs2 = a2*ca2/cf2;   /* -- same as 0.5*(a2 + b2 - scrh0) -- */
    
  cf = sqrt(cf2);
  cs = sqrt(cs2);
  ca = sqrt(ca2);
  a  = sqrt(a2); 

  if (Btmag > 1.e-9) {
    SELECT(                     , 
           beta_y = dsign(By);  ,
           beta_y = By/Btmag; 
           beta_z = Bz/Btmag;)
  } else {
    SELECT(                       , 
           beta_y = 1.0;          ,
           beta_z = beta_y = sqrt(0.5);)
  }
    
  if (cf == cs) {
    alpha_f = 1.0;
    alpha_s = 0.0;
  }else if (a <= cs) {
    alpha_f = 0.0;
    alpha_s = 1.0;
  }else if (cf <= a){
    alpha_f = 1.0;
    alpha_s = 0.0;
  }else{ 
    scrh0   = 1.0/(cf2 - cs2);
    alpha_f = (a2  - cs2)*scrh0;
    alpha_s = (cf2 -  a2)*scrh0;
    alpha_f = dmax(0.0, alpha_f);
    alpha_s = dmax(0.0, alpha_s);
    alpha_f = sqrt(alpha_f);
    alpha_s = sqrt(alpha_s);
  }

/* --------------------------------------------------------
    Compute non-zero entries of conservative
    eigenvectors (Rc, Lc).
   -------------------------------------------------------- */
 
  #if EOS != ISOTHERMAL
   one_gmm = 1.0 - gmm;
   g1  = 0.5*(gmm - 1.0);
   g2  = (gmm - 2.0)/(gmm - 1.0);
   tau = (gmm - 1.0)/a2;
  #elif EOS == ISOTHERMAL 
   one_gmm = g1 = tau = 0.0;
  #endif   

  scrh0 = EXPAND(0.0, + beta_y*vy, + beta_z*vz);
  Gf = alpha_f*cf*vx - alpha_s*cs*S*scrh0;
  Ga = EXPAND(0.0,    , + S*(beta_z*vy - beta_y*vz));
  Gs = alpha_s*cs*vx + alpha_f*cf*S*scrh0;
  
/* -----------------------
    FAST WAVE  (u - c_f) 
   ----------------------- */

  k = KFASTM;
  lambda[k] = vx - cf;

  scrh0 = alpha_s*cs*S;
  scrh1 = alpha_s*a*one_sqrho;
  Rc[DN][k] = alpha_f;
  EXPAND( Rc[M1][k] = alpha_f*lambda[k];          ,
          Rc[M2][k] = alpha_f*vy + scrh0*beta_y;  ,
          Rc[M3][k] = alpha_f*vz + scrh0*beta_z; ) 
  EXPAND(                            ,                                
          Rc[B2][k] = scrh1*beta_y;  ,
          Rc[B3][k] = scrh1*beta_z; )
  #if EOS == IDEAL
   Rc[EN][k] = alpha_f*(0.5*v2 + cf2 - g2*a2) - Gf;
  #endif

  Lc[k][DN] = (g1*alpha_f*v2 + Gf)*0.5/a2; 

  #if EOS == ISOTHERMAL
   Lc[k][DN] += alpha_f*0.5;
  #endif
  
  EXPAND( Lc[k][M1] = (one_gmm*alpha_f*vx - alpha_f*cf)  *0.5/a2;  ,
          Lc[k][M2] = (one_gmm*alpha_f*vy + scrh0*beta_y)*0.5/a2;  ,
          Lc[k][M3] = (one_gmm*alpha_f*vz + scrh0*beta_z)*0.5/a2;) 
  EXPAND( Lc[k][B1] =  LBX*one_gmm*alpha_f*Bx*0.5/a2;                      ,                          
          Lc[k][B2] = (one_gmm*alpha_f*By + scrh1*rho*beta_y)*0.5/a2;  ,
          Lc[k][B3] = (one_gmm*alpha_f*Bz + scrh1*rho*beta_z)*0.5/a2; )
  #if EOS == IDEAL
   Lc[k][EN] = alpha_f*(gmm - 1.0)*0.5/a2;
  #endif

  
  /* -----------------------
      FAST WAVE  (u + c_f) 
     ----------------------- */

  k = KFASTP;
  lambda[k] = vx + cf;

  Rc[DN][k] = alpha_f;
  EXPAND( Rc[M1][k] = alpha_f*lambda[k];          ,
          Rc[M2][k] = alpha_f*vy - scrh0*beta_y;  ,
          Rc[M3][k] = alpha_f*vz - scrh0*beta_z; ) 
  EXPAND(                            ,                                
          Rc[B2][k] = scrh1*beta_y;  ,
          Rc[B3][k] = scrh1*beta_z; )
  #if EOS == IDEAL
   Rc[EN][k] = alpha_f*(0.5*v2 + cf2 - g2*a2) + Gf;
  #endif

  Lc[k][DN] = (g1*alpha_f*v2 - Gf)*0.5/a2; 

  #if EOS == ISOTHERMAL
   Lc[k][DN] += alpha_f*0.5;
  #endif
  
  EXPAND( Lc[k][M1] = (one_gmm*alpha_f*vx + alpha_f*cf)  *0.5/a2;  ,
          Lc[k][M2] = (one_gmm*alpha_f*vy - scrh0*beta_y)*0.5/a2;  ,
          Lc[k][M3] = (one_gmm*alpha_f*vz - scrh0*beta_z)*0.5/a2;) 
  EXPAND( Lc[k][B1] =  LBX*one_gmm*alpha_f*Bx*0.5/a2;                                ,                                
          Lc[k][B2] = (one_gmm*alpha_f*By + sqrt(rho)*a*alpha_s*beta_y)*0.5/a2;  ,
          Lc[k][B3] = (one_gmm*alpha_f*Bz + sqrt(rho)*a*alpha_s*beta_z)*0.5/a2; )
  #if EOS == IDEAL
   Lc[k][EN] = alpha_f*(gmm - 1.0)*0.5/a2;
  #endif
  

  /* -----------------------
      Entropy wave  (u) 
     ----------------------- */

  #if EOS == IDEAL
   k = KENTRP;
   lambda[k] = vx;

   Rc[DN][k] = 1.0;
   EXPAND( Rc[M1][k] = vx; ,
           Rc[M2][k] = vy; ,
           Rc[M3][k] = vz; )
   Rc[EN][k] = 0.5*v2;

   Lc[k][DN] = 1.0 - 0.5*tau*v2;
   EXPAND(Lc[k][V1] = tau*vx;  ,
          Lc[k][V2] = tau*vy;  ,
          Lc[k][V3] = tau*vz;)
   EXPAND(Lc[k][B1] = LBX*tau*Bx;  ,
          Lc[k][B2] = tau*By;  ,
          Lc[k][B3] = tau*Bz;)
   Lc[k][EN] = -tau;
  #endif

  /* --------------------------------
            div.B wave  (u) 
     -------------------------------- */

  #ifndef GLM_MHD
   k = KDIVB;
   #if MHD_FORMULATION == EIGHT_WAVES
    lambda[k] = vx;
    Rc[B1][k] = 1.0;
    Lc[k][B1] = 1.0;
   #else
    lambda[k] = 0.0;
    Rc[B1][k] = Lc[k][B1] = 0.0;
   #endif
  #endif
    
  #if COMPONENTS > 1    

 /* -----------------------
     SLOW WAVE  (u - c_s) 
    ----------------------- */

   k = KSLOWM;
   lambda[k] = vx - cs;
   scrh0 = alpha_f*cf*S;

   Rc[DN][k] = alpha_s;
   EXPAND( Rc[M1][k] = alpha_s*lambda[k];          ,
           Rc[M2][k] = alpha_s*vy - scrh0*beta_y;  ,
           Rc[M3][k] = alpha_s*vz - scrh0*beta_z; ) 
   EXPAND(                                            ,                                
           Rc[B2][k] = - alpha_f*a*beta_y*one_sqrho;  ,
           Rc[B3][k] = - alpha_f*a*beta_z*one_sqrho; )

   #if EOS == IDEAL
    Rc[EN][k] = alpha_s*(0.5*v2 + cs2 - g2*a2) - Gs;
   #endif

   Lc[k][DN] = (g1*alpha_s*v2 + Gs)*0.5/a2; 
   
   #if EOS == ISOTHERMAL
    Lc[k][DN] += alpha_s*0.5;
   #endif
   
   EXPAND( Lc[k][M1] = (one_gmm*alpha_s*vx - alpha_s*cs)  *0.5/a2;  ,
           Lc[k][M2] = (one_gmm*alpha_s*vy - scrh0*beta_y)*0.5/a2;  ,
           Lc[k][M3] = (one_gmm*alpha_s*vz - scrh0*beta_z)*0.5/a2;) 
   EXPAND( Lc[k][B1] = LBX*one_gmm*alpha_s*Bx*0.5/a2;                                ,                                
           Lc[k][B2] = (one_gmm*alpha_s*By - sqrt(rho)*a*alpha_f*beta_y)*0.5/a2;  ,
           Lc[k][B3] = (one_gmm*alpha_s*Bz - sqrt(rho)*a*alpha_f*beta_z)*0.5/a2; )
   #if EOS == IDEAL
    Lc[k][EN] = alpha_s*(gmm - 1.0)*0.5/a2;
   #endif
   
   
   /* -----------------------
       SLOW WAVE  (u + c_s) 
      ----------------------- */

   k = KSLOWP;
   lambda[k] = vx + cs; 

   Rc[DN][k] = alpha_s;
   EXPAND( Rc[M1][k] = alpha_s*lambda[k];          ,
           Rc[M2][k] = alpha_s*vy + scrh0*beta_y;  ,
           Rc[M3][k] = alpha_s*vz + scrh0*beta_z; ) 
   EXPAND(                                            ,                                
           Rc[B2][k] = - alpha_f*a*beta_y*one_sqrho;  ,
           Rc[B3][k] = - alpha_f*a*beta_z*one_sqrho; )

   #if EOS == IDEAL
    Rc[EN][k] = alpha_s*(0.5*v2 + cs2 - g2*a2) + Gs;
   #endif

   Lc[k][DN] = (g1*alpha_s*v2 - Gs)*0.5/a2; 

   #if EOS == ISOTHERMAL
    Lc[k][DN] += alpha_s*0.5;
   #endif
   
   EXPAND( Lc[k][M1] = (one_gmm*alpha_s*vx + alpha_s*cs)  *0.5/a2;  ,
           Lc[k][M2] = (one_gmm*alpha_s*vy + scrh0*beta_y)*0.5/a2;  ,
           Lc[k][M3] = (one_gmm*alpha_s*vz + scrh0*beta_z)*0.5/a2;) 
   EXPAND( Lc[k][B1] = LBX*one_gmm*alpha_s*Bx*0.5/a2;                                 ,               
           Lc[k][B2] = (one_gmm*alpha_s*By - sqrt(rho)*a*alpha_f*beta_y)*0.5/a2;  ,
           Lc[k][B3] = (one_gmm*alpha_s*Bz - sqrt(rho)*a*alpha_f*beta_z)*0.5/a2; )
   #if EOS == IDEAL
    Lc[k][EN] = alpha_s*(gmm - 1.0)*0.5/a2;
   #endif
   
  
  #endif

  #if COMPONENTS == 3

 /* ------------------------
     Alfven WAVE  (u - c_a) 
    ------------------------ */

   k = KALFVM;
   lambda[k] = vx - ca;

   Rc[M2][k] = - beta_z*S;  
   Rc[M3][k] = + beta_y*S;
   Rc[B2][k] = - beta_z*one_sqrho;
   Rc[B3][k] =   beta_y*one_sqrho;
   #if EOS == IDEAL
    Rc[EN][k] = - Ga;
   #endif

   Lc[k][DN] = 0.5*Ga;
   Lc[k][M2] = - 0.5*beta_z*S;
   Lc[k][M3] =   0.5*beta_y*S;
   Lc[k][B2] = - 0.5*sqrt(rho)*beta_z;
   Lc[k][B3] =   0.5*sqrt(rho)*beta_y;

 /* -----------------------
     Alfven WAVE  (u + c_a) 
    ----------------------- */

   k = KALFVP;
   lambda[k] = vx + ca;

   Rc[M2][k] = Rc[M2][KALFVM];  
   Rc[M3][k] = Rc[M3][KALFVM];
   Rc[B2][k] = - Rc[B2][KALFVM];   
   Rc[B3][k] = - Rc[B3][KALFVM];
   #if EOS == IDEAL
    Rc[EN][k] = Rc[EN][KALFVM];
   #endif

   Lc[k][DN] =   Lc[KALFVM][DN];
   Lc[k][M2] =   Lc[KALFVM][M2];
   Lc[k][M3] =   Lc[KALFVM][M3];
   Lc[k][B2] = - Lc[KALFVM][B2];
   Lc[k][B3] = - Lc[KALFVM][B3];
   #if EOS == IDEAL
    Lc[k][EN] =   Lc[KALFVM][EN];
   #endif

  #endif

  #ifdef GLM_MHD

  /* -------------------------
      GLM wave,  -Ch_GLM
     -------------------------  */

   k = KPSI_GLMM;
   lambda[k] = -Ch_GLM;
   Rc[B1][k]      =  1.0;
   Rc[PSI_GLM][k] = -Ch_GLM;

   Lc[k][B1]      =  0.5;
   Lc[k][PSI_GLM] = -0.5/Ch_GLM;

  /* -------------------------
      GLM wave,  +Ch_GLM
     -------------------------  */

   k = KPSI_GLMP;
   lambda[k] = Ch_GLM;
   Rc[B1][k]      =  1.0;
   Rc[PSI_GLM][k] =  Ch_GLM;

   Lc[k][B1]      =  0.5;
   Lc[k][PSI_GLM] =  0.5/Ch_GLM;

  #endif


/*
for (k = 0; k < NVAR; k++){
for (i = 0; i < NVAR; i++){
  if (fabs(Lc2[k][i] - Lc[k][i]) > 1.e-4){
    printf ("L's not equal, %d %d,  %12.6e  %12.6e\n",k,i,Lc2[k][i],Lc[k][i]);
    PRINTROW(Lc2,k);
    PRINTROW(Lc,k);

    exit(1);
  }
  if (fabs(Rc2[k][i] - Rc[k][i]) > 1.e-4){
    printf ("R's not equal, %d %d,  %12.6e  %12.6e\n",k,i,Rc2[k][i],Rc[k][i]);
    PRINTCOL(Rc2,i);
    PRINTCOL(Rc,i);
    exit(1);
  }
}}
*/

/* -----------------------------------------
         Check eigenvectors consistency
   ----------------------------------------- */

#if CHECK_EIGENVECTORS == YES
{
  double AA[NFLX][NFLX], Aw1[NFLX], a;
  int ii, jj,kk;

  for (ii = 0; ii <NFLX;ii++){
    for (jj = 0; jj <NFLX;jj++){
      a = 0.0;
      for (kk = 0; kk <NFLX;kk++){
        a += Lc[jj][kk]*Rc[kk][ii];
      }
      if (ii==jj && fabs(a-1.0)>1.e-8) {
        print ("! Eigenvectors not orthogonal!  %d  %d  %12.6e \n",ii,jj,a);
        print ("DIR: %d\n",DIR);
        exit(1);
      }
      if(ii!=jj && fabs(a)>1.e-8) {
        print ("! Eigenvectors not orthogonal (2) %d  %d  %12.6e !\n",ii,jj,a);
        print ("DIR: %d\n",DIR);
        exit(1);
      }
    }
  }
}
#endif
}

/* ********************************************************** */
void LpPROJECT (double **Lp, double *v, double *w)
/*
 *
 *  Compute the matrix-vector product between the
 *  the L matrix (containing primitive left eigenvectors) 
 *  and the vector v. The result containing the characteristic
 *  variables is stored in the vector w = L.v
 *
 *  For efficiency purpose, multiplication is done 
 *  explicitly, so that only nonzero entries
 *  of the left primitive eigenvectors are considered.
 *  
 *
 ************************************************************ */
{
  int    k;
  double wv, wB, *L;

/* -- fast waves -- */

  L = Lp[KFASTM];
  wv = EXPAND(L[V1]*v[V1], + L[V2]*v[V2], + L[V3]*v[V3]); 
  #if EOS == IDEAL
   wB = EXPAND(L[PR]*v[PR], + L[B2]*v[B2], + L[B3]*v[B3]); 
  #elif EOS == ISOTHERMAL
   wB = EXPAND(L[DN]*v[DN], + L[B2]*v[B2], + L[B3]*v[B3]); 
  #endif
  w[KFASTM] =  wv + wB;
  w[KFASTP] = -wv + wB;

/* -- entropy -- */

  #if EOS == IDEAL
   L = Lp[KENTRP];
   w[KENTRP] = L[DN]*v[DN] + L[PR]*v[PR];
  #endif

  #ifndef GLM_MHD
   L = Lp[KDIVB];
   #if MHD_FORMULATION == EIGHT_WAVES
    w[KDIVB] = v[B1];
   #else
    w[KDIVB] = 0.0;
   #endif
  #endif

  #if COMPONENTS > 1
   L = Lp[KSLOWM];
   wv = EXPAND(L[V1]*v[V1], + L[V2]*v[V2], + L[V3]*v[V3]); 

   #if EOS == IDEAL
    wB = EXPAND(L[PR]*v[PR], + L[B2]*v[B2], + L[B3]*v[B3]); 
   #elif EOS == ISOTHERMAL
    wB = EXPAND(L[DN]*v[DN], + L[B2]*v[B2], + L[B3]*v[B3]); 
   #endif

   w[KSLOWM] =  wv + wB;
   w[KSLOWP] = -wv + wB;

   #if COMPONENTS == 3
    L = Lp[KALFVM];
    wv = L[V2]*v[V2] + L[V3]*v[V3]; 
    wB = L[B2]*v[B2] + L[B3]*v[B3]; 
    w[KALFVM] = wv + wB;
    w[KALFVP] = wv - wB;
   #endif
  #endif
  
  #ifdef GLM_MHD
   L = Lp[KPSI_GLMP];
   w[KPSI_GLMM] = L[B1]*v[B1] - L[PSI_GLM]*v[PSI_GLM];
   w[KPSI_GLMP] = L[B1]*v[B1] + L[PSI_GLM]*v[PSI_GLM];

  #endif

  /* ----------------------------------------------- 
       For passive scalars, the characteristic 
       variable is equal to the primitive one, 
       since  l = r = (0,..., 1 , 0 ,....)
     ----------------------------------------------- */		   

  #if NFLX != NVAR
   for (k = NFLX; k < NVAR; k++){
     w[k] = v[k];
   }
  #endif   
}

void LcWAVE (double **Lc, double **uc, double *w)
{
  int k,nv;

  for (k = 0; k < NVAR; k++){
    w[k] = 0.0;
    for (nv = 0; nv < NVAR; nv++){
      w[k] += Lc[k][nv]*uc[k][nv];
    }
  }
}





int PRINTCOL (double **A, int n)
{
  int k;
  for (k = 0; k < NVAR; k++){ 
    printf ("%12.6e  ",A[k][n]);
  }
  printf ("\n");
  return(0);
}
int PRINTROW (double **A, int n)
{
  int k;
  for (k = 0; k < NVAR; k++){ 
    printf ("%12.6e  ",A[n][k]);
  }
  printf ("\n");
  return(0);
}



/* *********************************************************************** */
void PRIM_EIGENVECTORS(double *q, double a2, double h, double *lambda,
                       double **LL, double **RR)
/*
 *
 *  PURPOSE
 *
 *    provide left and right eigenvectors and corresponding 
 *    eigenvalues for the PRIMITIVE form of the MHD equations 
 *    (adiabatic & isothermal cases).
 *    
 *    It is highly recommended that LL and RR be initialized to 
 *    zero *BEFORE* since only non-zero entries are treated here. 
 *
 *    Wave names and their order are defined as enumeration 
 *    constants in mod_defs.h. 
 *    Notice that, the characteristic decomposition may differ 
 *    depending on the way div.B is treated.
 * 
 *    Advection modes associated with passive scalars are simple cases 
 *    for which lambda = u (entropy mode) and l = r = (0, ... , 1, 0, ...). 
 *    For this reason they are NOT defined here and must be treated 
 *    seperately.
 *
 * ----------------------------------------------------------------
 *  Reference papers:
 *
 *  "Notes on the Eigensystem of Magnetohydrodynamics",
 *  P.L. Roe, D.S. Balsara,
 *  SIAM Journal on Applied Mathematics, 56, 57 (1996)
 *
 * "A solution adaptive upwind scheme for ideal MHD",
 *  K. G. Powell, P. L. Roe, and T. J. Linde,
 *  Journal of Computational Physics, 154, 284-309, (1999).
 *
 *  "ATHENA: A new code for astrophysical MHD",
 *  J. Stone, T. Gardiner, 
 *  ApJS, 178, 137 (2008)
 *
 *  --------------------------------------------------------------- 
 * 
 * The derivation of the isothermal eigenvectors follows the
 * consideration given in roe.c
 *
 * 
 * Last Modified:  Nov 16 2009 by A. Mignone (mignone@ph.unito.it)
 *
 *********************************************************************** */
#define  sqrt_1_2  (0.70710678118654752440)
{
  int  k;
  real scrh0, scrh1, scrh2, scrh3, scrh4;
  real u, a, ca2, cf2, cs2;
  real cs, ca, cf, b2, A2, At2;
  real tau, S;
  real alpha_f, alpha_s, beta_y, beta_z;
  real sqrt_rho;

  u   = q[V1];
  tau = 1.0/q[DN];
  sqrt_rho = sqrt(q[DN]);

  scrh2 = q[B1]*q[B1];                                 /*  Bx^2 */
  scrh3 = EXPAND(0.0, + q[B2]*q[B2],  + q[B3]*q[B3]);  /*  Bt^2 */

  b2  = scrh2 + scrh3;     /*  B^2 = Bx^2 + Bt^2 */
  ca2 = scrh2*tau;         /*  Bx^2/rho          */
  A2  = b2*tau;            /*  B^2/rho           */
  At2 = scrh3*tau;         /*  Bt^2/rho           */

  scrh1 = a2 - A2;
  scrh0 = sqrt(scrh1*scrh1 + 4.0*a2*At2);  /* sqrt( (g*p/rho-B^2/rho)^2 
                                                   + 4*g*p/rho*Bt^2/rho)  */
/* --  Now get fast and slow speeds -- */
    
  cf2 = 0.5*(a2 + A2 + scrh0);
  cs2 = a2*ca2/cf2;

  cf = sqrt(cf2);
  cs = sqrt(cs2);
  ca = sqrt(ca2);
  a  = sqrt(a2);

  if (cf == cs) {
    alpha_f = 1.0;
    alpha_s = 0.0;
  }else{
    scrh0   = 1.0/scrh0;
    alpha_f = (a2 - cs2)*scrh0;
    alpha_s = (cf2 - a2)*scrh0;

    alpha_f = dmax(0.0, alpha_f);
    alpha_s = dmax(0.0, alpha_s);

    alpha_f = sqrt(alpha_f);
    alpha_s = sqrt(alpha_s);
  }

  scrh0 = sqrt(scrh3);

  if (scrh0 > 1.e-9) {
    SELECT (                        , 
            beta_y = dsign(q[B2]);  ,
            beta_y = q[B2] / scrh0; 
            beta_z = q[B3] / scrh0;)
  } else {
    SELECT (                  , 
            beta_y = 1.0;     ,
            beta_z = beta_y = sqrt_1_2;)
  }

  S = (q[B1] >= 0.0 ? 1.0 : -1.0);

/*  ------------------------------------------------------------
     define primitive right and left eigenvectors (RR and LL),
     for all of the 8 waves;
     left eigenvectors for fast & slow waves can be defined
     in terms of right eigenvectors (see page 296)  
    ------------------------------------------------------------  */

  /* -------------------------
      FAST WAVE,  (u - c_f)
     -------------------------  */

  k = KFASTM;  
  lambda[k] = u - cf;
  scrh0 = alpha_s*cs*S;
  scrh1 = alpha_s*sqrt_rho*a;
  scrh2 = 0.5 / a2;
  scrh3 = scrh2*tau;

  RR[k][DN] = q[DN]*alpha_f;        
  EXPAND(RR[k][V1] = -cf*alpha_f;   ,
         RR[k][V2] = scrh0*beta_y;  ,
         RR[k][V3] = scrh0*beta_z;)
  EXPAND(                           , 
         RR[k][B2] = scrh1*beta_y;  ,
         RR[k][B3] = scrh1*beta_z;)

  #if EOS == IDEAL
   scrh4 = alpha_f*gmm*q[PR];
   RR[k][PR] = scrh4;
  #endif

  #if EOS == ISOTHERMAL
   LL[k][DN] = 0.5*alpha_f/q[DN];
  #endif
  EXPAND(LL[k][V1] = RR[k][V1]*scrh2; ,
         LL[k][V2] = RR[k][V2]*scrh2; ,
         LL[k][V3] = RR[k][V3]*scrh2;) 
  EXPAND(                             , 
         LL[k][B2] = RR[k][B2]*scrh3; ,
         LL[k][B3] = RR[k][B3]*scrh3;)
  #if EOS == IDEAL
   LL[k][PR] = alpha_f*scrh3;
  #endif

  /* -------------------------
      FAST WAVE,  (u + c_f)
     -------------------------  */

  k = KFASTP; 
  lambda[k] = u + cf;
  RR[k][DN] = RR[KFASTM][DN];
  EXPAND(RR[k][V1] = -RR[KFASTM][V1];  ,
         RR[k][V2] = -RR[KFASTM][V2];  ,
         RR[k][V3] = -RR[KFASTM][V3];)
  EXPAND(                             ,
         RR[k][B2] = RR[KFASTM][B2];  ,
         RR[k][B3] = RR[KFASTM][B3];)
  #if EOS == IDEAL
   RR[k][PR] = RR[KFASTM][PR];
  #endif

  #if EOS == ISOTHERMAL
   LL[k][DN] = LL[KFASTM][DN];
  #endif
  EXPAND(LL[k][V1] = -LL[KFASTM][V1];  ,
         LL[k][V2] = -LL[KFASTM][V2];  ,
         LL[k][V3] = -LL[KFASTM][V3];) 
  EXPAND(                              ,                         
         LL[k][B2] = LL[KFASTM][B2];  ,
         LL[k][B3] = LL[KFASTM][B3];)
  #if EOS == IDEAL
   LL[k][PR] = LL[KFASTM][PR]; 
  #endif

  /* -------------------------
      entropy wave,  (u) only
      in ideal MHD
     -------------------------  */

  #if EOS == IDEAL 
   k = KENTRP;
   lambda[k] = u;
   RR[k][DN] =   1.0;
   LL[k][DN] =   1.0; 
   LL[k][PR] = - 1.0/a2;
  #endif

  /* -------------------------
        magnetic flux, (u)
     -------------------------  */

  #ifndef GLM_MHD
   k = KDIVB;
   #if MHD_FORMULATION == EIGHT_WAVES
    lambda[k] = u;
    RR[k][B1] = 1.0;
    LL[k][B1] = 1.0;
   #else  
    lambda[k] = 0.0;
   #endif
  #endif

  #if COMPONENTS > 1

  /* -------------------------
      SLOW WAVE,  (u - c_s)
     -------------------------  */

   k = KSLOWM;
   lambda[k] = u - cs;
   scrh0 = alpha_f*cf*S;
   scrh1 = alpha_f*sqrt_rho*a;

   RR[k][DN] = q[DN]*alpha_s;
   EXPAND(RR[k][V1] = -cs*alpha_s;     ,
          RR[k][V2] = -scrh0*beta_y;   ,
          RR[k][V3] = -scrh0*beta_z;)
   EXPAND(                            ,
          RR[k][B2] = -scrh1*beta_y;  ,
          RR[k][B3] = -scrh1*beta_z;)
   #if EOS == IDEAL
    scrh4 = alpha_s*gmm*q[PR]; 
    RR[k][PR] = scrh4;
   #endif

   #if EOS == ISOTHERMAL
    LL[k][DN] = 0.5*alpha_s/q[DN];
   #endif
   EXPAND(LL[k][V1] = RR[k][V1]*scrh2; ,
          LL[k][V2] = RR[k][V2]*scrh2; ,
          LL[k][V3] = RR[k][V3]*scrh2;) 
   EXPAND(                             ,
          LL[k][B2] = RR[k][B2]*scrh3; ,
          LL[k][B3] = RR[k][B3]*scrh3;) 

   #if EOS == IDEAL
    LL[k][PR] = alpha_s*scrh3;
   #endif

  /* -------------------------
      SLOW WAVE,  (u + c_s)
     -------------------------  */

   k = KSLOWP;
   lambda[k] = u + cs;

   RR[k][DN] = RR[KSLOWM][DN];
   EXPAND(RR[k][V1] = -RR[KSLOWM][V1];   ,
          RR[k][V2] = -RR[KSLOWM][V2];   ,
          RR[k][V3] = -RR[KSLOWM][V3];)

   EXPAND(                             ,
          RR[k][B2] = RR[KSLOWM][B2];  ,
          RR[k][B3] = RR[KSLOWM][B3];)
   #if EOS == IDEAL
    RR[k][PR] = scrh4;
   #endif

   #if EOS == ISOTHERMAL
    LL[k][DN] = LL[KSLOWM][DN];
   #endif
   EXPAND(LL[k][V1] = -LL[KSLOWM][V1];   ,
          LL[k][V2] = -LL[KSLOWM][V2];   ,
          LL[k][V3] = -LL[KSLOWM][V3];) 
   EXPAND(                               ,
          LL[k][B2] = LL[KSLOWM][B2];   ,
          LL[k][B3] = LL[KSLOWM][B3];) 

   #if EOS == IDEAL
    LL[k][PR] = LL[KSLOWM][PR];
   #endif
  #endif

  #if COMPONENTS == 3

  /* -------------------------
      Alfven WAVE,  (u - c_a)
     -------------------------  */
   
   k = KALFVM;
   lambda[k] = u - ca;
   scrh2 = beta_y*sqrt_1_2;
   scrh3 = beta_z*sqrt_1_2;

   RR[k][V2] = -scrh3;  
   RR[k][V3] =  scrh2;
   RR[k][B2] = -scrh3*sqrt_rho*S;   
   RR[k][B3] =  scrh2*sqrt_rho*S;   

   LL[k][V2] = RR[k][V2]; 
   LL[k][V3] = RR[k][V3]; 
   LL[k][B2] = RR[k][B2]*tau;
   LL[k][B3] = RR[k][B3]*tau; 

  /* -------------------------
      Alfven WAVE,  (u + c_a)
      (-R, -L of the eigenv 
      defined by Gardiner Stone)
     -------------------------  */

   k = KALFVP;
   lambda[k] = u + ca;
   RR[k][V2] =   RR[KALFVM][V2]; 
   RR[k][V3] =   RR[KALFVM][V3]; 
   RR[k][B2] = - RR[KALFVM][B2]; 
   RR[k][B3] = - RR[KALFVM][B3]; 

   LL[k][V2] =   LL[KALFVM][V2];
   LL[k][V3] =   LL[KALFVM][V3];
   LL[k][B2] = - LL[KALFVM][B2];
   LL[k][B3] = - LL[KALFVM][B3];

  /* ------------------------------------
      HotFix: when B=0 in a 2D plane and 
      only Bz != 0, enforce zero jumps in
      B2. This is useful with CharTracing
      since 2 equal and opposite jumps 
      may accumulate due to round-off.
      It also perfectly consistent, since
      no (Bx,By) can be generated.  
     ------------------------------------ */

   #if DIMENSIONS <= 2
    if (q[B1] == 0 && q[B2] == 0.0) 
      for (k = 0; k < NFLX; k++) RR[k][B2] = 0.0;
   #endif

  #endif


  #ifdef GLM_MHD

  /* -------------------------
      GLM wave,  -Ch_GLM
     -------------------------  */

   k = KPSI_GLMM;
   lambda[k] = -Ch_GLM;
   RR[k][B1]      =  1.0;
   RR[k][PSI_GLM] = -Ch_GLM;

   LL[k][B1]      =  0.5;
   LL[k][PSI_GLM] = -0.5/Ch_GLM;

  /* -------------------------
      GLM wave,  +Ch_GLM
     -------------------------  */

   k = KPSI_GLMP;
   lambda[k] = Ch_GLM;
   RR[k][B1]      =  1.0;
   RR[k][PSI_GLM] =  Ch_GLM;

   LL[k][B1]      =  0.5;
   LL[k][PSI_GLM] =  0.5/Ch_GLM;

  #endif

/* -----------------------------------------
         Check eigenvectors consistency
   ----------------------------------------- */

#if CHECK_EIGENVECTORS == YES
{
  int  i, j, l;
  int  ka, kb;
  real A[NFLX][NFLX], ALR[NFLX][NFLX];
  real da;

 /* --------------------------------
      make an imaginary jump dq = 2q
    -------------------------------- */

  for (i = 0; i < NFLX; i++){
  for (j = 0; j < NFLX; j++){
    A[i][j] = ALR[i][j] = 0.0;
  }}

  #if EOS == IDEAL

   A[DN][DN] = q[V1]    ; A[DN][V1] = q[DN];
   A[V1][V1] = q[V1]    ; A[V1][B2] =  q[B2]*tau; A[V1][B3] = q[B3]*tau; A[V1][PR] = tau;
   A[V2][V2] = q[V1]    ; A[V2][B2] = -q[B1]*tau; 
   A[V3][V3] = q[V1]    ; A[V3][B3] = -q[B1]*tau; 
   A[B2][V1] = q[B2]    ; A[B2][V2] = -q[B1]; A[B2][B1] = -q[V2]; A[B2][B2] = q[V1];
   A[B3][V1] = q[B3]    ; A[B3][V3] = -q[B1]; A[B3][B1] = -q[V3]; A[B3][B3] = q[V1];
   A[PR][V1] = gmm*q[PR]; A[PR][PR] =  q[V1];

  #elif EOS == ISOTHERMAL

   A[DN][DN] = q[V1]    ; A[DN][V1] = q[DN];
   A[V1][V1] = q[V1]    ; A[V1][B2] =  q[B2]*tau; A[V1][B3] = q[B3]*tau; 
   A[V1][DN] = tau*C_ISO2;
   A[V2][V2] = q[V1]    ; A[V2][B2] = -q[B1]*tau; 
   A[V3][V3] = q[V1]    ; A[V3][B3] = -q[B1]*tau; 
   A[B2][V1] = q[B2]    ; A[B2][V2] = -q[B1]; A[B2][B1] = -q[V2]; A[B2][B2] = q[V1];
   A[B3][V1] = q[B3]    ; A[B3][V3] = -q[B1]; A[B3][B1] = -q[V3]; A[B3][B3] = q[V1];

  #endif

  #ifdef GLM_MHD
   A[B1][PSI_GLM] = 1.0;
   A[PSI_GLM][B1] = Ch_GLM*Ch_GLM;
  #endif

  for (i = 0; i < NFLX; i++){
  for (j = 0; j < NFLX; j++){
    ALR[i][j] = 0.0;
    for (k = 0; k < NFLX; k++){
      ALR[i][j] += RR[k][i]*lambda[k]*LL[k][j];
    }
  }}

  for (i = 0; i < NFLX; i++){
  for (j = 0; j < NFLX; j++){

  /* ------------------------------------------
     NOTE: if the standard 7-wave formulation
           is adopted, the column and the row 
           corresponding B(normal) do not
           exists. However, ORION2 uses a 
           primitive matrix with 8 entries 
           and the B(normal) column contains 
           two entries (-vy, -vz) which cannot
           be recovered using a 7x7 system.
     ------------------------------------------ */    

    if (j == B1) continue;

    da = ALR[i][j] - A[i][j];
    if (fabs(da) > 1.e-6){
      print ("! different in element %d, %d,  diff = %12.6e\n",i,j, da);
      print ("! A[%d][%d] = %16.9e, ALR[%d][%d] = %16.9e\n",
                i,j, A[i][j], i,j,ALR[i][j]);
      print ("\n\n A = \n");
      for (ka = 0; ka < NFLX; ka++){
        for (kb = 0; kb < NFLX; kb++){
          print ("%12.3e   ", fabs(A[ka][kb]) > 1.e-8 ? A[ka][kb]:0.0);
        }
        print("\n");
      }

      print ("\n\n ALR = \n");
      for (ka = 0; ka < NFLX; ka++){
        for (kb = 0; kb < NFLX; kb++){
          print ("%12.3e   ", fabs(ALR[ka][kb]) > 1.e-8 ? ALR[ka][kb]:0.0);
        }
        print("\n");
      }
      QUIT_ORION2(1);
    }
  }}  

/*  Check eigenvector orthonormality   */

  for (ka = 0; ka < NFLX; ka++){
    for (kb = 0; kb < NFLX; kb++){

      #if MHD_FORMULATION == NONE || MHD_FORMULATION == FLUX_CT
       if (ka == KDIVB || kb == KDIVB) continue;
      #endif
      a = 0.0;
      for (i = 0; i < NFLX; i++){
        a += LL[ka][i]*RR[kb][i];
      }
      if (ka == kb && fabs(a-1.0)>1.e-8) {
        print (" ! Eigenvectors not orthogonal!  %d  %d  %12.6e \n",ka,kb,a);
        print ("DIR: %d\n",DIR);
        exit(1);
      }
      if(ka != kb && fabs(a)>1.e-8) {
        print (" ! Eigenvectors not orthogonal (2) %d  %d  %12.6e !\n",ka,kb,a);
        print ("DIR: %d\n",DIR);
        exit(1);
      }
    }
  }
}
#endif
}
#undef sqrt_1_2

/* **************************************************************************** */
void CONS_EIGENVECTORS(real *u, real *v, real **Lc, real **Rc, real *lambda)
/*
 *
 * Provide conservative eigenvectors for MHD equations
 * Reference paper:
 *
 *  "A High-order WENO Finite Difference Scheme for the Equations
 *   of Ideal MHD"
 *  Jiang,Wu, JCP 150,561 (1999)
 *
 *  With the following corrections:
 *
 *  The components (By, Bz) of  L_{1,7}  page 571 should
 *  be +\sqrt{rho}a\alpha_s instead of -.
 *  Also, since Bx is not considered as a parameter, 
 *  one must also add a component in the fast and slow waves.
 *  This can be seen by forming the conservative
 *  eigenvectors from the primitive ones, see the paper
 *  from Powell, Roe Linde.
 *
 *  
 *  The Lc_{1,3,5,7}[Bx] components, according to Serna 09
 *  are -(1-gmm)*a_{f,s}*Bx and not (1-gmm)*a_{f,s}*Bx. Both are 
 *  orthonormal though. She is WRONG!
 *  -Petros-
 **************************************************************************** */
{
  int    nv, i,j,k;
  double rho;
  double vx, vy, vz;
  double Bx, By, Bz;
  double bx, by, bz;
  double beta_y, beta_z;
  double one_sqrho;
  double cf2, cs2, ca2, a2, v2;
  double cf, cs, ca, a;
  double alpha_f, alpha_s;
  double g1, g2, tau, Gf, Ga, Gs;
  double scrh0, scrh1, S, bt2, b2, Btmag;
  double one_gmm;
  double LBX = 0.0;  /* setting LBX to 1.0 will include Powell's */
                     /* eight wave. Set it to 0 to recover the   */ 
                     /* standard 7 wave formulation              */  

  rho = v[DN];

  EXPAND (vx = v[V1];  ,
          vy = v[V2];  ,
          vz = v[V3];)

  EXPAND (Bx = v[B1];  ,
          By = v[B2];  ,
          Bz = v[B3];)
  one_sqrho = 1.0/sqrt(rho);
  
  S   = (Bx >= 0.0 ? 1.0 : -1.0);
  #if EOS == IDEAL
   a2 = gmm*v[PR]/rho;
  #elif EOS == ISOTHERMAL
   a2 = C_ISO2;
  #endif
 
  EXPAND(bx = Bx*one_sqrho;  ,
         by = By*one_sqrho;  ,
         bz = Bz*one_sqrho;)
    
  bt2   = EXPAND(0.0  , + by*by, + bz*bz);
  b2    = bx*bx + bt2;
  Btmag = sqrt(bt2*rho);
  v2    = EXPAND(vx*vx , + vy*vy, + vz*vz);

/* ------------------------------------------------------------
    Compute fast and slow magnetosonic speeds.

    The following expression appearing in the definitions
    of the fast magnetosonic speed 
    
     (a^2 - b^2)^2 + 4*a^2*bt^2 = (a^2 + b^2)^2 - 4*a^2*bx^2

    is always positive and avoids round-off errors.
   ------------------------------------------------------------ */
        
  scrh0 = a2 - b2;
  ca2   = bx*bx;
  scrh0 = scrh0*scrh0 + 4.0*bt2*a2;    
  scrh0 = sqrt(scrh0);    

  cf2 = 0.5*(a2 + b2 + scrh0); 
  cs2 = a2*ca2/cf2;   /* -- same as 0.5*(a2 + b2 - scrh0) -- */
    
  cf = sqrt(cf2);
  cs = sqrt(cs2);
  ca = sqrt(ca2);
  a  = sqrt(a2); 

  if (Btmag > 1.e-9) {
    SELECT(                     , 
           beta_y = dsign(By);  ,
           beta_y = By/Btmag; 
           beta_z = Bz/Btmag;)
  } else {
    SELECT(                       , 
           beta_y = 1.0;          ,
           beta_z = beta_y = sqrt(0.5);)
  }
    
  if (cf == cs) {
    alpha_f = 1.0;
    alpha_s = 0.0;
  }else if (a <= cs) {
    alpha_f = 0.0;
    alpha_s = 1.0;
  }else if (cf <= a){
    alpha_f = 1.0;
    alpha_s = 0.0;
  }else{ 
    scrh0   = 1.0/(cf2 - cs2);
    alpha_f = (a2  - cs2)*scrh0;
    alpha_s = (cf2 -  a2)*scrh0;
    alpha_f = dmax(0.0, alpha_f);
    alpha_s = dmax(0.0, alpha_s);
    alpha_f = sqrt(alpha_f);
    alpha_s = sqrt(alpha_s);
  }

/* --------------------------------------------------------
    Compute non-zero entries of conservative
    eigenvectors (Rc, Lc).
   -------------------------------------------------------- */
 
  #if EOS != ISOTHERMAL
   one_gmm = 1.0 - gmm;
   g1  = 0.5*(gmm - 1.0);
   g2  = (gmm - 2.0)/(gmm - 1.0);
   tau = (gmm - 1.0)/a2;
  #elif EOS == ISOTHERMAL 
   one_gmm = g1 = tau = 0.0;
  #endif   

  scrh0 = EXPAND(0.0, + beta_y*vy, + beta_z*vz);
  Gf = alpha_f*cf*vx - alpha_s*cs*S*scrh0;
  Ga = EXPAND(0.0,    , + S*(beta_z*vy - beta_y*vz));
  Gs = alpha_s*cs*vx + alpha_f*cf*S*scrh0;
  
/* -----------------------
    FAST WAVE  (u - c_f) 
   ----------------------- */

  k = KFASTM;
  lambda[k] = vx - cf;

  scrh0 = alpha_s*cs*S;
  scrh1 = alpha_s*a*one_sqrho;
  Rc[k][DN] = alpha_f;
  EXPAND( Rc[k][M1] = alpha_f*lambda[k];          ,
          Rc[k][M2] = alpha_f*vy + scrh0*beta_y;  ,
          Rc[k][M3] = alpha_f*vz + scrh0*beta_z; ) 
  EXPAND(                            ,                                
          Rc[k][B2] = scrh1*beta_y;  ,
          Rc[k][B3] = scrh1*beta_z; )
  #if EOS == IDEAL
   Rc[k][EN] = alpha_f*(0.5*v2 + cf2 - g2*a2) - Gf;
  #endif

  Lc[k][DN] = (g1*alpha_f*v2 + Gf)*0.5/a2; 

  #if EOS == ISOTHERMAL
   Lc[k][DN] += alpha_f*0.5;
  #endif
  
  EXPAND( Lc[k][M1] = (one_gmm*alpha_f*vx - alpha_f*cf)  *0.5/a2;  ,
          Lc[k][M2] = (one_gmm*alpha_f*vy + scrh0*beta_y)*0.5/a2;  ,
          Lc[k][M3] = (one_gmm*alpha_f*vz + scrh0*beta_z)*0.5/a2;) 
  EXPAND( Lc[k][B1] =  LBX*one_gmm*alpha_f*Bx*0.5/a2;                      ,                          
          Lc[k][B2] = (one_gmm*alpha_f*By + scrh1*rho*beta_y)*0.5/a2;  ,
          Lc[k][B3] = (one_gmm*alpha_f*Bz + scrh1*rho*beta_z)*0.5/a2; )
  #if EOS == IDEAL
   Lc[k][EN] = alpha_f*(gmm - 1.0)*0.5/a2;
  #endif

  
  /* -----------------------
      FAST WAVE  (u + c_f) 
     ----------------------- */

  k = KFASTP;
  lambda[k] = vx + cf;

  Rc[k][DN] = alpha_f;
  EXPAND( Rc[k][M1] = alpha_f*lambda[k];          ,
          Rc[k][M2] = alpha_f*vy - scrh0*beta_y;  ,
          Rc[k][M3] = alpha_f*vz - scrh0*beta_z; ) 
  EXPAND(                            ,                                
          Rc[k][B2] = scrh1*beta_y;  ,
          Rc[k][B3] = scrh1*beta_z; )
  #if EOS == IDEAL
   Rc[k][EN] = alpha_f*(0.5*v2 + cf2 - g2*a2) + Gf;
  #endif

  Lc[k][DN] = (g1*alpha_f*v2 - Gf)*0.5/a2; 

  #if EOS == ISOTHERMAL
   Lc[k][DN] += alpha_f*0.5;
  #endif
  
  EXPAND( Lc[k][M1] = (one_gmm*alpha_f*vx + alpha_f*cf)  *0.5/a2;  ,
          Lc[k][M2] = (one_gmm*alpha_f*vy - scrh0*beta_y)*0.5/a2;  ,
          Lc[k][M3] = (one_gmm*alpha_f*vz - scrh0*beta_z)*0.5/a2;) 
  EXPAND( Lc[k][B1] =  LBX*one_gmm*alpha_f*Bx*0.5/a2;                                ,                                
          Lc[k][B2] = (one_gmm*alpha_f*By + sqrt(rho)*a*alpha_s*beta_y)*0.5/a2;  ,
          Lc[k][B3] = (one_gmm*alpha_f*Bz + sqrt(rho)*a*alpha_s*beta_z)*0.5/a2; )
  #if EOS == IDEAL
   Lc[k][EN] = alpha_f*(gmm - 1.0)*0.5/a2;
  #endif
  

  /* -----------------------
      Entropy wave  (u) 
     ----------------------- */

  #if EOS == IDEAL
   k = KENTRP;
   lambda[k] = vx;

   Rc[k][DN] = 1.0;
   EXPAND( Rc[k][M1] = vx; ,
           Rc[k][M2] = vy; ,
           Rc[k][M3] = vz; )
   Rc[k][EN] = 0.5*v2;

   Lc[k][DN] = 1.0 - 0.5*tau*v2;
   EXPAND(Lc[k][V1] = tau*vx;  ,
          Lc[k][V2] = tau*vy;  ,
          Lc[k][V3] = tau*vz;)
   EXPAND(Lc[k][B1] = LBX*tau*Bx;  ,
          Lc[k][B2] = tau*By;  ,
          Lc[k][B3] = tau*Bz;)
   Lc[k][EN] = -tau;
  #endif

  /* --------------------------------
            div.B wave  (u) 
     -------------------------------- */

  #ifndef GLM_MHD
   k = KDIVB;
   #if MHD_FORMULATION == EIGHT_WAVES
    lambda[k] = vx;
    Rc[k][B1] = 1.0;
    Lc[k][B1] = 1.0;
   #else
    lambda[k] = 0.0;
    Rc[k][B1] = Lc[k][B1] = 0.0;
   #endif
  #endif
    
  #if COMPONENTS > 1    

 /* -----------------------
     SLOW WAVE  (u - c_s) 
    ----------------------- */

   k = KSLOWM;
   lambda[k] = vx - cs;
   scrh0 = alpha_f*cf*S;

   Rc[k][DN] = alpha_s;
   EXPAND( Rc[k][M1] = alpha_s*lambda[k];          ,
           Rc[k][M2] = alpha_s*vy - scrh0*beta_y;  ,
           Rc[k][M3] = alpha_s*vz - scrh0*beta_z; ) 
   EXPAND(                                            ,                                
           Rc[k][B2] = - alpha_f*a*beta_y*one_sqrho;  ,
           Rc[k][B3] = - alpha_f*a*beta_z*one_sqrho; )

   #if EOS == IDEAL
    Rc[k][EN] = alpha_s*(0.5*v2 + cs2 - g2*a2) - Gs;
   #endif

   Lc[k][DN] = (g1*alpha_s*v2 + Gs)*0.5/a2; 
   
   #if EOS == ISOTHERMAL
    Lc[k][DN] += alpha_s*0.5;
   #endif
   
   EXPAND( Lc[k][M1] = (one_gmm*alpha_s*vx - alpha_s*cs)  *0.5/a2;  ,
           Lc[k][M2] = (one_gmm*alpha_s*vy - scrh0*beta_y)*0.5/a2;  ,
           Lc[k][M3] = (one_gmm*alpha_s*vz - scrh0*beta_z)*0.5/a2;) 
   EXPAND( Lc[k][B1] = LBX*one_gmm*alpha_s*Bx*0.5/a2;                                ,                                
           Lc[k][B2] = (one_gmm*alpha_s*By - sqrt(rho)*a*alpha_f*beta_y)*0.5/a2;  ,
           Lc[k][B3] = (one_gmm*alpha_s*Bz - sqrt(rho)*a*alpha_f*beta_z)*0.5/a2; )
   #if EOS == IDEAL
    Lc[k][EN] = alpha_s*(gmm - 1.0)*0.5/a2;
   #endif
   
   
   /* -----------------------
       SLOW WAVE  (u + c_s) 
      ----------------------- */

   k = KSLOWP;
   lambda[k] = vx + cs; 

   Rc[k][DN] = alpha_s;
   EXPAND( Rc[k][M1] = alpha_s*lambda[k];          ,
           Rc[k][M2] = alpha_s*vy + scrh0*beta_y;  ,
           Rc[k][M3] = alpha_s*vz + scrh0*beta_z; ) 
   EXPAND(                                            ,                                
           Rc[k][B2] = - alpha_f*a*beta_y*one_sqrho;  ,
           Rc[k][B3] = - alpha_f*a*beta_z*one_sqrho; )

   #if EOS == IDEAL
    Rc[k][EN] = alpha_s*(0.5*v2 + cs2 - g2*a2) + Gs;
   #endif

   Lc[k][DN] = (g1*alpha_s*v2 - Gs)*0.5/a2; 

   #if EOS == ISOTHERMAL
    Lc[k][DN] += alpha_s*0.5;
   #endif
   
   EXPAND( Lc[k][M1] = (one_gmm*alpha_s*vx + alpha_s*cs)  *0.5/a2;  ,
           Lc[k][M2] = (one_gmm*alpha_s*vy + scrh0*beta_y)*0.5/a2;  ,
           Lc[k][M3] = (one_gmm*alpha_s*vz + scrh0*beta_z)*0.5/a2;) 
   EXPAND( Lc[k][B1] = LBX*one_gmm*alpha_s*Bx*0.5/a2;                                 ,               
           Lc[k][B2] = (one_gmm*alpha_s*By - sqrt(rho)*a*alpha_f*beta_y)*0.5/a2;  ,
           Lc[k][B3] = (one_gmm*alpha_s*Bz - sqrt(rho)*a*alpha_f*beta_z)*0.5/a2; )
   #if EOS == IDEAL
    Lc[k][EN] = alpha_s*(gmm - 1.0)*0.5/a2;
   #endif
   
  
  #endif

  #if COMPONENTS == 3

 /* ------------------------
     Alfven WAVE  (u - c_a) 
    ------------------------ */

   k = KALFVM;
   lambda[k] = vx - ca;

   Rc[k][M2] = - beta_z*S;  
   Rc[k][M3] = + beta_y*S;
   Rc[k][B2] = - beta_z*one_sqrho;
   Rc[k][B3] =   beta_y*one_sqrho;
   #if EOS == IDEAL
    Rc[k][EN] = - Ga;
   #endif

   Lc[k][DN] = 0.5*Ga;
   Lc[k][M2] = - 0.5*beta_z*S;
   Lc[k][M3] =   0.5*beta_y*S;
   Lc[k][B2] = - 0.5*sqrt(rho)*beta_z;
   Lc[k][B3] =   0.5*sqrt(rho)*beta_y;

 /* -----------------------
     Alfven WAVE  (u + c_a) 
    ----------------------- */

   k = KALFVP;
   lambda[k] = vx + ca;

   Rc[k][M2] =   Rc[KALFVM][M2];  
   Rc[k][M3] =   Rc[KALFVM][M3];
   Rc[k][B2] = - Rc[KALFVM][B2];   
   Rc[k][B3] = - Rc[KALFVM][B3];
   #if EOS == IDEAL
    Rc[k][EN] = Rc[KALFVM][EN];
   #endif

   Lc[k][DN] =   Lc[KALFVM][DN];
   Lc[k][M2] =   Lc[KALFVM][M2];
   Lc[k][M3] =   Lc[KALFVM][M3];
   Lc[k][B2] = - Lc[KALFVM][B2];
   Lc[k][B3] = - Lc[KALFVM][B3];
   #if EOS == IDEAL
    Lc[k][EN] =   Lc[KALFVM][EN];
   #endif

  #endif

  #ifdef GLM_MHD

  /* -------------------------
      GLM wave,  -Ch_GLM
     -------------------------  */

   k = KPSI_GLMM;
   lambda[k] = -Ch_GLM;
   Rc[k][B1]      =  1.0;
   Rc[k][PSI_GLM] = -Ch_GLM;

   Lc[k][B1]      =  0.5;
   Lc[k][PSI_GLM] = -0.5/Ch_GLM;

  /* -------------------------
      GLM wave,  +Ch_GLM
     -------------------------  */

   k = KPSI_GLMP;
   lambda[k] = Ch_GLM;
   Rc[k][B1]      =  1.0;
   Rc[k][PSI_GLM] =  Ch_GLM;

   Lc[k][B1]      =  0.5;
   Lc[k][PSI_GLM] =  0.5/Ch_GLM;

  #endif


/*
for (k = 0; k < NVAR; k++){
for (i = 0; i < NVAR; i++){
  if (fabs(Lc2[k][i] - Lc[k][i]) > 1.e-4){
    printf ("L's not equal, %d %d,  %12.6e  %12.6e\n",k,i,Lc2[k][i],Lc[k][i]);
    PRINTROW(Lc2,k);
    PRINTROW(Lc,k);

    exit(1);
  }
  if (fabs(Rc2[k][i] - Rc[k][i]) > 1.e-4){
    printf ("R's not equal, %d %d,  %12.6e  %12.6e\n",k,i,Rc2[k][i],Rc[k][i]);
    PRINTCOL(Rc2,i);
    PRINTCOL(Rc,i);
    exit(1);
  }
}}
*/

/* -----------------------------------------
         Check eigenvectors consistency
   ----------------------------------------- */

#if CHECK_EIGENVECTORS == YES
{
  double AA[NFLX][NFLX], Aw1[NFLX], a;
  int ii, jj,kk;

  for (ii = 0; ii <NFLX;ii++){
    for (jj = 0; jj <NFLX;jj++){
      a = 0.0;
      for (kk = 0; kk <NFLX;kk++){
        a += Lc[jj][kk]*Rc[ii][kk];
      }
      if (ii==jj && fabs(a-1.0)>1.e-8) {
        print ("! Eigenvectors not orthogonal!  %d  %d  %12.6e \n",ii,jj,a);
        print ("DIR: %d\n",DIR);
        exit(1);
      }
      if(ii!=jj && fabs(a)>1.e-8) {
        print ("! Eigenvectors not orthogonal (2) %d  %d  %12.6e !\n",ii,jj,a);
        print ("DIR: %d\n",DIR);
        exit(1);
      }
    }
  }
}
#endif
}
