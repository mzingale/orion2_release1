#include "orion2.h"

/* ********************************************************************* */
void MUSTA_FLUX (real **vL, real **vR, real **flux, 
                Grid *grid, real *cmax)
/*
 *
 * NAME
 *
 *   LF_SOLVER
 *
 *
 * PURPOSE
 *
 *   Solve riemann problem for the Euler equations 
 *   using th Lax-Friedrichs Rusanov solver. 
 * 
 * ARGUMENTS
 *
 *   vL (IN)       1-D array of left-edge primitive values at i+1/2
 *   vR (IN)       1-D array of right-edge primitive values at i+1/2
 *   flux (OUT)    1-D array of numerical fluxes, not including pressure
 *   grid (IN)     Array of grids
 *   cmax(OUT)     Array of maximum characteristic speeds in this direction
 *
 *
 *
 * LAST_MODIFIED
 *
 *   August, 17/2004, written by Andrea Mignone  (mignone@to.astro.it)
 *              
 *
 *
 **************************************************************************** */

{
  int      nv, i, beg, end;
  static real **ur, **ul, **vRL;
  static real *cRL;
int k;  
real dtdx, dxdt;
static real **fR, **fM, **fL, **wL, **wR, **qR, **qL, **f, **qM, **vM;
  static real **fct_flx;

  beg = grid[DIR].lbeg - 1;
  end = grid[DIR].lend;

  if (ur == NULL){
    ur  = Array_2D(NMAX_POINT, NVAR, double);
    ul  = Array_2D(NMAX_POINT, NVAR, double);
    vRL = Array_2D(NMAX_POINT, NVAR, double);
    cRL = Array_1D(NMAX_POINT, double);

fR = Array_2D(NMAX_POINT, NVAR, double);
fM = Array_2D(NMAX_POINT, NVAR, double);
fL = Array_2D(NMAX_POINT, NVAR, double);
f  = Array_2D(NMAX_POINT, NVAR, double);
wR = Array_2D(NMAX_POINT, NVAR, double);
wL = Array_2D(NMAX_POINT, NVAR, double);
qR = Array_2D(NMAX_POINT, NVAR, double);
qL = Array_2D(NMAX_POINT, NVAR, double);
qM = Array_2D(NMAX_POINT, NVAR, double);
vM = Array_2D(NMAX_POINT, NVAR, double);
fct_flx = Array_2D(NMAX_POINT, NVAR, double);

  }

/* -------------------------------------------------------------------
                     i  -- >   i + 1/2
   ------------------------------------------------------------------- */

  for (i = beg; i <= end; i++)           {
  for (nv = 0; nv < NFLX; nv++) {
    vRL[i][nv] = 0.5*(vL[i][nv] + vR[i][nv]);
  }}

/* -----------  COMPUTE MAX EIGENVALUE -------------------------  */

  MAX_CH_SPEED (vRL, cRL, grid, beg, end);

  for (i = beg; i <= end; i++) {
    *cmax = dmax (cRL[i], *cmax);
  }

  for (i = beg; i <= end; i++) {
  for (nv = 0; nv < NFLX; nv++) {
    wL[i][nv] = vL[i][nv];
    wR[i][nv] = vR[i][nv];
  }}
        
  dtdx = delta_t/grid[DIR].dx[3];
  dxdt = 1.0/dtdx;
  	
  for (k = 1; k <= 4; k++){
    FLUX (qL, wL, fL, beg, end);
    FLUX (qR, wR, fR, beg, end);

    for (i = beg; i <= end; i++) {
    for (nv = 0; nv < NFLX; nv++) {
      qM[i][nv] = 0.5*(qL[i][nv] + qR[i][nv]) -
                  0.5*dtdx*(fR[i][nv] - fL[i][nv]);
    }}
    CONTOPRIM (qM, vM, beg, end);
    FLUX      (qM, vM, fM, beg, end);
      		     


    for (i = beg; i <= end; i++) {
    for (nv = 0; nv < NFLX; nv++) {
      flux[i][nv] = 0.25*(fL[i][nv] + 2.0*fM[i][nv] + fR[i][nv] 
                           - cRL[i]*(qR[i][nv] - qL[i][nv]));
/*    
  flux[i][nv] = 0.5*(fL[i][nv] + fR[i][nv] 
                           - cRL[i]*(qR[i][nv] - qL[i][nv]));
*/

    }}

    for (i = beg; i <= end; i++) {
    for (nv = 0; nv < NFLX; nv++) {
      qL[i][nv] -= dtdx*(flux[i][nv] - fL[i][nv]);
      qR[i][nv] -= dtdx*(fR[i][nv] - flux[i][nv]);
    }}
    CONTOPRIM (qL, wL, beg, end);
    CONTOPRIM (qR, wR, beg, end);
  }

/* ---- save fluxes for constrained transport ---- */

  #if MHD_FORMULATION == FLUX_CT
  #if CT_EMF_AVERAGE == UPWIND
   print (" ! UPWIND flux ct not defined in hlld.c\n");
   QUIT_ORION2(1);
  #endif
  #endif

  #if MHD_FORMULATION == FLUX_CT
  #if CT_EMF_AVERAGE == ARITHMETIC
   EMF (flux, -1);
  #endif
  #endif

}
