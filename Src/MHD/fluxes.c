#include "orion2.h"

/* ******************************************************************** */
void FLUX (real **ucons, real **wprim, real **bck, 
           real **fx, real *p, int beg, int end)

/*
 *
 *   V1 (M1)        = NORMAL VEL. COMPONENT;
 *   V2 (M2), V3(M3) = TRANSVERSE VEL. COMP;
 *
 *
 *********************************************************************** */
{
  int nv, i;
  real vB, ptot;
  real bt1, bt2, bt3;
  real *w, *u;

  for (i = beg; i <= end; i++) {

    w = wprim[i];
    u = ucons[i];
    
    ptot  = 0.5*(EXPAND(w[BX]*w[BX] , + w[BY]*w[BY], + w[BZ]*w[BZ]));

    #if EOS == IDEAL
     ptot += w[PR];
    #elif EOS == ISOTHERMAL
     ptot += C_ISO2*w[DN];
    #endif

    vB    = EXPAND(w[VX]*w[BX] , + w[VY]*w[BY], + w[VZ]*w[BZ]);

    #if INCLUDE_BACKGROUND_FIELD == YES
     ptot += EXPAND(bck[i][BX]*w[BX], + bck[i][BY]*w[BY], + bck[i][BZ]*w[BZ]);

     EXPAND(bt1 = w[B1] + bck[i][B1];  ,
            bt2 = w[B2] + bck[i][B2];  ,
            bt3 = w[B3] + bck[i][B3];)

     fx[i][DN] = u[M1];
     EXPAND(fx[i][MX] = w[V1]*u[MX] - bt1*w[BX] - w[B1]*bck[i][BX];  ,
            fx[i][MY] = w[V1]*u[MY] - bt1*w[BY] - w[B1]*bck[i][BY];  ,
            fx[i][MZ] = w[V1]*u[MZ] - bt1*w[BZ] - w[B1]*bck[i][BZ]; )

     EXPAND(fx[i][B1] = 0.0;                    ,
            fx[i][B2] = w[V1]*bt2 - bt1*w[V2];  ,
            fx[i][B3] = w[V1]*bt3 - bt1*w[V3]; )
     #if EOS == IDEAL
      fx[i][EN] = (u[EN] + ptot)*w[V1] - bt1*vB;
     #endif
    #else
     fx[i][DN] = u[M1];
     EXPAND(fx[i][MX] = w[V1]*u[MX] - w[B1]*w[BX];  ,
            fx[i][MY] = w[V1]*u[MY] - w[B1]*w[BY];  ,
            fx[i][MZ] = w[V1]*u[MZ] - w[B1]*w[BZ]; ) 

     EXPAND(fx[i][B1] = 0.0;                         ,
            fx[i][B2] = w[V1]*w[B2] - w[B1]*w[V2];   ,
            fx[i][B3] = w[V1]*w[B3] - w[B1]*w[V3]; )
     #if EOS == IDEAL
      fx[i][EN] = (u[EN] + ptot)*w[V1] - w[B1]*vB;
     #endif
    #endif

    p[i] = ptot;

    #ifdef GLM_MHD
     fx[i][B1]      = w[PSI_GLM];
     fx[i][PSI_GLM] = Ch_GLM*Ch_GLM*w[B1];
    #endif


  }
}
