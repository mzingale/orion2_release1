#include "orion2.h"

/* ********************************************************************** */
void FILL_MAGNETIC_FIELD (Data_Arr U, int side, Grid *grid)
/*
 *
 *
 *  Assign boundary conditions to the staggered component 
 *  of magnetic fields normal to the direction specified 
 *  by 'side' using the div.B = 0 condition.
 *  In 2-D this amounts to use the other 3 components, 
 *  whereas in 3-D one uses the other 5 components.
 *  This function must be called after the tangential 
 *  components have already been set. 
 *  
 *  For the component Bn, only nghost-1 zones 
 *  are assigned:
 * 
 *                    |
 *  +-----+-----+-----+-----+-----+--
 *  |     |     |     |     |     |
 *  |     X     X     |     |     |
 *  |     |     |     |     |     |
 *  +-----+-----+-----+-----+-----+--
 *                    |
 *  <-----------------> BEG
 *   Physical boundary     
 *
 *   X = components assigned in this function.
 *
 *********************************************************************** */
{
  int  ibeg, jbeg, kbeg;
  int  iend, jend, kend;
  int  i,j,k;
  int  di,dj,dk;

  real r, dmu;
  real bxp, byp, bzp;
  real bxm, bym, bzm;
  real dBx = 0.0, dBy = 0.0, dBz = 0.0;
  real dVx, dVy, dVz;
  real  dx,  dy,  dz;
  real rp, sp, Axp, Ayp, Azp;
  real rm, sm, Axm, Aym, Azm;

  ibeg = 1; iend = NX_TOT-1; di = 1;
  jbeg = 1; jend = NY_TOT-1; dj = 1;
  #if DIMENSIONS == 3
   kbeg = 1; kend = NZ_TOT-1; dk = 1;
  #else
   kbeg = kend = 0, dk = 1;
  #endif

  if (side == X1_BEG) {ibeg = IBEG-1; iend = 1; di = -1;}
  if (side == X1_END)  ibeg = IEND+1;

  if (side == X2_BEG) {jbeg = JBEG-1; jend = 1; dj = -1;}
  if (side == X2_END)  jbeg = JEND+1;

  if (side == X3_BEG) {kbeg = KBEG-1; kend = 1; dk = -1;}
  if (side == X3_END)  kbeg = KEND+1;

   /*PS: IGEB, JBEG, and KBEG - 1 may be wrong.
  if (side == X1_BEG) {ibeg = IBEG; iend = 1; di = -1;}
  if (side == X1_END)  ibeg = IEND+1;

  if (side == X2_BEG) {jbeg = JBEG; jend = 1; dj = -1;}
  if (side == X2_END)  jbeg = JEND+1;

  if (side == X3_BEG) {kbeg = KBEG; kend = 1; dk = -1;}
  if (side == X3_END)  kbeg = KEND+1;
   */

  for (k = kbeg; dk*k <= dk*kend; k += dk){
  for (j = jbeg; dj*j <= dj*jend; j += dj){
  for (i = ibeg; di*i <= di*iend; i += di){

    r  = grid[IDIR].x[i];
    rp = grid[IDIR].xr[i];
    rm = grid[IDIR].xl[i];

    dx = grid[IDIR].dx[i];
    dy = grid[JDIR].dx[j];
    dz = grid[KDIR].dx[k];

    #if GEOMETRY == SPHERICAL
     dmu = grid[JDIR].dV[j];
     sp  = grid[JDIR].A[j];
     sm  = grid[JDIR].A[j - 1];
    #endif

    #if DIMENSIONS == 2
     dz = 1.0;
    #endif

    D_EXPAND(bxp = U[BXs][k][j][i]; bxm = U[BXs][k][j][i - 1];  ,
             byp = U[BYs][k][j][i]; bym = U[BYs][k][j - 1][i];  ,
             bzp = U[BZs][k][j][i]; bzm = U[BZs][k - 1][j][i];)

  /* -------------------------------------------------------
       Divergence is written as 

         dVx*(Axp*bxp - Axm*dxm) + 
         dVy*(Ayp*byp - Aym*dym) + 
         dVz*(Azp*bzp - Azm*dzm) = 0

       so that the k-th component can be 
       recovered as
 
      bkp = bkm*Akm/Akp + 
            sum_(j != k) (Ajp*bjp - Ajm*bjm)*dVj/(dVk*Akp)
    ------------------------------------------------------- */

    #if GEOMETRY == CARTESIAN

     dVx = dy*dz; Axp = Axm = 1.0;
     dVy = dx*dz; Ayp = Aym = 1.0;
     dVz = dx*dy; Azp = Azm = 1.0;

    #elif GEOMETRY == CYLINDRICAL

     dVx =   dy*dz; Axp = rp ; Axm = rm;
     dVy = r*dx*dz; Ayp = 1.0; Aym = 1.0;
     dVz =   dx*dy; Azp = 1.0; Azm = 1.0;

    #elif GEOMETRY == POLAR

     dVx =   dy*dz; Axp = rp ; Axm = rm;
     dVy =   dx*dz; Ayp = 1.0; Aym = 1.0;
     dVz = r*dx*dy; Azp = 1.0; Azm = 1.0;

    #elif GEOMETRY == SPHERICAL

     dVx =  dmu*dz; Axp = rp*rp; Axm = rm*rm;
     dVy = r*dx*dz; Ayp = sp   ; Aym = sm;
     dVz = r*dx*dy; Azp = 1.0  ; Azm = 1.0;

    #endif

    D_EXPAND(dBx = dVx*(Axp*bxp - Axm*bxm);  ,
             dBy = dVy*(Ayp*byp - Aym*bym);  ,
             dBz = dVz*(Azp*bzp - Azm*bzm); )

/* -------------------------------------------------
      Assign a single face magnetic field 
   ------------------------------------------------- */

    if (side == X1_BEG){

      bxm = (bxp*Axp + (dBy + dBz)/dVx)/Axm;
      U[BXs][k][j][i - 1] = bxm;

    }else if (side == X1_END){

      bxp = (bxm*Axm - (dBy + dBz)/dVx)/Axp;
      U[BXs][k][j][i] = bxp;

    }else if (side == X2_BEG){

      bym = (byp*Ayp + (dBx + dBz)/dVy)/Aym;
      U[BYs][k][j - 1][i] = bym;

    }else if (side == X2_END){
  
      byp = (bym*Aym - (dBx + dBz)/dVy)/Ayp;
      U[BYs][k][j][i] = byp;

    }else if (side == X3_BEG){

      bzm = (bzp*Azp + (dBx + dBy)/dVz)/Azm;
      U[BZs][k - 1][j][i] = bzm;

    }else if (side == X3_END){

      bzp = (bzm*Azm - (dBx + dBy)/dVz)/Azp;
      U[BZs][k][j][i] = bzp;

    }
  }}}

}


