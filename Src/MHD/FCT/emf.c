/*
*      ____                         __ __
*     / __ \ ____ __ ___   __  __  / // /
*    / /_/ // __// // _ \ /  \/ / / // /
*    \____//_/  /_/ \___//_/\__/ /_//_/
*    
*    Based on Pluto 3.
*
*    Please refer to COPYING in Pluto's root directory.
*
*
*    Modification: Orional code based on PLUTO3.0
*    1) PS(03/17/09): Increase local memory allocation for AMR.
*    2) PS(04/09/09): Add function EMF_RETRIEVE to retrieve emf for
*                   refluxing.
*    3) PS(09/23/09): Upgrade to PLUTO 3.0.1.
*
*/

#include "orion2.h"

static EMF emf, emf_res;

#define eps_UCT_CONTACT   1.e-6
#define EX(k,j,i)  (vz[k][j][i]*By[k][j][i] - vy[k][j][i]*Bz[k][j][i])
#define EY(k,j,i)  (vx[k][j][i]*Bz[k][j][i] - vz[k][j][i]*Bx[k][j][i])
#define EZ(k,j,i)  (vy[k][j][i]*Bx[k][j][i] - vx[k][j][i]*By[k][j][i])

/* *********************************************************** */
void EMF_PUT (const State_1D *state, int beg, int end, Grid *grid)
/* 
 *
 * PURPOSE
 *
 *  Store electric field components and other related
 *  information computed during one-dimensional sweeps
 *  for later reuse.
 *  Only advective terms (E = - v X B) should be 
 *  considered here.
 *
 ************************************************************* */
{
  int i, j, k, s;
  /*PS: declare all Array_3D here to be static
  static Data_Arr emf.ex, emf.ey, emf.ez;
  static Data_Arr emf.ezi, emf.ezj, emf.exj, emf.exk, emf.eyi, emf.eyk;
  */
/* ----------------------------------------------------
     Allocate memory for EMF structure and 
     check for incompatible combinations of algorithms 
   ---------------------------------------------------- */
  if (emf.ez == NULL){

    emf.ibeg = emf.iend = 0;
    emf.jbeg = emf.jend = 0;
    emf.kbeg = emf.kend = 0;

  /* -- memory allocation -- */

    /*PS: AMR grid size */
    D_EXPAND(                                             ,
      emf.ez = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);  ,
      emf.ex = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
      emf.ey = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);   
    )
    #if CT_EMF_AVERAGE == UCT_CONTACT
     D_EXPAND(
       emf.svx = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, char);  ,
       emf.svy = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, char);  ,
       emf.svz = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, char);
     )
    #endif

     D_EXPAND(                                              ,
       emf.ezi = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double); 
       emf.ezj = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);  ,

       emf.exj = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
       emf.exk = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);

       emf.eyi = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
       emf.eyk = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
     )

    #if    CT_EMF_AVERAGE == UCT_HLL

     D_EXPAND(
       emf.SxL = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
       emf.SxR = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);  ,

       emf.SyL = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
       emf.SyR = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);  ,

       emf.SzL = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
       emf.SzR = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
     )
    #endif
  }

/* ------------------------------------------------------
     Store emf components or other necessary 1-D data
   ------------------------------------------------------ */

 /* ---- Save electric field  ---- */

  if (DIR == IDIR){

    emf.ibeg = beg; emf.iend = end;
    for (i = beg; i <= end+1; i++) { 
      /*
      printf("%d %d %d\n",*NZ_PT,*NY_PT,i);
      */
      D_EXPAND(emf.ezi[*NZ_PT][*NY_PT][i] = -state->flux[i][BY];  ,
                                                                  ,
               emf.eyi[*NZ_PT][*NY_PT][i] =  state->flux[i][BZ]; ) 

      #if CT_EMF_AVERAGE == UCT_CONTACT
       if      (state->flux[i][DN] >  eps_UCT_CONTACT) s = 1;
       else if (state->flux[i][DN] < -eps_UCT_CONTACT) s = -1;
       else s = 0;

       emf.svx[*NZ_PT][*NY_PT][i] = s;
      #endif 

      #if CT_EMF_AVERAGE == UCT_HLL
       emf.SxL[*NZ_PT][*NY_PT][i] = dmax(0.0, -state->SL[i]); 
       emf.SxR[*NZ_PT][*NY_PT][i] = dmax(0.0,  state->SR[i]); 
      #endif

      #if CT_EMF_AVERAGE == RIEMANN_2D
       emf.ezi[*NZ_PT][*NY_PT][i] = -2.0*(state->pnt_flx[i][BY] + 
                                          state->dff_flx[i][BY]); 
      #endif

    }

  }else if (DIR == JDIR){

    emf.jbeg = beg; emf.jend = end;
    for (j = beg; j <= end+1; j++) {

      D_EXPAND(                                                    ,
               emf.ezj[*NZ_PT][j][*NX_PT] =  state->flux[j][BX];   ,
               emf.exj[*NZ_PT][j][*NX_PT] = -state->flux[j][BZ]; )

      #if CT_EMF_AVERAGE == UCT_CONTACT
       if      (state->flux[j][DN] >  eps_UCT_CONTACT) s = 1;
       else if (state->flux[j][DN] < -eps_UCT_CONTACT) s = -1;
       else s = 0;
       emf.svy[*NZ_PT][j][*NX_PT] = s;
      #endif

      #if CT_EMF_AVERAGE == UCT_HLL            
       emf.SyL[*NZ_PT][j][*NX_PT] = dmax(0.0, -state->SL[j]); 
       emf.SyR[*NZ_PT][j][*NX_PT] = dmax(0.0,  state->SR[j]); 
      #endif

      #if CT_EMF_AVERAGE == RIEMANN_2D 
//       emf.ezj[*NZ_PT][j][*NX_PT] += state->dff_flx[j][BX];  
       emf.ezj[*NZ_PT][j][*NX_PT]   = 2.0*(state->pnt_flx[j][BX] + 
                                           state->dff_flx[j][BX]); 
      #endif
    }

  }else if (DIR == KDIR){

    emf.kbeg = beg; emf.kend = end;
    for (k = beg; k <= end+1; k++) {

      emf.eyk[k][*NY_PT][*NX_PT] = -state->flux[k][BX]; 
      emf.exk[k][*NY_PT][*NX_PT] =  state->flux[k][BY]; 

      #if CT_EMF_AVERAGE == UCT_CONTACT
       if      (state->flux[k][DN] >  eps_UCT_CONTACT) s = 1;
       else if (state->flux[k][DN] < -eps_UCT_CONTACT) s = -1;
       else s = 0;
       emf.svz[k][*NY_PT][*NX_PT] = s;
      #endif

      #if CT_EMF_AVERAGE == UCT_HLL            
       emf.SzL[k][*NY_PT][*NX_PT] = dmax(0.0, -state->SL[k]); 
       emf.SzR[k][*NY_PT][*NX_PT] = dmax(0.0,  state->SR[k]); 
      #endif

    }
  }

/* ------------------------------------------------------
         Store velocity slopes if necessary 
   ------------------------------------------------------ */

  #if CT_EMF_AVERAGE == UCT_HLL
   #ifdef SINGLE_STEP 
    if (ISTEP == 2) return;    
   #endif

   /* -- "end+1" needed to save dvx_dx -- */

   STORE_VEL_SLOPES (&emf, state, beg, end + 1); 

  #endif

}

/* *********************************************************** */
void EMF_DIFF_PUT (const State_1D *state, int beg, int end, Grid *grid)
/* 
 *
 * PURPOSE
 *
 *  Store electric field components and other related
 *  information computed during one-dimensional sweeps
 *  for later reuse.
 *  Only advective terms (E = - v X B) should be 
 *  considered here.
 *
 *
 ************************************************************* */
{
  int i, j, k;

/* ------------------------------------------------------
     Store emf components or other necessary 1-D data
   ------------------------------------------------------ */

  if (DIR == IDIR){

    emf.ibeg = beg; emf.iend = end;
    for (i = beg; i <= end+1; i++) { /* -- "+1" needed to save dvx_dx -- */
  
      D_EXPAND(emf.ezi[*NZ_PT][*NY_PT][i] = -state->dff_flx[i][BY];  ,
                                                                  ,
               emf.eyi[*NZ_PT][*NY_PT][i] =  state->dff_flx[i][BZ]; ) 

    }

  }else if (DIR == JDIR){

    emf.jbeg = beg; emf.jend = end;
    for (j = beg; j <= end+1; j++) {

      D_EXPAND(                                                       ,
               emf.ezj[*NZ_PT][j][*NX_PT] =  state->dff_flx[j][BX];   ,
               emf.exj[*NZ_PT][j][*NX_PT] = -state->dff_flx[j][BZ]; )
    }

  }else if (DIR == KDIR){

  }

}

/* *********************************************************** */
void EMF_PUT_RES (const State_1D *state, int beg, int end, Grid *grid)
/* 
 * 
 * PURPOSE
 *
 *  Save electric field components associated with 
 *  resistive terms ONLY.
 *  TODO: some memory could be saved here...
 *
 ************************************************************* */
{
  int i, j, k;
 
  if (emf_res.ezi == NULL){
    D_EXPAND(                                                  ,

      emf_res.ezi = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);  
      emf_res.ezj = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);  ,

      emf_res.exj = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
      emf_res.exk = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);

      emf_res.eyi = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
      emf_res.eyk = Array_3D(NMAX_POINT, NMAX_POINT, NMAX_POINT, double);
    )
  }

/* ------------------------------------------------------
     Store emf component or other necessary 1-D data
   ------------------------------------------------------ */

  if (DIR == IDIR){

    for (i = beg; i <= end; i++) {
      D_EXPAND(emf_res.ezi[*NZ_PT][*NY_PT][i] = -state->res_flx[i][BY];  ,
                                                                         ,
               emf_res.eyi[*NZ_PT][*NY_PT][i] =  state->res_flx[i][BZ]; ) 
     }

  }else if (DIR == JDIR){

    for (j = beg; j <= end; j++) {
       D_EXPAND(                                                          ,
                emf_res.ezj[*NZ_PT][j][*NX_PT] =  state->res_flx[j][BX];  ,
                emf_res.exj[*NZ_PT][j][*NX_PT] = -state->res_flx[j][BZ]; )
    }

  }else if (DIR == KDIR){

    for (k = beg; k <= end; k++) {
       emf_res.eyk[k][*NY_PT][*NX_PT] = -state->res_flx[k][BX]; 
       emf_res.exk[k][*NY_PT][*NX_PT] =  state->res_flx[k][BY]; 
    }
  }
}

/* *********************************************************** */
EMF *EMF_GET (const Data *d, int step, Grid *grid)
/* 
 *
 * PURPOSE
 *
 *   Obtain edge-center electric field from quantities
 *   stored at face centers during 1-D sweeps.
 *
 *
 *
 ************************************************************* */
{
  int    i, j, k;
  double ***vx, ***vy, ***vz;
  double ***Bx, ***By, ***Bz;

  #if !(CT_EMF_AVERAGE == ARITHMETIC  || \
        CT_EMF_AVERAGE == RIEMANN_2D  || \
        CT_EMF_AVERAGE == UCT_CONTACT || \
        CT_EMF_AVERAGE == UCT0        || \
        CT_EMF_AVERAGE == UCT_HLL)
    print1 ("! Unknown EMF average\n");
    QUIT_ORION2(1);
  #endif

/* -------------------------------------
       set boundary conditions on 
       face-centered electric fields
   ------------------------------------- */
/*
  #ifdef SINGLE_STEP
   if (step == 2)
  #endif
  EMF_BOUNDARY (&emf, grid);
*/

/* ------------------------------------------------------
       Compute slopes of staggered magnetic fields 
   ------------------------------------------------------ */

  #if CT_EMF_AVERAGE == UCT_HLL
   #ifdef SINGLE_STEP
    if (ISTEP == 1)
   #endif
    STAGGERED_SLOPES(d->Vs, &emf, grid);
  #endif

/* -----------------------------------------------------
                 Select average 
   ----------------------------------------------------- */

  #if CT_EMF_AVERAGE == ARITHMETIC || CT_EMF_AVERAGE == RIEMANN_2D

   ARITHMETIC_AVERAGE (&emf, 0.25);

  #elif CT_EMF_AVERAGE == UCT_CONTACT

   ARITHMETIC_AVERAGE (&emf, 1.0);
   INTEGRATE_EMF_CORNER (d, &emf, grid);
   for (k = emf.kbeg; k <= emf.kend + KOFFSET; k++){
   for (j = emf.jbeg; j <= emf.jend + JOFFSET; j++){
   for (i = emf.ibeg; i <= emf.iend + IOFFSET; i++){      
     #if DIMENSIONS == 3
      emf.ex[k][j][i] *= 0.25;
      emf.ey[k][j][i] *= 0.25;
     #endif
     emf.ez[k][j][i] *= 0.25;
   }}}

  #elif CT_EMF_AVERAGE == UCT_HLL

   #ifdef SINGLE_STEP
    if (ISTEP == 1) CMUSCL_AVERAGE (d, &emf, grid);
    else   
   #endif
   UCT_HLL_AVERAGE (d, &emf, grid);

  #elif CT_EMF_AVERAGE == UCT0

/* -- Subtract cell-centered contribution -- */

   EXPAND(vx = d->Vc[VX]; Bx = d->Vc[BX];  ,
          vy = d->Vc[VY]; By = d->Vc[BY];  ,
          vz = d->Vc[VZ]; Bz = d->Vc[BZ];)

   for (k = emf.kbeg; k <= emf.kend + KOFFSET; k++){
   for (j = emf.jbeg; j <= emf.jend + JOFFSET; j++){
   for (i = emf.ibeg; i <= emf.iend + IOFFSET; i++){      
     #if DIMENSIONS == 3
      emf.exj[k][j][i] *= 2.0;
      emf.exk[k][j][i] *= 2.0;
      emf.eyi[k][j][i] *= 2.0;
      emf.eyk[k][j][i] *= 2.0;

      emf.exj[k][j][i] -= 0.5*(EX(k,j,i) + EX(k,j+1,i));
      emf.exk[k][j][i] -= 0.5*(EX(k,j,i) + EX(k+1,j,i));

      emf.eyi[k][j][i] -= 0.5*(EY(k,j,i) + EY(k,j,i+1));
      emf.eyk[k][j][i] -= 0.5*(EY(k,j,i) + EY(k+1,j,i));
     #endif
     emf.ezi[k][j][i] *= 2.0;
     emf.ezj[k][j][i] *= 2.0;
     emf.ezi[k][j][i] -= 0.5*(EZ(k,j,i) + EZ(k,j,i+1));
     emf.ezj[k][j][i] -= 0.5*(EZ(k,j,i) + EZ(k,j+1,i));
   }}}

   ARITHMETIC_AVERAGE (&emf, 0.25);

  #endif

/* -----------------------------------------------------
     Contributions from resistive terms is accounted
     using the standard arithmetic average.
     We'll keep this in absence of something better...
   ----------------------------------------------------- */

  #if RESISTIVE_MHD == EXPLICIT 
   #ifdef SINGLE_STEP
    if (ISTEP == 1) return (&emf);
   #endif 
   for (k = emf.kbeg; k <= emf.kend; k++){
   for (j = emf.jbeg; j <= emf.jend; j++){
   for (i = emf.ibeg; i <= emf.iend; i++){      
     #if DIMENSIONS == 3
      emf.ex[k][j][i] += 0.25*( emf_res.exk[k][j][i] + emf_res.exk[k][j + 1][i] 
                              + emf_res.exj[k][j][i] + emf_res.exj[k + 1][j][i]);
      emf.ey[k][j][i] += 0.25*( emf_res.eyi[k][j][i] + emf_res.eyi[k + 1][j][i] 
                              + emf_res.eyk[k][j][i] + emf_res.eyk[k][j][i + 1]);
     #endif 
     emf.ez[k][j][i] += 0.25*( emf_res.ezi[k][j][i] + emf_res.ezi[k][j + 1][i] 
                             + emf_res.ezj[k][j][i] + emf_res.ezj[k][j][i + 1]);
   }}}
  #endif
   
/* -------------------------------------------------------------
    Fine Tuning: EMF_USERDEF_BOUNDARY can be used to directly
    set the edge-centered electric field
   ------------------------------------------------------------- */
/*
  #ifdef SINGLE_STEP
   if (step == 2)
  #endif
  {
    int lside[3] = {X1_BEG, X2_BEG, X3_BEG};
    int rside[3] = {X1_END, X2_END, X3_END};
    int dir;

    for (dir = 0; dir < DIMENSIONS; dir++){
      if (grid[dir].lbound == USERDEF)
        EMF_USERDEF_BOUNDARY (&emf, lside[dir], EDGE_EMF, grid);  
      if (grid[dir].rbound == USERDEF)
        EMF_USERDEF_BOUNDARY (&emf, rside[dir], EDGE_EMF, grid);  
    }   
  }
*/

  return (&emf);
}

/* *********************************************************** */
void EMF_RETRIEVE (real ***temfx, real ***temfy, real ***temfz)
/* 
 *
 * PURPOSE
 *
 *  Retrieve the stored electric field components.
 *
 ************************************************************* */
{
  int    i, j, k;

  for (k = emf.kbeg; k <= emf.kend + KOFFSET; k++){
     for (j = emf.jbeg; j <= emf.jend + JOFFSET; j++){
       for (i = emf.ibeg; i <= emf.iend + IOFFSET; i++){      
	 temfx[k+1][j+1][i]=emf.ex[k][j][i];
	 temfy[k+1][j][i+1]=emf.ey[k][j][i];
	 temfz[k][j+1][i+1]=emf.ez[k][j][i];
       }
     }
   }

}
#undef EX
#undef EY
#undef EZ

#undef eps_UCT_CONTACT
