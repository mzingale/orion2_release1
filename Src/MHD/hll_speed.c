#include"orion2.h"

/*   ****  switches, see below for description  ****  */

#define ROE_ESTIMATE    NO
#define DAVIS_ESTIMATE  YES   


/* ***************************************************************************** */
void HLL_SPEED (real **vL, real **vR, real **bgf, 
                real *SL, real *SR, int beg, int end)
/* 
 *
 *
 * NAME
 *
 *   HLL_SPEED
 *
 *
 * PURPOSE
 *
 *   Compute leftmost (SL) and rightmost (SR) speed for the Riemann fan
 * 
 *
 * ARGUMENTS
 *
 *   vL (IN)       1-D array of left-edge primitive values at i+1/2
 *   vR (IN)       1-D array of right-edge primitive values at i+1/2
 *   grid (IN)     Array of grids
 *   cmax(OUT)     Array of maximum characteristic speeds in this direction
 *
 * SWITCHES
 *
 *    ROE_ESTIMATE (YES/NO), DAVIS_ESTIMATE (YES/NO). TVD_ESTIMATE (YES/NO)
 *    JAN_HLL (YES/NO) 
 *
 *    These switches set how the wave speed estimates are
 *    going to be computed. Only one can be set to 'YES', and
 *    the rest of them must be set to 'NO'  
 *
 *     ROE_ESTIMATE:    b_m = \min(0, \min(u_R - c_R, u_L - c_L, u_{roe} - c_{roe}))     
 *                      b_m = \min(0, \min(u_R + c_R, u_L + c_L, u_{roe} + c_{roe}))
 * 
 *                      where u_{roe} and c_{roe} are computed using Roe averages.
 *  
 *     DAVIS_ESTIMATE:  b_m = \min(0, \min(u_R - c_R, u_L - c_L))     
 *                      b_m = \min(0, \min(u_R + c_R, u_L + c_L))  
 *
 *
 *
 *
 *
 * LAST_MODIFIED
 *
 *   March, 31/2006, Andrea Mignone  (mignone@to.astro.it)
 *              
 *
 ******************************************************************************** */
{
  int  i;
  real scrh, s, c;
  static real *sl_min, *sl_max;
  static real *sr_min, *sr_max;
  static real *sm_min, *sm_max;
  static real **vm;

  if (sl_min == NULL){
    vm = Array_2D(NMAX_POINT, NVAR, double);
    sl_min = Array_1D(NMAX_POINT, double);
    sl_max = Array_1D(NMAX_POINT, double);

    sr_min = Array_1D(NMAX_POINT, double);
    sr_max = Array_1D(NMAX_POINT, double);

    sm_min = Array_1D(NMAX_POINT, double);
    sm_max = Array_1D(NMAX_POINT, double);
  }

/* ----------------------------------------------
              DAVIS Estimate  
   ---------------------------------------------- */

  #if DAVIS_ESTIMATE == YES

   MAX_CH_SPEED (vL, sl_min, sl_max, bgf, beg, end);
   MAX_CH_SPEED (vR, sr_min, sr_max, bgf, beg, end);

   for (i = beg; i <= end; i++) {

    SL[i] = dmin(sl_min[i], sr_min[i]);
    SR[i] = dmax(sl_max[i], sr_max[i]);

    #if EOS == IDEAL   
     scrh  = fabs(vL[i][V1]) + fabs(vR[i][V1]);    
     scrh = scrh/(sqrt(gmm*vL[i][PR]/vL[i][DN]) + sqrt(gmm*vR[i][PR]/vR[i][DN]));
    #elif EOS == ISOTHERMAL
     scrh  = 0.5*( fabs(vL[i][V1]) + fabs(vR[i][V1]) )/C_ISO;    
    #endif

    MAX_MACH_NUMBER = dmax(scrh, MAX_MACH_NUMBER);  

   }

  #endif

/* ----------------------------------------------
              Roe-like Estimate  
   ---------------------------------------------- */

  #if ROE_ESTIMATE == YES

   MAX_CH_SPEED (vL, sl_min, sl_max, bgf, beg, end);
   MAX_CH_SPEED (vR, sr_min, sr_max, bgf, beg, end);

   for (i = beg; i <= end; i++) {

    scrh = sqrt(vR[i][DN]/vL[i][DN]);
    s    = 1.0/(1.0 + scrh);
    c    = 1.0 - s;

    vm[i][DN] = vL[i][DN]*scrh;
    vm[i][V1] = s*vL[i][V1] + c*vR[i][V1];

  /*  ----------------------------------
       the next definition is not the 
       same as the one given by Roe; it 
       is used here to define the sound 
       speed.
      ----------------------------------  */

    #if EOS == IDEAL
     vm[i][PR] = s*gpl + c*gpr;  
    #endif
 
  /* ---------------------------------------
      this is the definitions given by
      Cargo and Gallice, see the Roe 
      Solver
     --------------------------------------- */

    EXPAND(vm[i][B1] = c*vL[i][B1] + s*vR[i][B1];   ,
           vm[i][B2] = c*vL[i][B2] + s*vR[i][B2];   ,
           vm[i][B3] = c*vL[i][B3] + s*vR[i][B3];)

   }

   MAX_CH_SPEED(vm, sm_min, sm_max, bgf, beg, end);

   for (i = beg; i <= end; i++) {
     SL[i] = dmin(sl_min[i], sm_min[i]);
     SR[i] = dmax(sr_max[i], sm_max[i]);

     #if EOS == IDEAL
      scrh  = fabs(vm[i][V1])/sqrt(vm[i][PR]/vm[i][DN]);
     #elif EOS == ISOTHERMAL
      scrh  = fabs(vm[i][V1])/C_ISO;
     #endif   
     MAX_MACH_NUMBER = dmax(scrh, MAX_MACH_NUMBER); 
   }

  #endif
   
}
#undef DAVIS_ESTIMATE
#undef ROE_ESTIMATE

