#include "orion2.h"

/* **************************************************************  */
void RESTART (Input *ini, int nrestart)
/*
 *
 *
 * PURPOSE
 *
 *
 ****************************************************************  */
{
  int    nv, single_file, origin;
  char   filename[128];
  Output *output;
  FILE *fr, *fbin;

  RESTART_GET (ini, nrestart);

  output = ini->output;
  if (output->type != DBL_OUTPUT){
    print1 (" ! Error: restart possibile only from double-precision data\n");
    QUIT_ORION2(1);
  }
  print1 ("> restarting from file #%d\n",output->nfile);
  single_file = strcmp(output->mode,"single_file") == 0;
  
/* -----------------------------------------------------------------
                     Read data from disk
   ----------------------------------------------------------------- */

  if (single_file){ 

    sprintf (filename, "data.%04d.dbl", output->nfile);

    fbin = FILE_OPEN (filename, sizeof(double), "r");
    for (nv = 0; nv < output->nvar; nv++) {
      if (output->dump_var[nv]) {
/*        printf ("output var name = %s\n",output->var_name[nv]); */
        READ_BIN_ARRAY (output->V[nv], fbin, output->stag_var[nv]);
      }
    }
    FILE_CLOSE (fbin, sizeof(double));

  }else{

    for (nv = 0; nv < output->nvar; nv++) {
      if (output->dump_var[nv]){
        sprintf (filename, "%s.%04d.dbl", output->var_name[nv], 
                                              output->nfile);
        fbin = FILE_OPEN (filename, sizeof(double), "r");
        READ_BIN_ARRAY (output->V[nv], fbin, output->stag_var[nv]);
        FILE_CLOSE (fbin, sizeof(double));
      }
    }
  }

/* ----------------------------------------------
    Increment *ALL* output file numbers for
    next output 
   ---------------------------------------------- */
 
  for (nv = 0; nv < MAX_OUTPUT_TYPES; nv++) output[nv].nfile++;

}

/* ******************************************************* */
void RESTART_GET (Input *ini, int nrestart)
/*
 *
 * PURPOSE 
 *
 *   Collect runtime information needed for (potential)
 *   later restarts.
 *
 ********************************************************* */
{
  int  origin, n;
  Runtime runtime;
  FILE *fr;

  if (prank == 0) {

    fr = fopen ("restart.out", "rb");
    ERROR (fr == NULL,"Can not find restart.out\n");

    origin = nrestart < 0 ? SEEK_END:SEEK_SET;

    if (fseek (fr, nrestart*sizeof(Runtime), origin)){
      print (" ! Error: cannot determine restart position \n"); 
      QUIT_ORION2(1);
    }
    if (fread (&runtime, sizeof (Runtime), 1, fr) == 0) {
      print (" ! Error: cannot restart from file #%d\n",nrestart);
      QUIT_ORION2(1);
    }
    fclose(fr);
  }

  #ifdef PARALLEL
   MPI_Bcast (&runtime, sizeof (Runtime), MPI_BYTE, 0, MPI_COMM_WORLD);
  #endif

  glob_time = runtime.t;
  delta_t   = runtime.dt;
  NSTEP     = runtime.nstep;
  for (n = 0; n < MAX_OUTPUT_TYPES; n++){
    ini->output[n].nfile = runtime.nfile[n];
  }
}

/* ******************************************************* */
void RESTART_DUMP (Input *ini, int nfile)
/*
 *
 * PURPOSE 
 *
 *   Collect runtime information needed for (potential)
 *   later restarts.
 *
 ********************************************************* */
{
  int n;
  Runtime runtime;
  FILE *fr;

/* --------------------------------------------------
    define runtime structure elements here
   -------------------------------------------------- */

  runtime.t  = glob_time;
  runtime.dt = delta_t;
  runtime.nstep = NSTEP;
  for (n = 0; n < MAX_OUTPUT_TYPES; n++){
    runtime.nfile[n] = ini->output[n].nfile - 1;
  }  

/* --------------------------------------------------
         dump structure to disk
   -------------------------------------------------- */

  if (prank == 0) {
    if (nfile == 0) {
      fr = fopen ("restart.out", "wb");
    }else {
      fr = fopen ("restart.out", "r+b");
      fseek (fr, nfile*sizeof(Runtime), SEEK_SET); 
    }

    fwrite (&runtime, sizeof(Runtime), 1, fr);
    fclose(fr);
  }

}




