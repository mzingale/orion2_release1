#include "orion2.h"

/* ************************************************************* */
void RECONSTRUCT (const State_1D *state, Grid *grid)
/* 
 *
 *
 *  PURPOSE
 *    
 *    provide piece-wise linear reconstruction inside each 
 *    cell. Notice that vL and vR are left and right states
 *    with respect to the INTERFACE, while am and ap (later
 *    in this function) refer to the cell center, that is:
 *
 *                    VL-> <-VR
 *      |--------*--------|--------*--------|
 *       <-am   (i)   ap->       (i+1)
 *    
 *
 **************************************************************** */
{
  int nv, i, beg, end;
  
  #if INTERPOLATION == WENO5 || \
      INTERPOLATION == WENO7 || \
      INTERPOLATION == WENO9
   return;
  #endif
  
  beg = grid[DIR].lbeg - grid[DIR].nghost;
  end = grid[DIR].lend + grid[DIR].nghost;

  for (i = beg; i <= end; i++) {
  for (nv = 0; nv < NVAR; nv++) {
    state->vm[i][nv] = state->vp[i][nv] = state->v[i][nv];
  }}
  
/*  -------------------------------------------
      Assign face-centered magnetic field
    -------------------------------------------  */

  #ifdef STAGGERED_MHD
   for (i = beg; i <= end-1; i++) {
     state->vR[i][B1] = state->vL[i][B1] = state->bn[i];
   }
  #endif

}
