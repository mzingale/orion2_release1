#include "orion2.h"

/* ************************************************************* */
void RECONSTRUCT (const State_1D *state, Grid *grid)
/* 
 *
 *
 *  PURPOSE
 *    
 *    provide convex eno interpolation inside each 
 *    cell. Notice that vL and vR are left and right states
 *    with respect to the INTERFACE, while vm and vp (later
 *    in this function) refer to the cell center, that is:
 *
 *                    vL-> <-vR
 *      |--------*--------|--------*--------|
 *       <-vm   (i)   vp->       (i+1)
 *    
 *
 *
 *  Limiters are set in the function set_limiters(). 
 *  A default setup is used when LIMITER is set to DEFAULT in
 *  in your definitions.h. 
 *
 *  The convex eno routine works in the finite difference sense;
 *  this would require a change in the definition of the fluxes
 *  used in the update routine, i.e.
 *
 *   f(i+1/2) --> (1 - D2/24) f(i+1/2)
 *  
 *  however, this step is not implemented in the version of ORION2 
 *  and therefore, the scheme retains a 2nd order global 
 *  accuracy.
 *
 *  A stencil of 5 zones is required, and therefore
 *  at least 3 ghost zones are necessary.
 * 
 *  REFERENCE:
 *
 *   "An efficient shoc-capturing central-type scheme 
 *    for multidimensional relativistic flows"
 *   L. Del Zanna and N. Bucciantini
 *   A & A, (2002), 390, 1177
 *
 *
 *  The following MAPLE script can be used to check the values of the 3 
 *  Polynomial at x(i-1/2) and x(i+1/2):
 
restart;

Q[k] := v[k] + (v[k+1] - v[k-1])/2*(x - k) + (v[k+1] - 2*v[k] + v[k-1])/2*(x - k)^2;

Pm(x) := eval(Q[k],k=-1);
Pc(x) := eval(Q[k],k=0);
Pp(x) := eval(Q[k],k=1);

Pm_left := eval(Pm(x), x=-1/2);
Pc_left := eval(Pc(x), x=-1/2);
Pp_left := eval(Pp(x), x=-1/2);

Pm_rght := eval(Pm(x), x=1/2);
Pc_rght := eval(Pc(x), x=1/2);
Pp_rght := eval(Pp(x), x=1/2);

 *  LAST MODIFIED
 *
 *   July 10, 2006 by A. mignone (mignone@to.astro.it).
 *
 **************************************************************** */
{
  int    nv, i, beg, end;
  static Limiter *limiter[NVAR];
  static real **dlim, *d;
  real **v, **vp, **vm, scrh;

  real QM, QC, QP;
  real dM, dC, dP;
  real L;

  #if TIME_STEPPING != RK2 && TIME_STEPPING != RK3
   print1 (" ! CENO interpolation works with RK integrators only\n");
   QUIT_ORION2(1);
  #endif

  beg = grid[DIR].lbeg - grid[DIR].nghost + 2;
  end = grid[DIR].lend + grid[DIR].nghost - 2;

/* -----------------------------------------------------------
            Set pointers to limiter functions         
   ----------------------------------------------------------- */

  if (limiter[0] == NULL) {
    SET_LIMITER (limiter);
    d    = Array_1D(NMAX_POINT, double);
    dlim = Array_2D(NVAR, NMAX_POINT, double);
  }

  v  = state->v;
  vp = state->vp;
  vm = state->vm;

/* ----------------------------------------------------------
       compute slopes and left and right interface values 
   ---------------------------------------------------------- */

  for (nv = NVAR; nv--; ){

    #ifdef STAGGERED_MHD
     if (nv == B1) continue;
    #endif

    for (i = beg - 1; i <= end + 1; i++) {
      d[i]  = v[i + 1][nv] - v[i][nv];
    }

    (limiter[nv])(d, d - 1, dlim[nv], beg, end, grid + DIR);

    for (i = beg; i <= end; i++) {

     /* ---------------------------------------------------
          Make left edge state,  vm at x = x(i) - dx/2

         Here QM, QC and QP are the values of the 
         three polynomials Q^k_i computed at x = x(i-1/2);
        --------------------------------------------------- */

      L  = v[i][nv] - 0.5*dlim[nv][i];

      QM = ( -     v[i - 2][nv] +  6.0*v[i - 1][nv] + 3.0*v[i    ][nv])*0.125;
      QC = (   3.0*v[i - 1][nv] +  6.0*v[i    ][nv] -     v[i + 1][nv])*0.125;
      QP = (  15.0*v[i    ][nv] - 10.0*v[i + 1][nv] + 3.0*v[i + 2][nv])*0.125;

      dM = QM - L;
      dC = 0.7*(QC - L);
      dP = QP - L;

      if ( (dM > 0.0 && dC > 0.0 && dP > 0.0) ||
           (dM < 0.0 && dC < 0.0 && dP < 0.0) ){

        if (fabs(dM) < fabs(dC)) {
          if (fabs(dM) < fabs(dP)) vm[i][nv] = QM; 
          else                     vm[i][nv] = QP; 
        }else{
          if (fabs(dC) < fabs(dP)) vm[i][nv] = QC; 
          else                     vm[i][nv] = QP; 
        }
      }else{
        vm[i][nv] = L;
      }

     /* ---------------------------------------------------
           Make right edge state,  vp at x = x(i) + dx/2

         Here QM, QC and QP are the values of the 
         three polynomials Q^k_i computed at x = x(i+1/2);
        --------------------------------------------------- */

      L  = v[i][nv] + 0.5*dlim[nv][i];

      QM = (  15.0*v[i    ][nv] - 10.0*v[i - 1][nv] + 3.0*v[i - 2][nv])*0.125;
      QC = (   3.0*v[i + 1][nv] +  6.0*v[i    ][nv] -     v[i - 1][nv])*0.125;
      QP = ( -     v[i + 2][nv] +  6.0*v[i + 1][nv] + 3.0*v[i    ][nv])*0.125;

      dM = QM - L;
      dC = 0.7*(QC - L);
      dP = QP - L;

      if ( (dM > 0.0 && dC > 0.0 && dP > 0.0) ||
           (dM < 0.0 && dC < 0.0 && dP < 0.0) ){

        if (fabs(dM) < fabs(dC)) {
          if (fabs(dM) < fabs(dP)) vp[i][nv] = QM; 
          else                     vp[i][nv] = QP; 
        }else{
          if (fabs(dC) < fabs(dP)) vp[i][nv] = QC; 
          else                     vp[i][nv] = QP; 
        }
      }else{
        vp[i][nv] = L;
      }
    }
  }

 /* -------------------------------------------------
     check if negative densities or pressures are 
     produced during the interpolation.
     If so, switch to Linear interpolation.
    ------------------------------------------------- */

  for (i = beg; i <= end; i++) {

    if (   vp[i][DN] < 0.0 || vm[i][DN] < 0.0
        || vp[i][PR] < 0.0 || vm[i][PR] < 0.0){
      for (nv = 0; nv < NFLX; nv++){
        vp[i][nv] = v[i][nv] + 0.5*dlim[nv][i];
        vm[i][nv] = v[i][nv] - 0.5*dlim[nv][i];
      }
    }
  }

/*  -------------------------------------------
        Shock flattening 
    -------------------------------------------  */

  #if SHOCK_FLATTENING != NO 
   FLATTEN (state, beg, end, grid);
  #endif

/* --------------------------------------------------
            Relativistic Limiter
   -------------------------------------------------- */

  #if PHYSICS == RHD || PHYSICS == RMHD
   RELATIVISTIC_LIM (state, beg, end);
  #endif

/*  -------------------------------------------
      Assign face-centered magnetic field
    -------------------------------------------  */

  #ifdef STAGGERED_MHD
   for (i = beg-1; i <= end; i++) {
     state->vR[i][B1] = state->vL[i][B1] = state->bn[i];
   }
  #endif

}
