#include "orion2.h"

/* ************************************************************* */
void RECONSTRUCT (const State_1D *state, Grid *grid)
/* 
 *
 *
 *  PURPOSE
 *    
 *    provide piece-wise linear reconstruction inside each 
 *    cell. Notice that vL and vR are left and right states
 *    with respect to the INTERFACE, while vm and vp (later
 *    in this function) refer to the cell center, that is:
 *
 *                    vL-> <-vR
 *      |--------*--------|--------*--------|
 *       <-vm   (i)   vp->       (i+1)
 *    
 *
 *
 *  Limiters are set in the function set_limiters(). 
 *  A default setup is used when LIMITER is set to DEFAULT in
 *  in your definitions.h. 
 *  Characteristic limiting is enabled when CHAR_LIMITING is set
 *  to YES in your definitions.h
 *
 *
 *  A stencil of 3 zones is required for all limiters 
 *  except for the fourth_order_lim which requires 
 *  5 zones. 
 *
 *  LAST MODIFIED
 *
 *   May 2, 2008 by A. mignone (mignone@to.astro.it).
 *
 **************************************************************** */
{
  int    nv, i, beg, end;
  static int s;
  static Limiter *limiter[NVAR];
  static real **dv, **dv_lim;
  static real *d, *dlim;
  real **v, **vp, **vm;
  double dvc;

  /* PS: Flux-ct scheme, before upgrade
#ifdef STAGGERED_MHD
  beg = grid[DIR].lbeg - 3;
  end = grid[DIR].lend + 3;
#else
  beg = grid[DIR].lbeg - 1;
  end = grid[DIR].lend + 1;
#endif
  */

  v  = state->v;
  vp = state->vp;
  vm = state->vm;

/* -----------------------------------------------------------
            Set pointers to limiter functions         
   ----------------------------------------------------------- */

  if (d == NULL) {
    SET_LIMITER (limiter);
    d      = Array_1D(NMAX_POINT, double);
    dlim   = Array_1D(NMAX_POINT, double);
    dv     = Array_2D(NMAX_POINT, NVAR, double);
    dv_lim = Array_2D(NMAX_POINT, NVAR, double);

    s = 1; /* -- default left/right stencil for linear interpolation -- */
    for (nv = NVAR; nv--; ) if  (limiter[nv] == fourth_order_lim) s = 2;
  }

/* ---- get stencil ---- */

  beg = grid[DIR].lbeg - grid[DIR].nghost + s;
  end = grid[DIR].lend + grid[DIR].nghost - s;

/* ----------------------------------------------------------
       compute slopes and left and right interface values 
   ---------------------------------------------------------- */

  #if CHAR_LIMITING == NO   /* ---- primitive variable limiting ---- */

   for (nv = NVAR; nv--; ){

     #ifdef STAGGERED_MHD
      if (nv == B1) continue;
     #endif

     for (i = 0; i <= end+s-1; i++) {
       d[i] = v[i + 1][nv] - v[i][nv];
     }

     (limiter[nv])(d, d - 1, dlim, beg, end, grid + DIR);
     for (i = beg; i <= end; i++) {
/*
dvc=Lmc_lim(d[i],d[i-1]);
vp[i][nv] = v[i][nv] + 0.5*dvc;
vm[i][nv] = v[i][nv] - 0.5*dvc;
*/
       vp[i][nv] = v[i][nv] + 0.5*dlim[i];
       vm[i][nv] = v[i][nv] - 0.5*dlim[i];
     }
   }

  #else                     /* ---- characteristic variable limiting ---- */

   CHAR_SLOPES (state, dv, beg, end, grid);
   for (i = beg; i <= end; i++){
   for (nv = NVAR; nv--;  ){
     vp[i][nv] = v[i][nv] + 0.5*dv[i][nv];
     vm[i][nv] = v[i][nv] - 0.5*dv[i][nv];
   }}
   
  #endif
   
/*  -------------------------------------------
               Shock flattening 
    -------------------------------------------  */

  #if SHOCK_FLATTENING == MULTID && CHAR_LIMITING == NO
   FLATTEN (state, beg, end, grid);
  #endif

/* --------------------------------------------------
            Relativistic Limiter
   -------------------------------------------------- */

  #if PHYSICS == RHD || PHYSICS == RMHD
   RELATIVISTIC_LIM (state, beg, end);
  #endif

/*  -------------------------------------------
        Shock flattening 
    -------------------------------------------  */

  #if SHOCK_FLATTENING == ONED 
   FLATTEN (state, beg, end, grid);
  #endif

/*  -------------------------------------------
      Assign face-centered magnetic field
    -------------------------------------------  */

  #ifdef STAGGERED_MHD
   for (i = beg - 1; i <= end; i++) {
     state->vR[i][B1] = state->vL[i][B1] = state->bn[i];
   }
  #endif

}

