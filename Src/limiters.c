#include"orion2.h"


/* ********************************************************************** */
double Lflat_lim (const double dp, const double dm)
/* 
 *
 *   Zero-slope; first order interpolation
 *
 ************************************************************************ */
{
  return 0.0;
}


double Lminmod_lim (const double dp, const double dm)
/* 
 *
 * Evaluate slopes using the minmod limiter:
 *
 *
 *                   xm  when |xm| < |xp|  AND xp*xm > 0
 *  minmod(xp, xm) = xp  when |xp| < |xm|  AND xp*xm > 0
 *                   0   otherwise
 *
 *  Here xp and xm are approximations to forward and backward first 
 *  derivative.
 * 
 ************************************************************************ */
{
  return (dp*dm > 0.0 ? (fabs(dp) < fabs(dm) ? dp:dm):0.0);
}

#define EPS_VA  1.e-12
double Lvanalbada_lim (const double dp, const double dm)
/* 
 *
 *
 *   va(x,y) = x*y*(x + y)/(x*x + y*y)
 *
 *
 * Notice although this limiter does not require the switch x*y > 0,
 * we nevertheless employ it.
 * 
 ************************************************************************ */
{
  double s;
  s = dp*dm;
  return (s > 0.0 ? s*(dp + dm)/(dp*dp + dm*dm + EPS_VA):0.0);
}
#undef EPS_VA
      


/* ********************************************************************** */
double Lvanleer_lim (const double dp, const double dm)
/* 
 *
 *    vl = 2*x*y/(x + y)   when x*y > 0
 *    vl = 0               otherwise
 *
 ************************************************************************ */
{
  double s;
  s = dp*dm;
  return (s > 0.0 ? 2.0*s/(dp + dm): 0.0);
}

/* ********************************************************************** */
double Lmc_lim (const double dp, const double dm)
/* 
 *
 *
 * Evaluate slopes using the monotonized central difference limiter:
 *
 *      mc_lim (xp, xm, xc) = MinMod [2*xp , 2*xm , xc]
 *
 *  Where xp, xm and xc are, respectively, approximations to 
 *  the forward, backward and central derivatives.
 *
 ************************************************************************ */
{
  double dc, d2;
  if (dp*dm < 0.0) return 0.0;
  dc = 0.5*(dp + dm);
  d2 = 2.0*(fabs(dp) < fabs(dm) ? dp:dm);
  return (fabs(d2) < fabs(dc) ? d2:dc);
}











/* *******************************************************************
 
   FILE: limiters.c

   PURPOSE: Compute slope limiters needed in the interpolation 
            routines; slopes are returned as

                           dV|
            dV_LIM[i]  =   --|  Delta x_i 
                           dx|i 
  
            where dV/dx depends on the choice of the limiter and the 
            grid type (uniform or not).


   ARGUMENTS:

     dwp (in)    : array w/forward  undivided differences 
     dwm (in)    : array w/backward undivided differences
     dw_lim(out) : array of limited slopes
     ibeg (in)   : starting point
     iend (in)   : ending point
     GG (in)     : grid structure in the current sweep direction 


   NOTE:   In developing new slopes limiters, keep in mind that 
           in order to satisfy the TVD condition you must have
           (see TORO, pg 477):

        min(U[i], U[i+1]) <= UR = U[i] + dV_LIM[i] <= max (U[i], U[i+1])

        min(U[i], U[i-1]) <= UL = U[i] - dV_LIM[i] <= max (U[i], U[i-1])
 
   ******************************************************************* */


/* ********************************************************************** */
void flat_lim (real *dwp, real *dwm, real *dv_lim,
               int ibeg, int iend, Grid *grid)
/* 
 *
 *   Zero-slope; first order interpolation
 *
 ************************************************************************ */
{
  int i;

  for (i = ibeg; i <= iend; i++){
    dv_lim[i] = 0.0;
  }
}

/* ********************************************************************** */
void minmod_lim (real *dwp, real *dwm, real *dv_lim,
                 int ibeg, int iend, Grid *grid)
/* 
 *
 * Evaluate slopes using the minmod limiter:
 *
 *
 *                   xm  when |xm| < |xp|  AND xp*xm > 0
 *  minmod(xp, xm) = xp  when |xp| < |xm|  AND xp*xm > 0
 *                   0   otherwise
 *
 *  Here xp and xm are approximations to forward and backward first 
 *  derivative.
 * 
 ************************************************************************ */
{
  int i;
  real xp, xm;

  for (i = ibeg; i <= iend; i++){
    if (dwp[i]*dwm[i] > 0.0){
      dv_lim[i] = fabs(dwp[i]) < fabs(dwm[i]) ? dwp[i]:dwm[i];
    }else{
      dv_lim[i] = 0.0;
    }      
  }

}

/* ********************************************************************** */
void vanleer_lim (real *dwp, real *dwm, real *dv_lim,
                  int ibeg, int iend, Grid *grid)
/* 
 *
 *    vl = 2*x*y/(x + y)   when x*y > 0
 *    vl = 0               otherwise
 *
 ************************************************************************ */
{
  int i;
  real scrh, s;

  for (i = ibeg; i <= iend; i++){
    s         = dwp[i]*dwm[i];
    dv_lim[i] = s > 0.0 ? 2.0*s/(dwp[i] + dwm[i]):0.0;        
  }
}

/* ********************************************************************** */
void mc_lim (real *dwp, real *dwm, real *dv_lim,
             int ibeg, int iend, Grid *grid)
/* 
 *
 *
 * Evaluate slopes using the monotonized central difference limiter:
 *
 *      mc_lim (xp, xm, xc) = MinMod [2*xp , 2*xm , xc]
 *
 *  Where xp, xm and xc are, respectively, approximations to 
 *  the forward, backward and central derivatives.
 *
 ************************************************************************ */
{
  int i;
  real xc, scrh;

  for (i = ibeg; i <= iend; i++){
    if ( dwp[i]*dwm[i] > 0.0 ){
      xc        = 0.5*(dwp[i] + dwm[i]);
      scrh      = 2.0*(fabs(dwp[i]) < fabs(dwm[i]) ? dwp[i]:dwm[i]);
      dv_lim[i] = fabs(scrh) < fabs(xc) ? scrh: xc;
    }else{
      dv_lim[i] = 0.0;
    }
  }
  
}

/* ********************************************************************** */
void gminmod_lim (real *dwp, real *dwm, real *dv_lim,
                  int ibeg, int iend, Grid *grid)
/* 
 *
 *
 * Evaluate slopes using the general mindmod limiter:
 *
 *      gminmod (xp, xm, xc) = MinMod [a*xp , a*xm , xc]
 *
 *  Where xp, xm and xc are, respectively, approximations to 
 *  the forward, backward and central derivatives, and 
 *  1 < a < 2.
 *
 * setting a = 1  yields  the minmod limiter
 * setting a = 2  yields  the mc     limiter
 *
 *
 ************************************************************************ */
{
  int i;
  real xc, scrh, a = 1.5;

  for (i = ibeg; i <= iend; i++){
    if ( dwp[i]*dwm[i] > 0.0 ){
      xc     = 0.5*(dwp[i] + dwm[i]);
      scrh   = a*(fabs(dwp[i]) < fabs(dwm[i]) ? dwp[i]:dwm[i]);
      dv_lim[i] = fabs(scrh) < fabs(xc) ? scrh: xc;
    }else{
      dv_lim[i] = 0.0;
    }
  }
  
}

#define EPS_VA  1.e-18
/* ********************************************************************** */
void vanalbada_lim (real *dwp, real *dwm, real *dv_lim,
                    int ibeg, int iend, Grid *grid)
/* 
 *
 *
 *   va(x,y) = x*y*(x + y)/(x*x + y*y)
 *
 *
 * Notice although this limiter does not require the switch x*y > 0,
 * we nevertheless employ it.
 * 
 ************************************************************************ */
{
  int  i;
  real s, xp, xm;
  real dwp2, dwm2, xp2, xm2;

  for (i = ibeg; i <= iend; i++){
    if ( (s = dwp[i]*dwm[i]) > 0.0) {    
      dwp2 = dwp[i]*dwp[i];
      dwm2 = dwm[i]*dwm[i];
      dv_lim[i] = (dwp[i]*(dwm2 + EPS_VA) + dwm[i]*(dwp2 + EPS_VA))
                   /(dwp2 + dwm2 + EPS_VA);
    } else {
      dv_lim[i] = 0.0;
    }
  }
      
}
#undef EPS_VA
/* ********************************************************************** */
void umist_lim (real *dwp, real *dwm, real *dv_lim, 
                int ibeg, int iend, Grid *grid)
/* 
 *
 *
 * Evaluate slopes using the Umist Limiter :
 *
 *   UMIST (x,y) = MinMod [2*xp , 2*xm , 1/4 (xp + 3xm) , 1/4 (xm + 3xp)]
 *
 *
 ************************************************************************ */
{
  int  i;
  real x1,x2, xp, xm, scrh;
 
  for (i = ibeg; i <= iend; i++){
    if ( dwp[i]*dwm[i] > 0.0) {    
      x1 = 0.25*(dwp[i] + 3.0*dwm[i]);
      x2 = 0.25*(dwm[i] + 3.0*dwp[i]);                                   
      scrh = 2.0*(fabs(dwp[i]) < fabs(dwm[i]) ? dwp[i]:dwm[i]);
      scrh = fabs(scrh) < fabs(x1) ? scrh:x1;
      dv_lim[i] = fabs(scrh) < fabs(x2) ? scrh:x2;
    } else {
      dv_lim[i] = 0.0;
    }
  }

}

/* ********************************************************************** */
void fourth_order_lim (real *dwp, real *dwm, real *dv_lim,
                       int ibeg, int iend, Grid *grid)
/* 
 *
 *   Evaluate slopes using Colella's fourth-order slope
 * 
 *  Ref:  Miller, G.H and P. COlella, 
 *        "A high-order Eulerian Godunov Method for 
 *         Elastic-Plastic Flow in Solids", JCP 167,131-176 (2001)
 *    
 *                             +
 *
 *        Saltzman, J, " An Unsplit 3D Upwind Method for 
 *                       Hyperbolic Conservation Laws", 
 *                       JCP 115, 153-168 (1994)
 *
 ************************************************************************ */
{
  int i;
  static real *s;
  static real *dqf, *dqc, *dqlim;
  real scrh, alpha;

  if (s == NULL){
    s     = Array_1D(NMAX_POINT, double);
    dqf   = Array_1D(NMAX_POINT, double);
    dqc   = Array_1D(NMAX_POINT, double);
    dqlim = Array_1D(NMAX_POINT, double);
  }

  alpha = 2.0;

  for (i = ibeg - 1; i <= iend + 1; i++){
    dqc[i] = 0.5*(dwp[i] + dwm[i]);
      s[i] =  (dwp[i] > 0.0 ? 0.5:-0.5) 
            + (dwm[i] > 0.0 ? 0.5:-0.5);
    dqlim[i] = alpha*dmin( fabs(dwp[i]) , fabs(dwm[i]) );
    dqf[i]   = dmin( fabs(dqc[i]) , dqlim[i] )*s[i];
  }

  for (i = ibeg; i <= iend; i++){
    if (dwp[i]*dwm[i] > 0.0) {
      scrh = 4.0/3.0*dqc[i] - (dqf[i + 1] + dqf[i - 1])/6.0; 
      dv_lim[i] = dmin( fabs(scrh), dqlim[i])*s[i];
    }else{
      dv_lim[i] = 0.0;
    }
  } 
}

/* ********************************************************************** */
void triad_lim (real *dwp, real *dwm, real *dw_lim_p, real *dw_lim_m,
                int ibeg, int iend, Grid *grid)
/* 
 *
 * PURPOSE
 *
 *  Compute the left and right slopes using the triad
 *  limiter described in
 *
 *
 *  "Adaptive Limiters for Improving the Accuracy
 *   of the MUSCL Approach for Unsteady Flows"
 *
 *   G. Billet and O. Louedin, JCP, 170, 161 (2001)
 *
 *
 *  NOTE: this function provide two slopes per cell.
 *
 ************************************************************************ */
{
  int    i;
  real scrhp, scrhm;
  real A, B, i2, i4, om;
  real k, Dp, Dm;
  static real *s;

/* -----------------------------------------------------------
            Set pointers to limiter functions         
   ----------------------------------------------------------- */

  if (s == NULL) {
    s  = Array_1D(NMAX_POINT, double);
  }

/* ----------------------------------------------------------
       compute slopes and left and right interface values 
   ---------------------------------------------------------- */

  for (i = ibeg - 2; i <= iend + 1; i++) {
    s[i] = 0.0;
    if (dwp[i] > 0.0) s[i] =  1.0;
    if (dwp[i] < 0.0) s[i] = -1.0;
  }

  for (i = ibeg; i <= iend; i++) {
     
    i4 = 0.25*fabs(s[i-2] + s[i-1] + s[i] + s[i+1]);
    i2 = 0.5 *fabs(s[i-1] + s[i]);

    k  = i4/3.0 + (1.0 - i4)*i2;
    om = (3.0 - k)/(1.0 - k + 1.e-9)*i2 + (1.0 - i2);
              
    A = dwp[i]*s[i-1];
    B = dwm[i]*s[i];
    Dp = dmin(A, om*B);
    Dp = dmax(0.0, Dp)*s[i];

    Dm = dmin(B, om*A);
    Dm = dmax(0.0, Dm)*s[i-1];

    dw_lim_p[i] = 0.5*((1.0 - k)*Dm + (1.0 + k)*Dp);
    dw_lim_m[i] = 0.5*((1.0 - k)*Dp + (1.0 + k)*Dm);
      
  }       

}
