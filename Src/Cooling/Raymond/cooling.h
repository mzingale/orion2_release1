/* ############################################################
      
     FILE:     cooling.h

     PURPOSE:  contains common definitions for the 
               whole CODE

   ############################################################ */

#define NIONS  1
#define DNEUT   NFLX    /* -- this is the 'conservative' index -- */
#define FNEUT   DNEUT   /* -- this is the 'primitive' index -- */

real GET_MAX_RATE (real *, real *, real);
real MEAN_MOLECULAR_WEIGHT  (real *);
void RADIAT (real *, real *);










