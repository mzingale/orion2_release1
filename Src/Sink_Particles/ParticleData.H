#include <iostream>
using std::ostream;
using std::istream;
#include <REAL.H>

/* A bare-bones class for holding the information on a single particle.
   It just holds a particle position. */

class ParticleData {

  friend istream &operator>>(istream &is, ParticleData& partdata);
  friend ostream &operator<<(ostream &os, const ParticleData& partdata);

public: 

  /* The constructor */
  ParticleData(Real *newpos);
  ParticleData() {};

  /* The desctructor */
  virtual ~ParticleData();

  /* Method to return the position */
  Real* position();

  /* Assignment operator. This is made virtual so it can be overridden
     by derived classes. */
  virtual ParticleData &operator=(const ParticleData& rhs);
  Real pos[CH_SPACEDIM]; //ALR - moved here, code wouldn't compile otherwise after adding CPD
protected:

  //  Real pos[CH_SPACEDIM];     /* Position of particle */
};
