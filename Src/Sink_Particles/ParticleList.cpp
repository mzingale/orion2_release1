#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <iomanip>

#include <mpi.h>
#include <AMR.H>
#include <AMRLevelOrion.H>
#include <MayDay.H> // exit from error functions
#include <SPMD.H>   // mpi related parallel programming functions
#include <DataIterator.H>
#include <ParticleList.H>

/**********************************************************/
/* class ParticleList routines */

// Trivial template function to swap two values -- used by quickSort
template <class T> inline void qswap(T &a, T& b) {
  T temp;
  temp = a;
  a = b;
  b = temp;
}

template <class P>
ParticleList<P>::ParticleList(AMR* Parent)
{
  parent = Parent;
  ncreated = 0;
  ncreated_old = 0;
}

template <class P>
ParticleList<P>::ParticleList()
{
  parent = NULL;
  ncreated = 0;
  ncreated_old = 0;
}

template <class P>
ParticleList<P>::~ParticleList() {}

template <class P>
void
ParticleList<P>::addParticle(P &newpart)
{
  ParticleID newid(CHprocID(), ncreated);
  ncreated++;
  particles.resize(particles.size()+1, newpart);
  id.resize(id.size()+1, newid);
}

template <class P>
void
ParticleList<P>::deleteParticle(int num)
{
  particles.erase(particles.begin()+num);
  id.erase(id.begin()+num);
}

template <class P>
int
ParticleList<P>::numParticles()
{
  return(particles.size());
}

template <class P>
P&
ParticleList<P>::operator[](int n) {
  return(particles[n]);
}

template <class P>
void
ParticleList<P>::write(const std::string& dir, const std::string& filename)
{
  // Sync the particle list
  syncParticleList();

  // We are passed the directory into which we will create the
  // Particles file. First, we need to construct the name of
  // the file to create.
  std::string FullName = dir;
  if (!(FullName.size()==0) && FullName[FullName.size()-1] != '/')
    FullName += '/';
  FullName += filename;

  // Create the file stream and set up the io buffering
  //AJC VisMF::IO_Buffer io_buffer(VisMF::IO_Buffer_Size);
  std::ofstream ParticleFile;
  //AJC ParticleFile.rdbuf()->pubsetbuf(io_buffer.dataPtr(), io_buffer.size());

  //If we are using the Particle class which uses the gsl random number generator then we need to
  //also create a file that stores the random number generator
  //state per particle.


  // Only IO processor does this
  if (CHprocID()==uniqueProc(SerialTask::compute)) {

    // Open the file
    ParticleFile.open(FullName.c_str(), ios::out|ios::trunc);
    if (!ParticleFile.good()) MayDay::Error( "File Open Failed:" + *FullName.c_str());
    // Write the number of particles
    ParticleFile << numParticles() << " " << ncreated << std::endl;

    // Write the data
    for (int i=0; i<numParticles(); i++) {
      ParticleFile << particles[i] << " " << id[i].partNum() << std::endl;
    }
    // Close the file
    ParticleFile.close();
  }
}

template <class P>
void
ParticleList<P>::read(const std::string& dir, const std::string& filename)
{
  // We are passed the directory in which we will find the
  // Particles file. First, we need to construct the name of
  // the file to create.
  std::string FullName = dir;
  if (!(FullName.size()==0) && FullName[FullName.size()-1] != '/')
    FullName += '/';
  FullName += filename;

  // Create the file stream and set up the io buffering
  //AJC VisMF::IO_Buffer io_buffer(VisMF::IO_Buffer_Size);
  std::ifstream ParticleFile;
  //AJC ParticleFile.rdbuf()->pubsetbuf(io_buffer.dataPtr(), io_buffer.size());


  // Only IO processor does this
  if (CHprocID()==uniqueProc(SerialTask::compute)) {

    // Open the file
    ParticleFile.open(FullName.c_str(), ios::in);

    if (!ParticleFile.good()) {
      // We flag this with a warning rather than exiting -- that way
      // we maintain compatibility with old check files that don't
      // have particles.
      std::cerr << "restart: Warning: could not open " << FullName.c_str()
		<< std::endl;
      } else {

      // Read the number of sink particles
      int npart_read;
      ParticleFile >> npart_read >> ncreated;
      ncreated_old = ncreated;
      pout() << "read ncreated, = " << ncreated << endl;
      

      // Loop through particles
      for (int i=0; i<npart_read; i++) {
	P newpart;
	ParticleID newid;
        long newnum;
	// Read the data for the particle
	ParticleFile >> newpart;
	ParticleFile >> newnum;
	newid.setPartNum(newnum);
	
	pout() << "read newnum, = " << newid.partNum() << endl;
	
	// Add this particle to the particle list
	addParticle(newpart, newid);
      }

      // Close the file
      ParticleFile.close();
    }
  }
  //CEH
  //Pass ncreated_old to all processors, otherwise syncParticleList
  //will reset the all the Particle ID numbers.
  int myproc=CHprocID();
  int i = uniqueProc(SerialTask::compute); // IO processor
  pout() << "IO Processor = " << i << endl;
  broadcast(ncreated_old, i);
  // Sync the particle list
  syncParticleList();
}  

template <class P>
void
ParticleList<P>::syncParticleList()
{
  // Set up useful shorthands
  int nproc=numProc();
  int myproc=procID();
  int mypart=numParticles();

  // First find out how many particles there are on each processor
  int *nPartProc = new int[nproc];
  int totpart = 0, myptr = 0;


  for (int i=0; i<nproc; i++) {
    if (i==myproc) nPartProc[i] = numParticles();
    broadcast(nPartProc[i], i);
    totpart += nPartProc[i];
    if (i<myproc) myptr += nPartProc[i];
  }
  if (totpart==0) {
    delete [] nPartProc;
    nPartProc = NULL;
    return;
  }
  
  // Now assign each particle a priority equal to the finest grid on this
  // processor that contains the particle.
  int *priority = new int[totpart];
  for (int j=0; j<totpart; j++) priority[j]=-1;
  Vector<AMRLevel*> amrLevels = parent->getAMRLevels();
  for (int lev=0; lev<amrLevels.size(); lev++) { // Loop over levels
    AMRLevelOrion& level = *static_cast<AMRLevelOrion*>(amrLevels[lev]);
    // Grab data on this level. We don't need the state information (only
    // the grid information), but we want to use the MultiFabIterator to
    // handle the parallelism transparently.
    
    LevelData<FArrayBox>& state = level.getStateNew();
    DataIterator dit = state.dataIterator();
    for (dit.reset(); dit.ok(); ++dit) {
      // Loop through particles and find the boxes that contain them
      for (int j=0; j<mypart; j++) {
        // Find the IntVect of the cell containing this particle.
        IntVect iPos;
        for (int n=0; n<CH_SPACEDIM; n++)
          iPos[n] = floor((particles[j].position()[n]-DOM_XBEG[n])/level.getDx());
          if (state[dit()].box().contains(iPos)) priority[myptr+j] = lev;
      }//end loop over particles
    }//end loop over grids
  }//end loop over levers

  // Allocate storage space to hold a copy of every particle in
  // the calculation (and its id)
  vector<P> allParticles;
  vector<ParticleID> allIDs;
  allParticles.resize(totpart);
  allIDs.resize(totpart);


  // Now exchange the particle lists and priority lists so every processor
  // has a copy of every particle.
  for (int i=0, listptr=0; i<nproc; listptr+=nPartProc[i++]
       ) {

    // If there are no particles on this processor, skip it
    if (nPartProc[i] == 0) continue;

    if (i==myproc) { // Each processor loads its particles into the list
      for (int j=0; j<mypart; j++) {
	allParticles[listptr+j] = particles[j];
	allIDs[listptr+j] = id[j];
      }
    }

    /*
    // Now exchange particles, ids, and priorities
    ParallelDescriptor::BroadcastVoid((void *) &(allParticles[listptr]),
				      nPartProc[i]*sizeof(P), i);
    ParallelDescriptor::BroadcastVoid((void *) &(allIDs[listptr]),
				      nPartProc[i]*sizeof(ParticleID), i);
    ParallelDescriptor::Broadcast(&(priority[listptr]),
				  nPartProc[i], i);
    */

    // AJC - Chombo doesn't have BroadcastVoid, use mpi calls directly
    MPI_Bcast((void *) &(allParticles[listptr]), nPartProc[i]*sizeof(P), 
	      MPI_BYTE, i, MPI_COMM_WORLD);
    MPI_Bcast((void *) &(allIDs[listptr]), nPartProc[i]*sizeof(ParticleID), 
	      MPI_BYTE, i, MPI_COMM_WORLD);
    MPI_Bcast((void *) &(priority[listptr]), nPartProc[i], 
	      MPI_INT, i, MPI_COMM_WORLD);
  }


  // Delete the current particle list
  particles.clear();
  id.clear();

  // Reserve space in the particle and data lists. For large numbers
  // of particles this should significantly increase speed.
  particles.reserve(totpart);
  id.reserve(totpart);

  // Each sort the particles by ID
  quickSort(totpart, allIDs, allParticles, priority);

  // Go through the sorted list to find the copies. Then keep the copy
  // that has the highest priority.
  for (int j=0, listptr=0; j<=totpart; j++) {

    // If this particle is the same as the one at the beginning of this
    // block, just continue incrementing the pointer.
    if (j != totpart)
      if (allIDs[j] == allIDs[listptr]) continue;

    // This particle is not the same as the one at the beginning of the
    // block, so we have reached the end of this block of identical
    // particles. Go through the block of identical particles we just
    // finished and find the one with the highest priority.
    int partptr = -1;
    int maxpriority = -2;
    for (int i=listptr; i<j; i++) {
      if (priority[i] > maxpriority) {
	maxpriority = priority[i];
	partptr = i;
      }
    }

    // Add the highest priority copy of the particle to the list
    addParticle(allParticles[partptr], allIDs[partptr]);

    // Move listptr to point to the beginning of the next block
    listptr = j;
  }
  /*CEH
    If 2 particles are created at the same time on different processors,
    they could both have partnum = ncreated_old.  This section makes sure
    particles just created all have unique ID numbers
  */
  ncreated = ncreated_old;
  for (int j=0; j<particles.size(); j++){
    if((id[j].partNum() >= ncreated_old)){
      id[j].setPartNum(ncreated);
      ncreated++;
    }
  }
  ncreated_old = ncreated;

  // hack ctss
  pout() << "after syncParticleList, numParticles() =  " << numParticles() << endl;

  
  // Clean up memory
  delete [] priority;
  delete [] nPartProc;
  priority = NULL;
  nPartProc = NULL;
  particles.resize(particles.size());
  id.resize(id.size());
}


template <class P>
void
ParticleList<P>::quickSort(int n, vector<ParticleID>& id,
			vector<P>& part, int priority[])
{
  // Numerical recipes quicksort routine
#define M 7
#define NSTACK 50

  int i,ir=n-1,j,k,l=0,*istack;
  int jstack=0;
  ParticleID a1;
  P a2;
  int a3;

  if (!(istack=(int *) calloc(NSTACK,sizeof(int)))) {
    std::cerr << "ParticleList::quickSort: unable to allocate workspace" << std::endl;
    MayDay::Abort();
  }
  istack=istack-1;
  for (;;) {
    if (ir-l < M) {
      for (j=l+1;j<=ir;j++) {
	a1=id[j];
	a2=part[j];
	a3=priority[j];
	for (i=j-1;i>=l;i--) {
	  if (id[i] <= a1) break;
	  id[i+1]=id[i];
	  part[i+1]=part[i];
	  priority[i+1]=priority[i];
	}
	id[i+1]=a1;
	part[i+1]=a2;
	priority[i+1]=a3;
      }
      if (jstack == 0) break;
      ir=istack[jstack--];
      l=istack[jstack--];
    } else {
      k=(l+ir) >> 1;
      qswap(id[k],id[l+1]);
      qswap(part[k],part[l+1]);
      qswap(priority[k],priority[l+1]);
      if (id[l] > id[ir]) {
	qswap(id[l],id[ir]);
	qswap(part[l],part[ir]);
	qswap(priority[l],priority[ir]);
      }
      if (id[l+1] > id[ir]) {
	qswap(id[l+1],id[ir]);
	qswap(part[l+1],part[ir]);
	qswap(priority[l+1],priority[ir]);
      }
      if (id[l] > id[l+1]) {
	qswap(id[l],id[l+1]);
	qswap(part[l],part[l+1]);
	qswap(priority[l],priority[l+1]);
      }
      i=l+1;
      j=ir;
      a1=id[l+1];
      a2=part[l+1];
      a3=priority[l+1];
      for (;;) {
	do i++; while (id[i] < a1);
	do j--; while (id[j] > a1);
	if (j < i) break;
	qswap(id[i],id[j]);
	qswap(part[i],part[j]);
	qswap(priority[i],priority[j]);
      }
      id[l+1]=id[j];
      part[l+1]=part[j];
      priority[l+1]=priority[j];
      id[j]=a1;
      part[j]=a2;
      priority[j]=a3;
      jstack += 2;
      if (jstack > NSTACK) {
	std::cerr << "ParticleList::quickSort: stack size too small" << std::endl;
	MayDay::Abort();
      }
      if (ir-i+1 >= j-l) {
	istack[jstack]=ir;
	istack[jstack-1]=i;
	ir=j-1;
      } else {
	istack[jstack]=j-1;
	istack[jstack-1]=l;
	l=i;
      }
    }
  }
  free(istack+1);

#undef M
#undef NSTACK
}

template <class P>
void
ParticleList<P>::addParticle(P &newpart, ParticleID &newid)
{
  /*  
      The addParticle(newpart); is mostly to initialize addParticle to overcome
      aggressive compiler optimization.
  */
  if(newid.partNum() == -1){
    addParticle(newpart);
  }
  else{
    particles.resize(particles.size()+1, newpart);
    id.resize(id.size()+1, newid);
  }
}

#ifdef BACKUP
template <class P>
void
ParticleList<P>::saveBackup()
{
  id_backup = id;
  particles_backup = particles;
  ncreated_backup = ncreated;
}

template <class P>
void
ParticleList<P>::restoreBackup()
{
  id = id_backup;
  particles = particles_backup;
  ncreated = ncreated_backup;
}
#endif

//////////////////////////////////////////////
// Template explicit instantiations
//////////////////////////////////////////////

#ifdef CH_AIX
// On IBM systems, use the compiler's pragma directive because the
// compiler doesn't support the ISO standard
#  ifdef SINKPARTICLE
#    ifdef STARPARTICLE
#       include <StarParticleData.H>
#       pragma define(ParticleList<StarParticleData>);
#    elif defined(RADPARTICLE)
#       include <RadParticleData.H>
#       pragma define(ParticleList<RadParticleData>);
#    else
#       include <SinkParticleData.H>
#       pragma define(ParticleList<SinkParticleData>);
#    endif
#  endif
#else
// use ISO standard explicit instantiation, which is
// supported by compiler version 8.0 (and possibly earlier)
#  ifdef SINKPARTICLE
#    ifdef STARPARTICLE
#      include <StarParticleData.H>
       template class ParticleList<StarParticleData>;
#    elif defined(RADPARTICLE)
#      include <RadParticleData.H>
       template class ParticleList<RadParticleData>;
#    else
#      include <SinkParticleData.H>
       template class ParticleList<SinkParticleData>;
#    endif
#  endif //SINKPARTICLE
#endif
