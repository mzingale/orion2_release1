#include "REAL.H"

/* CAUTION: This is the ANSI C (only) version of the Numerical Recipes
   utility file nrutil.h.  Do not confuse this file with the same-named
   file nrutil.h that is supplied in the 'misc' subdirectory.
   *That* file is the one from the book, and contains both ANSI and
   traditional K&R versions, along with #ifdef macros to select the
   correct version.  *This* file contains only ANSI C.               */

#ifndef _NR_UTILS_H_
#define _NR_UTILS_H_

static Real sqrarg;
//#define SQR(a) ((sqrarg=(a)) == 0.0 ? 0.0 : sqrarg*sqrarg)
#define SQR(a)((a)*(a))

static double dsqrarg;
//#define DSQR(a) ((dsqrarg=(a)) == 0.0 ? 0.0 : dsqrarg*dsqrarg)
#define DSQR(a)((a)*(a))

static double dmaxarg1,dmaxarg2;
#define DMAX(a,b) (dmaxarg1=(a),dmaxarg2=(b),(dmaxarg1) > (dmaxarg2) ?\
        (dmaxarg1) : (dmaxarg2))

static double dminarg1,dminarg2;
#define DMIN(a,b) (dminarg1=(a),dminarg2=(b),(dminarg1) < (dminarg2) ?\
        (dminarg1) : (dminarg2))

static Real maxarg1,maxarg2;
#define FMAX(a,b) (maxarg1=(a),maxarg2=(b),(maxarg1) > (maxarg2) ?\
        (maxarg1) : (maxarg2))

static Real minarg1,minarg2;
#define FMIN(a,b) (minarg1=(a),minarg2=(b),(minarg1) < (minarg2) ?\
        (minarg1) : (minarg2))

static long lmaxarg1,lmaxarg2;
#define LMAX(a,b) (lmaxarg1=(a),lmaxarg2=(b),(lmaxarg1) > (lmaxarg2) ?\
        (lmaxarg1) : (lmaxarg2))

static long lminarg1,lminarg2;
#define LMIN(a,b) (lminarg1=(a),lminarg2=(b),(lminarg1) < (lminarg2) ?\
        (lminarg1) : (lminarg2))

static int imaxarg1,imaxarg2;
#define IMAX(a,b) (imaxarg1=(a),imaxarg2=(b),(imaxarg1) > (imaxarg2) ?\
        (imaxarg1) : (imaxarg2))

static int iminarg1,iminarg2;
#define IMIN(a,b) (iminarg1=(a),iminarg2=(b),(iminarg1) < (iminarg2) ?\
        (iminarg1) : (iminarg2))

#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

void nrerror(char error_text[]);
Real *nr_vector(long nl, long nh);
int *nr_ivector(long nl, long nh);
unsigned char *nr_cvector(long nl, long nh);
unsigned long *nr_lvector(long nl, long nh);
double *nr_dvector(long nl, long nh);
Real **nr_matrix(long nrl, long nrh, long ncl, long nch);
double **nr_dmatrix(long nrl, long nrh, long ncl, long nch);
int **nr_imatrix(long nrl, long nrh, long ncl, long nch);
Real **nr_submatrix(Real **a, long oldrl, long oldrh, long oldcl, long oldch,
	long newrl, long newcl);
Real **nr_convert_matrix(Real *a, long nrl, long nrh, long ncl, long nch);
Real ***nr_f3tensor(long nrl, long nrh, long ncl, long nch, long ndl, long ndh);
void free_nr_vector(Real *v, long nl, long nh);
void free_nr_ivector(int *v, long nl, long nh);
void free_nr_cvector(unsigned char *v, long nl, long nh);
void free_nr_lvector(unsigned long *v, long nl, long nh);
void free_nr_dvector(double *v, long nl, long nh);
void free_nr_matrix(Real **m, long nrl, long nrh, long ncl, long nch);
void free_nr_dmatrix(double **m, long nrl, long nrh, long ncl, long nch);
void free_nr_imatrix(int **m, long nrl, long nrh, long ncl, long nch);
void free_nr_submatrix(Real **b, long nrl, long nrh, long ncl, long nch);
void free_nr_convert_matrix(Real **b, long nrl, long nrh, long ncl, long nch);
void free_nr_f3tensor(Real ***t, long nrl, long nrh, long ncl, long nch,
	long ndl, long ndh);

#endif /* _NR_UTILS_H_ */

void ParticleInt(Real ystart[], int nvar, Real x1, Real x2, Real eps, Real h1,
	    Real hmin, int *nok, int *nbad, Real mass[], Real G, Real soften,
	    void (*derivs)(Real, Real [], Real [], Real [], Real, Real, int),
	    void (*bsstep)(Real [], Real [], int, Real *, Real, Real, Real [],
			   Real *, Real *, Real [], Real, Real,
			   void (*)(Real, Real [], Real [], Real [], Real,
				    Real, int)));

void ParticleDerivs(Real time, Real x[], Real d2xdt2[], Real mass[], Real G,
		    Real soften, int nvar);

void bsstep(Real y[], Real dydx[], int nv, Real *xx, Real htry, Real eps,
	    Real yscal[], Real *hdid, Real *hnext, Real mass [], Real G,
	    Real soften,
	    void (*derivs)(Real, Real [], Real [], Real [], Real, Real, int));

void pzextr(int iest, Real xest, Real yest[], Real yz[], Real dy[], int nv);

void stoerm(Real y[], Real d2y[], int nv, Real xs, Real htot, int nstep,
	    Real yout[], Real mass[], Real G, Real soften,
	    void (*derivs)(Real, Real [], Real [], Real [], Real, Real, int));



