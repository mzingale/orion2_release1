#ifdef CH_LANG_CC
/*
*      ____                         __ __
*     / __ \ ____ __ ___   __  __  / // /
*    / /_/ // __// // _ \ /  \/ / / // /
*    \____//_/  /_/ \___//_/\__/ /_//_/
*    
*    Based on Pluto 3 and Chombo.
*
*    Please refer to COPYING in Pluto's root directory and 
*    Copyright.txt, in Chombo's root directory.

*    Modification: Orional code based on PLUTO3.0
*    1) PS(08/06/10): Rename Pluto to Orion.
*
*/
#endif

#ifndef _AMR_LEVEL_GODUNOV_FACTORY_H_
#define _AMR_LEVEL_GODUNOV_FACTORY_H_

#include "AMRLevelFactory.H"
#include "AMRLevelOrion.H"

#ifdef SINKPARTICLE
#if defined(STARPARTICLE) 
#include <StarParticleList.H>
#include <StarParticleData.H>
#else
#include <SinkParticleList.H>
#include <SinkParticleData.H>
#endif //STARPARTICLE 
#endif //SINKPARTICLE

/// AMR Orion factory
/**
 */
class AMRLevelOrionFactory : public AMRLevelFactory
{
public:
  /// Null constructor
  /**
   */
  AMRLevelOrionFactory();

  /// Virtual constructor
  /**
   */
  virtual AMRLevel* new_amrlevel() const;

  /// Destructor
  /**
   */
  virtual ~AMRLevelOrionFactory();

  /// CFL number
  /**
   */
  virtual void CFL(Real a_cfl);

  /// Physical dimension of the longest side of the domain
  /**
   */
  virtual void domainLength(Real a_domainLength);

  ///
  /**
   */
  virtual void verbosity(const int& verbosity);

  /// Refinement threshold
  /**
   */
  virtual void densityGradThreshold(vector<Real> *a_thresh);
  virtual void velocityGradThreshold(vector<Real> *a_thresh);
  virtual void diskTagThreshold(vector<Real> *a_thresh);
  virtual void BGradThreshold(vector<Real> *a_thresh);
  virtual void radiationGradThreshold(vector<Real> *a_thresh);
  virtual void shockDetectionThreshold(vector<Real> *a_thresh);
  virtual void JeansNoThreshold(vector<Real> *a_thresh);
  virtual void Zoom(vector<Real> *a_thresh);
  virtual void Nested(vector<Real> *a_thresh);
  virtual void NestedSphere(vector<Real> *a_thresh);
  virtual void DensityTagThreshold(vector<Real> *a_thresh);
  /// Tag buffer size
  /**
   */
  void tagBufferSize(int a_tagBufferSize);

  /// Initial dt multiplier
  /**
   */
  void initialDtMultiplier(Real a_initialDtMultiplier);

  /// PatchOrion object (used as a factory)
  /**
   */
  virtual void patchOrion(PatchOrion* a_patchOrion);

  /// Check that everything is defined
  /**
   */
  bool isDefined() const;

#ifdef SINKPARTICLE
#if defined(STARPARTICLE) 
  void setSinkList(StarParticleList<StarParticleData>* a_sinks);
#else
  void setSinkList(SinkParticleList<SinkParticleData>* a_sinks);
#endif //STARPARTICLE 
#endif //SINKPARTICLE

protected:
  // Some default values
  void setDefaultValues();

  int m_verbosity;

  // CFL number
  Real m_cfl;
  bool m_cflSet;

  // Physical dimension of the longest side of the domain
  Real m_domainLength;
  bool m_domainLengthSet;

  // Refinement threshold for gradient
  vector<Real>  *m_densityGradThreshold, 
                *m_velocityGradThreshold, 
                *m_diskTagThreshold, 
                *m_BGradThreshold, 
                *m_radiationGradThreshold, 
                *m_shockDetectionThreshold, 
                *m_JeansNoThreshold,
                *m_Zoom,
                *m_Nested,
                *m_NestedSphere,
                *m_DensityTagThreshold;

  bool m_densityGradThresholdSet,
       m_velocityGradThresholdSet,
       m_diskTagThresholdSet,
       m_BGradThresholdSet,
       m_radiationGradThresholdSet, 
       m_shockDetectionThresholdSet, 
       m_JeansNoThresholdSet,
       m_ZoomSet,
       m_NestedSet,
       m_NestedSphereSet,
       m_DensityTagThresholdSet;

  // Tag buffer size
  int  m_tagBufferSize;
  bool m_tagBufferSizeSet;

  // Initial dt multiplier 
  Real m_initialDtMultiplier;
  bool m_initialDtMultiplierSet;

  // Patch integrator (used as a factory)
  const PatchOrion* m_patchOrion;
  bool              m_patchOrionSet;

#ifdef SINKPARTICLE
#if defined(STARPARTICLE) 
  static StarParticleList<StarParticleData> *m_sinks;
#else
  static SinkParticleList<SinkParticleData> *m_sinks;
#endif //STARPARTICLE 
#endif //SINKPARTICLE

private:
  // Disallowed for all the usual reasons
  void operator=(const AMRLevelOrionFactory& a_input)
  {
    MayDay::Error("invalid operator");
  }

  // Disallowed for all the usual reasons
  AMRLevelOrionFactory(const AMRLevelOrionFactory& a_input)
  {
    MayDay::Error("invalid operator");
  }
};

#endif
