/*
*      ____                         __ __
*     / __ \ ____ __ ___   __  __  / // /
*    / /_/ // __// // _ \ /  \/ / / // /
*    \____//_/  /_/ \___//_/\__/ /_//_/
*    
*    Based on Pluto 3 and Chombo.
*
*    Please refer to COPYING in Pluto's root directory and 
*    Copyright.txt, in Chombo's root directory.
*
*    Modification: Orional code based on PLUTO3.0b2
*    1) PS(09/25/08): Modified original code for CT scheme.
*    2) PS(12/04/08): Upgrade to PLUTO3 final release.
*    3) PS(02/20/09): Use FluxBox class for face-centered magnetic field.
*    4) PS(03/16/09): Modified the way putting the fluxes in a_F in PLUTO3
*                     to suit Orion2.
*    5) PS(04/09/09): Store EMF computed and transfer back to LevelPluto for
*                     refluxing.
*    6) PS(10/07/09): Upgrade to PLUTO 3.0.1 and modified SET_BOUNDARY for
*                     periodic boundaries to eliminate unnecessary physical
*                     boundaries update.
*    7) PS(11/05/09): Change temfx, temfy, and temfz not to include ghostzones.
*    8) PS(08/06/10): Rename Pluto to Orion.
*    9) PS(11/15/10): Add gravity source term in predictor step and final
*                     conservative update.
*/

#include <cstdio>
#include <string>
using std::string;

#include "PatchOrion.H"
#include "LoHiSide.H"
#include "ParmParse.H"

/* PS: Flux-ct scheme */
#ifdef STAGGERED_MHD
static void ADD_CTU_MHD_SOURCE (real **v, real **up, real **um,
                                real *dtdh, int beg, int end, Grid *grid);
#endif
#if BARO_COOL == YES
void GET_BAROTROPIC_COOL(const State_1D *, int, int, int, int, int, real, Grid *);
#endif
void SAVE_AMR_FLUXES (const State_1D *, int, int, Grid *);
static void SET_BOUNDARY (Data_Arr U,

/* PS: Flux-ct scheme */
#ifdef STAGGERED_MHD
                          Data_Arr Bs,
#endif
                          Data *d, Grid *grid);
static double level;

static double dxmin = 1.e40;

#ifdef STAGGERED_MHD
 #if TIME_EVOLUTION == CHARACTERISTIC_TRACING
  #define CTU_MHD_SOURCE YES
 #elif TIME_EVOLUTION == HANCOCK && PRIMITIVE_HANCOCK == YES
  #define CTU_MHD_SOURCE YES
 #else
  #define CTU_MHD_SOURCE NO
 #endif
#else
 #define CTU_MHD_SOURCE NO
#endif

void PatchOrion::UNSPLIT(FArrayBox&       U,
/* PS: Flux-ct scheme */
#ifdef STAGGERED_MHD
                         FArrayBox&       Bxs,
                         FArrayBox&       Bys,
                         FArrayBox&       Bzs,
                         FArrayBox&       EMFx,
                         FArrayBox&       EMFy,
                         FArrayBox&       EMFz,
#endif
#if defined(GRAVITY) || defined(SINKPARTICLE)
		         FArrayBox&       G,
#endif
#if defined(SINKPARTICLE)
#if defined(STARPARTICLE) 
			 StarParticleList<StarParticleData>& m_sinks,
#else
			 SinkParticleList<SinkParticleData>& m_sinks,
#endif
#endif //SINKPARTICLE
                         FArrayBox&       cover_tags,
                         FArrayBox&       split_tags,
                         FArrayBox        a_F[CH_SPACEDIM],
                         Time_Step        *Dts,
                         const Box&       UBox,  Grid *grid)
{
  CH_assert(isDefined());

  int nv;
  int nxf, nyf, nzf, indf;
  int nxb, nyb, nzb, nxe, nye, nze;
  int ii, jj, kk;
  int in;
  int *i, *j, *k;

  double cover, errp, errm, err;
  double ***UU[NVAR];
/* PS: Flux-ct scheme */
#ifdef STAGGERED_MHD
  double ***xtemp, ***ytemp, ***ztemp;
  double ***temfx, ***temfy, ***temfz;
  static real *dtdVx2, *dtdVy2, *dtdVz2;
  real *dtdV2;
  static Data_Arr BBs;
#endif
  double *du, **rhs;
#ifdef SKIP_SPLIT_CELLS
  double ***covercells, ***splitcells;
#endif
  static Data_Arr dU;
  static Data_Arr UPX, UMX;
  static Data_Arr UPY, UMY;
  static Data_Arr UPZ, UMZ;

  Data_Arr UP, UM;
  static Data d;
  Index indx;
  static State_1D state;
  double **up, **um;
#if defined(GRAVITY) || defined(SINKPARTICLE)
  static Data_Arr gf;
  double *gf1d, ***Gtemp[DIMENSIONS];
  real den, m2, b2, pr, cs, v, v2, vmax, sx;
  int vdir;
#endif
  static char *flagp, *flagm;  // these should go inside state !!
  static double dxmax = 0.0;

// Boxes for face centered fluxes
   Box fluxBox;

  Riemann_Solver *RIEMANN;
  RIEMANN = rsolver;

/* -----------------------------------------------------------------
               Check algorithm compatibilities
   ----------------------------------------------------------------- */

  #if INCLUDE_PARABOLIC_FLUX == YES && ARTIFICIAL_VISCOSITY == NO
   print1 ("! Parabolic Terms are 1st order in CTU at the moment\n");
   print1 ("! Uncomment this line if you wish to test them.\n");
   QUIT_ORION(1);
  #endif
 
  #if !(GEOMETRY == CARTESIAN || GEOMETRY == CYLINDRICAL)
   print1 ("! CTU only works in cartesian or cylindrical coordinates\n");
   QUIT_ORION(1);
  #endif     

  if (NX_TOT > NMAX_POINT || NY_TOT > NMAX_POINT || NZ_TOT > NMAX_POINT){
    printf ("!PatchUnsplit: need to re-allocate matrix\n");
    exit(1);
  }

/* -----------------------------------------------------------------
                          Allocate memory
   ----------------------------------------------------------------- */

  dxmax = dmax(dxmax, grid[IDIR].dx[IBEG]);
  level = log(dxmax/grid[IDIR].dx[IBEG])/log(2.0);

//printf (" Level = %f, %f %f\n",level, dxmax, grid[IDIR].dx[IBEG]);

  dxmin = dmin(dxmin, grid[IDIR].dx[IBEG]);

  for (nv=0 ; nv<NVAR ; nv ++){
    UU[nv] = chmatrix3(NZ_TOT,NY_TOT,NX_TOT,
              &U.dataPtr(0)[nv*NZ_TOT*NY_TOT*NX_TOT]);
  }

// PS: Flux-ct scheme
#ifdef STAGGERED_MHD
   xtemp = chmatrix3(NZ_TOT,NY_TOT,NX_TOT+1,&Bxs.dataPtr(0)[0]);
   ytemp = chmatrix3(NZ_TOT,NY_TOT+1,NX_TOT,&Bys.dataPtr(0)[0]);
   ztemp = chmatrix3(NZ_TOT+1,NY_TOT,NX_TOT,&Bzs.dataPtr(0)[0]);

   temfx = chmatrix3(NZ_TOT+1-2*m_numGhost,NY_TOT+1-2*m_numGhost,NX_TOT-2*m_numGhost,&EMFx.dataPtr(0)[0]);
   temfy = chmatrix3(NZ_TOT+1-2*m_numGhost,NY_TOT-2*m_numGhost,NX_TOT+1-2*m_numGhost,&EMFy.dataPtr(0)[0]);
   temfz = chmatrix3(NZ_TOT-2*m_numGhost,NY_TOT+1-2*m_numGhost,NX_TOT+1-2*m_numGhost,&EMFz.dataPtr(0)[0]);
#endif /* STAGGERED_MHD */

#if defined(GRAVITY) || defined(SINKPARTICLE)

   int numForceGhost = 2;
   int GNX = NX_TOT-2*(m_numGhost-numForceGhost);
   int GNY = NY_TOT-2*(m_numGhost-numForceGhost);
   int GNZ = NZ_TOT-2*(m_numGhost-numForceGhost);
   gf = Array_4D(DIMENSIONS,NZ_TOT,NY_TOT,NX_TOT,double);

   for (nv=0 ; nv<DIMENSIONS ; nv ++){
    Gtemp[nv] = chmatrix3(GNZ,GNY,GNX,&G.dataPtr(0)[nv*GNZ*GNY*GNX]);
   }

   int gxend = NX_TOT-numForceGhost;
   int gyend = NY_TOT-numForceGhost;
   int gzend = NZ_TOT-numForceGhost;

   for (kk = numForceGhost; kk < gzend; kk++) {
     for (jj = numForceGhost; jj < gyend; jj++) {
       for (ii = numForceGhost; ii < gxend; ii++) {
         gf[0][kk][jj][ii] = Gtemp[0][kk-numForceGhost][jj-numForceGhost][ii-numForceGhost];
         gf[1][kk][jj][ii] = Gtemp[1][kk-numForceGhost][jj-numForceGhost][ii-numForceGhost];
         gf[2][kk][jj][ii] = Gtemp[2][kk-numForceGhost][jj-numForceGhost][ii-numForceGhost];
       }
     }
   }
   gf1d = Array_1D(NMAX_POINT, double);
#endif /* GRAVITY */

  #ifdef SKIP_SPLIT_CELLS
   covercells = chmatrix3(NZ_TOT,NY_TOT,NX_TOT,cover_tags.dataPtr(0));
   splitcells = chmatrix (KBEG, KEND, JBEG, JEND, IBEG, IEND, split_tags.dataPtr(0));
  #endif

/* -----------------------------------------------------------
         Allocate static memory areas
   -----------------------------------------------------------  */

  if (state.flux == NULL){
/* PS: Flux-ct scheme */
#ifdef STAGGERED_MHD
    dtdVx2 = Array_1D(NMAX_POINT, double);
    dtdVy2 = Array_1D(NMAX_POINT, double);
    dtdVz2 = Array_1D(NMAX_POINT, double);
#endif

    Make_State (&state);

    #if DIMENSIONS ==  2
     nxf = NMAX_POINT; 
     nyf = NMAX_POINT;
     nzf = 1;
    #elif DIMENSIONS == 3
     nxf = NMAX_POINT; 
     nyf = NMAX_POINT;
     nzf = NMAX_POINT;
    #endif

    d.Vc   = Array_4D(NVAR, nzf, nyf, nxf, double);
    d.flag = Array_3D(nzf, nyf, nxf, char);
// PS: Flux-ct scheme
#ifdef STAGGERED_MHD
    d.Vs   = Array_4D(3, nzf, nyf, nxf, double);
    BBs    = Array_4D(3, nzf, nyf, nxf, double);
#endif
 
    flagp = Array_1D(NMAX_POINT, char);
    flagm = Array_1D(NMAX_POINT, char);

    dU  = Array_4D(nzf, nyf, nxf, NVAR, double);

  /* ------------------------------------------
      corner-coupled multidimensional arrays 
      are ordered in memory following the same 
      convention adopted when sweeping along 
      the coordinate directions, i.e., 

         (z,y,x)->(z,x,y)->(y,x,z).

      This allows 1-D arrays to conveniently
      point at the fastest running indexes 
      of the respective multi-D ones.
     ----------------------------------------- */  

    UMX  = Array_4D(nzf, nyf, nxf, NVAR, double);
    UPX  = Array_4D(nzf, nyf, nxf, NVAR, double);

    UMY  = Array_4D(nzf, nxf, nyf, NVAR, double);
    UPY  = Array_4D(nzf, nxf, nyf, NVAR, double);
   
    #if DIMENSIONS == 3
     UMZ  = Array_4D(nyf, nxf, nzf, NVAR, double);
     UPZ  = Array_4D(nyf, nxf, nzf, NVAR, double);
    #endif
  }

/* PS: Flux-ct scheme */
#ifdef STAGGERED_MHD
/*PS: setup BBs by skipping the 1st grid point of temp in each direction
      to keep the array size the same as cell-centered variables */

   for (kk = 0; kk < NZ_TOT; kk++) {
     for (jj = 0; jj < NY_TOT; jj++) {
       for (ii = 0; ii < NX_TOT; ii++) {
         BBs[BXs][kk][jj][ii] = xtemp[kk][jj][ii+1];
         BBs[BYs][kk][jj][ii] = ytemp[kk][jj+1][ii];
         BBs[BZs][kk][jj][ii] = ztemp[kk+1][jj][ii];
       }
     }
   }
#endif

/* ----------------------------------------------------
           SET BOUNDARY CONDITIONS  
   ---------------------------------------------------- */

  ISTEP = 1;
  SET_BOUNDARY (UU,
// PS: Flux-ct scheme
#ifdef STAGGERED_MHD
                BBs,
#endif
                &d, grid);

  FLAG_RESET (&d);
  #if SHOCK_FLATTENING == MULTID
   FIND_SHOCK (&d, grid);
  #endif

  #ifdef SKIP_SPLIT_CELLS
   DOM_LOOP(kk,jj,ii){
     if (splitcells[kk][jj][ii] < 0.5){
       d.flag[kk][jj][ii] |= FLAG_SPLIT_CELL;
     }
   }
  #endif

// PS: Flux-ct scheme
  Real dt2 = 0.5*delta_t;
#ifdef STAGGERED_MHD
  D_EXPAND(
   ITOT_LOOP(ii) dtdVx2[ii] = dt2/grid[IDIR].dV[ii]; ,
   JTOT_LOOP(jj) dtdVy2[jj] = dt2/grid[JDIR].dV[jj]; ,
   KTOT_LOOP(kk) dtdVz2[kk] = dt2/grid[KDIR].dV[kk];
  )
#endif

/* ----------------------------------------------------
     1. Normal predictors
   ---------------------------------------------------- */

  rhs = state.rhs;
  for (DIR = 0; DIR < DIMENSIONS; DIR++){
// PS: Flux-ct scheme
#ifdef STAGGERED_MHD
    if      (DIR == IDIR) {UP = UPX; UM = UMX; dtdV2 = dtdVx2;}
    else if (DIR == JDIR) {UP = UPY; UM = UMY; dtdV2 = dtdVy2;}
    else if (DIR == KDIR) {UP = UPZ; UM = UMZ; dtdV2 = dtdVz2;}
#else    
    if      (DIR == IDIR) {UP = UPX; UM = UMX;}
    else if (DIR == JDIR) {UP = UPY; UM = UMY;}
    else if (DIR == KDIR) {UP = UPZ; UM = UMZ;}
#endif

    SET_INDEXES (&indx, &state, grid);
    TRANSVERSE_LOOP(indx,in,i,j,k){  
      state.up  = UP[indx.t2][indx.t1]; state.uL = state.up;
      state.um  = UM[indx.t2][indx.t1]; state.uR = state.um + 1;

      if (DIR == IDIR) {
	state.rhs = dU[*k][*j];
        for (nv = NVAR; nv--;  )
          state.rhs[indx.beg-1][nv] = state.rhs[indx.end+1][nv] = 0.0;
      }else{
	state.rhs = rhs;
      }

      for (in = 0; in < indx.ntot; in++) {
	for (nv = NVAR; nv--;  ) state.v[in][nv] = d.Vc[nv][*k][*j][*i];
// PS: Flux-ct scheme
#ifdef STAGGERED_MHD
	state.bn[in] = d.Vs[DIR][*k][*j][*i];
#endif
      }

      CHECK_NAN (state.v, 0, indx.ntot - 1, 0);

      PRIMTOCON (state.v, state.u, 0, indx.ntot-1);

      STATES    (&state, indx.beg - 1, indx.end + 1, delta_t, grid);

      RIEMANN (&state, indx.beg - 1, indx.end, Dts->cmax + DIR, grid);

      /* PS: Flux-ct scheme */
      #ifdef STAGGERED_MHD
         EMF_PUT (&state, indx.beg-1, indx.end, grid);
      #endif

      /*PS: Gravity */
#if defined(GRAVITY) || defined(SINKPARTICLE)
      for (in = 0; in < indx.ntot; in++) {
	gf1d[in]=gf[DIR][*k][*j][*i];
      }

      GET_RHS (&state, indx.beg, indx.end, dt2, gf1d, grid);
#else
      GET_RHS (&state, indx.beg, indx.end, dt2, grid);
#endif

#if BARO_COOL == YES
      //GET_BAROTROPIC_COOL(&state, indx.beg, indx.end, i[0], j[0], k[0], dt2, grid);
#endif

      /* PS: Flux-ct scheme */
      #if CTU_MHD_SOURCE == YES
       ADD_CTU_MHD_SOURCE (state.v, state.up, state.um,
                           dtdV2, indx.beg - 1, indx.end + 1, grid);
      #endif
     
   /* --------------------------------------------------
        Compute time-centered cell-centered state for
        source term computation (in the final step)
      -------------------------------------------------- */

      if (DIR == IDIR){
        for (in = indx.beg; in <= indx.end; in++) {

          du = state.rhs[in];
          for (nv = NVAR; nv--; ){
            state.up[in][nv] -= du[nv];
            state.um[in][nv] -= du[nv];
          }
        }
      }else{
        for (in = indx.beg; in <= indx.end; in++) {
          du = state.rhs[in];
          for (nv = NVAR; nv--; ){
            dU[*k][*j][*i][nv] += du[nv];
            state.up[in][nv]   -= du[nv];
            state.um[in][nv]   -= du[nv];
          }
        }
      }

    } /* -- end loop on transverse directions -- */
  } /* -- end loop on dimensions -- */

/* ----------------------------------------------------
     2. Corner coupled states 
   ---------------------------------------------------- */

  /* --------------------------------------
      Correct + and - states in the 
      x1-direction by adding contributions 
      from the transverse directions
     -------------------------------------- */
  DIR = IDIR;

/* PS: Flux-ct scheme */
  SET_INDEXES (&indx, &state, grid);
  TRANSVERSE_LOOP(indx,in,i,j,k){  
    up = UPX[indx.t2][indx.t1];
    um = UMX[indx.t2][indx.t1];
    for (in = indx.beg - 1; in <= indx.end + 1; in++) {
      du = dU[*k][*j][*i];
      for (nv = NVAR; nv--; ){
        up[in][nv] += du[nv];
        um[in][nv] += du[nv];
      }
    }
  }

  /* --------------------------------------
      Correct + and - states in the 
      x2-direction by adding contributions 
      from the transverse directions
     -------------------------------------- */
  DIR = JDIR;

/* PS: Flux-ct scheme */
  SET_INDEXES (&indx, &state, grid);
  TRANSVERSE_LOOP(indx,in,i,j,k){  
    up = UPY[indx.t2][indx.t1];
    um = UMY[indx.t2][indx.t1];
    for (in = indx.beg - 1; in <= indx.end + 1; in++) {
      du = dU[*k][*j][*i];
      for (nv = NVAR; nv--; ){
        up[in][nv] += du[nv];
        um[in][nv] += du[nv];
      }
    }
  }

  #if DIMENSIONS == 3

  /* --------------------------------------
      Correct + and - states in the 
      x3-direction by adding contributions 
      from the transverse directions
     -------------------------------------- */

   DIR = KDIR;

/* PS: Flux-ct scheme */
   SET_INDEXES (&indx, &state, grid);
   TRANSVERSE_LOOP(indx,in,i,j,k){  
     up = UPZ[indx.t2][indx.t1];
     um = UMZ[indx.t2][indx.t1];
     for (in = indx.beg - 1; in <= indx.end + 1; in++) {
       du = dU[*k][*j][*i];
       for (nv = NVAR; nv--; ){
         up[in][nv] += du[nv];
         um[in][nv] += du[nv];
       }
     }
   }
  #endif

/* PS: Flux-ct scheme */
  #ifdef STAGGERED_MHD     /* ------------------------------
                                advance of staggered 
                                magnetic field of dt/2  
                              ------------------------------ */
   FCT_UPDATE (ISTEP, m_numGhost, temfx, temfy, temfz, &d, grid);

  #endif

/*   ----------------------------------------------------
                    Compute FINAL RHS
     ---------------------------------------------------- */

  ISTEP = 2;

  for (DIR = 0; DIR < DIMENSIONS; DIR++){

    nxf = grid[IDIR].np_int + (DIR == IDIR);
    nyf = grid[JDIR].np_int + (DIR == JDIR);
    nzf = grid[KDIR].np_int + (DIR == KDIR);

    nxb = grid[IDIR].lbeg - (DIR == IDIR);
    nyb = grid[JDIR].lbeg - (DIR == JDIR);
    nzb = grid[KDIR].lbeg - (DIR == KDIR);
    nxe = grid[IDIR].lend;
    nye = grid[JDIR].lend;
    nze = grid[KDIR].lend;

    fluxBox = UBox;
    fluxBox.surroundingNodes(DIR);
    FArrayBox& F = a_F[DIR];
    F.resize(fluxBox,NVAR);
    /*
    cout << F.loVect()[0] << " " << F.hiVect()[0] << " "
         << F.loVect()[1] << " " << F.hiVect()[1] << " "
         << F.loVect()[2] << " " << F.hiVect()[2] << endl;
    */
    if      (DIR == IDIR) {UP = UPX; UM = UMX;}
    else if (DIR == JDIR) {UP = UPY; UM = UMY;}
    else if (DIR == KDIR) {UP = UPZ; UM = UMZ;}

    SET_INDEXES (&indx, &state, grid);
    /*
    cout << DIR << " " << F.size() << endl;
    cout << nxb << " " << nyb << " " << nzb << endl;
    cout << "nxf = " << nxf << " " << "nyf = " << nyf << " " << "nzf = " << nzf << endl;
    cout << indx.beg << " " << indx.end << " " << indx.t1_beg << " " << indx.t1_end << " " << indx.t2_beg << " " << indx.t2_end << endl;
    */

    TRANSVERSE_LOOP(indx,in,i,j,k){  

      state.up  = UP[indx.t2][indx.t1]; state.uL = state.up;
      state.um  = UM[indx.t2][indx.t1]; state.uR = state.um + 1;

/* PS: Flux-ct scheme */
#ifdef STAGGERED_MHD
      for (in = indx.beg - 2; in <= indx.end + 1; in++){
	state.uL[in][B1] = state.uR[in][B1] = d.Vs[BXs+DIR][*k][*j][*i];
      }
#endif

      errm = CONTOPRIM (state.um, state.vm, indx.beg - 1, indx.end + 1, flagm);
      errp = CONTOPRIM (state.up, state.vp, indx.beg - 1, indx.end + 1, flagp);

    /* ----------------------------------------------
            cell-centered state for 
            source term computations 
       ---------------------------------------------- */

      if (DIR == IDIR){
        for (in = indx.beg-1; in <= indx.end+1; in++) { /* +1 or -1 needed for STAGGERED_MHD */
        for (nv = NVAR; nv--;   ) {
          dU[*k][*j][*i][nv] += UU[nv][*k][*j][*i];
          state.uh[in][nv] = dU[*k][*j][*i][nv];
        }}
      }else{
        for (in = indx.beg; in <= indx.end; in++) {
        for (nv = NVAR; nv--;   ) {
          state.uh[in][nv] = dU[*k][*j][*i][nv];
        }
        }
      }



      err  = CONTOPRIM (state.uh, state.vh, indx.beg, indx.end, state.flag);

      if (err || errm || errp){
	WARNING(print ("! Corner coupled states not physical: reverting to 1st order\n");
	      print ("! Level = %f\n",level);
	      )
	for (in = indx.beg - 1; in <= indx.end + 1; in++){
	  if (flagp[in] || flagm[in]){
	    for (nv = 0; nv < NVAR; nv++) {
	      state.v[in][nv] = d.Vc[nv][*k][*j][*i];
	    }
	    PRIMTOCON (state.v, state.u, in, in);            
	    for (nv = 0; nv < NVAR; nv++) {
	      state.vm[in][nv] = state.vp[in][nv] = state.vh[in][nv] = state.v[in][nv];
	      state.um[in][nv] = state.up[in][nv] = state.uh[in][nv] = state.u[in][nv];
	    }
	  }
	}
      }
      RIEMANN (&state,  indx.beg - 1, indx.end, Dts->cmax + DIR, grid);

#if INCLUDE_PARABOLIC_FLUX == YES
      PARABOLIC_FLUX (d.Vc, &state, indx.beg-1,indx.end,Dts, grid);
#endif

/* PS: Flux-ct scheme */
#ifdef STAGGERED_MHD
      EMF_PUT (&state, indx.beg - 1, indx.end, grid);
#endif

      /*PS: Gravity */
#if defined(GRAVITY) || defined(SINKPARTICLE)
      for (in = 0; in < indx.ntot; in++) {
	gf1d[in]=gf[DIR][*k][*j][*i];
      }
      GET_RHS (&state, indx.beg, indx.end, delta_t, gf1d, grid);
#else
      GET_RHS (&state, indx.beg, indx.end, delta_t, grid);
#endif

      SAVE_AMR_FLUXES (&state, indx.beg - 1, indx.end, grid);

      for (in = indx.beg; in <= indx.end; in++) {
	for (nv = 0; nv < NVAR; nv++) {
	  UU[nv][*k][*j][*i] += state.rhs[in][nv];
	}
#ifdef GRAVITY
       /*PS: compute sound speed */
#if EOS == IDEAL
       den = UU[DN][*k][*j][*i];
       m2 = UU[MX][*k][*j][*i]*UU[MX][*k][*j][*i]+UU[MY][*k][*j][*i]*UU[MY][*k][*j][*i]
          + UU[MZ][*k][*j][*i]*UU[MZ][*k][*j][*i];
#ifdef STAGGERED_MHD
       b2 = UU[BX][*k][*j][*i]*UU[BX][*k][*j][*i]+UU[BY][*k][*j][*i]*UU[BY][*k][*j][*i]
          + UU[BZ][*k][*j][*i]*UU[BZ][*k][*j][*i];
       pr = (gmm-1.0) * (UU[EN][*k][*j][*i] - 0.5*(m2/den + b2));
#else /* STAGGERED_MHD */
       pr = (gmm-1.0) * (UU[EN][*k][*j][*i] - 0.5*m2/den);
#endif /* STAGGERED_MHD */
       if (pr < 0.0) pr = SMALL_PR;
       cs = sqrt(gmm*pr/den);
#endif
#if EOS == ISOTHERMAL
       cs = C_ISO;
#endif
       /*PS: compute cmax by comparing with that from gravity acceleration. */
       vmax=0.0;
       for (vdir = 0; vdir < 3; vdir++) {
         v = abs(d.Vc[MX][*k][*j][*i])+cs;
         sx = abs(gf[vdir][*k][*j][*i])*grid[DIR].dx[IBEG];
	 v2 = v*v;
	 if (sx > SMALL_PR*v2) v = sx/(sqrt(v2+2.0*sx) - v);
         vmax=max(v,vmax);
	 }
       *(Dts->cmax + DIR) =max(2.0*vmax,*(Dts->cmax + DIR));
#endif /* GRAVITY */
      }

// Put fluxes in the FarrayBox a_F to be passed to Chombo
      if (DIR == IDIR){
	if(*k >= nzb && *k <= nze && *j >= nyb && *j <= nye){
	  for (in = indx.beg-1; in <= indx.end; in++) {
	    for (nv = 0; nv < NVAR; nv++) {
	      indf = nv*nzf*nyf*nxf + (*k - nzb)*nyf*nxf 
		   + (*j - nyb)*nxf 
	           + (*i - nxb);
	      F.dataPtr(0)[indf] = state.flux[in][nv];
	    }
	  }
	}
      }
      if (DIR == JDIR){
	if(*k >= nzb && *k <= nze && *i >= nxb && *i <= nxe){
	  for (in = indx.beg-1; in <= indx.end; in++) {
	    for (nv = 0; nv < NVAR; nv++) {
	      indf = nv*nzf*nyf*nxf + (*k - nzb)*nyf*nxf 
		   + (*j - nyb)*nxf 
	           + (*i - nxb);
	      F.dataPtr(0)[indf] = state.flux[in][nv];
	    }
	  }
	}
      }
      if (DIR == KDIR){
	if(*j >= nyb && *j <= nye && *i >= nxb && *i <= nxe){
	  for (in = indx.beg-1; in <= indx.end; in++) {
	    for (nv = 0; nv < NVAR; nv++) {
	      indf = nv*nzf*nyf*nxf + (*k - nzb)*nyf*nxf 
		   + (*j - nyb)*nxf 
	           + (*i - nxb);
	      F.dataPtr(0)[indf] = state.flux[in][nv];
	    }
	  }
	}
      }
    }
  }


#if BARO_COOL == YES
    DIR = IDIR;
    SET_INDEXES(&indx, &state, grid);
    NZ_PT = &kk; NY_PT = &jj;
    for (kk=KBEG; kk <= KEND; kk++){
      for (jj=JBEG; jj<=JEND; jj++){
	for (ii=IBEG; ii<=IEND; ii++){
	  for(nv=NVAR;nv--;){
	    state.u[ii][nv] = UU[nv][kk][jj][ii];
	    state.rhs[ii][nv] = 0.;
	    }
	  }
	  GET_BAROTROPIC_COOL (&state, IBEG, IEND, IBEG, jj, kk, delta_t, grid);
	  for (ii=IBEG; ii<=IEND; ii++){
	      UU[EN][kk][jj][ii] = state.rhs[ii][EN];
	  }
	}
     }
#endif

  #if CT_EMF_AVERAGE != ARITHMETIC

  DIR = IDIR;
  SET_INDEXES (&indx, &state, grid);
  for (kk = KBEG - KOFFSET; kk <= KEND + KOFFSET; kk++){ NZ_PT = &kk;
  for (jj = JBEG - JOFFSET; jj <= JEND + JOFFSET; jj++){ NY_PT = &jj;
    CONTOPRIM (dU[kk][jj], state.v, IBEG - 1, IEND + 1, state.flag);
    for (ii = IBEG - 1; ii <= IEND + 1; ii++){
    for (nv = 0; nv < NVAR; nv++) {
      d.Vc[nv][kk][jj][ii] = state.v[ii][nv];
    }}  
  }}

  #endif

/* PS: Flux-ct scheme */
#ifdef STAGGERED_MHD
  FCT_UPDATE (ISTEP, m_numGhost, temfx, temfy, temfz, &d, grid);

  AVERAGE_MAGNETIC_FIELD (d.Vs, UU, grid);

/* ----------------------------------------------
           convert to primitive 
   ---------------------------------------------- */

  /*PS Update cell surface fields */
  for (kk = 0; kk < NZ_TOT; kk++){
    for (jj = 0; jj < NY_TOT; jj++){
      for (ii = 0; ii < NX_TOT; ii++){
	for (nv = 3; nv--;  ) {
	  BBs[nv][kk][jj][ii] = d.Vs[nv][kk][jj][ii];
	}
      }
    }
  }
#endif

  #ifdef PSI_GLM
   GLM_SOURCE (UU, grid);
  #endif

/* ----------------------------------------------
     Need to include Cooling ? 
     --> convert to primitive space
   ---------------------------------------------- */

  #if INCLUDE_COOLING != NO 
   DIR = IDIR;
   SET_INDEXES (&indx, &state, grid);
   NZ_PT = &kk; NY_PT = &jj;
   for (kk = KBEG; kk <= KEND; kk++){ 
   for (jj = JBEG; jj <= JEND; jj++){ 
     for (ii = IBEG; ii <= IEND; ii++){
     for (nv = NVAR; nv--;  ) {
       state.u[ii][nv] = UU[nv][kk][jj][ii];
     }}
     CONTOPRIM (state.u, state.v, IBEG, IEND, state.flag);
     for (ii = IBEG; ii <= IEND; ii++){
     for (nv = NVAR; nv--;  ) {
       d.Vc[nv][kk][jj][ii] = state.v[ii][nv];
     }} 
   }}

/*  ----  Optically thin Cooling sources  ----  */

   #if INCLUDE_COOLING != NO
    #if INCLUDE_COOLING == POWER_LAW  /* -- solve exactly -- */
     POWER_LAW_COOLING (d.Vc, delta_t, Dts, grid);
    #else
     COOLING_SOURCE (&d, delta_t, Dts, grid);
    #endif
   #endif

 //  SPLIT_SOURCE (&d, delta_t, Dts, grid);

   DIR = IDIR;
   SET_INDEXES (&indx, &state, grid);
   for (kk = KBEG; kk <= KEND; kk++){
   for (jj = JBEG; jj <= JEND; jj++){
     for (ii = IBEG; ii <= IEND; ii++){
     for (nv = NVAR; nv--;  ) {
       state.v[ii][nv] = d.Vc[nv][kk][jj][ii];
     }}
     PRIMTOCON (state.v, state.u, IBEG, IEND);

     for (ii = IBEG; ii <= IEND; ii++){
     for (nv = NVAR; nv--;  ) {
       UU[nv][kk][jj][ii] = state.u[ii][nv];
     }
     } 
   }}
  #endif

/* ---------------------------------------------------------------
    In cylindrical geom. we pass U*r back to Chombo rather
    than U.
   --------------------------------------------------------------- */

  #if GEOMETRY == CYLINDRICAL
 
  /* -- do we need boundary vals as well ? -- */

   nxb = IBEG - (DIMENSIONS >= 1); nxf = IEND + (DIMENSIONS >= 1);
   nyb = JBEG - (DIMENSIONS >= 2); nyf = JEND + (DIMENSIONS >= 2);
   nzb = KBEG - (DIMENSIONS >= 3); nzf = KEND + (DIMENSIONS >= 3);

   for (nv = NVAR; nv--;   ){
   for (kk = nzb; kk <= nzf; kk++){
   for (jj = nyb; jj <= nyf; jj++){
   for (ii = nxb; ii <= nxf; ii++){
     UU[nv][kk][jj][ii] *= grid[IDIR].x[ii];
   }}}}
  #endif

   /* PS: Flux-ct scheme */
#ifdef STAGGERED_MHD
/*PS: copy BBs back to temp */
   for (kk = 0; kk < NZ_TOT; kk++) {
     for (jj = 0; jj < NY_TOT; jj++) {
       for (ii = 0; ii < NX_TOT; ii++) {
         xtemp[kk][jj][ii+1] = BBs[BXs][kk][jj][ii];
         ytemp[kk][jj+1][ii] = BBs[BYs][kk][jj][ii];
         ztemp[kk+1][jj][ii] = BBs[BZs][kk][jj][ii];
       }
     }
   }
#endif /* STAGGERED_MHD */

/* -------------------------------------------------
               Free memory 
   ------------------------------------------------- */

  for (nv = 0; nv < NVAR; nv++) free_chmatrix3(UU[nv]);
/* PS: Flux-ct scheme */
#ifdef STAGGERED_MHD
  free_chmatrix3(xtemp);
  free_chmatrix3(ytemp);
  free_chmatrix3(ztemp);
  free_chmatrix3(temfx);
  free_chmatrix3(temfy);
  free_chmatrix3(temfz);
#endif
#if defined(GRAVITY) || defined(SINKPARTICLE)
  for (nv = 0; nv < DIMENSIONS; nv++) free_chmatrix3(Gtemp[nv]);
  free_array_4D(gf);
  free_array_1D(gf1d);
#endif
  #ifdef SKIP_SPLIT_CELLS
   free_chmatrix3(covercells);
   free_chmatrix (splitcells, KBEG, KEND, JBEG, JEND, IBEG, IEND);
  #endif
}

/* *********************************************************** */
void SAVE_AMR_FLUXES (const State_1D *state, int beg, int end, Grid *grid)
/* 
 *
 *  Rebuild fluxes in a way suitable for AMR operation
 *  by adding pressure and multiplying by area.
 *
 ************************************************************* */
{
  int  i, nv;
  double **f, *p, r, *A;

  f = state->flux;
  p = state->press;

  for (i = beg; i <= end; i++) {
    f[i][M1] += p[i];
    #ifdef PSI_GLM
     f[i][B1] = state->glm_flx_Bn[i]; /* ? resistive MHD ? */
    #endif
  }    
  #if GEOMETRY == CYLINDRICAL
   if (DIR == IDIR){
     A = grid[IDIR].xr;
     for (i = beg; i <= end; i++) {
     for (nv = 0; nv < NVAR; nv++) {
       f[i][nv] *= fabs(A[i]);
     }}
   }else{
     r = fabs(grid[IDIR].x[*NX_PT]);
     for (i = beg; i <= end; i++) {
     for (nv = 0; nv < NVAR; nv++) {
       f[i][nv] *= r;
     }}
   }
  #endif
}



/* ********************************************************* */
void SET_BOUNDARY (Data_Arr U,
// PS: Flux-ct scheme
#ifdef STAGGERED_MHD
                   Data_Arr Bs,
#endif
                   Data *d, Grid *grid)
/*
 *
 *
 * Set physical boundary conditions and convert
 * the conservative matrix U into primitive values
 * d->Vc.
 *
 * 
 *********************************************************** */
{
  int i, j, k, nv, dir;
  int nx, ny, nz;
  int ibeg, jbeg, kbeg;
  int iend, jend, kend;
  int lft_side[3], rgt_side[3];
  static real **u, **v;
  static char *flag;
  
  if (u == NULL){
    u = Array_2D(NMAX_POINT, NVAR, double);
    v = Array_2D(NMAX_POINT, NVAR, double);
    flag = Array_1D(NMAX_POINT, char);
  }

  nx = grid[IDIR].np_tot;
  ny = grid[JDIR].np_tot;
  nz = grid[KDIR].np_tot;

/* ------------------------------------------------------- 
     check whether the patch touches a physical boundary
   ------------------------------------------------------- */

  for (dir = 0; dir < DIMENSIONS; dir++){
    lft_side[dir] = (grid[dir - IDIR].lbound != 0);
    rgt_side[dir] = (grid[dir - IDIR].rbound != 0);
  }

/* ---------------------------------------------------
     Extract the portion of the domain where U 
     is defined (i.e. NOT in the physical boundary).
   --------------------------------------------------- */

  ibeg = 0; iend = nx - 1;
  jbeg = 0; jend = ny - 1;
  kbeg = 0; kend = nz - 1;

  #if GEOMETRY == CYLINDRICAL
   for (nv = NVAR; nv--;   ){
   for (k = kbeg; k <= kend; k++){
   for (j = jbeg; j <= jend; j++){
   for (i = ibeg; i <= iend; i++){
     U[nv][k][j][i] /= grid[IDIR].x[i];
   }}}}
  #endif

/* -- exclude a physical boundary -- */

  if (lft_side[IDIR]) ibeg = IBEG;
  if (lft_side[JDIR]) jbeg = JBEG;
  if (lft_side[KDIR]) kbeg = KBEG;

  if (rgt_side[IDIR]) iend = IEND;
  if (rgt_side[JDIR]) jend = JEND;
  if (rgt_side[KDIR]) kend = KEND;

/* -- convert from conservative to primitive -- */

  DIR = IDIR;
  NZ_PT = &k; NY_PT = &j;
  for (k = kbeg; k <= kend; k++){
  for (j = jbeg; j <= jend; j++){
    for (i = ibeg; i <= iend; i++){
    for (nv = NVAR; nv--;   ){
      u[i][nv] = U[nv][k][j][i];
    }}

    CONTOPRIM (u, v, ibeg, iend, flag);

    for (i = ibeg; i <= iend; i++){
      for (nv = NVAR; nv--;   ){
        d->Vc[nv][k][j][i] = v[i][nv];
      }

// PS: Flux-ct scheme
    }
  }}

// PS: Flux-ct scheme
#ifdef STAGGERED_MHD
  for (k = 0; k < nz; k++){
    for (j = 0; j < ny; j++){
      for (i = 0; i < nx; i++){
        for (nv = 3; nv--;   ){
          d->Vs[nv][k][j][i] = Bs[nv][k][j][i];
        }
      }
    }
  }
#endif

/* -- now set boundary on primitive variables -- */

  if (lft_side[IDIR] || rgt_side[IDIR] ||
      lft_side[JDIR] || rgt_side[JDIR] ||
      lft_side[KDIR] || rgt_side[KDIR]) {
    BOUNDARY (d, ALLDIR, grid);

/* -------------------------------------------------------
     Convert physical boundary values into conservative 
     variables.
   ------------------------------------------------------- */

  for (dir = 0; dir < DIMENSIONS; dir++){

  /* -- convert boundary values in the physical left boundary -- */

    ibeg = 0; iend = nx - 1;
    jbeg = 0; jend = ny - 1;
    kbeg = 0; kend = nz - 1;

    if (lft_side[dir] && dir == IDIR) iend = IBEG - 1;
    if (lft_side[dir] && dir == JDIR) jend = JBEG - 1;
    if (lft_side[dir] && dir == KDIR) kend = KBEG - 1;

    for (k = kbeg; k <= kend; k++){
    for (j = jbeg; j <= jend; j++){
      for (i = ibeg; i <= iend; i++){
      for (nv = NVAR; nv--;   ){
        v[i][nv] = d->Vc[nv][k][j][i];
      }}
      PRIMTOCON (v, u, ibeg, iend);
      for (i = ibeg; i <= iend; i++){
      for (nv = NVAR; nv--;   ){
        U[nv][k][j][i] = u[i][nv];
      }
// PS: Flux-ct scheme: this may not be needed. Check later.
      }
    }}

  /* -- convert boundary values in the physical right boundary -- */

    ibeg = 0; iend = nx - 1;
    jbeg = 0; jend = ny - 1;
    kbeg = 0; kend = nz - 1;

    if (rgt_side[dir] && dir == IDIR) ibeg = IEND + 1;
    if (rgt_side[dir] && dir == JDIR) jbeg = JEND + 1;
    if (rgt_side[dir] && dir == KDIR) kbeg = KEND + 1;

    for (k = kbeg; k <= kend; k++){
    for (j = jbeg; j <= jend; j++){
      for (i = ibeg; i <= iend; i++){
      for (nv = NVAR; nv--;   ){
        v[i][nv] = d->Vc[nv][k][j][i];
      }}
      PRIMTOCON (v, u, ibeg, iend);
      for (i = ibeg; i <= iend; i++){
      for (nv = NVAR; nv--;   ){
        U[nv][k][j][i] = u[i][nv];
      }
// PS: Flux-ct scheme
      }
    }}

  }
  }
}

/* PS: Flux-ct scheme */
#if CTU_MHD_SOURCE == YES
/* ********************************************************* */
void ADD_CTU_MHD_SOURCE (real **v, real **up, real **um,
                         real *dtdV, int beg, int end, Grid *grid)
/*
 *
 * PURPOSE
 *
 *   Add source terms to conservative left and 
 *   right states obtained from the primitive form 
 *   of the equations. The source terms are:
 *
 *
 *     m  += dt/2 *  B  * dbx/dx
 *     Bt += dt/2 * vt  * dbx/dx   (t = transverse component)
 *     E  += dt/2 * v*B * dbx/dx
 *
 *   These terms are NOT accounted for when the primitive 
 *   form of the equations is used (see 
 *   Gardiner & Stone JCP (2005), Crockett et al. 
 *   JCP(2005)). This is true for both the Charactheristic 
 *   Tracing AND the primitive Hancock scheme when the 
 *   constrained transport is used, since the resulting 
 *   system is 7x7. To better understand this, you can
 *   consider the stationary solution rho = p = 1, v = 0
 *   and Bx = x, By = -y. If these terms were not included
 *   the code would generate spurious velocities.
 *
 *********************************************************** */
{
  int    i;
  real   scrh, *dx, *A;
  static real *db;

  if (db == NULL) db = Array_1D(NMAX_POINT, double);

/* ----------------------------------------
              comput db/dx
   ---------------------------------------- */

  #if GEOMETRY == CARTESIAN
   for (i = beg; i <= end; i++){
     db[i] = dtdV[i]*(up[i][B1] - um[i][B1]); 
   }
  #elif GEOMETRY == CYLINDRICAL
   if (DIR == IDIR){
     A = grid[IDIR].A;
     for (i = beg; i <= end; i++){
       db[i] = dtdV[i]*(up[i][B1]*A[i] - um[i][B1]*A[i - 1]);
     }
   }else{
     for (i = beg; i <= end; i++){
       db[i] = dtdV[i]*(up[i][B1] - um[i][B1]); 
     }
   }
  #else
   print1 (" ! CTU-MHD does not work in this geometry\n");
   QUIT_ORION(1);
  #endif

/* --------------------------------------------
         Add source terms
   -------------------------------------------- */

  for (i = beg; i <= end; i++){
    
    EXPAND( up[i][MX] += v[i][BX]*db[i];
            um[i][MX] += v[i][BX]*db[i];   ,
            up[i][MY] += v[i][BY]*db[i];
            um[i][MY] += v[i][BY]*db[i];   ,
            up[i][MZ] += v[i][BZ]*db[i];
            um[i][MZ] += v[i][BZ]*db[i]; ) 

    EXPAND(                                ,
            up[i][B2] += v[i][V2]*db[i]; 
            um[i][B2] += v[i][V2]*db[i];   ,
            up[i][B3] += v[i][V3]*db[i]; 
            um[i][B3] += v[i][V3]*db[i];)

    #if EOS != ISOTHERMAL
     scrh = EXPAND(   v[i][VX]*v[i][BX]  , 
                    + v[i][VY]*v[i][BY]  , 
                    + v[i][VZ]*v[i][BZ]);
     up[i][EN] += scrh*db[i];
     um[i][EN] += scrh*db[i];
    #endif

  }
}
#endif



#if BARO_COOL == YES
/* WJG,Drummond,SRO */
// ATL: This routine alters the EOS used at mass densities. Several user-defined parameters are necessary to use.
// barochoice (=1 or 2) Alters EOS to that used in (1) Hansen et al 2012 or (2) the Machida ea 2007 power-law EOS 
// cs0 = sound speed at T0 (likely ~18800 cm/s)
// T0 = isothermal temp (likely 10)
// rhobaro = critical mass density for EOS. Must be defined for either but only used with barochoice=1 (Hansen et al used 2e-13)
#define MIN(a,b)(((a)<(b))?(a):(b))
#define MAX(a,b)(((a)>(b))?(a):(b))
void GET_BAROTROPIC_COOL(const State_1D *state, int beg, int end, int ibeg, int jbeg, int kbeg, real dt, Grid *grid){
    real x1,x2,x3,rho,Tzero,rhocut,ciso;
    real pbaro,term,f,baro_gmm,Eth;
    int i,choice;

    x1 = DOM_XBEG[IDIR] + grid[IDIR].x[ibeg];
    x2 = DOM_XBEG[JDIR] + grid[JDIR].x[jbeg];
    x3 = DOM_XBEG[KDIR] + grid[KDIR].x[kbeg];

    for(i=beg;i<=end;i++){
      #if EOS != ISOTHERMAL
        rho = state->u[i][DN];
        rhocut = aux[rhobaro];
        Tzero = aux[T0];
        ciso = aux[cs0];
	choice = int(aux[barochoice]);
        f = 1.0;
        baro_gmm = 5./3.;
        #if TIME_STEPPING == HANCOCK && PRIMITIVE_HANCOCK == NO
          printf("not implemented for TIME_STEPPING == HANCOCK && PRIMITIVE_HANCOCK == NO");
        #else
      
        if(DIR==IDIR) x1 = DOM_XBEG[IDIR] + grid[IDIR].x[i];
        else if(DIR==JDIR) x2 = DOM_XBEG[JDIR] + grid[JDIR].x[i];
        else if(DIR==KDIR) x3 = DOM_XBEG[KDIR] + grid[KDIR].x[i];

        Eth = 0.0e0;
        Eth = state->u[i][PR] - 0.5*(state->u[i][VX]*state->u[i][VX] +
				     state->u[i][VY]*state->u[i][VY] +
				     state->u[i][VZ]*state->u[i][VZ] )/state->u[i][DN];
        #if PHYSICS !=HD
          Eth -= 0.5*(state->u[i][BX]*state->u[i][BX] +
		      state->u[i][BY]*state->u[i][BY] +
		      state->u[i][BZ]*state->u[i][BZ]);
        #endif
      
	  if(choice == 2){
	    if(rho < 3.84e-13){
	      Eth = rho * ciso * ciso/(gmm-1.0);
	    } else if( rho > 3.84e-13 && rho < 3.84e-8 ){
	      Eth = 3.84e-13 * ciso * ciso * pow( (rho/3.84e-13),7./5.)/(gmm-1.0);
	    } else if( rho > 3.84e-8 && rho < 3.84e-3 ) {
	      Eth = 3.84e-13 * ciso * ciso * pow( (3.84e-8/3.84e-13),7./5.) * pow( (rho/3.84e-8),1.1) /(gmm-1.0);
	    } else if( rho > 3.84e-3 ){
	      Eth = 3.84e-13 * ciso * ciso * pow( (3.84e-8/3.84e-13),7./5.) * pow( (3.84e-3/3.84e-8),1.1) * pow( (rho/3.84e-3),5./3.) /(gmm-1.0);
	    }
	  }else{
	    pbaro = rho * ciso * ciso * (1.0 + pow( (rho/rhocut), (baro_gmm-1.0) ) );
	    Eth = pbaro/(gmm-1.0);
	  }

        term = Eth + 0.5*(state->u[i][VX]*state->u[i][VX] +
			  state->u[i][VY]*state->u[i][VY] +
			  state->u[i][VZ]*state->u[i][VZ] ) / state->u[i][DN];

        #if PHYSICS !=HD
          term += 0.5*(state->u[i][BX]*state->u[i][BX] +
                       state->u[i][BY]*state->u[i][BY] +
                       state->u[i][BZ]*state->u[i][BZ]);
        #endif

	  //printf("rho: %6.4e ciso: %6.4e Eth: %6.4e pbaro: %6.4e iso: %6.4e \n",rho,ciso,Eth,pbaro,rho*ciso*ciso);

        state->rhs[i][EN] = term;
        #endif // ENDS TIME_STEP IF
      #endif // END ISOTHERMAL IF
    }
}
#undef MIN
#undef MAX
#endif //ENDS BARO_COOL
