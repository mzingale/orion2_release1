#ifdef CH_LANG_CC
/*
*      ____                         __ __
*     / __ \ ____ __ ___   __  __  / // /
*    / /_/ // __// // _ \ /  \/ / / // /
*    \____//_/  /_/ \___//_/\__/ /_//_/
*    
*    Based on Pluto 3 and Chombo.
*
*    Please refer to COPYING in Pluto's root directory and 
*    Copyright.txt, in Chombo's root directory.
*
*    Modification: Orional code based on PLUTO3.0b2
*    1) PS(08/06/10): Rename Pluto to Orion.
*/
#endif

#include "AMRLevelOrionFactory.H"

// static member initialization
#ifdef SINKPARTICLE
#if defined(STARPARTICLE) 
StarParticleList<StarParticleData> *(AMRLevelOrionFactory::m_sinks) = NULL;
#else
SinkParticleList<SinkParticleData> *(AMRLevelOrionFactory::m_sinks) = NULL;
#endif //STARPARTICLE 
#endif //SINKPARTICLE

AMRLevelOrionFactory::AMRLevelOrionFactory()
{
  setDefaultValues();
}

// Virtual constructor
AMRLevel* AMRLevelOrionFactory::new_amrlevel() const
{
  // Make sure everything is defined
  CH_assert(isDefined());

  // Create a new AMRLevelOrion
  AMRLevelOrion* amrGodPtr = new AMRLevelOrion();

  // Set up new object
  amrGodPtr->CFL(m_cfl);
  amrGodPtr->domainLength(m_domainLength);
  amrGodPtr->densityGradThreshold(m_densityGradThreshold);
  amrGodPtr->velocityGradThreshold(m_velocityGradThreshold);
  amrGodPtr->diskTagThreshold(m_diskTagThreshold);
  amrGodPtr->BGradThreshold(m_BGradThreshold);
  amrGodPtr->radiationGradThreshold(m_radiationGradThreshold);
  amrGodPtr->shockDetectionThreshold(m_shockDetectionThreshold);
  amrGodPtr->JeansNoThreshold(m_JeansNoThreshold);
  amrGodPtr->Zoom(m_Zoom);
  amrGodPtr->Nested(m_Nested);
  amrGodPtr->NestedSphere(m_NestedSphere);
  amrGodPtr->DensityTagThreshold(m_DensityTagThreshold);
  amrGodPtr->tagBufferSize(m_tagBufferSize);
  amrGodPtr->initialDtMultiplier(m_initialDtMultiplier);
  amrGodPtr->patchOrion(m_patchOrion);
  amrGodPtr->verbosity(m_verbosity);
#ifdef SINKPARTICLE
  amrGodPtr->setSinkList(m_sinks);
#endif //SINKPARTICLE

  // Return it
  return amrGodPtr;
}

AMRLevelOrionFactory::~AMRLevelOrionFactory()
{
}

// CFL number
void AMRLevelOrionFactory::CFL(Real a_cfl)
{
  m_cfl = a_cfl;
  m_cflSet = true;
}

void AMRLevelOrionFactory::verbosity(const int& a_verbosity)
{
  m_verbosity = a_verbosity;
}

// Physical dimension of the longest side of the domain
void AMRLevelOrionFactory::domainLength(Real a_domainLength)
{
  m_domainLength = a_domainLength;
  m_domainLengthSet = true;
}

// Refinement threshold
void AMRLevelOrionFactory::densityGradThreshold(vector<Real> *a_thresh)
{
  m_densityGradThreshold = a_thresh;
  m_densityGradThresholdSet = true;
}
void AMRLevelOrionFactory::velocityGradThreshold(vector<Real> *a_thresh)
{
  m_velocityGradThreshold = a_thresh;
  m_velocityGradThresholdSet = true;
}
void AMRLevelOrionFactory::diskTagThreshold(vector<Real> *a_thresh)
{
  m_diskTagThreshold = a_thresh;
  m_diskTagThresholdSet = true;
}
void AMRLevelOrionFactory::BGradThreshold(vector<Real> *a_thresh)
{
  m_BGradThreshold = a_thresh;
  m_BGradThresholdSet = true;
}
void AMRLevelOrionFactory::radiationGradThreshold(vector<Real> *a_thresh)
{
  m_radiationGradThreshold = a_thresh;
  m_radiationGradThresholdSet = true;
}
void AMRLevelOrionFactory::shockDetectionThreshold(vector<Real> *a_thresh)
{
  m_shockDetectionThreshold = a_thresh;
  m_shockDetectionThresholdSet = true;
}
void AMRLevelOrionFactory::JeansNoThreshold(vector<Real> *a_thresh)
{
  m_JeansNoThreshold = a_thresh;
  m_JeansNoThresholdSet = true;
}
void AMRLevelOrionFactory::Zoom(vector<Real> *a_thresh)
{
  m_Zoom = a_thresh;
  m_ZoomSet = true;
}
void AMRLevelOrionFactory::Nested(vector<Real> *a_thresh)
{
  m_Nested = a_thresh;
  m_NestedSet = true;
}

void AMRLevelOrionFactory::NestedSphere(vector<Real> *a_thresh)
{
  m_NestedSphere = a_thresh;
  m_NestedSphereSet = true;
}

void AMRLevelOrionFactory::DensityTagThreshold(vector<Real> *a_thresh)
{
  m_DensityTagThreshold = a_thresh;
  m_DensityTagThresholdSet = true;
}

// Tag buffer size
void AMRLevelOrionFactory::tagBufferSize(int a_tagBufferSize)
{
  m_tagBufferSize = a_tagBufferSize;
  m_tagBufferSizeSet = true;
}

// Initial dt multiplier
void AMRLevelOrionFactory::initialDtMultiplier(Real a_initialDtMultiplier)
{
  m_initialDtMultiplier = a_initialDtMultiplier;
  m_initialDtMultiplierSet = true;
}

// PatchOrion object (used as a factory)
void AMRLevelOrionFactory::patchOrion(PatchOrion* a_patchOrion)
{
  m_patchOrion = a_patchOrion;
  m_patchOrionSet = true;
}

// Check that everything is defined
bool AMRLevelOrionFactory::isDefined() const
{
  return (m_cflSet &&
          m_domainLengthSet &&
	  m_densityGradThresholdSet &&
	  m_velocityGradThresholdSet &&
	  m_diskTagThresholdSet &&
	  m_BGradThresholdSet &&
	  m_radiationGradThresholdSet &&
	  m_densityGradThresholdSet && 
	  m_velocityGradThresholdSet && 
	  m_diskTagThresholdSet &&
	  m_BGradThresholdSet && 
	  m_shockDetectionThresholdSet &&
	  m_JeansNoThresholdSet &&
	  m_ZoomSet &&
	  m_NestedSet &&
	  m_NestedSphereSet &&
	  m_DensityTagThresholdSet &&
          m_tagBufferSizeSet &&
          m_initialDtMultiplierSet &&
          m_patchOrionSet);
}

// Some default values
void AMRLevelOrionFactory::setDefaultValues()
{
  CFL(0.8);
  domainLength(1.0);
  tagBufferSize(2);
  initialDtMultiplier(0.1);
  m_patchOrionSet = false;
  m_verbosity = 0;
#ifdef SINKPARTICLE
  m_sinks = NULL;
#endif //SINKPARTICLE
}

#ifdef SINKPARTICLE
#if defined(STARPARTICLE) 
void AMRLevelOrionFactory::setSinkList(StarParticleList<StarParticleData> *a_sinks) {
  m_sinks = a_sinks;
}
#else
void AMRLevelOrionFactory::setSinkList(SinkParticleList<SinkParticleData> *a_sinks) {
  m_sinks = a_sinks;
}
#endif //STARPARTICLE 
#endif //SINKPARTICLE
