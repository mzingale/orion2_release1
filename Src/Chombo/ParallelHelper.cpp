#include "ParallelHelper.H"
#include "SPMD.H"
#include <ctime>
Real reduceRealMax(Real a_value) {
  Real val = a_value;
#ifdef CH_MPI
  Real sendBuf = a_value;
  int result = MPI_Allreduce(&sendBuf, &val, 1, MPI_CH_REAL,MPI_MAX,
                             Chombo_MPI::comm);

  if (result != MPI_SUCCESS)
    {
      MayDay::Error("Sorry, but I had a communication error in reduceRealMax");
    }
#endif
  return val;
}

int reduceIntMax(int a_value) {
  int val = a_value;
#ifdef CH_MPI
  int sendBuf = a_value;
  int result = MPI_Allreduce(&sendBuf, &val, 1, MPI_INT,MPI_MAX,
                             Chombo_MPI::comm);

  if (result != MPI_SUCCESS)
    {
      MayDay::Error("Sorry, but I had a communication error in reduceRealMax");
    }
#endif
  return val;
}

Real reduceRealMin(Real a_value) {
  Real val = a_value;
#ifdef CH_MPI
  Real sendBuf = a_value;
  int result = MPI_Allreduce(&sendBuf, &val, 1, MPI_CH_REAL,MPI_MIN,
                             Chombo_MPI::comm);

  if (result != MPI_SUCCESS)
    {
      MayDay::Error("Sorry, but I had a communication error in reduceRealMin");
    }
#endif
  return val;
}

Real reduceRealSum(Real a_value) {
  Real val = a_value;
#ifdef CH_MPI
  Real sendBuf = a_value;
  int result = MPI_Allreduce(&sendBuf, &val, 1, MPI_CH_REAL,MPI_SUM,
                             Chombo_MPI::comm);

  if (result != MPI_SUCCESS)
    {
      MayDay::Error("Sorry, but I had a communication error in reduceRealSum");
    }
#endif
  return val;
}


//From ParallelDescriptor in ORION1 - 
//needed for RayTrace routines (RayParticleList.cpp) 
//Added for ray trace ALR 10/24/13
bool
ReduceBoolOr (bool a_value)
{
  int val = int(a_value);
#ifdef CH_MPI
  int sendBuf = int(a_value);
  int result = MPI_Allreduce(&sendBuf, &val, 1, MPI_INT, 
			     MPI_SUM, Chombo_MPI::comm);

if (result != MPI_SUCCESS)
  {
    MayDay::Error("Sorry, but I had a communication error in reduceRealOr");
  }
#endif
bool val_bool = (val == 0) ? false : true;
return val_bool;
}
