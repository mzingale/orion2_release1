#include "orion2.h"
#define NONZERO_INITIALIZE YES

/* ****************************************************************** */
void free_array_1D (real *v)
/* 
 *
 *
 ******************************************************************** */
{
  free ((char *)v);
}
/* ***************************************************************** */
void free_array_2D (real **m)
/* 
 *
 * 
 ******************************************************************* */
{
  free ((char *) m[0]);
  free ((char *) m);
}
/* ***************************************************************** */
void free_array_3D (real ***m)
/* 
 *
 *
 ******************************************************************* */
{
  free ((char *) m[0][0]);
  free ((char *) m[0]);
  free ((char *) m);
}
/* ***************************************************************** */
void free_array_4D (real ****m)
/* 
 *
 *
 ******************************************************************* */
{
  free ((char *) m[0][0][0]);
  free ((char *) m[0][0]);
  free ((char *) m[0]);
  free ((char *) m);
}

/* *************************************************************** */
char *ARRAY_1D (int nx, size_t dsize)
/* 
 *
 *   Allocate memory for 1-D real vector
 *
 ***************************************************************** */
{
  char *v;
  v = (char *) malloc (nx*dsize);
  ERROR (!v, "Allocation failure in ARRAY_1D ");
  USED_MEMORY += nx*dsize;

  #if NONZERO_INITIALIZE == YES
   if (dsize==sizeof(double)){
     int i;
     double *q;
     q = (double *) v;
     for (i = nx; i--; ) q[i] = i*1.e18 - 1.e16;
     /*PS: if it doesn't work, use the following
       for (i = nx; i--; ) q[i] = 0.0;
     */
   }
  #endif
  return v;

}

/* ************************************************************** */
char **ARRAY_2D (int nx, int ny, size_t dsize)
/*
 *
 *************************************************************** */
{
  int i;
  char **m;
 
  m    = (char **)malloc ((size_t) nx*sizeof(char *));
  m[0] = (char *) malloc ((size_t) nx*ny*dsize);
 
  for (i = 1; i < nx; i++) m[i] = m[(i - 1)] + ny*dsize;
 
  USED_MEMORY += nx*ny*dsize;

  #if NONZERO_INITIALIZE == YES
   if (dsize==sizeof(double)){
     int j;
     double **q;
     q = (double **)m;
     for (i = nx; i--; ){
     for (j = ny; j--; ){
       q[i][j] = i*j*1.e18 + 1.e16*j;
     /*PS: if it doesn't work, use the following
	q[i][j] = 0.0;
     */
     }} 
   }
  #endif

  return m;
}

/* ************************************************************ */
char ***ARRAY_3D (int nx, int ny, int nz, size_t dsize)
/* 
 *
 *  Allocate memory for a 3-D real vector
 *
 ************************************************************** */
{
  int i, j;
  char ***m;

  m = (char ***) malloc ((size_t) nx*sizeof (char **));

  ERROR (!m, "Allocation failure in ARRAY_3D (1)");

  m[0] = (char **) malloc ((size_t) nx*ny*sizeof(char *));

  ERROR (!m[0],"Allocation failure in ARRAY_3D (2)");

  m[0][0] = (char *) malloc ((size_t) nx*ny*nz*dsize);

  ERROR (!m[0][0],"Allocation failure in ARRAY_3D (3)");

/* ---------------------------
       single subscript: i
   --------------------------- */

  for (i = 1; i < nx; i++) m[i] = m[i - 1] + ny;
  
/* ---------------------------
     real subscript:
     
      (i,0)  (0,j) 
      (i,j)  
   --------------------------- */
  
  for (j = 1; j < ny; j++) m[0][j] = m[0][j - 1] + nz*dsize;
  for (i = 1; i < nx; i++) m[i][0] = m[i - 1][0] + ny*nz*dsize;

  for (i = 1; i < nx; i++) {
  for (j = 1; j < ny; j++) {
    m[i][j] = m[i][j - 1] + nz*dsize;
  }}

  for (j = 0; j < ny; j++){
  for (i = 0; i < nx; i++){
    if (m[i][j] == NULL){
      print ("! Allocation failure in ARRAY_3D\n");
      QUIT_ORION2(1);
    }
  }}
  
  USED_MEMORY += nx*ny*nz*dsize;

  #if NONZERO_INITIALIZE == YES
   if (dsize==sizeof(double)){
     double ***q;
     int k;
     q = (double ***)m;
     for (i = nx; i--; ){
     for (j = ny; j--; ){
     for (k = nz; k--; ){
       q[i][j][k] = 1.e18*i + 1.e17*j + 1.e16*k + 1.e15;
     /*PS: if it doesn't work, use the following
       q[i][j][k] = 0.0;
     */
     }}}
   }
  #endif

  return m;
}
/* ************************************************************* */
char ****ARRAY_4D (int nx, int ny, int nz, int nv, size_t dsize)
/* 
 *
 *  Allocate memory for a 4-D real vector
 *
 ************************************************************** */
{
  int i, j, k;
  char ****m;

  m = (char ****) malloc ((size_t) nx*sizeof (char ***));

  m[0] = (char ***) malloc ((size_t) nx*ny*sizeof (char **));

  m[0][0] = (char **) malloc ((size_t) nx*ny*nz*sizeof (char *));

  m[0][0][0] = (char *) malloc ((size_t) nx*ny*nz*nv*dsize);

/* ---------------------------
       single subscript: i
   --------------------------- */
   
  for (i = 1; i < nx; i++) m[i] = m[i - 1] + ny;

/* ---------------------------
     real subscript:
     
      (i,0)  (0,j) 
      (i,j)  
   --------------------------- */

  for (i = 1; i < nx; i++) {
    m[i][0] = m[i - 1][0] + ny*nz;
  }
  for (j = 1; j < ny; j++) {
    m[0][j] = m[0][j - 1] + nz;
  }

  for (i = 1; i < nx; i++) {
  for (j = 1; j < ny; j++) {
    m[i][j] = m[i][j - 1] + nz;
  }}

/* ---------------------------
     triple subscript:
     
     (i,0,0) (0,j,0) (0,0,k)
     (i,j,0) (i,0,k) (0,j,k)
     (i,j,k)
   --------------------------- */

  for (i = 1; i < nx; i++) {
    m[i][0][0] = m[i - 1][0][0] + ny*nz*nv*dsize;
  }
  for (j = 1; j < ny; j++) {
    m[0][j][0] = m[0][j - 1][0] + nz*nv*dsize;
  }
  
  for (k = 1; k < nz; k++) {
    m[0][0][k] = m[0][0][k - 1] + nv*dsize;
  }
  
  
  for (i = 1; i < nx; i++) {
  for (j = 1; j < ny; j++) {
    m[i][j][0] = m[i][j - 1][0] + nz*nv*dsize;
  }}
   
  for (i = 1; i < nx; i++) {
  for (k = 1; k < nz; k++) {
    m[i][0][k] = m[i][0][k - 1] + nv*dsize;
  }}
  
  for (j = 1; j < ny; j++) {
  for (k = 1; k < nz; k++) {
    m[0][j][k] = m[0][j][k - 1] + nv*dsize;
  }}

  for (i = 1; i < nx; i++) {
    for (j = 1; j < ny; j++) {
      for (k = 1; k < nz; k++) {
        m[i][j][k] = m[i][j][k - 1] + nv*dsize;
      }
    }
  }
      
  USED_MEMORY += nx*ny*nz*nv*dsize;

  #if NONZERO_INITIALIZE == YES
   if (dsize==sizeof(double)){
     int l;
     double ****q;
     q = (double ****)m;
     for (i = nx; i--; ){
     for (j = ny; j--; ){
     for (k = nz; k--; ){
     for (l = nv; l--; ){
       q[i][j][k][l] = l*1.e18*i + 1.e17*i*j - 1.e16*k*j + 1.e17;
     /*PS: if it doesn't work, use the following
       q[i][j][k][l] = 0.0;
     */
     }}}}
   }
  #endif

  return m;
}
#undef NONZERO_INITIALIZE

