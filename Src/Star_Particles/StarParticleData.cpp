#include "StarParticleData.H"
#include <cmath>
#include <CH_assert.H>


// Define some unit conversions used in this routine
#define MSUN     1.989e33
#define YRSEC    3.15576e7  /* Seconds in a year */
#define LSUN     3.84e33
#define RSUN     6.96e10

/* Physical constants */
#define G      6.67e-8
#define A      7.56e-15
#define KB     1.38e-16
#define MH     1.67e-24
#define SIGMA  5.67e-5
#define NAVOG  6.022e23
#define MU     0.613  /* Mean molecular weight of a fully ionized gas of
			 solar composition */

/***********************/
/* Friend IO operators */
/***********************/

#define IOPREC 20

ostream &operator<<(ostream &os, const StarParticleData& partdata) {
  // Set precision to IOPREC decimal places
  int oldprec = os.precision(IOPREC);
  // Output order is mass, position, momentum
  os << partdata.m << " ";
  for (int i=0; i<CH_SPACEDIM; i++)
    os << partdata.pos[i] << " ";
  for (int i=0; i<CH_SPACEDIM; i++)
    os << partdata.mom[i] << " ";
  for (int i=0; i<CH_SPACEDIM; i++)
    os << partdata.angmom[i] << " ";
  int burnVal = partdata.burnState;
  os << partdata.mlast << " " << partdata.r << " " << partdata.mdeut
     << " " << partdata.n << " " << partdata.mdot << " "
    /* PS: Add netlum */
     << partdata.netlum << " "
     << burnVal << " " << partdata.l_hist;

  // Restore old precision
  os.precision(oldprec);

  return(os);
}

istream &operator>>(istream &is, StarParticleData& partdata) {
  // Set precision to IOPREC decimal places
  int oldprec = is.precision(IOPREC);

  // Input order is mass, position, momentum
  is >> partdata.m;
  for (int i=0; i<CH_SPACEDIM; i++) is >> partdata.pos[i];
  for (int i=0; i<CH_SPACEDIM; i++) is >> partdata.mom[i];
  for (int i=0; i<CH_SPACEDIM; i++) is >> partdata.angmom[i];
  int burnVal = partdata.burnState;
  /* PS: Add netlum */
  is >> partdata.mlast >> partdata.r >> partdata.mdeut >> partdata.n
     >> partdata.mdot 
     >> partdata.netlum
     >> burnVal;
  partdata.burnState = (StarParticleData::burningState) burnVal;

#ifdef NHIST
  // Kludge to allow setting of mdot history on restarts
  partdata.mdot_hist[0] = partdata.mdot;
  for (int i=1; i<NHIST; i++) partdata.mdot_hist[i] = partdata.mdot;
  // MG star particles need to know mass history to ramp up lum slowly
    is >> partdata.l_hist;
#endif
    
  // Restore old precision
  is.precision(oldprec);

  return(is);
}

/*************************/
/* Utility class methods */
/*************************/

StarParticleData::StarParticleData(Real *newpos, Real *newmom,
				   Real *newangmom, Real newmass) :
  SinkParticleData(newpos, newmom, newangmom, newmass)
{
  // get the curent angular momentum treatment option
  mlast = newmass; // Set this for safety, so mdot = 0 initially
  mdeut = newmass;
  mdot = 0.0;
  burnState = Uninitialized;
  r = radInit(1.0e-4*MSUN/YRSEC);
  n = nInit(1.0e-4*MSUN/YRSEC);
#ifdef NHIST
  for (int i=0; i<NHIST; i++) mdot_hist[i]=-1.0;
  l_hist=1.0e-2*LSUN;
#endif

    
 
}

StarParticleData::~StarParticleData() {}

StarParticleData&
StarParticleData::operator=(const StarParticleData& rhs) {
  for (int i=0; i<CH_SPACEDIM; i++) {
    pos[i]=rhs.pos[i];
    mom[i]=rhs.mom[i];
    angmom[i]=rhs.angmom[i];
  }
  m = rhs.m;
  mlast = rhs.mlast;
  mdeut = rhs.mdeut;
  r = rhs.r;
  n = rhs.n;
  mdot = rhs.mdot;
  /* PS: Add netlum */
  netlum=rhs.netlum;
#ifdef NHIST
  for (int i=0; i<NHIST; i++) mdot_hist[i]=rhs.mdot_hist[i];
  l_hist=rhs.l_hist;
#endif
  burnState = rhs.burnState;

  return(*this);
}

StarParticleData
StarParticleData::operator+(const StarParticleData& rhs) {
  StarParticleData result;
  result.m = m+rhs.m;
  for (int i=0; i<CH_SPACEDIM; i++) {
    result.pos[i]=(pos[i]*m+rhs.pos[i]*rhs.m)/result.m;
    result.mom[i]=mom[i]+rhs.mom[i];
    result.angmom[i]=angmom[i]+rhs.angmom[i]; // add the spin angular momenta
  }

  if((rhs.burnState == Uninitialized || burnState == Uninitialized) &&
     !(rhs.burnState == Uninitialized && burnState == Uninitialized)) {
    // set mlast to the mass of the larger star if the smaller star is uninitialized
    // this causes the merged companion particle mass to be included in the accretion
    // luminosity in the next time step
    // ... but, if both stars are uninitialized, do not included merged companions in
    // the accretion or wind luminosity
    if(rhs.m > m) {
      result.mlast = rhs.mlast;
      result.mdeut = rhs.mdeut;
    } else {
      result.mlast = mlast;
      result.mdeut = mdeut;
    }
  } else {
    // otherwise set mlast to the sum
    result.mlast = mlast + rhs.mlast;
    result.mdeut = mdeut + rhs.mdeut;
  }
  result.mdot = mdot + rhs.mdot;
  /*PS: Add netlum */
  result.netlum = netlum + rhs.netlum;

#ifdef NHIST
  result.l_hist = l_hist + rhs.l_hist;
  for (int i=0; i<NHIST; i++) {
    if (mdot_hist[i]>0.0) {
      if (rhs.mdot_hist[i]>0.0) {
	result.mdot_hist[i]=mdot_hist[i]+rhs.mdot_hist[i];
      } else {
	result.mdot_hist[i]=mdot_hist[i];
      }
    } else {
      result.mdot_hist[i]=rhs.mdot_hist[i];
    }
  }
#endif

  // Two cases: if one of the stars is >10 times the mass of the other,
  // just copy the larger star's internal model. Otherwise reset the
  // model to its initial state.
  if (m > 10*rhs.m) {
    result.burnState = burnState;
    result.r = r;
    /*if (CHprocID()==uniqueProc(SerialTask::compute)) {
      pout() << "Merging stars, masses = "
	   << m/2.0e33 << " " << rhs.m/2.0e33
	   << ", leaving burnState = " << result.burnState
	   << std::endl;
    }*/
  } else if (rhs.m > 10*m) {
    result.burnState = rhs.burnState;
    result.r = rhs.r;
    /*if (CHprocID()==uniqueProc(SerialTask::compute)) {
      pout() << "Merging stars, masses = "
	   << rhs.m/2.0e33 << " " << m/2.0e33
	   << ", leaving burnState = " << result.burnState
	   << std::endl;
    }*/
  } else {
    result.burnState = Uninitialized;
    /*if (CHprocID()==uniqueProc(SerialTask::compute)) {
      pout() << "Merging stars, masses = "
	   << rhs.m/2.0e33 << " " << m/2.0e33
	   << ", setting burnState = " << result.burnState
	   << std::endl;
    }*/
  }

  // Add the orbital angular momentum of the two stars about the center
  // of mass to the spin angular momentum of the individual stars
  if (CH_SPACEDIM==3) {
    Real rorbit[CH_SPACEDIM], vorbit[CH_SPACEDIM];
    Real RedMass;
    RedMass = m * rhs.m/result.m;
    for (int i=0; i<CH_SPACEDIM; i++) {
      rorbit[i] = pos[i] - rhs.pos[i];
      vorbit[i] = mom[i]/m - rhs.mom[i]/rhs.m;
    }
    result.angmom[0] += RedMass*(rorbit[1]*vorbit[2] - rorbit[2]*vorbit[1]);
    result.angmom[1] += RedMass*(rorbit[2]*vorbit[0] - rorbit[0]*vorbit[2]);
    result.angmom[2] += RedMass*(rorbit[0]*vorbit[1] - rorbit[1]*vorbit[0]);
  }
  // For angmom_method==1:
  // Cap orbital angular momentum magnitude to that of a circular orbit about the center
  // of mass at the radius of the merged star.
  // Mergers between comparable mass stars resets the burn model.
  // 
  // Stars with uninitialized burn models have no radius (r=0).  In this case
  // conserve angular momentum without reguard to subgrid angular momemtum dissipation.
  // Mergers of stars with the same mass to within an order of magnitude reset thier 
  // evolution models to burnState = 0.  In this case, use the radius of the larger
  // of the initial stars as the keplerian radius.
  if (result.burnState==Uninitialized) r=max(result.r,rhs.r);
  if (angmom_method==1 and r>0.) {
    Real tempL=0.;
    // Drummond: Cap angular momentum to breakup
    for (int i=0; i<CH_SPACEDIM; i++) {
  	  tempL += result.angmom[i]*result.angmom[i];
    }
    tempL = sqrt(tempL);
    if (tempL > sqrt(G*m*m*m*r)) {
      for (int i=0; i<CH_SPACEDIM; i++) {
        result.angmom[i] = sqrt(G*m*m*m*r)*result.angmom[i]/tempL ;
      }
    }
    // pout() << "[+] Ang Mom After Cap: " << result.angmom[0] << ' ' <<result.angmom[1] << ' ' <<result.angmom[2] << std::endl; 
  }
    
  return(result);
}

/* Cleaned up star merging algorithm. Eliminates some
   inconsistencies found in previous versions -- PS, AL 2016 */
StarParticleData&
StarParticleData::operator+=(const StarParticleData& rhs)
{
  Real x1[CH_SPACEDIM], x2[CH_SPACEDIM], p1[CH_SPACEDIM], p2[CH_SPACEDIM];
  Real m1, m2;

  // Assumptions: Particle 1 will be *this particle.
  // Particle 2 will be the passed in rhs particle.

  m1 = m;
  m2 = rhs.m;
  int m1burn = burnState;
  int m2burn = rhs.burnState;

  // Calculates center of mass position and momentum
  // Position and Momentums are updated
  for (int i=0; i<CH_SPACEDIM; i++) {
    x1[i]=pos[i];
    x2[i]=rhs.pos[i];
    p1[i]=mom[i];
    p2[i]=rhs.mom[i];
    pos[i]=pos[i]*m1+rhs.pos[i]*m2;
    mom[i]+=rhs.mom[i];
    p1[i]=m1*(p1[i]/m1 - mom[i]/(m1+m2));
    p2[i]=m2*(p2[i]/m2 - mom[i]/(m1+m2));
    angmom[i]+=rhs.angmom[i]; // Meaningless. Eventually overwritten below.
  }
  for (int i=0; i<CH_SPACEDIM; i++) pos[i] /= (m1+m2);

  // Updates particle's mass as the sum
  m+=rhs.m;

  // Add the orbital angular momentum of the two stars about the center
  // of mass to the spin angular momentum of the individual stars
  // Corrections are potentially made at the end of this routine if the stars
  // have too much angular momentum.
  if (CH_SPACEDIM==3)
  {
    Real rorbit[CH_SPACEDIM], vorbit[CH_SPACEDIM];
    Real ReducedMass;
    ReducedMass = m1*m2/(m1+m2);
    for (int i=0; i<CH_SPACEDIM; i++) {
      rorbit[i] = x1[i] - x2[i];    // x1 and x2: original positions
      vorbit[i] = p1[i]/m1 - p2[i]/m2; // p1 and p2: original momentums
    }
    angmom[0] += ReducedMass*(rorbit[1]*vorbit[2] - rorbit[2]*vorbit[1]);
    angmom[1] += ReducedMass*(rorbit[2]*vorbit[0] - rorbit[0]*vorbit[2]);
    angmom[2] += ReducedMass*(rorbit[0]*vorbit[1] - rorbit[1]*vorbit[0]);
  }

// Tracks Mdot histories as sum
#ifdef NHIST
  for (int i=0; i<NHIST; i++) {
    if (mdot_hist[i]>0.0) {
      if (rhs.mdot_hist[i]>0.0) {
        mdot_hist[i]=mdot_hist[i]+rhs.mdot_hist[i];
      } else {
        mdot_hist[i]=mdot_hist[i];
      }
    } else {
      mdot_hist[i]=rhs.mdot_hist[i];
    }
  }
#endif


  // If only one star is uninitalized, and it's the smaller of the two particles
  // set mlast and mdeut to the values of the larger particle. Else we will add
  // their masses together (which means the merged mass will not be included
  // in the calculations of accretion luminosity or wind luminosity).
  if( (m1burn==Uninitialized || m2burn==Uninitialized) &&
     !(m1burn==Uninitialized && m2burn==Uninitialized)    )
     {
         // Only one but not both are un-init
         if(m2>m1 && m1burn==Uninitialized)
         {
             mlast = rhs.mlast;
             mdeut = rhs.mdeut;
         }
         else
         {
             // Either m1>m2 and m2 is unitialized, or
             // m2>m1 but m1 is initialized.

             // Keep values the same as particle 1's values.
             // No changes needed.
         }
     }
   else
     {
         // Both are initialized or both are uninitialized
         // (Can this merging algorithm even occur if both are initialized??) --AL
         mlast = mlast + rhs.mlast;
         mdeut = mdeut + rhs.mdeut;
     }


  // ORIGINAL COMMENT REGARDING MLAST and MDEUT merging:
    // set mlast to the mass of the larger star if the smaller star is uninitialized
    // this causes the merged companion particle mass to be included in the accretion
    // luminosity in the next time step
    // ... but, if both stars are uninitialized, do not included merged companions in
    // the accretion or wind luminosity


  // Mass accretion rate is the sum
  mdot = mdot + rhs.mdot;
  /*PS: Add netlum */
  netlum = netlum + rhs.netlum;


  // If one star is >10 times the mass of the other,
  // copy the larger star's internal model. Else reset it.
  // Before there was an error here, it was comparing the merged mass to the original
  // mass of the rhs particle.
  if(m2>10*m1)
  {
      burnState = rhs.burnState;
      r = rhs.r;
  }
  else if(m1>10*m2)
  {
      // Keep internal state as is
  }
  else
  {
      burnState = Uninitialized;
  }


  // Angular momentum correction, not changed in cleanup --AL 2016
  // For angmom_method==1:
  // Cap orbital angular momentum magnitude to the breakup rate
  //
  // Stars with uninitialized burn models have no radius (r=0).  In this case
  // conserve angular momentum without reguard to subgrid angular momemtum dissipation.
  // Mergers of stars with the same mass to within an order of magnitude reset thier
  // evolution models to burnState = 0.  In this case, use the radius of the larger
  // of the initial stars as the keplerian radius.
  if (burnState==Uninitialized) r=max(r,rhs.r);
  if (angmom_method==1 and r>0.) {
    Real tempL=0.;
    for (int i=0; i<CH_SPACEDIM; i++) {
      tempL += angmom[i]*angmom[i];
    }
    tempL = sqrt(tempL);
    if (tempL > sqrt(G*m*m*m*r)) {
      for (int i=0; i<CH_SPACEDIM; i++) {
        angmom[i] = sqrt(G*m*m*m*r)*angmom[i]/tempL ;
      }
    }
  }
  // pout() << "[+=] Ang Mom After Cap: " << angmom[0] << angmom[1] << angmom[2] << std::endl;


  return(*this);
}


/***************************/
/* Initialization routines */
/***************************/

inline
Real
StarParticleData::nInit(Real mdotInit) {
  if (mdotInit == 0.0) return(1.5);
  Real aGinit = 1.475 + 0.07*log10(mdotInit*YRSEC/MSUN);
  Real nval = 5.0 - 3.0/aGinit;
  if (nval < 1.5) nval = 1.5;
  if (nval > 3.0) nval = 3.0;
  return( nval );
}

inline
Real
StarParticleData::radInit(Real mdotInit) {
  // AJC - floor the expression so that even for small mdotInit,
  // radInit is always >= 2 rsun.
  return ( RSUN * max(2.5*pow(mdotInit*YRSEC/MSUN*1.0e5, 0.2), 2.0) );
}

/*****************************/
/* Polytropic model routines */
/*****************************/

// For a polytrope, the gravitational energy is aG G M^2 / R, aG = -3/(5-n)
inline
Real
StarParticleData::aG() { return( 3.0/(5.0-n) ); }

// The central density in a polytropic model, found by table lookup.
// See Kippenhahn & Weigert.
inline
Real
StarParticleData::rhoc(Real mass) {
  CH_assert ((n>=1.5) && (n<=3.0));
  // Table of values of rho_mean / rho_c for n=1.5 to 3.1 in intervals of 0.1
  static Real rhofactab[] = {
    0.166931, 0.14742, 0.129933, 0.114265, 0.100242,
    0.0877, 0.0764968, 0.0665109, 0.0576198, 0.0497216,
    0.0427224, 0.0365357, 0.0310837, 0.0262952, 0.0221057,
    0.0184553, 0.01529
  };
  int itab = (int) floor((n-1.5)/0.1);
  Real wgt = (n - (1.5 + 0.1*itab)) / 0.1;
  Real rhofac = rhofactab[itab]*(1.0-wgt) + rhofactab[itab+1]*wgt;
  return( mass / (4./3.*M_PI*r*r*r) / rhofac );
}

// The central pressure in a polytropic model, found by table lookup.
// See Kippenhahn & Weigert.
inline
Real
StarParticleData::Pc(Real mass) {
  static Real pfactab[] = {
    0.770087, 0.889001, 1.02979, 1.19731, 1.39753,
    1.63818, 1.92909, 2.2825, 2.71504, 3.24792, 3.90921,
    4.73657, 5.78067, 7.11088, 8.82286, 11.0515, 13.9885
  };
  int itab = (int) floor((n-1.5)/0.1);
  Real wgt = (n - (1.5 + 0.1*itab)) / 0.1;
  Real pfac = pfactab[itab]*(1.0-wgt) + pfactab[itab+1]*wgt;
  return( pfac * G * mass*mass/(r*r*r*r) );
}

// The central temperature in a protostar, found by using a bisection
// method to solve Pc = rho_c k Tc / (mu mH) + 1/3 a Tc^4.
Real
StarParticleData::Tc(Real mass, Real rhoc1, Real Pc1) {
  if (rhoc1 == -1.0) rhoc1 = rhoc(mass);
  if (Pc1 == -1.0) Pc1 = Pc(mass);
#define JMAX 40
#define TOL 1.0e-7
  Real Tgas, Trad;
  int j;
  Real dx, f, fmid, xmid, rtb;
  Real x1, x2;
  char errstr[256];

  x1 = 0.0;
  Tgas = Pc1*MU*MH/(KB*rhoc1);
  Trad = pow(3*Pc1/A, 0.25);
  x2 = (Trad > Tgas) ? 2*Trad : 2*Tgas;
  f = Pc1 - rhoc1*KB*x1/(MU*MH) - A*pow(x1,4)/3.0;
  fmid=Pc1 - rhoc1*KB*x2/(MU*MH) - A*pow(x2,4)/3.0;
  rtb = f < 0.0 ? (dx=x2-x1,x1) : (dx=x1-x2,x2);
  for (j=1;j<=JMAX;j++) {
    xmid=rtb+(dx *= 0.5);
    fmid = Pc1 - rhoc1*KB*xmid/(MU*MH) - A*pow(xmid,4)/3.0;
    if (fmid <= 0.0) rtb=xmid;
    if (fabs(dx) < TOL*fabs(xmid) || fmid == 0.0) return rtb;
  }
  sprintf(errstr,
	  "SinkParticleData::Tc(): bisection solve didn't converge, P_c = %e, rho_c = %e, mass = %e Tgas = %e Trad = %e rad = %e ",
	  Pc1, rhoc1, mass, Tgas, Trad, r);
  MayDay::Error(errstr);
  return(-1);
#undef JMAX
#undef TOL
}

inline
Real
StarParticleData::betac(Real mass, Real rhoc1, Real Pc1, Real Tc1) {
  if (rhoc1 == -1.0) rhoc1 = rhoc(mass);
  if (Pc1 == -1.0) Pc1 = Pc(mass);
  if (Tc1 == -1.0) Tc1 = Tc(mass, rhoc1, Pc1);
  return( rhoc1*KB*Tc1/(MU*MH) / Pc1 );
}

Real
StarParticleData::beta(Real mass) {
  // This function just does simple bi-linear interpolation on a
  // pre-computed table of beta versus mass and polytropic index; note
  // that beta here is the mean value over the whole star, not the
  // central value. The table has been computed numerically as a
  // function of log M (where M is in units of Msun) from log M = 0 -
  // 2.5 and n = 1.5 - 3, in steps of 0.1 in both dimensions. Lower
  // and higher masses are handled by taking the limiting cases of the
  // solution. Values of n outside the allowed range should never occur.

  // Variables describing the table
  static Real logm0 = 0.0, dlogm = 0.1;
  static Real n0 = 1.5, dn = 0.1;
  static int nm = 26, nn = 16;

  // Table of numerical values
  static Real betatab[26][16] =
    {{0.999492, 0.999504, 0.999515, 0.999525, 0.999534, 0.999541, 
      0.999548, 0.999553, 0.999558, 0.999562, 0.999566, 0.999569, 
      0.999571, 0.999572, 0.999573, 0.999573},
     {0.999196, 0.999215, 0.999233, 0.999248, 0.999262, 0.999274,
      0.999284, 0.999293, 0.999301, 0.999307, 0.999313, 0.999317,
      0.99932, 0.999323, 0.999324, 0.999324},
     {0.998728, 0.998759, 0.998787, 0.998811, 0.998832, 
      0.998851, 0.998867, 0.998881, 0.998894, 0.998904, 0.998912, 
      0.998919, 0.998924, 0.998928, 0.99893, 0.998931},
     {0.997991, 0.99804, 0.998083, 0.998121, 0.998155, 0.998184, 0.99821,
      0.998232, 0.998251, 0.998267, 0.998281, 0.998291, 0.9983, 0.998305,
      0.998309, 0.99831},
     {0.996834, 0.99691, 0.996978, 0.997037, 0.997089, 0.997135, 0.997176,
      0.99721, 0.99724, 0.997265, 0.997286, 0.997303, 0.997316, 0.997325,
      0.99733, 0.997332},
     {0.995025, 0.995143, 0.995248, 0.99534, 0.995421, 0.995493, 0.995555,
      0.995609, 0.995655, 0.995694, 0.995726, 0.995752, 0.995772, 0.995786,
      0.995795, 0.995797},
     {0.992221, 0.992402, 0.992562, 0.992703, 0.992827, 
      0.992936, 0.993032, 0.993114, 0.993185, 0.993245, 0.993294, 
      0.993334, 0.993365, 0.993386, 0.993399, 0.993403},
     {0.987924, 0.988195, 0.988435, 0.988647, 0.988833, 0.988998, 0.989141, 
      0.989265, 0.989372, 0.989462, 0.989536, 0.989596, 0.989642, 
      0.989674, 0.989693, 0.9897},
     {0.981449, 0.981844, 0.982193, 
      0.982503, 0.982776, 0.983017, 0.983227, 0.983409, 0.983565, 
      0.983697, 0.983806, 0.983894, 0.983961, 0.984008, 0.984036, 
      0.984045},
     {0.971919, 0.972472, 0.972964, 0.973399, 0.973785, 
      0.974124, 0.97442, 0.974677, 0.974897, 0.975084, 0.975237, 0.975361,
      0.975455, 0.975521, 0.97556, 0.975573},
     {0.95832, 0.959059, 
      0.959716, 0.9603, 0.960817, 0.961271, 0.961669, 0.962014, 0.962309, 
      0.962559, 0.962764, 0.962929, 0.963054, 0.963142, 0.963194, 
      0.963211},
     {0.939649, 0.940579, 0.941407, 0.942143, 0.942794, 
      0.943367, 0.943868, 0.944301, 0.944673, 0.944985, 0.945242, 
      0.945448, 0.945603, 0.945712, 0.945776, 0.945797},
     {0.91513, 
      0.916223, 0.917197, 0.918062, 0.918826, 0.919497, 0.920083, 
      0.920588, 0.92102, 0.921382, 0.92168, 0.921915, 0.922094, 0.922217, 
      0.92229, 0.922313},
     {0.884431, 0.885628, 0.886692, 0.887634, 
      0.888465, 0.889192, 0.889824, 0.890367, 0.890829, 0.891214, 
      0.891528, 0.891776, 0.891962, 0.892091, 0.892165, 
      0.892189},
     {0.847787, 0.849009, 0.85009, 0.851042, 0.851877, 
      0.852605, 0.853233, 0.85377, 0.854223, 0.854598, 0.854902, 0.85514, 
      0.855317, 0.855439, 0.855508, 0.855531},
     {0.805975, 0.80714, 
      0.808165, 0.809062, 0.809842, 0.810516, 0.811094, 0.811583, 
      0.811993, 0.812329, 0.812598, 0.812808, 0.812962, 0.813066, 
      0.813126, 0.813145},
     {0.76016, 0.761206, 0.762117, 0.762907, 
      0.763588, 0.76417, 0.764664, 0.765078, 0.765421, 0.765699, 0.76592, 
      0.766089, 0.766212, 0.766295, 0.766341, 0.766356},
     {0.711692, 
      0.71258, 0.713344, 0.713999, 0.714557, 0.715029, 0.715424, 0.71575, 
      0.716017, 0.71623, 0.716397, 0.716523, 0.716614, 0.716673, 0.716706,
      0.716716},
     {0.661917, 0.662632, 0.66324, 0.663753, 0.664184, 
      0.664542, 0.664838, 0.665078, 0.665271, 0.665422, 0.665538, 
      0.665623, 0.665683, 0.665722, 0.665742, 0.665748},
     {0.612041, 
      0.612589, 0.613047, 0.613427, 0.61374, 0.613995, 0.614201, 0.614364,
      0.614491, 0.614589, 0.614661, 0.614712, 0.614746, 0.614766, 
      0.614776, 0.614779},
     {0.563063, 0.563461, 0.563787, 0.564051, 
      0.564262, 0.564429, 0.56456, 0.564659, 0.564733, 0.564786, 0.564822,
      0.564846, 0.564859, 0.564866, 0.564868, 0.564868},
     {0.51575, 
      0.516022, 0.516238, 0.516406, 0.516535, 0.516632, 0.516702, 
      0.516751, 0.516782, 0.516801, 0.51681, 0.516813, 0.51681, 0.516806, 
      0.516802, 0.516801},
     {0.470653, 0.470824, 0.470952, 0.471044, 
      0.471109, 0.471151, 0.471175, 0.471186, 0.471186, 0.47118, 0.471169,
      0.471156, 0.471142, 0.471131, 0.471122, 0.471119},
     {0.428134, 
      0.428225, 0.428286, 0.428321, 0.428337, 0.428339, 0.428329, 
      0.428312, 0.42829, 0.428265, 0.42824, 0.428216, 0.428195, 0.428178, 
      0.428167, 0.428163},
     {0.388399, 0.388431, 0.38844, 0.388434, 
      0.388415, 0.388387, 0.388354, 0.388317, 0.38828, 0.388243, 0.388208,
      0.388177, 0.388151, 0.388131, 0.388118, 0.388114},
     {0.351535, 
      0.351522, 0.351496, 0.35146, 0.351416, 0.351369, 0.351319, 0.35127, 
      0.351222, 0.351178, 0.351137, 0.351102, 0.351073, 0.351051, 
      0.351037, 0.351033}};

  // Got log mass and handle various cases
  double logm = log10(mass/MSUN);
  if (logm <= logm0) {
    
    // Set beta = 1 exactly for M < 1 Msun
    return 1.0;
    
  } else if (logm < logm0 + nm*dlogm) {

    // Interior of the table, so do bilinear interpolation
    int midx = (int) floor( (logm-logm0 / dlogm) );
    Real mwgt = (logm - (logm0 + midx*dlogm)) / dlogm;
    int nidx = (int) floor( (n-n0) / dn);
    Real nwgt = (n - (n0 + nidx*dn)) / dn;

    // Handle special cases n = 1.5 and n = 3 exactly to avoid
    // roundoff issues
    if (n == 1.5) {
      nidx = 0;
      nwgt = 0.0;
    } else if (n == 3.0) {
      nidx = nn-1;
      nwgt = 1.0;
    }

    // Do linear interolation
    double beta_interp =
      betatab[midx][nidx]*(1.0-mwgt)*(1.0-nwgt) +
      betatab[midx+1][nidx]*mwgt*(1.0-nwgt) +
      betatab[midx][nidx+1]*(1.0-mwgt)*nwgt +
      betatab[midx+1][nidx+1]*mwgt*nwgt;
    return beta_interp;

  } else {

    // If we are off the table high, interpolate on the last row in
    // n, then use the analytic extrapolation beta ~= M^(-1/2)
    int nidx = (int) floor( (n-n0) / dn);
    Real nwgt = (n - (n0 + nidx*dn)) / dn;
    
    // Handle special cases n = 1.5 and n = 3 exactly to avoid
    // roundoff issues
    if (n == 1.5) {
      nidx = 0;
      nwgt = 0.0;
    } else if (n == 3.0) {
      nidx = nn-2;
      nwgt = 1.0;
    }

    double beta_table_end = (1.0-nwgt) * betatab[nm-1][nidx] +
      nwgt * betatab[nm-1][nidx+1];
    return beta_table_end / sqrt(pow(10., logm - logm0 - nm*dlogm));

  }
}


#define DM (0.01*m)
Real
StarParticleData::dlogBetaOverBetac_dlogM(Real beta_1) {
  // If n==3, beta = beta_c independent of M, so return 0
  if (n==3) return(0.0);

  // Otherwise take a numerical derivative
  Real beta1;
  if (beta_1==-1.0) beta1 = beta(m);
  else beta1 = beta_1;
  Real beta2 = beta(m+DM);
  Real betac1 = betac(m);
  Real betac2 = betac(m+DM);
  return( m/(beta1/betac1) * ((beta2/betac2) - (beta1/betac1)) / DM );
}

Real
StarParticleData::dlogBeta_dlogM(Real beta_1) {
  // Take a numerical derivative
  Real beta1;
  if (beta_1==-1.0) beta1 = beta(m);
  else beta1 = beta_1;
  Real beta2 = beta(m+DM);
  return( m/beta1 * (beta2-beta1) / DM );
}
#undef DM

/*******************/
/* Model functions */
/*******************/

#define ERGEV  1.6e-12    /* Number of ergs per eV */
#define FACC   0.5        /* Fraction of accreted energy that comes out as
			     radiation, rather than being advected into the
			     stellar interior or used to drive a wind */
#define FK     0.5        /* Fraction of energy the falls into the sink
			     particle but is radiated away from the the
			     inner disk before reaching the stellar surface */

#define FWIND  0.21        /* Fraction of accreted mass ejected in a wind */

#define FRAD   0.33       /* A radiative barrier forms when L_deuterium <=
			     FRAD * L_ZAMS. See McKee & Tan 2002 */
#define SHELLFAC 2.1      /* Radius increases by SHELLFAC when shell burning
			     starts */
#define THAY   3000.0     /* Hayashi temperature */
#define TDEUT  1.5e6      /* Temperature when deuterium burning starts */
#define PSIION (16.8*ERGEV*NAVOG) /* Energy per gram needed to dissociate
				     and ionize a molecular gas with solar
				     abundances */
#define PSID   (100*ERGEV*NAVOG)  /* Energy per gram released by burning
				     the deuterium in a gas with solar
				     abundances */
/*PS: base on 1st core is about 0.04 MSUN*/
#define MRADMIN (0.04*MSUN)   /* Minimum mass at which we use the model */

Real 
StarParticleData::luminosity(Real tnetlum) {
#ifndef RADPARTICLE
  if (burnState == Uninitialized) return(0.0);
  //PS: Add netlum from last step if it is negative.
  Real lum;
  if (tnetlum < 0.0) {
    lum =  lStar() + lAcc() + lDisk() + tnetlum;
  }
  else {
    lum =  lStar() + lAcc() + lDisk();
  }
  if (lum > 1.0e6*3.839e33) {
    cout << "really high total luminosity in luminosity()... " << lum << endl;
    cout << "lstar = " << lStar() << endl;
    cout << "ldisk = " << lDisk() << endl;
  }
#ifdef NHIST
  //SSRO take the smaller of the new luminosity or 
  //25% greater than the old luminosity.
  l_hist=(lum<1.25*l_hist)?lum:1.25*l_hist;
  //PS: lum can be negative now.  If l_hist < 0, set to 0.
  if (l_hist < 0.0) l_hist =0.0;  
#endif
  return( lum );
#else // if using RADPARTICLE
  return 6.24e38;
#endif
}

Real
StarParticleData::lStar() {
  /*PS: Change temperature check to lZAMS only
  Real lstar = lZAMS() + lAcc();
  */
  Real lstar = lZAMS();  
  Real Teff = pow(lstar / (4. * M_PI * r*r * SIGMA), 0.25);

  if (lstar > 1.0e6*3.839e33) {
    cout << "really high total luminosity in lStar()... " << lstar << endl;
    cout << "ZAMS = " << lZAMS() << endl;
    cout << "lAcc = " << lAcc() << endl;
    cout << "teff = " << Teff << endl;
    cout << "r = " << r << endl;
    cout << "m = " << m << endl;
    cout << "mdot = " << mdot << endl;
  }

  if (Teff > THAY) {
    return( lstar );
  } else {
    return( 4.*M_PI*r*r*SIGMA*pow(THAY, 4) );
  }
}

inline
Real
StarParticleData::lAcc() {
  //PS: Include ionization in energy balance.
  Real lacc = FACC * (FK * G * m / r - PSIION) * mdot;
  return( lacc );
}

Real
StarParticleData::lDisk() {
  //PS: Include ionization in energy balance.
  Real ldisc = ((1.0 - FK) * (G * m / r - PSIION)) * mdot;
  return ( ldisc );
}

Real
StarParticleData::lDeut(Real beta1) {
  switch (burnState) {
  case Uninitialized: return(0.0);
  case None: return(0.0);
  case VariableCoreDeuterium: {
    if (beta1 == -1.0) beta1=beta(m);
    return( lStar() + eDotIon() + G*m*mdot/r *
	    (1.0 - FK - aG()*beta1/2.0 *
	     (1.0 + dlogBetaOverBetac_dlogM(beta1))) );
  }
  case SteadyCoreDeuterium: return( mdot * PSID );
  case ShellDeuterium: return( mdot * PSID );
  default: MayDay::Error("SinkParticleData::lDeut(): bad value of burnState");
  }
  return(-1.0); // Never get here
}
  
inline
Real
StarParticleData::eDotIon() {
  return( mdot * PSIION );
}

inline
Real
StarParticleData::dlogR_dlogM(Real beta1) {
  if (beta1==-1.0) beta1 = beta(m);
  return( 2.0 - 2.0/(aG()*beta1) * (1.0 - FK) +
	  dlogBeta_dlogM(beta1) - 2.0*r/(aG()*beta1*G*m*mdot) * 
	  (lStar() + eDotIon() - lDeut(beta1)) );
}

Real
StarParticleData::vWind(Real vw_fkep, Real vw_max, Real rlaunch) {
  // Set wind velocity equal to Keplerian velocity
  if (burnState == Uninitialized) return(0.0);
  //return( sqrt(G*m/r) );
  return( min(vw_fkep*sqrt(G*m/r),vw_max) * sqrt(1.+2.*r/(pow(vw_fkep,2)*rlaunch)));
  //return( std::min(sqrt(G*m/r),300e5) );
}


Real
StarParticleData::iso_mloss() {
  // Compute isotropic wind for MS OB stars; see Vink et al. 2001, Langer et al. 95
  // No line-driven winds for protostars that have Teff<12,500 K
  //OR if they aren't on the ZAMS for cooler stars.
  Real Teff = std::pow(10, get_logTeff());
  if (burnState != ZAMS && Teff < 1.25e4) return(0.0);
  Real vrat = 1.3;   // Ratio of the terminal velocity to the escape speed.                          
                      // Bi-uniform with break at B1: m~17Msun, Teff~21,000 K (now calculate Tjump from Vink+2001, bistability jump around Teff~25 kK
  Real Z = 1.0;       // Metallicity, not currently in our stellar model in units of solar metallicity
  Real lstar = lStar();
  Real logmdot = 0.0; //Log mass loss rate in (Msun/yr)
  
  //Get the bi-stability jump parameter for this star, equations 14 & 15 from Vink+2001
  //Thomson scattering values from Lamers & Leitherer 1993
  Real sigmat = 0.31; //cm^2/g for Teff<30 kK
  if (Teff >= 35e3)
    sigmat = 0.34;
  else if (Teff < 35e3 && Teff >= 30e3)
    sigmat = 0.32;
  //From Vink+2001, temperature location of bistability jump at around 25 kK
  //The following vrat factors and mdots depend on Tjump
  Real eddRatio = 7.66e-5 * sigmat * (lstar/LSUN) * (MSUN/m); //eqn 11
  Real logRho = -14.94 + 0.85*log(Z/1.0) + 3.2 * eddRatio; //eqn 14
  Real Tjump = (61.2+2.59 *logRho) * 1e3; //eqn 15

  if (Teff >= Tjump){
    vrat = 2.6;  // O stars, hot side of bistability jump
  }else if (Teff < 1.25e4){
    vrat = 0.7; // A-F stars
  }else{
    vrat = 1.3; // Bstars, cool side of the bistability jump
  }
  //Note log used in Vink+2001 is log based 10
  if (Teff < Tjump) {
    //Cool side of the bistability jump at T~ 25 kK 
    //logmdot = -6.688+2.210*log(lstar/(1.0e5*LSUN)) - 1.339*log(m/(30.*MSUN)) 
    //          -1.601*log(vrat/2.0)+1.07*log(Teff/2.0e4)+0.85*log(Z/1.0);
    logmdot = -6.688+2.210*std::log10(lstar/(1.0e5*LSUN)) - 1.339*std::log10(m/(30.*MSUN))
      -1.601*std::log10(vrat/2.0)+1.07*std::log10(Teff/2.0e4)+0.85*std::log10(Z/1.0);
  }else{
    //Hot side of the bistability jump
    logmdot = -6.697+2.194*std::log10(lstar/(1.0e5*LSUN)) - 1.313*std::log10(m/(30.*MSUN))
      -1.226*std::log10(vrat/2.0)+0.933*std::log10(Teff/4.0e4) - 10.92*std::pow(std::log10(Teff/4.0e4),2.0)+0.85*std::log10(Z/1.0);
  
    //logmdot = -6.697+2.194*log(lstar/(1.0e5*LSUN)) - 1.313*log(m/(30.*MSUN))
    //  -1.226*log(vrat/2.0)+0.933*log(Teff/4.0e4) - 10.92*pow(log(Teff/4.0e4),2.0)+0.85*log(Z/1.0);
    
  }
  
  return (std::pow(10, logmdot)*MSUN/YRSEC);
}

Real
StarParticleData::iso_vWind(Real vw_max) {
  // Compute isotropic wind for MS OB stars; see Leitherer et al 92
  // No line-driven winds for protostars that have Teff<12,500 K
  //OR if they aren't on the ZAMS for cooler stars.
  Real Z = 1.0;       // Metallicity, not currently in our stellar model
  Real Teff = std::pow(10, get_logTeff());
  if (burnState != ZAMS && Teff < 1.25e4) return(0.0);

  Real lstar = lStar();
  //Log terminal velocity in km/s (from Leitherer+1992) eqn. 2
  //Note log used in this paper is NATURAL log
  Real logvout = 1.23-0.3*std::log(lstar/LSUN)+0.55*std::log(m/MSUN)+0.64*std::log(Teff)+0.13*std::log(Z);
  //pout() << "ALR: logvout = "<<  logvout << std::endl;
  return (min(std::exp(logvout)*1e5,vw_max));
}

// Conversions from solar to CGS units

/* 
 Determines the total luminosity of a population III star
 Uses the models of Stacy et al., which are power-law fits
 to Hosokawa's stellar evolution models. -- ALee 2016
 */


void
StarParticleData::updateState(Real dt) {

  // Note that we don't update the mass, since that is handled by 
  // SinkParticle::accrete. We only update mdot and mdeut.
#ifdef NHIST
  for (int i=NHIST-1; i>0; i--) mdot_hist[i] = mdot_hist[i-1];
  mdot_hist[0] = (m - mlast) / dt;
  mdot = 0;
  int nhist=0;
  for (int i=0; i<NHIST; i++) {
    if (mdot_hist[i] != -1.0) {
      mdot += mdot_hist[i];
      nhist++;
    }
  }
  mdot /= nhist;
#else 
  mdot = (m - mlast) / dt;
  
  pout() << "SPD::updateState: M = " << m << ", mlast = " << mlast << ", mdot = " << mdot << std::endl;  
#endif
  mdeut += m - mlast;
    
// We switch to a different stellar model when we are dealing with primordial stars.
  // Do nothing if we are below the minimum mass or we have just been
  // created and thus don't have a valid mdot. Otherwise, if we're not
  // initilized, then initialize here
  if (burnState == Uninitialized) {
    if ((m < MRADMIN) || (mdot == 0.0)) {
      mlast = m;
      return;
    }
    n = nInit(mdot);
    r = radInit(mdot);
    burnState = None;
    if (CHprocID()==uniqueProc(SerialTask::compute)) {
      pout() << "Initializing star at m = " << m/2.0e33 << " M_sun..."
	   << std::endl;
    }
  }

  // Update the radius
  if (burnState != ZAMS) {
    Real beta1 = beta(m);
    Real dr = (2.0*mdot/m*r*(FK/(aG()*beta1)+1.0-1.0/(aG()*beta1))
	     + beta1/m * dlogBeta_dlogM(beta1) * mdot * r / beta1
	     - 2.0/(beta1*aG())*r*r/(G*m*m)*(lStar()+eDotIon()-lDeut(beta1)));
    Real rdottime = fabs(r/dr  )/100.0;
    Real mdottime = fabs(m/mdot)/100.0;
    
    if( rdottime < dt)
      {
	int rdotfac = ceil(dt/rdottime);
	Real rdotfacr = rdotfac;
	Real dtprime = dt/rdotfac;
       	//printf("In Loop: rdottime: %6.4e rdotfac: %i rdotfacr: %6.4e dtprime: %6.4e radius: %6.4e dt: %6.4e\n",rdottime,rdotfac,rdotfacr,dtprime,r,dt);
	for(int rdotloop = 0; rdotloop < rdotfac; rdotloop++)
	  {
	    beta1 = beta(m);
	    dr = (2.0*mdot/m*r*(FK/(aG()*beta1)+1.0-1.0/(aG()*beta1))
	     + beta1/m * dlogBeta_dlogM(beta1) * mdot * r / beta1
	     - 2.0/(beta1*aG())*r*r/(G*m*m)*(lStar()+eDotIon()-lDeut(beta1)));
	    r += dtprime * dr;
	  }

      }else if( mdottime < dt )
      {
	int mdotfac = ceil(dt/mdottime);
	Real mdotfacr = mdotfac;
	Real dtprime = dt/mdotfacr;
	//printf("In Loop: mdottime: %6.4e mdotfac: %i mdotfacr: %6.4e dtprime: %6.4e mass: %6.4e mdot: %6.4e dt: %6.4e\n",mdottime,mdotfac,mdotfacr,dtprime,m,mdot,dt);
	for(int mdotloop = 0; mdotloop < mdotfac; mdotloop++)
	  {
	    beta1=beta(m);
	    dr = (2.0*mdot/m*r*(FK/(aG()*beta1)+1.0-1.0/(aG()*beta1))
	     + beta1/m * dlogBeta_dlogM(beta1) * mdot * r / beta1
	     - 2.0/(beta1*aG())*r*r/(G*m*m)*(lStar()+eDotIon()-lDeut(beta1)));
	    r += dtprime * dr;
	  }
      } else
      {
	beta1=beta(m);
	dr = (2.0*mdot/m*r*(FK/(aG()*beta1)+1.0-1.0/(aG()*beta1))
	     + beta1/m * dlogBeta_dlogM(beta1) * mdot * r / beta1
	     - 2.0/(beta1*aG())*r*r/(G*m*m)*(lStar()+eDotIon()-lDeut(beta1)));
	r += dt * dr;
      }
    if(r < 0.0e0)
    {
      r = 0.2*6.96e10; //Worst case and we do get a neg radius. reset it
      pout() << "Star Particle updating radius: Found negative radius. Resetting to 0.2 R_sun" << std::endl;
    }
  }

  // Update the burning state and things associated with it
  switch (burnState) {

  case None: {
    // No burning yet, so check for the onset of D burning in the core
    n = nInit(mdot);
    if (Tc(m) > TDEUT) {
      burnState = VariableCoreDeuterium;
      n = 1.5; // Star becomes convective
    }
    break;
  }

  case VariableCoreDeuterium: {
    // We are burning deuterium at a variable rate to keep the core
    // temperature constant. Check to make sure we haven't exhausted
    // our supply of D, in which case we change to steady core burning.
    mdeut -= lDeut()*dt/PSID;
    if ( mdeut <= mdot*dt ) {
      burnState = SteadyCoreDeuterium;
      mdeut = 0.0;
    }
    break;
  }

  case SteadyCoreDeuterium: {
    // We are burning deuterium in the core at the rate it comes in. Check
    // to see if a radiative barrier forms, which stops convection, shuts
    // off core deuterium burning, and starts shell burning.
    mdeut = 0.0;
    if ( lDeut() <= FRAD*lZAMS() ) {
      burnState = ShellDeuterium;
      n = 3.0;
      r *= SHELLFAC;
    }
    break;
  }

  case ShellDeuterium: {
    // We are burning deuterium in a shell. Check if the radius has
    // decreased to the ZAMS radius, in which case we stay on the ZAMS
    // from now on.
    mdeut = 0.0;
    if ( r <= rZAMS() ) {
      burnState = ZAMS;
      r = rZAMS();
    }
    break;
  }

  case ZAMS: {
    mdeut = 0.0;
    break;
  }
  }

    
  mlast = m;
    
} // end of updateState(double dt)

/**********************/
/* Main sequence fits */
/**********************/

// Parameters for the main sequence luminosity and radius fitting formulae
// from Tout et al (1996)
#define ALPHA    0.39704170
#define BETA     8.52762600
#define GAMMA    0.00025546
#define DELTA    5.43288900
#define EPSILON  5.56357900
#define ZETA     0.78866060
#define ETA      0.00586685 
#define THETA    1.71535900
#define IOTA     6.59778800
#define KAPPA   10.08855000
#define LAMBDA   1.01249500
#define MU_TOUT  0.07490166
#define NU       0.01077422
#define XI       3.08223400
#define UPSILON 17.84778000
#define PI_TOUT  0.00022582


inline
Real
StarParticleData::lZAMS()
{
  Real msol = m/MSUN;
  Real lsol = (ALPHA*pow(msol,5.5) + BETA*pow(msol,11)) /
    (GAMMA+pow(msol,3)+DELTA*pow(msol,5)+EPSILON*pow(msol,7)+
     ZETA*pow(msol,8)+ETA*pow(msol,9.5));
  return(lsol*LSUN);
}

inline
Real
StarParticleData::rZAMS()
{
  Real msol = m/MSUN;
  Real rsol = (THETA*pow(msol,2.5)+IOTA*pow(msol,6.5)+KAPPA*pow(msol,11)+
	       LAMBDA*pow(msol,19)+MU_TOUT*pow(msol,19.5)) /
    (NU+XI*pow(msol,2)+UPSILON*pow(msol,8.5)+pow(msol,18.5)+
     PI_TOUT*pow(msol,19.5));
  return(rsol*RSUN);
}

#undef ALPHA
#undef BETA
#undef GAMMA
#undef DELTA
#undef EPSILON
#undef ZETA
#undef ETA
#undef THETA
#undef IOTA
#undef KAPPA
#undef LAMBDA
#undef MU
#undef NU
#undef XI
#undef UPSILON
#undef PI_TOUT

Real
StarParticleData::get_logTeff() {
  Real lstar = lStar();
  Real Teff = pow(lstar / (4. * M_PI * r*r * SIGMA), 0.25);
  Real logTeff;
  if (Teff > THAY)
    logTeff = std::log10(Teff);
  else
    logTeff = std::log10(THAY);
  return(logTeff);
}

//Routines specific to Raytracing. ALR
