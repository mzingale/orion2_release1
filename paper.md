---
title: 'ORION2: A magnetohydrodynamics code for star formation'
tags:
  - C++
  - Fortran
  - astronomy
  - star formation
  - molecular clouds
  - magnetohydrodynamics
  - adaptive mesh refinement
authors:
  - name: Pak Shing Li
    affiliation: 1
  - name: Andrew J. Cunningham
    affiliation: 2
  - name: Brandt L. Gaches
    affiliation: 3
  - name: Richard I. Klein
    affiliation: "1, 2"
  - name: Mark R. Krumholz
    affiliation: 4
  - name: Aaron T. Lee
    orcid: 0000-0002-8428-8050
    affiliation: 5
  - name: Christopher F. McKee
    affiliation: 1
  - name: Stella S. R. Offner
    orcid: 0000-0003-1252-9916
    affiliation: 6
  - name: Anna L. Rosen
    affiliation: 7
  - name: Aaron Skinner
    affiliation: 2
    
affiliations:
 - name: UC Berkeley
   index: 1
 - name: Lawrence Livermore National Laboratory
   index: 2
 - name: University of Cologne
   index: 3
 - name: Australia National University
   index: 4
 - name: Saint Mary's College
   index: 5
 - name: The University of Texas at Austin
   index: 6
 - name: Center for Astrophysics | Harvard & Smithsonian
   index: 7
   
date: "26 July 2021"
bibliography: paper.bib

# Optional fields if submitting to a AAS journal too, see this blog post:
# https://blog.joss.theoj.org/2018/12/a-new-collaboration-with-aas-publishing
aas-doi: 10.3847/xxxxx <- update this with the DOI from AAS once you know it.
aas-journal: Astrophysical Journal <- The name of the AAS journal.
# Enzo Example: https://joss.theoj.org/papers/10.21105/joss.01636
---

# Summary

The formation of stars and stellar clusters remains a grand challenge 
problem in astrophysics that has important implications for the evolution 
of the interstellar medium as well as shaping the evolution of galaxies. 
The computational challenges are formidable and involve a coupling of 
highly non-linear physical processes such as hydrodynamics, self-gravity, 
magnetic fields, radiation transfer, supersonic turbulence, ionization, 
protostellar outflows, stellar winds and chemistry that have both 
disparate timescales as well as operate over many decades of physical 
length scale.  These processes can regulate the feedback from nascent 
protostars onto the surrounding turbulent gas clouds that are the embryos 
of new star formation, and as a result, the feedback itself can influence 
the gaseous reservoir feeding newly formed protostars which in turn 
influence the star formation process.  

# Statement of need

To address the myriad of problems associated with star and cluster formation we have advanced
the development of ORION2 over the past several years. ORION2 is a 
radiation-magneto-hydrodynamic (MHD) 3-D code that operates in the block 
structured Adaptive Mesh Refinement (AMR) framework of CHOMBO for parallel 
computation.  The code is written in C++, C and Fortran.
We developed a new magneto-hydrodynamic (MHD) module using a Constrainted Transport scheme for 
adaptive mesh refinement [@Li:2012] based on the framework 
of the publicly released PLUTO code version 3.0 [@Mignone:2007; @Mignone:2012]. We also implemented a variety
of additional functionality needed for modeling our target problems, which
focus on the dynamics of the interstellar medium and star formation.
Our first code release includes MHD, self-gravity, sink and star particles, 
protostellar outflows and main sequence stellar winds.  The MHD code 
with AMR scales efficiently to many tens of 
thousands of cores. Future releases of ORION2 will include a hybrid ray 
trace moment radiative transfer method enabling the computation of 
radiative forces associated with massive star formation.  Data from ORION2 
simulations can be analyzed straightforwardly using yt and VISIT. Example 
Python analysis scripts are included in the release.

The ORION2 methodology in this release has been described in a variety of prior publications:

  * MHD: [@Li:2012] 
  * Gravity: [@Miniati:2007; @Martin:2008]
  * Sink particles: [@Krumholz:2004]
  * Star particles: [@Offner:2009a]
  * Protostellar Outflows: [@Cunningham:2011]
  * Stellar Winds: [@Offner:2015; @Rosen:2021]

# Research with ORION2

ORION2 has been used to explore a variety of problems in the field of star formation. Notable papers that utilize the release functionality have been written on:

 * The Jeans condition and resolving gravitational fragmentation: [@Truelove:1997] 
 * Bondi accretion under turbulent conditions with and without magnetic fields: [@Krumholz:2005; @Lee:2014]
 * Properties of stars and dense cores under driven and decaying turbulence conditions: [@Offner:2008; @Offner:2008b; @Offner:2009b]
 * Stellar kinematics and clustering of young star clusters: [@Offner:2009c; @Kirk:2014]
 * Impact of protostellar outflows on low-mass star formation: [@Hansen:2012]
 * Chemical mixing in star-forming clouds and metallicity homogeneity in open clusters: [@Feng:2014]
 * Momentum- and energy-driven feedback from stellar winds in star-forming environments: [@Offner:2018; @Rosen:2021]
 * Magnetized turbulence excited by stellar winds: [@Offner:2018]
 * Binary and multiple star formation in magnetized clouds: [@Lee:2019]
 * Magnetic properties of cloud clumps: [@Li:2015] 
 * Cluster formation in filamentary dark clouds: [@Li:2018] 
 * Infrared dark clouds formation simulation: [@Li:2019]


# Acknowledgements

We acknowledge ORION2 code contributions from Daniel Martin, Drummond Fielding and Andrew Myers as well as from the PLUTO 
codebase and development team. We also acknowledge contributors to ORION, the forerunner of ORION2, by Kelly Truelove, 
Charles Hansen and Robert Fisher. Support for ORION2 development and code release has been funded by the following sources: 
NSF though grant AST-1211729;  NASA through the Astrophysical Theory  Program (ATP) through grants  NNX09AK31G; NNX13AB84G; NNX17AK39G; 
80NSSC20K0530; NASA through the TCAN PROGRAM grant NNX14AB45G; NASA through Hubble Fellowship grant HF-51311.01 awarded by the Space Telescope Science Institute, which is operated 
by the Association of Universities for Research in Astronomy, Inc., for NASA, under contract NAS 5-26555; NSF Career grant 
AST-1748571; NASA through Einstein Postdoctoral Fellowship grant No. PF7-180166 awarded by the Chandra X-ray Center, 
which is operated by the Smithsonian Astrophysical Observatory for NASA under contract NAS8-03060.

# References
