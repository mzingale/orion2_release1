#define    PHYSICS   MHD
#define    DIMENSIONS   3
#define    COMPONENTS   3
#define    GEOMETRY   CARTESIAN
#define    INCLUDE_BODY_FORCE   NO
#define    INCLUDE_COOLING   NO
#define    INCLUDE_PARTICLES   NO
#define    INTERPOLATION   LINEAR
#define    TIME_STEPPING   CHARACTERISTIC_TRACING
#define    DIMENSIONAL_SPLITTING   NO
#define    NTRACER   1
#define    USER_DEF_PARAMETERS   6
#define    TURBULENCE_DRIVING   NO
#define    PRINT_TO_STDOUT   YES
#define    RAREFACTION_FLATTEN   YES
#define    SINK_PARTICLES   YES
#define    STAR_PARTICLES   YES
#define    CLUSTER_PARTICLES   NO
#define    RAY_PARTICLES   NO
#define    RAD_PARTICLES   NO
#define    STAR_TYPE   CONTEMPORARY
#define    CIC   NO
#define    RADIATION   NO
#define    DUST_GAS_DECOUPLE   NO
#define    RAYTRACE_DIRECT_RAD   NO
#define    RAYTRACE_IONIZATION   NO
#define    DOEINTAVE   NO
#define    DOEINTREFLUX   NO
#define    RAYTRACE_FUV   NO
#define    IONZATION_CONS_RECOMB   NO
#define    VARIABLEGAMMA   NO
#define    DO_CHEMISTRY   NO
#define    CHEM_NSPECIES   0
#define    NFBINS   1
#define    SELF_GRAVITY   NO
#define    NEWTON_COOL   NO
#define    DIRECT_RAD_TEST   NO
#define    BARO_COOL   NO
#define    READHDF5ICS   NO
#define    EXTRACTICS   NO

/* -- physics dependent declarations -- */

#define    EOS   ISOTHERMAL
#define    ENTROPY_SWITCH   NO
#define    MHD_FORMULATION   FLUX_CT
#define    INCLUDE_BACKGROUND_FIELD   NO
#define    RESISTIVE_MHD   NO
#define    THERMAL_CONDUCTION   NO

/* -- pointers to user-def parameters -- */

#define  GAMMA   0
#define  ldrv   1
#define  vinit   2
#define  RHO   3
#define  TCLOUD   4
#define  BZ0   5

/* -- supplementary constants (user editable) -- */ 

#define  INITIAL_SMOOTHING     NO
#define  WARNING_MESSAGES      NO
#define  PRINT_TO_FILE         YES
#define  SHOCK_FLATTENING      MULTID
#define  ARTIFICIAL_VISCOSITY  NO
#define  CHAR_LIMITING         YES
#define  LIMITER               mc_lim
#define  CT_EMF_AVERAGE        UCT_HLL
#define  CT_EN_CORRECTION      YES
#define  CT_VEC_POT_INIT       NO
#define  COMPUTE_DIVB          NO
