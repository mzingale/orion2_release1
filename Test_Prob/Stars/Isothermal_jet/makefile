#######################################################
#                                                     #
#            ORION2   Makefile                        #
#                                                     #
#######################################################

orion2:                              # Default target

include make.vars
ORION2_DIR    = /home/brandt/codes/ORION2_LIVE/orion2
SRC          = $(ORION2_DIR)/Src

INCLUDE_DIRS  = -I. -I$(SRC) -I$(SRC)/Chombo  
INCLUDE_DIRS += -I$(CHOMBO_HOME)/src/AMRTimeDependent 
INCLUDE_DIRS += -I$(CHOMBO_HOME)/src/AMRTools 
INCLUDE_DIRS += -I$(CHOMBO_HOME)/src/BoxTools
INCLUDE_DIRS += -I$(CHOMBO_HOME)/src/BaseTools
INCLUDE_DIRS += -I$(CHOMBO_HOME)/src/MHDTools
INCLUDE_DIRS += -I$(CHOMBO_HOME)/src/AMRElliptic
#INCLUDE_DIRS += -I$(CHOMBO_HOME)/src/OldAMRElliptic

VPATH        = ./:$(SRC):$(SRC)/Time_Stepping:$(SRC)/Interpolants
VPATH       += $(SRC)/Chombo
VPATH       += $(SRC)/Radiation

ChFpp = perl -I $(CHOMBO_HOME)/util/chfpp $(CHOMBO_HOME)/util/chfpp/uber.pl

# CC:      is the name of your favourite C compiler
# CHOMBCFLAGS:  optimization flags for the C compiler

# default c compiler name
CC = mpicc
# ajc - pick gcc if g++ is used for chombo
#       pick icc if icpc is used for chombo
ifeq "$(findstring g++, $(CXX))" "g++"
  CC   = gcc
endif
ifeq "$(findstring icpc, $(CXX))" "icpc"
  CC   = icc
endif


# CTSS: for proper fortran file compilation (uncomment this for intel compilers)
# FC = ifort -extend_source -r8

# kraken uses just cc for the mpi c compiler
ifeq "$(findstring kraken, $(shell uname -n))" "kraken"
  CC   = cc
endif

# edison uses just cc for the mpi c compiler
ifeq "$(findstring edison, $(shell uname -n))" "edison"
  CC   = cc
endif

# so does pleiades
ifeq "$(findstring pfe, $(shell uname -n))" "pfe"
  CC   = icc
  FC   = ifort -extend_source -r8
endif

# UCSC hyades
ifeq "$(findstring hyades, $(shell uname -n))" "hyades"
  CC  = icc
  FC  = ifort -extend_source -r8
  CXX = mpiicpc
endif

# raijin
ifeq "$(findstring raijin, $(shell uname -n))" "raijin"
  CXX    = icpc 
  FC     = ifort -extend-source
  MPICXX = mpicxx
endif

# set the optimization options for orion2 c-code the same
# as the setting for the chombo library
unexport CHOMBCFLAGS
ifeq ($(OPT),TRUE)
  CHOMBCFLAGS += $(cxxoptflags)
  CHOMBFFLAGS += $(foptflags)
endif
ifeq ($(DEBUG),TRUE)
  CHOMBCFLAGS += $(cxxdbgflags)
  CHOMBFFLAGS += $(fdbgflags)
endif

CHOMBCFLAGS += -DCH_LANG_CC -DCH_SPACEDIM=$(DIM) -c
CHOMBFFLAGS += -DCH_LANG_FORT -DCH_SPACEDIM=$(DIM) -c

####################################################
#
# The following section has been built following
# Chombo/lib/mk/Make.example, 
# Chombo/lib/mk/Make.defs, and 
# Chombo/lib/mk/Make.rules
#
# On Franklin, MHDTools has to be in the front of
# all Chombo libraries.  On Bassi, at the end.
###################################################

LibNames := MHDTools AMRElliptic AMRTimeDependent AMRTools BoxTools BaseTools
#LibNames += OldAMRElliptic

_lib_names := $(shell echo $(LibNames) | tr 'A-Z' 'a-z')
_libflags := -L$(CHOMBO_HOME) $(patsubst %,-l%$(config),$(_lib_names)) \
$(subst FALSE,$(HDFLIBFLAGS),$(subst TRUE,$(HDFMPILIBFLAGS) $(mpilibflags),$(MPI))) \
$(flibflags) $(syslibflags) $(XTRALDFLAGS)

CPPFLAGS += -DCH_LANG_CC
LDFLAGS += $(_libflags)

# 
# the $(cxxc) variable (see Make.rules) contains
# the path to g++ (serial) or mpiCC (parallel) 
# and is the one to be used for compiling .cpp files.
#

CXX = $(cxxc)

print:
	@echo $(CPPFLAGS)
	@echo " --------------------- "
	@echo $(CXXFLAGS)
	@echo " --------------------- "
	@echo $(LDFLAGS)
	@echo " -------------------- "
	@echo $(INCLUDE_DIRS)


#
# --------------------------------------------------
#   Set headers and object files 
# --------------------------------------------------
#

HEADERS = orion2.h  definitions.h  mod_defs.h 
HEADERS += AMRLevelOrionFactory.H AMRLevelOrion.H memusage.H Timer.H
HEADERS += LevelOrion.H PatchOrion.H PatchGrid.H

OBJ = add_body_force.o adv_flux.o arrays.o body_force.o boundary.o \
      char_slopes.o check_states.o cmd_line_opt.o    \
      external_init.o findshock.o flag.o flatten.o get_nghost.o \
      init.o limiters.o \
      parse_file.o print_config.o \
      rhs.o  set_indexes.o set_limiter.o \
      setgeometry.o setgrid.o setup.o tools.o var_names.o

OBJ += AMRLevelOrion.o AMRLevelOrionFactory.o amrOrion.o LevelOrion.o memusage.o \
       PatchOrion.o PatchOrionUnsplit.o startupOrion.o TagCells.o Timer.o PatchGrid.o

-include local_make

OBJ += commons.o ParallelHelper.o amrorionfort.o
CHF_HEADERS += commons_F.H amrorionfort_F.H
HEADERS += commons_F.H ParallelHelper.H amrorionfort_F.H

#  Additional_object_files_here 

OBJ += slopes_plm.o
OBJ += char_tracing.o
include $(SRC)/MHD/makefile
include $(SRC)/MHD/FCT/makefile
include $(SRC)/Sink_Particles/makefile
include $(SRC)/Star_Particles/makefile
CPPFLAGS += -DGRAVITY
CPPFLAGS += -DNFBINS=1
CPPFLAGS += -DCHEMNSPECIES=0
CPPFLAGS += -I$(SRC)/
CPPFLAGS += -I$(SRC)/Sink_Particles
CPPFLAGS += -DSINKPARTICLE
CPPFLAGS += -DSINKID
CPPFLAGS += -I$(SRC)/
CPPFLAGS += -I$(SRC)/Star_Particles
CPPFLAGS += -DSTARPARTICLE

orion2: $(OBJ) 
	$(LD) $(OBJ) $(LDFLAGS) -o $@ 

#
#    suffix rule
#

# CTSS: suffixes needed for proper .F90 file compilation
.SUFFIXES: .f .F .F90

%_F.H: %.ChF
	$(ChFpp) -f $< -D $(DIM) -p /dev/null

%.o: %.ChF
	$(ChFpp) -f $< -D $(DIM) -c /dev/null
	$(FC) $(CHOMBFFLAGS) $(INCLUDE_DIRS) $(CPP_FLAGS) -DCH_LANG_FORT $*.F

.f.o:
	$(FC) $(CHOMBFFLAGS) $(INCLUDE_DIRS) $<

.F.o:
	$(FC) $(CHOMBFFLAGS) $(INCLUDE_DIRS) $<

.F90.o:
	$(FC) $(CHOMBFFLAGS) $(INCLUDE_DIRS) $<

.c.o:
	$(CC) $(CHOMBCFLAGS) $(INCLUDE_DIRS) $<

.cpp.o: 
	$(CXX) -c $(CXXFLAGS) $(CPPFLAGS) $(INCLUDE_DIRS) $<

clean:
	@rm -f	*.o $(CHF_HEADERS)
	@echo OBJECTS files removed.


#
# DEPENDENCIES FOR OBJECTS FILES
#


$(OBJ):  $(HEADERS)


