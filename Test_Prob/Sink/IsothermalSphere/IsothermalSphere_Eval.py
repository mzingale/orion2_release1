from yt.mods import *
import sys
import pylab as plt
import numpy as np
from tabular import *
from math import pi

G = 6.6726e-8
c_iso = 1.8e4
t0 = 1.29302897416e+12 # s
msol = 1.98892e33

# look for sink particle file dumps
pfiles=os.popen("ls data.*.sink").read().strip(' \n').split("\n")
mass = []
massana = []
time = []
for file in pfiles:
    f=open(file)
    f.readline()
    time.append(load(file[0:len(file)-5]+'.hdf5').current_time)
    mass.append(float(f.readline().split(' ')[0])/msol)
    massana.append(DELTA**2*alpha[0]*(DELTA+v[0])*c_iso**3*(time[-1]+t0)/G/msol)

mass = np.array(mass)
massana = np.array(massana)

L2Error = np.sum(np.sqrt(((mass-massana)/massana)**2))/len(mass)
LInfError = np.max(np.abs((mass-massana)/massana))

# AJC as of 9/20/2018, we have switced from bicongjugate gradient as the
# base solver to a relaxation solver for the gravity base level solve.
# The reason for th echange is the biconjugate solver does not work
# on problems with outflow boundaries
# While the errors on the test have increased, this is mostly due to an
# initial transient at early time.
#L2Error = 0.0735753352607
#LInfError = 0.179168685555

# as of 10/10/2011 (the first cut at the code for this problem), 
# the L2 and Linf errors are:
# L2Error = 0.0367239649748
# LInfError = 0.0828887270188

# on 3/8/2011, ATM raised the tolerances to reflect changes in the
# accretion model. The new errors are:
#L2Error = 0.0725675475759
#LInfError = 0.108279486431

print L2Error
print LInfError
if L2Error < 0.08 and LInfError < 0.18 and os.path.exists("data.0020.3d.sink"):
    print 'Pass!'
    writer=open('test_pass', 'w')
    writer.close()
else:
    print 'FAIL!'

