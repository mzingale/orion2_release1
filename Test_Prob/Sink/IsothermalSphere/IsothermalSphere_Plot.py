import yt
#from yt.mods import *
import sys
import pylab as plt
import numpy as np
from tabular import *
from math import pi
import glob
from yt.utilities.exceptions import \
    YTFieldNotFound
from matplotlib.font_manager import FontProperties

G = 6.6726e-8
c_iso = 1.8e4
t0 = 1.29302897416e+12 # s
msol = 1.98892e33

labels = ['sim', 'ana']
colors = ['Teal', 'DeepPink']
ls = ['-', '--']
lt = 2
legPos = 1
fontP = FontProperties()
fontP.set_size('small')

fpath = 'data.[0-9][0-9][0-9][0-9].3d.hdf5'
pfiles = glob.glob(fpath)
pfiles.sort()
# look for the last dump
fn = pfiles[-1]
#Load the dataset
ds = yt.load(fn)
cen = (ds.domain_left_edge + ds.domain_right_edge) / 2

start = [ds.domain_left_edge[0],cen[1],cen[2]]
end = [ds.domain_right_edge[0],cen[1],cen[2]]
ray = ds.r[start:end]
#ray = pf.h.ray([pf.domain_left_edge[0],cen[1],cen[2]], [pf.domain_right_edge[0],cen[1],cen[2]])

x0 = ray['x'].v
index = np.argsort(x0)
x = x0[index]
den = ray['density'].v[index]
vx = np.abs(ray['velocity_x']).v[index]
time = ds.current_time.v

# compute analytic solution from self-similar model of Shu (1977)
# tabulated shu solution (DELTA from tabular.py)
xana = c_iso*(time+t0)*np.linspace(DELTA, (len(alpha)+1)*DELTA, len(alpha), endpoint=True)
denana = np.array(alpha)/(4*pi*G*(time+t0)**2)
vxana = np.array(v)*c_iso
xana = np.concatenate((-xana[::-1],xana))
denana = np.concatenate((denana[::-1],denana))
vxana = np.concatenate((vxana[::-1],vxana))

kau = 1.49598e16 # cm
plt.clf()
plt.plot(x/kau,den, color = colors[0], ls = ls[0], label = labels[0], lw = lt)
plt.plot(xana/kau,denana, color = colors[1], ls = ls[1], label = labels[1], lw = lt)
plt.gca().set_xlabel(r'x [10$^3$ AU]')
plt.gca().set_ylabel(r'$\rho$ [g cm$^{-3}$]')
plt.gca().set_yscale('log')
plt.gca().set_xlim(-3.5,3.5)
handles, labels = plt.gca().get_legend_handles_labels()
lg = plt.gca().legend(handles, labels, loc=legPos, ncol=1, prop=fontP)
lg.draw_frame(False)
plt.savefig('IsothermalSphere_density.png')

plt.clf()
plt.plot(x/kau,vx,color = colors[0], ls = ls[0], label = labels[0], lw = lt)
plt.plot(xana/kau,vxana, color = colors[1], ls = ls[1], label = labels[1], lw = lt)
plt.gca().set_xlabel('x [1000 AU]')
plt.gca().set_ylabel(r'-v$_r$ [cm s$^{-1}$]')
plt.gca().set_yscale('log')
plt.gca().set_xlim(-3.5,3.5)
handles, labels = plt.gca().get_legend_handles_labels()
lg = plt.gca().legend(handles, labels, loc=legPos, ncol=1, prop=fontP)
lg.draw_frame(False)
plt.savefig('IsothermalSphere_v.png')

# look for sink particle file dumps
#pfiles=os.popen("ls data.*.sink").read().strip(' \n').split("\n")
mass = []
massana = []
time = []

ts = yt.DatasetSeries(pfiles)
for ds in ts.piter():
    time.append(ds.current_time.v)
    
    try:
        mstar = ds.r['particle_mass'].v/msol
    except YTFieldNotFound:
        mstar = 0
    mass.append(mstar)
    massana.append(DELTA**2*alpha[0]*(DELTA+v[0])*c_iso**3*(time[-1]+t0)/G/msol)

plt.clf()
plt.plot(time,mass, color = colors[0], ls = ls[0], label = labels[0], lw = lt)
plt.plot(time,massana, color = colors[1], ls = ls[1], label = labels[1], lw = lt)
plt.gca().set_xlabel('t [s]')
plt.gca().set_ylabel(r'M$_{\rm sink}$ [M$_\odot$]')
handles, labels = plt.gca().get_legend_handles_labels()
lg = plt.gca().legend(handles, labels, loc=legPos, ncol=1, prop=fontP)
lg.draw_frame(False)
plt.savefig('IsothermalSphere_msink.png')
