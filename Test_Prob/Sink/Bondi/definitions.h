#define    PHYSICS   MHD
#define    DIMENSIONS   3
#define    COMPONENTS   3
#define    GEOMETRY   CARTESIAN
#define    INCLUDE_BODY_FORCE   NO
#define    INCLUDE_COOLING   NO
#define    INCLUDE_PARTICLES   NO
#define    INTERPOLATION   LINEAR
#define    TIME_STEPPING   CHARACTERISTIC_TRACING
#define    DIMENSIONAL_SPLITTING   NO
#define    NTRACER   0
#define    USER_DEF_PARAMETERS   4
#define    TURBULENCE_DRIVING   NO
#define    PRINT_TO_STDOUT   NO
#define    RAREFACTION_FLATTEN   NO
#define    SINK_PARTICLES   YES
#define    STAR_PARTICLES   NO
#define    CLUSTER_PARTICLES   NO
#define    RAY_PARTICLES   NO
#define    RAD_PARTICLES   NO
#define    STAR_TYPE   CONTEMPORARY
#define    CIC   NO
#define    RADIATION   NO
#define    DUST_GAS_DECOUPLE   NO
#define    RAYTRACE_DIRECT_RAD   NO
#define    RAYTRACE_IONIZATION   NO
#define    DOEINTAVE   NO
#define    DOEINTREFLUX   NO
#define    RAYTRACE_FUV   NO
#define    IONZATION_CONS_RECOMB   NO
#define    VARIABLEGAMMA   NO
#define    DO_CHEMISTRY   NO
#define    CHEM_NSPECIES   0
#define    NFBINS   1
#define    SELF_GRAVITY   NO
#define    NEWTON_COOL   NO
#define    DIRECT_RAD_TEST   NO
#define    BARO_COOL   NO
#define    READHDF5ICS   NO
#define    EXTRACTICS   NO

/* -- physics dependent declarations -- */

#define    EOS   IDEAL
#define    ENTROPY_SWITCH   NO
#define    MHD_FORMULATION   FLUX_CT
#define    INCLUDE_BACKGROUND_FIELD   NO
#define    RESISTIVE_MHD   NO
#define    THERMAL_CONDUCTION   NO

/* -- pointers to user-def parameters -- */

#define  GAMMA   0
#define  den0   1
#define  cs0   2
#define  Jeans   3

/* -- supplementary constants (user editable) -- */ 

#define  INITIAL_SMOOTHING     YES
#define  WARNING_MESSAGES      YES
#define  PRINT_TO_FILE         YES
#define  SHOCK_FLATTENING      MULTID
#define  ARTIFICIAL_VISCOSITY  NO
#define  CHAR_LIMITING         YES
#define  LIMITER               vanleer_lim
#define  CT_EMF_AVERAGE        UCT_HLL
#define  CT_EN_CORRECTION      YES
#define  CT_VEC_POT_INIT       NO
#define  COMPUTE_DIVB       NO
