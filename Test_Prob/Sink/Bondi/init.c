#include "orion2.h"

/* Following the example of the Sink/IsothermalSphere test, include
   a header that defines the analytic solution we use to initialize
   the problem.
 */

#include "tabular.h"

void bondiSolution(real r, real *rho, real *u)
{
  /* Interpolate from the steady-state Bondi solution for the density
     and radial velocity.
   */

  real cs = aux[cs0]; // isothermal sound speed
  real m = 1.98892e34; // ten solar masses
  real r_bondi;
  real rho_inf = aux[den0];
  real v_interp, alpha_interp, x, xlo, xhi;
  int i;
  
  r_bondi = CONST_G * m / pow(cs, 2);
  
  x = r / r_bondi;
  i = (int)(x/DELTA-1.); // x is between i and i+1 in table
  if (i > N_TABLE-1) {
    printf("ERROR, init.c::analytic fell off the table, have a nice day! %d %f\n",i,x);
    fflush(stdout);
    i = N_TABLE-1;
  }
  if (i < 0 && x != 0.0) {
    printf("ERROR, init.c::analytic fell off the table, have a nice day! %d %f\n",i,x);
    fflush(stdout);
    i = 0;
  }
  // do some linear interpolation
  xlo = DELTA*(i);
  xhi = DELTA*(i+1.);
  v_interp = v[i] + (v[i+1] - v[i])*(x-xlo)/DELTA;
  alpha_interp = a[i] + (a[i+1] - a[i])*(x-xlo)/DELTA;
  // put in dimensional units
  *rho = alpha_interp * rho_inf;
  *u = v_interp * cs;
}

/* ************************************************************** */
void INIT (real *us, real x1, real x2, real x3,
           int i, int j, int k, int *prim_var)
/* 
 *
 * 
 * NAME 
 * 
 *   INIT
 *
 *
 * PURPOSE
 *
 *   Set inital condition. 
 *
 *
 * ARGUMENTS 
 *
 *   us (OUT)           a pointer to a array_1D of primitive 
 *                      (when *prim_var = 1) or conservative
 *                      (when *prim_var = 0) variables;
 *
 *   x1, x2, x3 (IN)    the three coordinates; the meaning is different
 *                      depending on the geometry:
 *
 *                       x1      x2      x3
 *                       -------------------                
 *                        x       y       z     in cartesian geometry
 *                        r       z       -     in cylindrical geometry 
 *                        r      phi      z     in polar geometry 
 *                        r     theta    phi    in spherical geometry
 *                  
 *   i , j , k  (IN)    the integer indexes of the cell containing the 
 *                      point x1, x2, x3.
 *                
 *   *prim_var (OUT)    an integer flag. When *prim_var = 1 initial 
 *                      conditions are assigned in terms of 
 *                      primitive values. When *prim_var = 0 initial
 *                      conditions are given in terms of conservative 
 *                      value.
 *
 *
 * Variable names are accessed as us[nv], where
 *
 *   nv = DN is density
 *   nv = PR is pressure
 *  
 *   Vector components are labelled always as VX,VY,VZ, but alternative
 *   labels may be used:
 *
 *   Cartesian       VX     VY      VZ
 *   Cylindrical    iVR    iVZ     iVPHI
 *   Polar          iVR    iVPHI   iVZ
 *   Spherical      iVR    iVTH    iVPHI
 *
 * 
 *
 **************************************************************** */
{

  real r, rho, u;
  *prim_var = 1;

  SMALL_DN = 1.e-32;
  SMALL_PR = 1.e-16;
  T_CUT_COOL = 1.0;

  r = D_EXPAND(x1*x1, + x2*x2, + x3*x3);
  r = sqrt(r);

  bondiSolution(r, &rho, &u);

  us[DN] = rho;

  #if EOS == IDEAL
  gmm = aux[GAMMA];
  us[PR] = rho * aux[cs0] * aux[cs0] / gmm;
  #else
  C_ISO = aux[cs0];
  #endif

  us[VX] = -u*x1/r;
  us[VY] = -u*x2/r;
  us[VZ] = -u*x3/r;

#if PHYSICS == MHD
   us[BX] = 0.0;
   us[BY] = 0.0;
   us[BZ] = 0.0; 
#endif
}
/* **************************************************************** */
void ANALYSIS (const Data *d, Grid *grid)
/* 
 *
 *
 * NAME
 *
 *   ANALYSIS
 *
 *
 * PURPOSE
 *  
 *   Perform some pre-processing data
 *
 * 
 * ARGUMENTS
 *
 *   uu(IN)     solution data.
 *
 *   GG(IN)     pointer to array of GRID structures  
 *
 **************************************************************** */
{

}
#if PHYSICS == MHD
/* ************************************************************** */
void BACKGROUND_FIELD (real x1, real x2, real x3, real *B0)
/* 
 *
 *
 * NAME
 * 
 *   BACKGROUND_FIELD
 *
 *
 * PURPOSE
 *
 *   Define the component of a static, curl-free background 
 *   magnetic field.
 *
 *
 * ARGUMENTS
 *
 *   x1, x2, x3  (IN)    coordinates
 *
 *   B0         (OUT)    array_1D component of the background field.
 *
 *
 **************************************************************** */
{
   B0[0] = 0.0;
   B0[1] = 0.0;
   B0[2] = 0.0;
}
#endif


/* ************************************************************** */
void USERDEF_BOUNDARY (const Data *d, int side, Grid *grid)
/* 
 *
 * 
 **************************************************************** */
{
  real r, rho, u, c;
  double x1, x2, x3;
  int i, j, k;

  if (side == X1_BEG) {
    X1_BEG_LOOP(k,j,i){
      x1 = grid[IDIR].x[i]; x2 = grid[JDIR].x[j]; x3 = grid[KDIR].x[k];
      r = D_EXPAND(x1*x1, + x2*x2, + x3*x3);
      r = sqrt(r);
      bondiSolution(r, &rho, &u);
      d->Vc[DN][k][j][i] = rho;
      d->Vc[VX][k][j][i] = -u*x1/r;
      d->Vc[VY][k][j][i] = -u*x2/r;
      d->Vc[VZ][k][j][i] = -u*x3/r;
      #if EOS == IDEAL
      d->Vc[PR][k][j][i] = rho * aux[cs0] * aux[cs0] / gmm;
      #endif
      d->Vc[BX][k][j][i] = 0.0;
      d->Vc[BY][k][j][i] = 0.0;
      d->Vc[BZ][k][j][i] = 0.0;
    }
  }

  if (side == X1_END) {
    X1_END_LOOP(k,j,i){
      x1 = grid[IDIR].x[i]; x2 = grid[JDIR].x[j]; x3 = grid[KDIR].x[k];
      r = D_EXPAND(x1*x1, + x2*x2, + x3*x3);
      r = sqrt(r);
      bondiSolution(r, &rho, &u);
      d->Vc[DN][k][j][i] = rho;
      d->Vc[VX][k][j][i] = -u*x1/r;
      d->Vc[VY][k][j][i] = -u*x2/r;
      d->Vc[VZ][k][j][i] = -u*x3/r;
      #if EOS == IDEAL
      d->Vc[PR][k][j][i] = rho * aux[cs0] * aux[cs0] / gmm;
      #endif 
      d->Vc[BX][k][j][i] = 0.0;
      d->Vc[BY][k][j][i] = 0.0;
      d->Vc[BZ][k][j][i] = 0.0;
    }
  }

  if (side == X2_BEG) {
    X2_BEG_LOOP(k,j,i){
      x1 = grid[IDIR].x[i]; x2 = grid[JDIR].x[j]; x3 = grid[KDIR].x[k];
      r = D_EXPAND(x1*x1, + x2*x2, + x3*x3);
      r = sqrt(r);
      bondiSolution(r, &rho, &u);
      d->Vc[DN][k][j][i] = rho;
      d->Vc[VX][k][j][i] = -u*x1/r;
      d->Vc[VY][k][j][i] = -u*x2/r;
      d->Vc[VZ][k][j][i] = -u*x3/r;
      #if EOS == IDEAL
      d->Vc[PR][k][j][i] = rho * aux[cs0] * aux[cs0] / gmm;
      #endif 
      d->Vc[BX][k][j][i] = 0.0;
      d->Vc[BY][k][j][i] = 0.0;
      d->Vc[BZ][k][j][i] = 0.0;
    }
  }

  if (side == X2_END) {
    X2_END_LOOP(k,j,i){
      x1 = grid[IDIR].x[i]; x2 = grid[JDIR].x[j]; x3 = grid[KDIR].x[k];
      r = D_EXPAND(x1*x1, + x2*x2, + x3*x3);
      r = sqrt(r);
      bondiSolution(r, &rho, &u);
      d->Vc[DN][k][j][i] = rho;
      d->Vc[VX][k][j][i] = -u*x1/r;
      d->Vc[VY][k][j][i] = -u*x2/r;
      d->Vc[VZ][k][j][i] = -u*x3/r;
      #if EOS == IDEAL
      d->Vc[PR][k][j][i] = rho * aux[cs0] * aux[cs0] / gmm;
      #endif 
      d->Vc[BX][k][j][i] = 0.0;
      d->Vc[BY][k][j][i] = 0.0;
      d->Vc[BZ][k][j][i] = 0.0;
    }
  }

  if (side == X3_BEG) {
    X3_BEG_LOOP(k,j,i){
      x1 = grid[IDIR].x[i]; x2 = grid[JDIR].x[j]; x3 = grid[KDIR].x[k];
      r = D_EXPAND(x1*x1, + x2*x2, + x3*x3);
      r = sqrt(r);
      bondiSolution(r, &rho, &u);
      d->Vc[DN][k][j][i] = rho;
      d->Vc[VX][k][j][i] = -u*x1/r;
      d->Vc[VY][k][j][i] = -u*x2/r;
      d->Vc[VZ][k][j][i] = -u*x3/r;
      #if EOS == IDEAL
      d->Vc[PR][k][j][i] = rho * aux[cs0] * aux[cs0] / gmm;
      #endif 
      d->Vc[BX][k][j][i] = 0.0;
      d->Vc[BY][k][j][i] = 0.0;
      d->Vc[BZ][k][j][i] = 0.0;
    }
  }

  if (side == X3_END) {
    X3_END_LOOP(k,j,i){
      x1 = grid[IDIR].x[i]; x2 = grid[JDIR].x[j]; x3 = grid[KDIR].x[k];
      r = D_EXPAND(x1*x1, + x2*x2, + x3*x3);
      r = sqrt(r);
      bondiSolution(r, &rho, &u);
      d->Vc[DN][k][j][i] = rho;
      d->Vc[VX][k][j][i] = -u*x1/r;
      d->Vc[VY][k][j][i] = -u*x2/r;
      d->Vc[VZ][k][j][i] = -u*x3/r;
      #if EOS == IDEAL
      d->Vc[PR][k][j][i] = rho * aux[cs0] * aux[cs0] / gmm;
      #endif
      d->Vc[BX][k][j][i] = 0.0;
      d->Vc[BY][k][j][i] = 0.0;
      d->Vc[BZ][k][j][i] = 0.0;
    }
  }
  
}



#if ADD_INTERNAL_BOUNDARY == YES
/* ************************************************************** */

void INTERNAL_BOUNDARY(real ***uu[], real ***uu_old[],
                       Grid *grid, 
                       int i0, int i1, int j0, int j1, int k0, int k1)

/* 
 *
 *
 * NAME
 *
 *   INTERNAL_BOUNDARY
 *
 *
 * PURPOSE
 *
 *   Allow the user to control 
 *
 *
 * ARGUMENTS
 *
 *   uu      (IN/OUT)    three-dimensional array_1D containing the solution data;
 *
 *   uu_old  (IN/OUT)    old, kept for backward compatibility;
 *
 *   grid    (IN)        pointer to grid structures;
 * 
 *   i0, j0, k0 (IN)     indexes of the lower-coordinate point inside
 *                       the domain; 
 *
 *   i1, j1, k1, (IN)    indexes of the upper-coordinate point inside
 *                       the domain.
 *   
 *  
 *
 *
 **************************************************************** */
{
  int  i, j, k;
  real x1, x2, x3;
  
  for (k = k0; k <= k1; k++) { x3 = grid[KDIR].x[k];  
  for (j = j0; j <= j1; j++) { x2 = grid[JDIR].x[j];  
  for (i = i0; i <= i1; i++) { x1 = grid[IDIR].x[i];  
         
  }}}
    
}

#endif
