#include "orion2.h"

/* ************************************************************** */
void INIT (real *us, real x1, real x2, real x3,
           int i, int j, int k, int *prim_var)
/* 
 *
 * 
 * NAME 
 * 
 *   INIT
 *
 *
 * PURPOSE
 *
 *   Set inital condition. 
 *
 *
 * ARGUMENTS 
 *
 *   us (OUT)           a pointer to a vector of primitive 
 *                      (when *prim_var = 1) or conservative
 *                      (when *prim_var = 0) variables;
 *
 *   x1, x2, x3 (IN)    the three coordinates; the meaning is different
 *                      depending on the geometry:
 *
 *                       x1      x2      x3
 *                       -------------------                
 *                        x       y       z     in cartesian geometry
 *                        r       z       -     in cylindrical geometry 
 *                        r      phi      z     in polar geometry 
 *                        r     theta    phi    in spherical geometry
 *                  
 *   i , j , k  (IN)    the integer indexes of the cell containing the 
 *                      point x1, x2, x3.
 *                
 *   *prim_var (OUT)    an integer flag. When *prim_var = 1 initial 
 *                      conditions are assigned in terms of 
 *                      primitive values. When *prim_var = 0 initial
 *                      conditions are given in terms of conservative 
 *                      value.
 *
 *
 * Variable names are accessed as us[nv], where
 *
 *   nv = DN is density
 *   nv = PR is pressure
 *  
 *   Vector components are labelled always as VX,VY,VZ, but alternative
 *   labels may be used:
 *
 *   Cartesian       VX     VY      VZ
 *   Cylindrical    iVR    iVZ     iVPHI
 *   Polar          iVR    iVPHI   iVZ
 *   Spherical      iVR    iVTH    iVPHI
 *
 * 
 *
 **************************************************************** */
{
  double c, s, v0=sqrt(5.0);
  double A=1.e-3, R=0.3,r;

  *prim_var = 1;

  r = sqrt(x1*x1 + x2*x2);
  c = 2.0/sqrt(6.0);
  s = 1.0/sqrt(6.0);
/*
  c = 1.0/sqrt(2.0);
  s = 1.0/sqrt(2.0);
*/
  us[DN] = 1.0;
  us[VX] = v0*c;
  us[VY] = v0*s;
  us[VZ] = 1.0;
  us[PR] = 1.0;
  us[TR] = 0.0;

  #if PHYSICS == MHD || PHYSICS == RMHD

  us[AX] = -A*x2/r*(r <= R);  /*  =   dAz/dy  */
  us[AY] =  A*x1/r*(r <= R);  /*  = - dAz/dx  */
  us[AZ] = 0.0;

  us[AX] = 0.0;
  us[AY] = 0.0;
  us[AZ] = A*(R - r)*(r <= R);

  #endif
}
/* **************************************************************** */
void ANALYSIS (const Data *d, Grid *grid)
/* 
 *
 * PURPOSE
 *  
 *   Perform some pre-processing data
 *
 * ARGUMENTS
 *
 *   d:      the ORION2 Data structure.
 *   grid:   pointer to array of GRID structures  
 *
 **************************************************************** */
{

}
#if PHYSICS == MHD
/* ************************************************************** */
void BACKGROUND_FIELD (real x1, real x2, real x3, real *B0)
/* 
 *
 * PURPOSE
 *
 *   Define the component of a static, curl-free background 
 *   magnetic field.
 *
 *
 * ARGUMENTS
 *
 *   x1, x2, x3  (IN)    coordinates
 *
 *   B0         (OUT)    vector component of the background field.
 *
 *
 **************************************************************** */
{
   B0[0] = 0.0;
   B0[1] = 0.0;
   B0[2] = 0.0;
}
#endif


/* ************************************************************** */
void USERDEF_BOUNDARY (const Data *d, int side, Grid *grid) 
/* 
 *
 * PURPOSE
 *
 *   Define user-defined boundary conditions.
 *
 *
 * ARGUMENTS
 * 
 *   uu (IN/OUT)       a three-dimensional vector uu[nv][k][j][i], 
 *                     containing the solution and boundary values 
 *                     to be assigned.
 *
 *   side (IN)         tells on which side boundary conditions need 
 *                     to be assigned. side can assume the following 
 *                     pre-definite values: X1_BEG, X1_END,
 *                                          X2_BEG, X2_END, 
 *                                          X3_BEG, X3_END
 *
 *                     When 
 *
 *                      side = Xn_BEG  -->  b.c. are assigned at the 
 *                                          beginning of the xn
 *                                          direction.
 *                      side = Xn_BEG  -->  b.c. are assigned at the 
 *                                          beginning of the xn
 *                                          direction.
 *
 **************************************************************** */
{
  int   i, j, k, nv;
  real  *x1, *x2, *x3;

  x1 = grid[IDIR].x;
  x2 = grid[JDIR].x;
  x3 = grid[KDIR].x;

  if (side == 0) {    /* -- check solution inside domain -- */
    DOM_LOOP(k,j,i){};
  }

  if (side == X1_BEG){  /* -- X1_BEG boundary -- */
    X1_BEG_LOOP(k,j,i){}
  }

  if (side == X1_END){  /* -- X1_END boundary -- */
    X1_END_LOOP(k,j,i){}
  }

  if (side == X2_BEG){  /* -- X2_BEG boundary -- */
    X2_BEG_LOOP(k,j,i){}
  }

  if (side == X2_END){  /* -- X2_END boundary -- */
    X2_END_LOOP(k,j,i){}
  }

  if (side == X3_BEG){  /* -- X3_BEG boundary -- */
    X3_BEG_LOOP(k,j,i){}
  }
 
  if (side == X3_END) {  /* -- X3_END boundary -- */
    X3_END_LOOP(k,j,i){}
  }
}
