import yt
import pylab as pl
import pickle
from BrioWu_Result_Eval import cast_ray, load_high_res_data

x, den, vel, pres, mag = load_high_res_data('BrioWu_High_Res.dat')

def add_subplot(field, plotnum):
    x, y = cast_ray('data.0001.3d.hdf5', field)
    pl.subplot(plotnum)
    pl.plot(x,y, 'bo')
    pl.gca().set_ylabel(field)

add_subplot('density', 221)
add_subplot('x-velocity', 222)
add_subplot('Pressure', 223)
add_subplot('Y-magnfield', 224)

pl.subplot(221)
pl.plot(x,den)
pl.subplot(222)
pl.plot(x,vel)
pl.subplot(223)
pl.plot(x,pres)
pl.subplot(224)
pl.plot(x,mag)

pl.savefig("BrioWu_Result")
