import yt
import pylab as pl
import pickle
import numpy as na

def load_high_res_data(fn):
    file = open(fn, 'rb')
    x = pickle.load(file, encoding='bytes')
    file.close()
    return x[b'x'], x[b'den'], x[b'vel'], x[b'pres'], x[b'mag']

def _xVelocity(field, data):
    """generate x-velocity from x-momentum and density
    
    """
    return data["chombo", "X-momentum"]/data["chombo", "density"]

def _Pressure(field,data):
    """M{(Gamma-1.0)*e, where e is thermal energy density
    NB: this will need to be modified for radiation
    """
    return (data.ds.gamma - 1.0)*data["ThermalEnergy"]

def _ThermalEnergy(field, data):
    return data["energy-density"] - 0.5 * (
        data["chombo", 'X-momentum']**2.0
        + data["chombo", "Y-momentum"]**2.0
        + data["chombo", "Z-momentum"]**2.0 )/ data["chombo","density"] 

def cast_ray(fn, field):
    ds = yt.load(fn)
    print(ds.h.field_list)
    ds.add_field(("chombo","x-velocity"),function=_xVelocity, take_log=False,
          units="cm/s")
    ds.add_field("ThermalEnergy", function=_ThermalEnergy,
          units="erg/cm**3",  force_override=True)
    ds.add_field("Pressure", function=_Pressure, units="dyne/cm**2", force_override=True)
    c = (ds.domain_left_edge + ds.domain_right_edge) / 2
    ray=ds.h.ray([ds.domain_left_edge[0],0,0], [ds.domain_right_edge[0],0,0])
    return ray['t'], ray[field] 

def rms(x):
    return na.sqrt(na.mean(x**2))

def error(field, data):
    xlow, ylow = cast_ray('data.0001.3d.hdf5', field)
    err = abs(ylow.v - data[1::10])
    return rms(err)

if __name__ == '__main__':
    x, den, vel, pres, mag = load_high_res_data('BrioWu_High_Res.dat')
    print(error('density', den), error('x-velocity', vel), error('Pressure', pres))
    if (error('density', den)    > 0.035 or
        error('x-velocity', vel) > 0.035 or
        error('Pressure', pres)   > 0.035):
        print("FAIL!")
    else:
        print("PASS!")
        writer = open('test_pass', 'w')
        writer.close()

