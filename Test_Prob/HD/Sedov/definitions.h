#define    PHYSICS   HD
#define    DIMENSIONS   3
#define    COMPONENTS   3
#define    GEOMETRY   CARTESIAN
#define    INCLUDE_BODY_FORCE   NO
#define    INCLUDE_COOLING   NO
#define    INCLUDE_PARTICLES   NO
#define    INTERPOLATION   LINEAR
#define    TIME_STEPPING   CHARACTERISTIC_TRACING
#define    DIMENSIONAL_SPLITTING   NO
#define    NTRACER   0
#define    USER_DEF_PARAMETERS   3
#define    TURBULENCE_DRIVING   NO
#define    PRINT_TO_STDOUT   NO
#define    RAREFACTION_FLATTEN   NO
#define    SINK_PARTICLES   NO
#define    STAR_PARTICLES   NO
#define    RAD_PARTICLES   NO
#define    RADIATION   NO
#define    SELF_GRAVITY   NO

/* -- physics dependent declarations -- */

#define    EOS   IDEAL
#define    THERMAL_CONDUCTION   NO

/* -- pointers to user-def parameters -- */

#define  ENRG0   0
#define  DNST0   1
#define  GAMMA   2

/* -- supplementary constants (user editable) -- */ 

#define  INITIAL_SMOOTHING     YES
#define  WARNING_MESSAGES      YES
#define  PRINT_TO_FILE         YES
#define  SHOCK_FLATTENING      MULTID
#define  ARTIFICIAL_VISCOSITY  NO
#define  CHAR_LIMITING         NO
#define  LIMITER               vanleer_lim
