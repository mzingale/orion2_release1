# This makefile doesn't build the Orion2 code; this is handled by
# setup.py. Instead, it just builds the documentation.

doc: htmldoc

htmldoc:
	cd Doc/ && sphinx-build -b html sphinx/ html/

pdfdoc:
	cd Doc/ && sphinx-build -b latex sphinx/ latex/
	cd Doc/latex/ && make
	mv Doc/latex/orion2.pdf ./orion2_documentation.pdf
