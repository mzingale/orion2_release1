.. highlight:: rest

.. _sec-compiling:

Configuring & Compilation
=========================

Configuring and Compiling Chombo
--------------------------------

The ORION2 code is built on the `Chombo library <https://commons.lbl.gov/display/chombo/Chombo+-+Software+for+Adaptive+Solutions+of+Partial+Differential+Equations>`_, which is a set of tools for implementing finite difference and finite volume methods for the solution of partial differential equations on block-structured adaptively refined rectangular grids.

First, navigate to the directory where the make files are stored::

    cd $ORION2_DIR/Lib/Chombo3.2/lib/mk

Now, look at the file “Make.defs.local.” Here is where you set the compilers, flags, libraries, and so forth that you want to use to compile Chombo. You probably only need to set the compilers, a few of the optimization flags, and to tell Chombo where to find the serial and parallel versions of HDF5 on your machine. You also need to set the dimensionality of the problem you want to run; right now, ORION2 only supports 3D simulations.

The ORION2/Chombo build system also checks in the path $ORION2_DIR/Lib/Chombo/lib/mk/Make.defs.<hostname>. If a matching file is found where hostname matches the output of “uname -a”, that file is used instead of Make.defs.local. We already have makefiles for some commonly used machines (e.g., Stampede2, Pleiades).  Scripts are present to build symlinks to match the hostname of all of the head nodes on most of the systems commonly used. ie)::

    cd $ORION2_DIR/Lib/Chombo3.2/lib/mk/local/
    sh symlinks.pleiades

will point to the proper makefiles on all of the NASA pleiades head nodes and::

    cd $ORION2_DIR/Lib/Chombo3.2/lib/mk/local/
    sh symlinks.stampede

will set up the build system for the TACC stampede system.

If you're running on a new system you'll need to create a new $ORION2_DIR/Lib/Chombo/lib/mk/local/Make.defs.<hostname>

Here is an example Make.defs with some of the important options set.::

    DIM           = 3
    DEBUG         = TRUE
    OPT           = FALSE
    PRECISION     = DOUBLE  
    CXX           = g++
    FC            = gfortran
    MPI           = TRUE                                                                                                 
    MPICXX        = mpicxx
    USE_HDF       = TRUE
    HDFINCFLAGS   = -I/home/yourusername/hdf5/include
    HDFLIBFLAGS   = -L/home/yourusername/hdf5/lib -lhdf5 -lz
    HDFMPIINCFLAGS= -I/home/yourusername/hdf5-parallel/include
    HDFMPILIBFLAGS= -L/home/yourusername/hdf5-parallel/lib -lhdf5 -lz
    CPP            = $(CXX) -E
    LD             = $(MPICXX)
    cxxoptflags   += -O3
    foptflags     += -O3 

There are some other Make.defs.local files for various machines in /ORION2/Lib/Chombo3.2/lib/mk/local that you may find useful in setting up your own. Note that you can also override some of these settings later at the command line without changing Make.defs.local, which is useful if you want to switch in and out of debug mode, for example.

If you are on a platform where the default version of HDF5 is version 1.8 or later, you will need to modify the HDFMPIINCFLAGS line as follows::

    HDFMPIINCFLAGS= -I/home/yourusername/hdf5-parallel/include -DH5_USE_16_API

The extra -DH5_USE_16_API flag specifies that the compilation should use the version 1.6 APIs in HDF5, which Chombo requires. The APIs in version 1.8 of HDF5 are not compatible with Chombo, but the default installation of version 1.8 of HDF5 provides backwards compatibility, which is enabled by #define'ing H5_USE_16_API at compile time.

When Make.defs.local is set, it's time to compile Chombo::

    cd $ORION2_DIR/Lib/Chombo3.2/lib/
    make -j4 lib

The “-j4” option allows you to run make on more than 1 core (here 4 cores), which can greatly speed up the compilation process. If everything goes according to plan, you should have the following libraries::
 
    libamrelliptic3d.Linux.mpicxx.gfortran.DEBUG.MPI.a
    libamrtimedependent3d.Linux.mpicxx.gfortran.DEBUG.MPI.a
    libamrtools3d.Linux.mpicxx.gfortran.DEBUG.MPI.a
    libbasetools3d.Linux.mpicxx.gfortran.DEBUG.MPI.a
    libboxtools3d.Linux.mpicxx.gfortran.DEBUG.MPI.a
    libmhdtools3d.Linux.mpicxx.gfortran.DEBUG.MPI.a
    liboldamrelliptic3d.Linux.mpicxx.gfortran.DEBUG.MPI.a
    libparticletools3d.Linux.mpicxx.gfortran.DEBUG.MPI.a

The precise names of these files will vary based on your Make.defs.local; this is an example using the configuration above. The first six of these are critical for running ORION2 (ORION2 does not use the Chombo 'particletools' or 'oldamrelliptic' libraries). If any of them is missing, that means the compilation of that toolbox has failed. If that happens, you should refer to the Chombo manual for more guidance, or send an email to our mailing list.


Compiling Hypre
----------------------------

The radiation module makes use of the Hypre library of sparse linear solvers. If you want to run the code with radiation (not included in the public release), you will need access to a Hypre installation as well. Fortunately, it's a pretty clean build. Usually, it's sufficient to do the following in the src directory::

    ./configure --prefix='/home/yourusername'
    make install


Compiling ORION2 Problems
----------------------------

At this point, all the setup is complete. Now let's get a problem running! Let's head over to :ref:`sec-quickstart` for a quick start example or :ref:`sec-running` for running a general (custom) problem.


Trouble-Shooting
----------------------------

This section lists some common compile errors and solutions that may occur when getting started for the first time.

   * Chombo fails to compile: Chombo does not compile with OpenMPI version 4 or above. This is because Chombo uses old MPI functions that were marked obsolete by the MPI committee in 1996, and formally removed from the MPI standard in 2012. Please see the `OpenMPI FAQ <https://www.open-mpi.org/faq/?category=mpi-removed#mpi.h-deleted-prototypes>`_ for more information. You have to either use OpenMPI version 3, or modify the Chombo source code as indicated `in the FAQ <https://www.open-mpi.org/faq/?category=mpi-removed#mpi-1-mpi-errhandler-create>`_.
   * On some machines the Fortran line length has a default value that is too small. This leads to code lines being truncated when ORION2 problems are compiled and a series of miscellaneous, cryptic messages occur such as::
       
       Error: Function 'growstate' at (1) has no IMPLICIT type
       /home1/00653/tg458122/ORION2_nov30/orion2/Src/Sink_Particles/SINKPARTICLE_3D.F:531:31:
       rhocell = growstate(idx(1)+i,idx(2)+j,idx(3)+k,DN)

     To fix, add `-ffixed-line-length-none` to the Make.defs.local foptflags and fdbgflags lines. Then run setup.py again. Chombo does not need to be recompiled.
   * On some machines the maximum instantiation depth for template classes is set to lower value than needed, e.g., 25. The compiler will generate an error indicating there is an issue with `-ftemplate-depth`. To fix, add `-ftemplate-depth=40` to list of cxxcppflags in your Make.defs.local.

