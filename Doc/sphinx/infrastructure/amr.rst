.. highlight:: rest

.. _ssec-amr:

Adaptive Mesh Refinement
=========================

Refinement tolerances may be set in the orion2.ini input file under the “ParmParse” stanza. ParmParse is a Chombo+Boxlib class that is somewhat more flexible than the PLUTO method of line-scraping the file. Any lines in the ParmParse stanza of the orion2.ini file are parsed with this class. To illustrate by example, the following refines level 0 to level 1 on density jumps larger than 25%, refines level 1 to level 2 on density jumps larger than 50%, refines level 2 to level 3 on density jumps larger than 60%, and refines any level with a jump in magnitude of B is greater than 25%. Any of the refinement operators may be turned off by setting them to -1.::
 
    [ParmParse]   

    refinement.DensityGradThreshold = 0.25 0.5 0.6
    refinement.BGradThreshold = 0.25
    refinement.VelocityGradThreshold = -1
    refinement.ShockDetectionThreshold = -1
    refinement.JeansNoThreshold = -1
    refinement.Nested = 0.5 0.2 -1

The nested refinement criterion is a little more complicated. The Nested parameter helps set up static mesh refinement (SMR) centered around a 3-dimensional array “Center”, which is defined in AMRLevelOrion.cpp.


