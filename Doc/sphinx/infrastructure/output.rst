.. highlight:: rest

.. _ssec-output:



Output Files
============

Data and Check-point Files
^^^^^^^^^^^^^^^^^^^^^^^^^^

ORION2 produces two types of output files that contain the physical quantitiAMR structure data after the completion of a coarse timestep (note: the frequency of the outputs can be set in orion2.ini).  Both file types are written using the Chombo HDF5 data storage format. Data files (data.xxxx.hdf5) contain the fundamental physical information used for data analysis, including state variables such as density, velocity, magnetic field etc. The checkpoint files (chk.xxxx.hdf) contain the larger set of data necessary for a code restart. The exact size of the files depends on the domain size and physics included in the calculation, but it is expected that the checkpoint files are at least several times larger than the data files. 

We describe the ORION2 data fields and the file structure in more detail below.


Sink Particle Files
^^^^^^^^^^^^^^^^^^^

When sink particles are turned on ascii files, data.XXXX.3d.sink, are produced. These can be opened and inspected in any text editor. The first line of each file contains two numbers: the total current number of sink particles (integer) and the number of sink particles created since the beginning of the run (integer). The latter number may be larger than the second if sinks were created and then merged. This number includes sink particles that do not live longer than a timestep since they form within a few cells of another particle and are merged immediately by construction. Each following row contains the following information about a sink particle, in order:
  * Mass (g)
  * x, y, z position (cm)
  * x, y, z momentum (g cm / s)
  * x, y, z angular momentum (g cm^2 / s)
  * Unique ID
    
The unique ID is an integer value that is the particle ID. These values increase monotonically by order of particle creation. However, note that if any particle merging occurs the integer values of the particle ID between successive rows may differ by more than 1.

Within the yt analysis package, the names of these quantities are listed when printing the field_list. They are prefaced by 'particle' just as::

	>>> import yt
	>>> ds = yt.load('data.0000.3d.sink')
	>>> ds.field_list
	[('all', 'particle_angmomen_x'), ('all', 'particle_angmomen_y'), ('all', 	'particle_angmomen_z'), ('all', 'particle_burnstate'), ('all', 'particle_id'), 	('all', 'particle_luminosity'), ('all', 'particle_mass'), ('all', 'particle_mdeut'), ('all', 'particle_mdot'), ('all', 'particle_mlast'), ('all', 'particle_momentum_x'), ('all', 'particle_momentum_y'), ('all', 'particle_momentum_z'), ('all', 'particle_n'), ('all', 'particle_position_x'), ('all', 'particle_position_y'), ('all', 'particle_position_z'), ('all', 'particle_r'), ('chombo', 'X-magnfield'), ('chombo', 'X-momentum'), ('chombo', 'Y-magnfield'), ('chombo', 'Y-momentum'), ('chombo', 'Z-magnfield'), ('chombo', 'Z-momentum'), ('chombo', 'density'), ('io', 'particle_angmomen_x'), ('io', 'particle_angmomen_y'), ('io', 'particle_angmomen_z'), ('io', 'particle_burnstate'), ('io', 'particle_id'), ('io', 'particle_luminosity'), ('io', 'particle_mass'), ('io', 'particle_mdeut'), ('io', 'particle_mdot'), ('io', 'particle_mlast'), ('io', 'particle_momentum_x'), ('io', 'particle_momentum_y'), ('io', 'particle_momentum_z'), ('io', 'particle_n'), ('io', 'particle_position_x'), ('io', 'particle_position_y'), ('io', 'particle_position_z'), ('io', 'particle_r')]

Note that some of the fields above are defined only for star particles, and will be filled with 0 or None for sink particles.


Stored Fields
=============
  
State Variables
^^^^^^^^^^^^^^^^

Each data file contains the hydrodynamic state vector and any other physical quantities. These are the 'chombo' variables listed when printing the field_list in yt:

* Gas density: density (:math:`\rm{g/cm}^3`)

* Gas Momentum: px (gcm/s) py (gcm/s) pz (gcm/s)

* Total gas energy density: E(:math:`\rm{gcm}^2/\rm{s}^2`)  The energy density, E, contains the sum of the kinetic (:math:`1/2 \rho v^2`) and thermal energy (:math:`kT/m`).

* Magnetic field: Bx(G) By(G) Bz(G)  Note that the magnetic field is normalized by a factor of :math:`\sqrt{4\pi}`, i.e., Bx actually stores :math:`Bx/\sqrt{4\pi}`.

* Tracer field: tracerX (:math:`\rm{g/cm}^3`) This is the mass density of some quantity that is being tracked in a given cell: tracerX <= density. See below.

When gravity is turned on the gravitational potential will be included in the list of stored fields.


Tracer Fields
^^^^^^^^^^^^^

The tracer field is a passively advected scalar that tracks some quantity specified by the user (see also the PLUTO user guide). It may, for example, track the gas launched in the outflow, track some initial distribution of gas such as a dense core pressure-confined by a lower density medium or track radiation. More than one tracer field can be set using NTRACER such that multiple quantities can be tracked at once. The tracer fields are initialized in init.c.

Examples of numerical studies using the tracer field are  `Feng & Krumholz (2014, Nature, 513,7519, 523) <https://ui.adsabs.harvard.edu/abs/2014Natur.513..523F/abstract>`_ (chemical mixing) and `Offner & Chaban (2017, ApJ, 847, 2, 104) <https://ui.adsabs.harvard.edu/abs/2017ApJ...847..104O/abstract>`_ (outflow entrainment).

Warning: In the case where the tracer field is small compared to the total density in a given cell it may become negative.


HDF5 File Structure
===================


The following information on the HDF5 file structure is synthesized from this `ChomboVis documentation <http://web.archive.org/web/20070329175448fw_/http://seesar.lbl.gov/anag/chombo/chomboVis/File_format.html#3.%20The%20ascii2hdf5%20tool>`_.

With this information, it is possible for someone with block-structured AMR data and a knowledge of the HDF5 API to write data files that Chombo applications, such as ORION2, will understand. Users may also introduce file items into the HDF5 files that are not mentioned here. 

Note that many data entries in the HDF5 file are optional. They are indicated below with the [OPTIONAL] tag.

Some data entries are repeated a number of times, for instance there are a number of repeated level definitions. For these cases, the repeated entry is wrapped in a [for n=0,num] scope, with the `n' being the increment character.

Chombo uses the NATIVE data type internally, which allows efficient conversions between binary data representations. Every NATIVE data type translates to a real data type for a particular computer architecture. Thus, where you see H5T_NATIVE_DOUBLE, you might get binary data of the form H5T_IEEE_F64LE on Intel 32 bit architectures.

Floating point numbers in Chombo HDF5 files may be single or double precision. Thus, everywhere H5T_NATIVE_DOUBLE appears below, you can substitute HDF5_NATIVE_FLOAT.

We also define three composite HDF5 data types. These depend on the dimensionality of the Chombo HDF5 file; here they are for 3D. ::

  DATATYPE ``intvect_id'' {
      H5T_NATIVE_INT "intvecti";
      H5T_NATIVE_INT "intvectj";
      H5T_NATIVE_INT "intvectk";
  }

  DATATYPE ``floatvect_id'' {
    H5T_NATIVE_DOUBLE "x";
    H5T_NATIVE_DOUBLE "y";
    H5T_NATIVE_DOUBLE "z";
  }

  DATATYPE ``box_id''{
    H5T_NATIVE_INT "lo_i";
    H5T_NATIVE_INT "lo_j";
    H5T_NATIVE_INT "lo_k";
    H5T_NATIVE_INT "hi_i";
    H5T_NATIVE_INT "hi_j";
    H5T_NATIVE_INT "hi_k";
  }
  
With these definitions in hand, we can proceed with the layout description.::

  GROUP "/" {
   ATTRIBUTE "time" [OPTIONAL]{
      DATATYPE H5T_NATIVE_DOUBLE
      DATASPACE SCALAR
   }
   ATTRIBUTE "iteration" [OPTIONAL] {
      DATATYPE H5T_NATIVE_INT
      DATASPACE SCALAR
   }
   ATTRIBUTE "data_centering" [OPTIONAL] {
      DATATYPE H5T_NATIVE_INT
      DATASPACE SCALAR
   } # 0=cell, 1=x-face, 2=y-face, 3=z-face, 4=x-edge, 5=y-edge, 6=z-edge, 7=node.
   ATTRIBUTE "origin" [OPTIONAL] { 
      DATATYPE floatvect_id
      DATASPACE SCALAR
   } # Spatial location of lower corner of [0,0,0] cell.
   ATTRIBUTE "anisotropic" [OPTIONAL] { 
      DATATYPE floatvect_id
      DATASPACE SCALAR
   } # Stretches dx along x, y and z axes.
   ATTRIBUTE "num_levels" {
      DATATYPE H5T_NATIVE_INT
      DATASPACE SCALAR
   }
   ATTRIBUTE "max_level" {
      DATATYPE H5T_NATIVE_INT
      DATASPACE SCALAR
   } # Anachronism: must equal num_levels-1
   ATTRIBUTE "num_components" {
      DATATYPE H5T_NATIVE_INT
      DATASPACE SCALAR
   }
   [for n=0,num_components  
   ATTRIBUTE "component_n" {
      DATATYPE {
         { STRSIZE ;
           STRPAD H5T_STR_NULLTERM;
           CSET H5T_CSET_ASCII;
           CTYPE H5T_C_S1;
         }
      }
      DATASPACE SCALAR
   }]
   GROUP "Chombo_global" {
      ATTRIBUTE "testReal" {
         DATATYPE H5T_NATIVE_DOUBLE
         DATASPACE SCALAR
      }
      ATTRIBUTE "SpaceDim" {
         DATATYPE H5T_NATIVE_INT
         DATASPACE SCALAR
      }
   }
   [for n=0,num_levels
   GROUP "level_n" {
      ATTRIBUTE "dt" [OPTIONAL] {
         DATATYPE H5T_NATIVE_DOUBLE
         DATASPACE SCALAR
      }
      ATTRIBUTE "dx" {
         DATATYPE H5T_NATIVE_DOUBLE
         DATASPACE SCALAR
      }
      ATTRIBUTE "ref_ratio" {
         DATATYPE H5T_NATIVE_INT
         DATASPACE SCALAR
      }
      ATTRIBUTE "prob_domain" {
         DATATYPE box_id
         DATASPACE SCALAR
      }
      DATASET "boxes" {
         DATATYPE box_id
         DATASPACE {SIMPLE 1D}
      } # see section 'Data Flattening'
      DATASET "data:datatype=0" {
         DATATYPE H5T_NATIVE_DOUBLE
         DATASPACE {SIMPLE 1D}
      } # see section 'Data Flattening'
      GROUP "data_attributes" {
         ATTRIBUTE "ghost" {
            DATATYPE intvect_id
            DATASPACE SCALAR
         }
         ATTRIBUTE "comps" {
            DATATYPE H5T_NATIVE_INT
            DATASPACE SCALAR
         }
         ATTRIBUTE "objectType" {
            DATATYPE {
               { STRSIZE ;
                 STRPAD H5T_STR_NULLTERM;
                 CSET H5T_CSET_ASCII;
                 CTYPE H5T_C_S1;
               }
            }
            DATASPACE SCALAR
         }
      }
   }]
   
   }


Regarding data flattening: Field data are stored, for each level, in a 1D HDF5 dataset. From fastest- to slowest-moving, the indices are i, j, k, box, and component. In other words, Fortran order, then box, then component.
