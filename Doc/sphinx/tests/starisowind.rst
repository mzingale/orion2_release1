~.. highlight:: rest

.. _ssec-tests-isowind:

Star Particles Isotropic Wind Subgrid Model Test
================================================

The test in this section applies to the module for isotropic stellar winds for star particles. This test is located in the ``Test_Prob/Stars/IsoWind`` directory. The available test is:

* :ref:`sssec-tests-stars-isowind`

.. _sssec-tests-stars-isowind:

Isotropic Wind Bubble
---------------------

Test description
^^^^^^^^^^^^^^^^

This test is described in Appendix A of `Offner & Arce (2015, ApJ, 811, 146) <https://ui.adsabs.harvard.edu/abs/2015ApJ...811..146O/abstract>`_. The initial setup consists of an isothermal T=10 K, motionless uniform density gas with number density :math:`n0 = 10^3 cm^{-3}` occupying the full domain with a 20 :math:`M_{\odot}` zero age main sequence (ZAMS) star at the center of the domain. At time zero the star gives off an isotropic wind  :math:`\dot{M}_{\mathrm{w}}` with wind velocity :math:`v_{w}` which we cap at 200 km/s and wind temperature of :math:`10^4` K assuming the wind is fully ionized. The interaction of the wind with the ambient medium drives an expanding bubble, in which for the slow wind case we model here, is momentum conserving.  This test checks for proper computation of the wind driven shell radius and compares it to the analytical solution.

How to run the test
^^^^^^^^^^^^^^^^^^^

The test setup may be found in the ``Test_Prob/Stars/IsoWind`` subdirectory. Compile the test as described in the :ref:`sec-quickstart` instructions. Specifically, ``cd`` into the test problem directory and do::

  python $ORION2_DIR/setup.py --with-orion2 --no-gui
  make -j8

This will build the ``orion2`` executable, which you can run as described under :ref:`sec-running`::

  mpirun -np X ./orion2

When the code finished running, it will have produced a series of output files, ``data.0000.3d.hdf5`` through ``data.0099.3d.hdf5``, containing the solution at times from 0 to 0.1 Myr.

What does success look like?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The numerical solution should closely match the analytic solution from Appendix A of `Offner & Arce (2015) <https://ui.adsabs.harvard.edu/abs/2015ApJ...811..146O/abstract>`_. Expect minor differences at larger distances from the bubble front at early times, but in general errors should be at the few percent level or less. The errors decrease as resolution and/or AMR increases. 

A script that uses `yt <https://yt-project.org/>` to produce diagnostic plots of the output is included in the directory as ``Isowind_Result_Plot.py``; if you have ``yt`` installed, you can run the script via::

  python Isowind_Result_Plot.py

and it will produce a file ``Isowind_Result.png`` containing diagnostic plots. This script can be ran in serial or parallel. A sample successful test, comparing the wind bubble radius radiation and gas energy densities as a function of dimensionless position to the analytically-predicted values (taken directly from Offner & Arce's equation 24) throughout the simulation, is shown below:

.. image:: results/IsoWind_Result.png
   :width: 800
