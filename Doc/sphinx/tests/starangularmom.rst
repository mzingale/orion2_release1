.. highlight:: rest
  
.. _ssec-starangularmom:

Star Angular Momentum Accretion Test
====================================

Test description
^^^^^^^^^^^^^^^^

This test problem is designed to check the accuracy of the accretion of angular momentum onto star particles due to the accretion of gas. See Fielding et al. 2015 for a description of the ORION2 angular momentum accretion treatment. Test results are also reported in Appendix A.

There are three scenarios that can be tested by modifying the Test_Prob/Stars/AngularMomentumAccretion setup: stationary, subsonic Galilean transformation, and supersonic Galilean transformation. The default settings test the supersonic Galilean transformation.

The test assumes the gas is isothermal. Self-gravity is not included (see definitions.h).

The setup contains one star particle, placed at the origin with mass 0.5 Msun and radius 2 Rsun. The star is initially moving across the grid with velocity (vx, vy, vz) = (1.5 km/s, 1 km/s, 0.5 km/s) to test Galilean invariance. The initial angular momentum is set to be 0 (see init.sink).

The grid length is [-32 au, 32 au] with a base grid of :math:`64^3`. 

The gas density is uniform with :math:`\rho = 10^{-11}` g/cm^3. The gas moves with a uniform velocity (vx, vy, vz) = (1.5 km/s, 1 km/s, 0.5 km/s) to test Galilean invariance -- identical to the star particle. In addition to the advection velocity, the gas is initialized to cylindrically rotate around the (off-axis) vector (1,1,1). The gas temperature is a uniform 10 K. The magnetic field is set to 0. The problem assumes outflow boundary conditions. 

The important parameters are (orion2.ini):
 
   * sink.angular_method = 1: turns on Fielding et al. 2015 angular momentum treatment.
   * den0: gas (uniform) density
   * vx0, vy0, vz0: gas velocity
   * cs0: isothermal sound speed
   * vlboost: constant scaling factor for gas angular momentum (set to 1.0)

Notes: The grid resolution can be varied to check the impact of resolution on the results by adjusting the X1-grid, X2-grid and X3-grid parameters in orion2.ini.

How to run the test
^^^^^^^^^^^^^^^^^^^

The test setup may be found in the ``Test_Prob/Stars/AngularMomentumAccretion`` subdirectory. Compile the test as described in the :ref:`sec-quickstart` instructions. Specifically, ``cd`` into the test problem directory and do::

  python $ORION2_DIR/setup.py --with-orion2 --no-gui
  make -j8

This will build the ``orion2`` executable, which you can run as described under :ref:`sec-running`::

  mpirun -np X ./orion2

When the code finished running, it will have produced a series of output files, ``data.0000.3d.hdf5`` through ``data.0004.3d.hdf5``, containing the solution at times from 0 to :math:`5\times 10^4` sec.

What does success look like?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 

The directory contains an evaluation script that can be run using Python. To run it do::

  python Eval.py
  

and it will produce output checking that the angular momentum is within 5% of the analytic  prediction:

| The measured specific angular momentum is      : 1.90892838956e+15
| The analytic specific angular momentum is      : 2e+15
| The angle between the measured and analytic is : 9.03549097569e-06
| The Ang Mom per unit mass is off by -4.55358052211 %
| Pass!

 
 and a file ``AngMom_Result.png`` that compares the actual angular momentum to the expected value. Given the setup parameters, the star angular momentum is expected to grow at a constant rate :math:`\Delta L(t) / \Delta M_* = {\rm vlboost} \times 2 \times 10^{15}`.

.. image:: results/AngMom_Result.png
   :width: 800
