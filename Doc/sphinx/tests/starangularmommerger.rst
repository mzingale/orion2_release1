.. highlight:: rest
  
.. _ssec-starangularmommerger:

Star Angular Momentum Merger Test
=================================

Test description
^^^^^^^^^^^^^^^^

This test problem is designed to verify that when Star Particles merge their mutual orbital angular momentum is added to the new star. This added angular momentum is capped at :math:`L_{\rm max} = \sqrt{GM^3R}`, which is the angular momentum of the star if it were rotating at the Keplerian velocity at its radius. This problem is designed not to run into the cap -- the cap can be tested by decreasing the initial radius of the stars in the init.sink file. See Fielding et al. (2015) for a description of the angular momentum method and Appendix A for more information about the test.

The two sink particles are initialized 5 grid cells away from each other so that they immediately merge on the first time step. The orbital angular momentum is oriented in a non-grid axis direction that was calculated by starting with the orbital angular momentum in the X-direction then rotating by 30 degrees in the X, then Y, then Z direction. On top of this the star particles are given a supersonic translational velocity to ensure the **Galilean invariance** of this method.

The test assumes the gas is isothermal. Self-gravity is on (see definitions.h).

The setup contains two star particles, placed at positions x1 = (0.3, 1.5, -1.8) au and x2 = (-4.5, 2.3, 1.6) au. They have masses 0.6 Msun and 0.04 Msun. The velocities are set such that the stars initially have a supersonic velocity. The initial angular momentum is set to 1.392e15 (see init.sink).

The grid length is [-32 au, 32 au] with a base grid of math:`64^3`. 

The gas density is uniform with :math:`\rho = 10^{-15}` g/cm^3. The gas is stationary. The gas temperature is a uniform 10 K. The magnetic field is set to 0. The problem assumes outflow boundary conditions. 

The important test parameters (orion2.ini):
 
   * sink.angular_method = 1: turns on Fielding et al. 2015 angular momentum treatment.
   * den0: gas (uniform) density
   * vx0, vy0, vz0: gas velocity
   * cs0: isothermal sound speed
   

Note: There is an identical test problem for Sink Particles that does not have the cap at all. 

How to run the test
^^^^^^^^^^^^^^^^^^^

The test setup may be found in the ``Test_Prob/Stars/AngularMomentumMerger`` subdirectory. Compile the test as described in the :ref:`sec-quickstart` instructions. Specifically, ``cd`` into the test problem directory and do::

  python $ORION2_DIR/setup.py --with-orion2 --no-gui
  make -j8

This will build the ``orion2`` executable, which you can run as described under :ref:`sec-running`::

  mpirun -np X ./orion2

When the code finished running, it will have produced an output file, ``data.0000.3d.hdf5`` with the first 1e5 sec.

What does success look like?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Given the setup parameters, the star angular momentum after merger is expected to be :math:`1.65 \times 10^{51}` g cm^2/s. 

The script reads the data file to check the total angular momentum of the merged particle. To run do::

  python Eval.py

It should produce the following output: 

| predicted        1.65e+51
| actual           1.6466707384868235e+51 

| Pass!
| The predicted ang. mom. was   : 1.65e+51
| and you got the ang.mom. to be: 1.6466707384868235e+51
| which is only off by          : -0.20177342504098927 %
| And the angle difference was  : 0.0
