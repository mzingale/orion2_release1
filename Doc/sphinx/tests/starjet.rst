~.. highlight:: rest

.. _ssec-tests-jet:

Star Particles Collimated Jet Subgrid Model Demonstration
=========================================================

The demonstration in this section shows the basics on how to setup the collimated protostellar jets. This test is located in the ``Test_Prob/Stars/Isothermal_jet`` directory. The available test is:

* :ref:`sssec-tests-stars-jet`

.. _sssec-tests-stars-jet:

Modified Shu Isothermal Collapse with Protostellar Jet
------------------------------------------------------

Demonstration description
^^^^^^^^^^^^^^^^^^^^^^^^^
The initial setup for this demonstration follows from :ref:`sssec-tests-stars-isowind`, instantiating an isothermal collapse solution from `Shu (1977, ApJ, 214, 488) <https://ui.adsabs.harvard.edu/abs/1977ApJ...214..488S/abstract>`_. The problem stars with a star particle in the center. The protostellar jet subgrid model is initiated with the following parameters::

  star.use_wind = 1
  star.vw_max = 1.0e7
  star.vw_fkep = 0.33
  star.wind_theta = 0.01

Here, ``star.use_wind`` turns on the protostellar wind subgrid model, ``star.vw_max`` sets a maximum outflow velocity, ``star.vw_fkep`` describes the ratio of the outflow velocity to the inflow Keplerian velocity and ``star.wind_theta`` denotes the opening angle. See `Cunningham et al. (2011, ApJ, 740, 140) <https://ui.adsabs.harvard.edu/abs/2011ApJ...740..107C/abstract>`_ for details.

How to run the test
^^^^^^^^^^^^^^^^^^^

The test setup may be found in the ``Test_Prob/Stars/Isothermal_jet`` subdirectory. Compile the test as described in the :ref:`sec-quickstart` instructions. Specifically, ``cd`` into the test problem directory and do::

  python $ORION2_DIR/setup.py --with-orion2 --no-gui
  make -j8

This will build the ``orion2`` executable, which you can run as described under :ref:`sec-running`::

  mpirun -np X ./orion2

When the code finished running, it will have produced a series of output files, ``data.0000.3d.hdf5``, ``data.0001.3d.hdf5``, ``data.0002.3d.hdf5``...

What does success look like?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The numerical solution should appear similarly to the density and kinetic energy slice plots shown below, created using ``yt`` and the ``data.0009.3d.hdf5`` output file.

.. image:: results/jet_density.png
   :width: 350

.. image:: results/jet_ke.png
   :width: 350
