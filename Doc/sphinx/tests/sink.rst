.. highlight:: sink
  
.. _ssec-tests-sink:

Sink Particle Tests
===================

The tests in this section test the performance of sink particles. They are located in the ``Test_Prop/Sink`` directory. The available tests are:

* :ref:`sssec-tests-sink-bondi`

.. _sssec-tests-sink-bondi:

Bondi accretion test
--------------------

Test description
^^^^^^^^^^^^^^^^

This test is described in section 3.2 of `Krumholz, McKee, & Klein (2004, ApJ, 611, 399) <https://ui.adsabs.harvard.edu/abs/2004ApJ...611..339K/abstract>`_. In this test, a sink particle is placed at the center of the computational domain. The domain size is chosen so that the Bondi radius of the sink particle is within the problem domain, and is marginally well-resolved. The gas around the sink particle is initialized to the analytic solution for Bondi flow, and the system is allowed to evolve for 1 Bondi time. The test checks that sink particle accretion works to maintain a proper Bondi accretion flow.

How to run the test
^^^^^^^^^^^^^^^^^^^

The test setup may be found in the ``Test_Prob/Sink/Bondi`` subdirectory. Compile the test as described in the :ref:`sec-quickstart` instructions. Specifically, ``cd`` into the test problem directory and do::

  python $ORION2_DIR/setup.py --with-orion2 --no-gui
  make -j8

This will build the ``orion2`` executable, which you can run as described under :ref:`sec-running`::

  mpirun -np X ./orion2

When the code finished running, it will have produced a series of output files, ``data.0000.3d.hdf5`` through ``data.0010.3d.hdf5``, containing the solution at times from 0 to 1 Bondi time.

What does success look like?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The numerical solution should have a density and velocity profile that closely matches the analytic solution for Bondi accretion at all radii larger than the sink radius. Expect minor differences in the velocity profile at small radii, since at small radii the spherical shells over which the solution is averaged are resolved very coarsely. Similarly, expect larger errors in the accreiton rate at early times, as the flow settles. Once the accretion rate reaches steady-state, it should match the analytic solution to a level of 10 - 20%.

A script that uses `yt <https://yt-project.org/>` to produce diagnostic plots of the output is included in the directory as ``Bondi_Result_Plot.py``; if you have ``yt`` installed, you can run the script via::

  python Bondi_Result_Plot.py

and it will produce a file ``Bondi_Result.png`` containing diagnostic plots. A sample successful test, comparing the analytic and numerical solutions, is shown below:

.. image:: results/Bondi_Result.png
   :width: 800

