.. highlight:: rest

.. _sec-running:

Running ORION2
================

Running ORION2 requires you to manually alter three important files. How to edit those files is described here.


Physics file: definitions.h
----------------------------
Most flags and options passed to the compiler are defined in defintions.h. These flags include declarations of the particular physics packages, whether sink particles are included, whether there are tracer fields, as well as the names of user-defined parameters (which are given values in orion2.ini). 

There are many options you can provide in this file, and the default option is generally OK. The most relevant choices you need to make are the following (possible options are given in brackets)::

   #define PHYSICS {HD,MHD}
   #define DIMENSIONS {3} -- questionable results may be generated if using something other than three dimensions
   #define NTRACER {number} -- number of tracer fields
   #define INCLUDE_PARTICLES {NO} -- tracers are different than sink particles, and are not currently supported
   #define USER_DEF_PARAMETERS {number < 25} -- number of user-defined parameters
   #define TURBULENCE_DRIVE {NO,YES,HDF5,INITIAL_ONLY} -- whether you're driving large-scale turbulence, see driving section
   #define SINK_PARTICLES {YES,NO} -- inclusion of accreting sink particles 
   #define SELF_GRAVITY {YES,NO} -- Solve Poisson equation for the gas potential

Below this are where you can define pointers to your user-defined parameters, followed by the number (not the value) of the paramter. Examples may include::

   #define GAMMA 0 -- 0th index entry is ''gamma''
   #define ALPHAB 1 -- 1th index entry is \alpha_B for recombinations, etc.

The defaults for most other parameters do not frequently require alteration.


Simulation parameters: orion2.ini
----------------------------------

Many of the values specific to your simulation setup are defined in orion2.ini. This file is accessed by ORION2 during the simulation and it is required that a copy of this file reside in the same directory as the executable.

The first section is [Grid], where the physical extent of the domain and the number of grid cells in each dimension on the base grid are defined. There are three lines that may look something like::

    X1-grid    1    -3.08567758e19    64   u    3.08567758e19
    X2-grid    1    -3.08567758e19    64   u    3.08567758e19
    X3-grid    1    -3.08567758e19    64   u    3.08567758e19

From the left, the 3rd number is the coordinate for the left-most cell in this dimension, the 4th number (64 in this example) is the number of cells on the base grid in this dimension, and the last (6th) number is the coordinate for the right-most cell in this dimension.

The next section is [Chombo refinement] where the AMR structure is defined. It may look something like this::

   Levels           2
   Ref_ratio        2 2 
   Regrid_interval  2 2 
   Tag_buffer_size  2
   Block_factor     32
   Max_grid_size    16
   Fill_ratio       0.9

Each of these entries require one or a string of numbers, depending on whether you have adaptive mesh refinement. 'Levels' is the number of AMR levels (i.e., beyond the base grid) that you will allow. Setting this value to 0 is uni-grid. The rest of this section is ignored in that case. Setting 'Levels' to 2 means you allow two levels of refinement on top of the base grid. 'Ref_ratio' is the factor in each dimension that the refine provides. This can be a single number, in which case it applies to every level, or it can be a set of numbers (length=Levels) separated by spaces, in which case designates the specific refinement for each level. Typical convention is to set this number to 2, meaning a factor of 2 refinement in each dimension at each level. 'Regrid_interval' designates how many timesteps on a given level pass before the grid is redrawn. It can again be a single number or a string of numbers. The 'Block_factor' designates how many cells on the AMR level are created when a zone is refined. A value of 32, for example, means that if a cell is tagged for refinement, 32 cells will be created around it at the next AMR level. The 'Max_grid_size' relates to the parallelization and represents the linear dimension of the maximum block of cells. The best setting depends somewhat on the number of processors and base grid size. Generally, values from 8-32 are efficient. Running with small values, i.e., 4 (4^3) would likely break up the domain too much; if the number is too large relative to the base grid, i.e., 64 with a 64^3 base grid, may result in the entire domain being distributed to one processor. The 'Fill_ratio' is related to how many additional cells are refined around a given tagged cell. Values of 0.7-0.9 are reasonable.

[Time] talks about how time is advanced in the simulation. It may appear like this::

   CFL              0.3
   CFL_max_var      1.25
   tstop            3.1e5
   first_dt         1.0e3

The 'CFL' value is the coefficient in front of the Courant-Friedrichs-Lewy condition.  0.5 is the maximum value dictated by stability; 0.35 or lower is typical and recommended for MHD problems. Lower numbers may be needed for strong B-field runs or runs with complicated physics. 'CFL_max_var' allows you to determine by what factor the simulation can experiment with increasing the size of the timestep. Larger values (greater than 1.0) will mean the simulation will try to increase the time step dramatically, setting it equal to 1.0 means the size of the timestep will never change. 'tstop' is the physical time that designates the end of the simulation. Simulations that are restarted from a file do not restart their time at zero. 'first_dt' gives your user-provided value for the first timestep when not restarting from a file.

The section [Boundary] gives the boundary type on the left and right sides of each dimension::

   X1-beg        outflow
   X1-end        outflow

The value of 'outflow' is an outflow boundary condition, 'periodic' is a periodic boundary condition.

The section [Chombo HDF5 output] provides information on how frequently you create checkpoint files (used to restart) and data files (used for analysis and data visualization)::

   Checkpoint_interval   7.75e4  0
   Plot_interval         7.75e4  0

The first number for both lines is the amount of elapsed time in the simulation that must pass between outputs. Replacing the value with a negative number will eliminate data IO of that file type. The second number allows you specify the number of base-grid time steps between outputs. The value is ignored if it is less than or equal to 0. 

The section [ParmParse] allows you to override default parameters that exist in particular physics modules. See the individual physics pages for more information.

Finally, [Parameters] is where you provide the values for the user-defined parameters you defined in definitions.h. For example, you may have::

    GAMMA         1.66667
    RHO           5.0e-21
    CS 		  18800.0 

Which will make pointers to these variable names point to the values given here. How they can be accessed is also demonstrated below. 

Initial conditions: init.c
-----------------------------------

The file init.c contains the function that is called to initialize the values of each of the state and tracer variables throughout the grid. It also can be used to define the values of global variables related to density and temperature floors, among other things. To ensure the latter are properly defined, the function in this file is also called during a restart, though it does not override any of the pre-existing state variables in that case.

The function is INIT::

   void INIT (real *us, real x1, real x2, real x3, int i, int j, int k, int *prim_var)

The important inputs are a pointer to the state 'us', the center value of the cell's coordinates 'x1', 'x2', 'x3', and the primitive variable pointer.

The state pointer (us) contains the values of the state. If you set::

   *prim_var = 1

then ORION2 file will expect you to provide values for the primitive variables: density (DN), velocity (VX,VY,VZ), magnetic field (if applicable, BX,BY,BZ), pressure (PR), and the radiation energy density (if applicable, ER). An example of how this might look is::

    us[DN] = aux[RHO]; // loads the value of density to be the user-provided value of RHO
    us[PR] = aux[RHO] * aux[CS] * aux[CS];
    us[VX] = 0; us[VY]=0; us[VZ]=0;
    #if PHYSICS==MHD
       us[BX] = 0; us[BZ]=0; us[BZ]=aux[B0];
    #endif
    ...etc

There are also global variables used throughout ORION2 that you can define here in init.c. These include::

   gmm // adiabatic index gamma
   SMALL_DN // floor for density in a cell
   SMALL_PR // floor for pressure in a cell
   FLOOR_TRAD // floor for radiation temperature
   FLOOR_TGAS // floor for gas temperature
   CEIL_VA // Alfven velocity ceiling in a cell


Many of the test problems have well-documented init.c files that are also illustrative.


Restarting from a checkpoint
-----------------------------

Restarting from a checkpoint file is done on the command line. Simply execute the code as before, but include the -restart option::

    mpirun -np X ./orion2 -restart 3

Omitting a restart number will start the code from the latest checkpoint file available. Use the format above. Users have noted that “-restart=3” results in an error, at least on Stampede.

NOTE on restarting: Chombo has problems restarting from the 0th (e.g., chk.0000.3d.hdf5) file. If you need to restart from this file, rename it as the 1st checkpoint file and restart from checkpoint 1.
