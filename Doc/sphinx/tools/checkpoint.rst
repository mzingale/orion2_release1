.. highlight:: rest

.. _ssec-checkpoint:

Checkpoint Manipulation
=========================

In simulations of star formation, it may be necessary to manipulate the checkpoint outputs. For example, you may wish to rerun part of a completed simulation but include a tracer field to trace outflow gas. Or, perhaps, you may realize that you have unnecessarily refined a large portion of the simulation domain and may wish to coarsen the base grid and allow adaptive mesh refinement to focus only on the sections of the domain undergoing collapse. Orion2 comes with a suite of Python tools that can manipulate your checkpoint files in these ways. 

A common example of where these tools are useful is when you have performed a high-resolution steady-state driven turbulence model without a tracer field, and you wish to restart the simulation with a tracer field and a coarser base resolution. In this case you would manipulate the final checkpoint to first add a tracer field and then coarsen the resulted file to have a smaller base resolution. However, since you plan on running the simulation with AMR, you do not want to lose any of the higher-resolution information, so you finally insert the original higher-resolution data as an AMR level into your file. With that on hand, you could restart the simulation allowing for refinement up to some level beyond the base grid and not worry about any loss of information.  


 * :ref:`ssec-addtracer`
 * :ref:`ssec-addrad`
 * :ref:`ssec-coarsen`
 * :ref:`ssec-addleveldata`
 * :ref:`ssec-refine`



.. _ssec-addtracer:

Adding a tracer field
-----------------------

The python tool ``add_tracer_checkpoint.py`` will copy a checkpoint file to a new file with a tracer field component inserted at the position you provide using the command line flag “index”. Additional documentation is included in the python file itself. By inspection of the ``AMRLevelOrion::readCheckpoint`` routine we see that the code expects the tracer field to live in the 8th state vector position for an MHD model (5th for HD runs). For example, if you had a checkpoint file named fine_notracer.hdf5 and wanted to create the same file with a tracer field in a file named fine_withtracer.hdf5, the syntax is::

   python ${ORION2_DIR}/Tools/Python/add_tracer_checkpoint.py --infile=fine_notracer.hdf5 --outfile=fine.hdf5 --index=8


.. _ssec-addrad:

Adding a diffuse radiation field
----------------------------------

The file ``add_radiation_energy_density_checkpoint.py`` allows you to add a new state field for the diffuse radiation component. This can be useful if you wish to restart a simulation and include the flux-limited diffuse radiation component in the simulation. It comes with a command line parser the takes the following:

* ``infile`` : Input file name (no radiation field)
* ``outfile`` : Output file name (with radiation field)
* ``index`` : Index in state vector to add radiation field (should come after all the normal (M)HD components and before tracer fields. 
* ``temperature`` : In calculating the energy density of the radiation field, what temperature is assumed? In Kelvin [TODO: make default value 10 K?]
* ``gamma_in`` : Assumed adiabatic index for the original value. Defaults to 1.0001. It is VERY important that it matches exactly--e.g., inputting 1.001 and 1.0001 can give answers that differ by an order of magnitude, since gamma-1 terms are involved. 

.. _ssec-coarsen:

Coarsening grid data
---------------------

The python file ``coarsen_checkpoint.py`` allows you to coarsen the data to a smaller base grid. This tool uses volume average coarsening for state vector components and tracer fields, and face area averaging for the staggered B field components. For example, if you had a checkpoint file named highres.hdf5 and wanted to coarsen it down by a factor of 2 in each dimension into a file named lowerres.hdf5, the syntax is::

   python ${ORION2_DIR}/Tools/Python/coarsen_checkpoint.py --infile=highres.hdf5 --outfile=lowerres.hdf5 --coarsenratio=2


.. _ssec-addleveldata:

Adding data as an AMR level
----------------------------

The python file ``add_level_checkpoint.py`` inserts base grid information of one file as an AMR level of another. If, for example, you coarsened a 512^3 grid file to a 256^3 grid file, you may wish to insert the 512^3 as level 1 data in the new file. This eliminates any loss of information as a result of the checkpoint manipulations. The volume fill fraction for level 1 starts as unity, but this will likely diminish rapidly, based on your refinement criteria. If you have a checkpoint called lowerres_nolevel.hdf5 and wish to insert data from highres.hdf5 as level 1 data, the syntax is::

   python ${ORION2_DIR}/Tools/Python/add_level_checkpoint.py --infile= highres.hdf5 --outfile= lowerres_nolevel.hdf5 --inlevel=0 --outlevel=1

Note that this overwrites the outfile. 



.. _ssec-refine:

Refining grid data during the simulation
-------------------------------------------

If you wish to start your simulation with additional levels of AMR, you do not need to manipulate your checkpoint files. Simply change the ``max_level`` value in the ``orion2.ini`` file to a larger number and adjust your refinement criteria appropriately. When the grid is redrawn at startup, it will refine up to your next max_level value. 




