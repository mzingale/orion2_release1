.. highlight:: rest

.. _sec-gettingstarted:

Getting Started
======================

Welcome to ORION2!

The public ORION2 repository can be found on bitbucket:

     https://bitbucket.org/orionmhdteam/orion2_release1/src/master/ 

This version currently contains the MHD, self-gravity and sink particles modules.

A closed, development version of ORION2, which includes radiative transfer, is
located on bitbucket:

     https://bitbucket.org/orionmhdteam/orion2/

Before you can access the private repo, you must be given an account by the ORION2
management team. Contact Aaron Skinner, or whoever is currently administrating
this development repo, for assistance.

Before compiling Orion2, you need to set the ORION2_DIR environment variable to
the base directory where ORION2 lives. An example of how this might look is::

    export ORION2_DIR=/global/home/users/yourusername/orion2/

with “yourusername” replaced by the appropriate directory name, assuming you have
pulled the repo into a directory called ORION2. If you plan on doing any extended work with ORION2, you'll probably find it convenient to add
this line to the end of your .bashrc (or the equivalent file in other shells)
so this will be automatically done each time you log on.

Depending on your needs, there are a few other environment variables that are
worth setting up now. They include the directory for your Chombo files (which
will be compiled in the next section) and a pointer to HYPRE if you are including
diffuse radiation::

    export CHOMBO_HOME=/global/home/users/yourusername/ORION2/Lib/Chombo3.2/lib/
    export HYPRE_DIR=/opt/apps/gcc5_2/cray_mpich_7_3/petsc/3.7/haswell/

Both of these lines will need to be edited for your particular architecture and
directory structure.

Once this is completed, move onto :ref:`sec-compiling` for assistance compiling
the Chombo library.
