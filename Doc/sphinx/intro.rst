.. highlight:: rest

.. _sec-intro:

Introduction
============

This is a guide for users and developers working with the ORION2 radiation-magnetohydrodynamics code. ORION2 is named in honor of the Orion star-forming region -- a region characterized by a complex environment that is forming both low- and high-mass stars. As one of the most well-studied and closest, massive star-forming regions, Orion serves as a paradigm of star formation. Modeling the Orion region properly requires the consideration of a variety of physics, from magnetic fields to radiative transfer, which motivated the development of ORION2.


Overview
--------

ORION2 is built into the chombo adaptive mesh refinement (AMR) framework. Different pieces of the code have been described in a variety of publications, but the primary references for the main modules are:

  * Magnetohydrodynamics (MHD): `Li et al. (2012, ApJ, 745, 139) <http://adsabs.harvard.edu/abs/2012ApJ...745..139L>`_ 
  * Gravity: `Miniati & Colella (2007), JCP, 227, 400) <https://ui.adsabs.harvard.edu/abs/2007JCoPh.227..400M/abstract>`_ and `Martin et al. (2008, JCP, 227, 1863) <https://ui.adsabs.harvard.edu/abs/2008JCoPh.227.1863M/abstract>`_
  * Flux-limited diffusion radiation: original implicit solution method by `Howell & Greenough (2003, JCP, 184, 53) <https://ui.adsabs.harvard.edu/abs/2003JCoPh.184...53H/abstract>`_. An extension including radiation pressure is presented in `Krumholz et al. (2007, ApJ, 667, 626) <http://adsabs.harvard.edu/abs/2007ApJ...667..626K>`_, and an improved solution method using pseudo-transient continuation is described in `Shestakov & Offner (2008, JCP, 227, 2154) <https://ui.adsabs.harvard.edu/abs/2008JCoPh.227.2154S/abstract>`_
  * Hybrid ray-moment method for radiation: `Rosen et al. (2017, JCP, 330, 924) <http://adsabs.harvard.edu/abs/2017JCoPh.330..924R>`_
  * Sink/star particles: the original sink particle method is presented in `Krumholz, Klein, & McKee (2004, ApJ, 611, 399) <http://adsabs.harvard.edu/abs/2004ApJ...611..399K>`_, and the star particle model, which evolves stellar properties and accounts for radiative feedback, is described in `Offner et al. (2009, ApJ, 703, 131) <http://adsabs.harvard.edu/abs/2009ApJ...703..131O>`_. Sinks have been further extended by:
     * An updated sink angular momentum treatment:  `Fielding et al. (2015, MNRAS,450, 3306) <https://arxiv.org/abs/1409.5148>`_
     * The protostellar outflow implementation: `Cunningham et al. (2011, ApJ, 740, 104) <http://adsabs.harvard.edu/abs/2011ApJ...740..107C>`_
     * Extension from HD to MHD: `Lee et al. (2014, ApJ, 783, 50) <http://adsabs.harvard.edu/abs/2014ApJ...783...50L>`_
     * Isotropic stellar winds for main sequence stars: `Offner & Arce (2015, ApJ, 811,1 46) <https://arxiv.org/abs/1508.07008>`_

Only the MHD, gravity, and sink particle modules are included in the first public release. The hybrid ray-moment method will become available in release 2. More details on each of the parts of the code and how to use them are provided in :ref:`sec-physics-modules`.


ORION History
-------------

ORION2 is a descendant of ORION, which was originally built on the BoxLib AMR library and created through a collaboration between R. Klein (LLNL, UC Berkeley), C. McKee (UC Berkeley) and a variety of student and postdoc collaborators. ORION originally consisted of hydrodynamics and self-gravity.  The origin of ORION derives from the first work introducing AMR based on BoxLib into the astrophysical community treating the interaction of strong hydrodynamic shocks with interstellar clouds `(Klein, McKee & Collela, 1994, ApJ, 213, 213) <https://ui.adsabs.harvard.edu/abs/1994ApJ...420..213K/abstract>`_. The first scientific results with ORION were published in 1997 `(Truelove et al. 1997, ApJ, 489, 179) <https://ui.adsabs.harvard.edu/abs/1997ApJ...489L.179T/abstract>`_. Over the next 10 years sink particles, radiative transfer, star particles and magnetic field capabilities were added. Many of these original components, either the original code or the methodology, were ported to ORION2 during a major code update completed in 2012 (`Li et al. (2012, ApJ, 745, 139) <http://adsabs.harvard.edu/abs/2012ApJ...745..139L>`_ ), in which ORION was revamped to adopt the Chombo AMR framework.

The upgrade of ORION2 in 2012 was based on the PLUTO code version 3.0 (`Mignone et al. 2007, ApJS, 170, 228 <http://adsabs.harvard.edu/abs/2007ApJS..170..228M>`_, `Mignone et al. 2012, ApJS, 198, 7 <http://adsabs.harvard.edu/abs/2012ApJS..198....7M>`_) to facilitate the quick migration of most of the physical modules from ORION to ORION2.
The public code `PLUTO <http://plutocode.ph.unito.it/>`_ is based on Chombo for parallel computation and communication over block-structured, AMR grids. The `chombo <https://commons.lbl.gov/display/chombo/Chombo+-+Software+for+Adaptive+Solutions+of+Partial+Differential+Equations)framework>`_ package is free software developed in the Applied Numerical Algorithms Group at LBNL and is originally a spin-off of the BoxLib library.  Therefore, the structures of PLUTO and ORION are somewhat similar. Instead of using the MHD modules for AMR computation from PLUTO or ORION, which were based on hyperbolic divergence cleaning algorithms, ORION2 uses an MHD algorithm based on the constrained transport scheme described in `Li et al. (2012, ApJ, 745, 139) <http://adsabs.harvard.edu/abs/2012ApJ...745..139L>`_, but it uses the Riemann solvers and interpolation schemes from the PLUTO code.  Functionalities, such as replacing the self-gravity solver in the ORION code by a highly scalable Poisson solver (developed by the Chombo group) and inserting the turbulence-driving module, were subsequently incorporated into ORION2, followed by the migration of other ORION code functionalities. The upgrade to Chombo conferred significant improvements in scalability and efficiency, enabling ORION2 to run multi-physics problems efficiently on thousands of cores. ORION2 currently uses Chombo version 3.2.
