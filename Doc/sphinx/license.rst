.. highlight:: rest

.. _sec-license:

License & Conduct Agreement
============================

Below is a summary of the rules governing the use of the ORION2 MHD code as well as being a positive active member in the ORION2 collaboration. Users are responsible for following all of the applicable rules described below.

Public Release & Open Source Users
----------------

* The code repository is hosted on bitbucket. Repoository administration is handled by Mark Krumholz, Stella Offner and Anna Rosen. Questions should be directed to the ORION2 users mailing list: orion-users@googlegroups.com.
* No code is bug free and codes derive signifciant benefit from alert users who identify and report issues. Identified bugs should be reported to the administrators by emailing orion-users@googlegroups.com or by opening an 'issue' describing the problem on the bitbucket repository page.
* Any publications using ORION2 should cite the appropriate method publications as well as the JOSS ORION2 code release paper.
* It is recommended that new publications using ORION2 be announced on the orion-users google group.


Developing Members
----------------  

Developing members represent a core group of ORION2 developers and active users. These members have access to the private code repository, including unlreased functionality. These members agree to the following:

* Changes to administration must be discussed among the voting members. Any changes must be publicly stated to the entire collaboration.
* New science projects using the private repository should be declared on the google developers list.
* New users should be announced on the developers mailing list.  
* Authorship should be offered according to the developers agreement.  
* Refrain from conduct detracting from your ability or that of your colleagues to work effectively.
* Respect the contributions of others whether personal or public.
* Accept appropriate responsibility for your behavior.
* Have an obligation to be familiar with this basic code of conduct.  Lack of awareness or misunderstanding of the code of conduct is not itself a defense to a charge of unethical conduct.
* Work toward maintaining the collaborative nature of the group. 


Acknowledgments in Publications
================================
All publications resulting from the use of the ORION2 code must acknowledge the appropriate methodology papers. The user should see the bibliography found in :ref:`sec-intro` to find relevant papers to cite in their work. All publications should cite the ORION2 JOSS code release paper.


