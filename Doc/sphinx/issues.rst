.. highlight:: rest

.. _sec-issues:
 
Known Issues
======================

No code is perfect, and here we outline some of the issues that are currently known and (hopefully) under investigation.

* Chombo has problems restarting from the 0th (e.g., chk.0000.3d.hdf5) file. If you need to restart from this file, rename it as the 1st checkpoint file and restart from checkpoint 1.
* When writing your own Fortran worker methods, take extreme caution in the calling sequence from C++. Bar none, the call from C++ to Fortran is the single weakest link in a C++/Fortran code : C++ does strong type checking, but that is all lost to the Fortran code. One could pass virtually anything and not have the compiler or linker complain – the code will break in an unexpected way at runtime instead. Using “implicit none” and error trapping code (i.e., checking data for expected sizes and ranges) helps eliminate many of these bugs.
* A number of non-fatal Warnings often appear during compilation that are harmless. E.g., on Lonestar5::
 warning: ISO C++ forbids converting a string constant to 'char*' [-Wwrite-strings]
     ErrorHandler("unable to allocate workspace in QuickSort"); 
* Chombo needs to be updated to avoid removed MPI constructs, as indicated here [https://www.open-mpi.org/faq/?category=mpi-removed#mpi-1-mpi-errhandler-create]. OpenMPI 4.0.x has a backward compatibility option (--enable-mpi1-compatibility) that avoids this problem temporarily, but future versions of OpenMPI will *not* allow this. ORION will not compile or run on future clusters that may use OpenMPI 5 unless this problem is fixed in the source code.
* With some HDF5 library versions, ORION crashes at the end of any given simulation with an HDF5 error that indicates the wrong API has been called, but it does not appear to affect any of the output files, e.g.::
 HDF5-DIAG: Error detected in HDF5 (1.10.4) MPI-process 0:
  #000: ../../../src/H5E.c line 1619 in H5Eget_auto2(): wrong API function, H5Eset_auto1 has been called
    major: Error API
    minor: Can't get value
* Several Python scripts still do not work with Python 3, such as ``Test_Prob/MHD/Blast/Blast_Result_Eval.py``, others?
* Please contact the developers if you encounter any other issues.
