.. highlight:: rest
  
.. _ssec-mhd:

Magneto-hydrodynamics
=======================

Since PLUTO does not have an AMR Constraint Transport (CT) scheme, an AMR Constraint Transprt scheme was developed based on the approach of the unigrid version of the Constraint Transport (flux-CT) scheme in PLUTO (version 3.0) for ORION2.  It will be useful for the users to read the PLUTO user manual (included in this repository) to understand the grid indexing for cell-centered and face-centered quantities of the MHD module.  Users may also want to read the manual of the Chombo library in order to have a better understanding of the data and grid structures in AMR computation using ORION2.

To make the PLUTO flux-CT scheme work with Chombo AMR library, a new MHDTools and some new classes in AMRTools are developed based on Dan Martin's Thesis work. The details of the implementation can be found in the MHD methodology paper Li et al. (2012) (https://ui.adsabs.harvard.edu/abs/2012ApJ...745..139L).  Here is a very brief summary:

1. The Gardiner & Stone (2005) CT scheme is used for the MHD module. The CT scheme is implemented upon a dimensionally unsplit corner transport upwind scheme (Colella 1990).  This CTU+CT scheme is explained in great detail in Stone et al. (2008).

2. The piecewise linear method (PLM) interpolation scheme is used to reconstruct the cell interface states from the cell-center values for a second order scheme. Setting "INTERPOLATION" in definitions.h to LINEAR will select this interpolation scheme.

3. The implementation preserves some of the flexibility of the PLUTO code on using different limiters for the preservation of monotonicity near a discontinuity during the reconstruction stage, from the very diffusive min-mod to least diffusive monotonized central difference limiter, and

4. Different Riemann solvers, such as Roe and Harten–Lax–vanLeer (HLL)-family solvers, to obtain fluxes at the cell interfaces based on the reconstructed cell interface states.  For MHD computation, the users are recommended to use the Harten-Lax-van Leer Discontinuities (HLLD) solver (Miyoshi & Kusano 2005) for robustness.  When the code has trouble dealing with very strong shocks, HLL solver will be automatically swapped in for the solution.

5. Users can choose different CT electromotive force (EMF) averaging schemes but the UCT-HLL scheme (Del Zanna et al. 2003) is found to be the best for AMR MHD computation.  Note that this scheme does require more Riemann solves.

6. At the course/fine boundaries, coarse-level solution values are interpolated to fill in ghost regions. The face-centered  magnetic field is defined using a two-step process. First, we interpolate the coarse-levelBfieldonto  the  new  faces  using the face-centered interpolation used to fill ghost faces at coarse–fine interfaces. Then, the newly interpolated values are projected using a variant ofthe face-centered projection described in Martin & Colella (2000) to ensure that they are discretely divergence free. This method requires an even number of ghostzones.

Several standard tests: MHD wave family, MHD Blast, Brio_Wu, Field_Loop, Sod shock, and the isothermal version of these tests are included. Some of these results are presented in Li et al. (2012).

To use this module, choose “MHD” in the “PHYSICS” when generating the makefile (see Compilation page on how to compile ORION2). Currently, only “FLUX_CT” in the “MHD_FORMULATION” setting will work in ORION2. There are different choices on how to average the EMF in CT scheme, including “ARITHMETIC”, “UCT0”, “UCT_CONTACT”, and “UCT_HLL”. UCT_HLL gives the best result in all my tests but it requires CFL number < 0.35. ARITHMETIC provides a simple and robust average scheme for driven turbulence simulation.

For high thermal Mach number computation, shock flattening will be very helpful for a stable compoutation.  Set "MULTID" for "SHOCK_FLATTENING" will activate this function.

Here are the recommended settings for a hghly supersonic turbulence run with ideal MHD computation::

  #define PHYSICS               MHD
  #define INTERPOLATION         LINEAR
  #define TIME_STEPPING         CHARACTERISTIC_TRACING
  #define DIMENSION_SPLITTING   NO
  #define MHD_FORMULATION       FLUX_CT
  #define TURBULENCE_DRIVING    HDF5
  #define EOS                   IDEAL
  #define RAREFACTION_FLATTEN   NO
  #define SHOCK_FLATTENING      MULTID
  #define CHAR_LIMITING         NO
  #define LIMITER               vanleer_lim
  #define CT_EMF_AVERAGE        UCT_HLL
  #define CT_EN_CORRECTION      YES

One can initialize MHD problem using vector potential by setting “CT_VEC_POT_INIT” to be “YES”. Also, set “CT_EN_CORRECTION” to “YES” when the problem has small plasma beta.

When there is a concern that large div B cells may appear in a run and causing crashes, user can set “COMPUTE_DIVB” to “YES”, which will turn on the computation of div B at different part of ORION2 and the div B information will be dumped out. This will slow down the code computation and therefore must set to “NO” for production run unless one wants to keep track of div B information.

In CT scheme, face-centered B field is used to evolve the B field of an MHD problem but only the cell centered B field, which is the average of face centered B field, will be dumped out in the data file.  The face-centered B field will be stored n the checkpoint file for restart.

UNITS: The B-field unit in the code is in B/sqrt(4 pi), as commonly done in other MHD codes. For example, if the B field is initialized as us[BZ] = 1.0e-5, the actual B field value is sqrt(4*pi)*10 micro Gauss.  The B field values written in the HDF5 file need to be multiplied by sqrt(4 pi) to convert back to Gauss unit for data analysis. Note that some Python scripts may have done this automatically when the HDF5 file is read.
