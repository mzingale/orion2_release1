.. highlight:: rest
  
.. _ssec-gravity: 

Gravity
=====================

The self-gravity Poisson solver implemented in ORION2, using the AMR multigrid ellitpic solver developed by Chombo group (Martin et al. 2008, JCP, 227, 1863), is based on the approach described in Miniati & Colella (2007) and basically followed the example AMRSelfGravity in previous Chombo release. Unfortunately, Chombo group had decided not to include this in their Chombo library release anymore for unknown reason.  There will be no upgrade of this module that we can reference in the future.  For more information about the Chombo AMR multigrid solver (AMRMultigrid), one can consult the Chombo user guide (https://commons.lbl.gov/download/attachments/73468344/chomboDesign.pdf?version=1&modificationDate=1554672305006&api=v2) and the doxygen document (http://davis.lbl.gov/Manuals/CHOMBO-RELEASE-3.2/classAMRMultiGrid.html)

There are a few settings that the users can adjust if they have problem using the default setting of the gravity solver.  These settings are currently hardwired near the beginning of AMRLevelOrion.cpp:

1. Bottom solver: Users can choose between the RelaxSolver and BiCGStabSolver.  BiCGStabSolver converges faster than RelaxSolver but not as robust as RelaxSolver.  If users have problem in convergence, they can switch to use RelaxSolver by setting ::

   #define USE_RELAX_SOLVER

   The default setting is ::

   #undef USE_RELAX_SOLVER

   which means using the BiCGStabSolver as the bottom solver.

2. Number of GSRB smoother passes: The number of GSRB smoother passes users make both on the down and up directions of multigrid, i.e. on a single MG level, the user does the following: Coarsen the finer residual to this level, make SOLVER_NUM_SMOOTH GSRB passes, coarsen to the next coarser MG level, recursively solve there. then interpolate back to this level, make SOLVER_NUM_SMOOTH GSRB passes, then interpolate on up the V-cycle.  The default setting is::

   #define SOLVER_NUM_SMOOTH 8

3. Multigrid cycle: This is a parameter that isn't used very much, the number of MG V-cycles you use each time you coarsen the problem.  Changing this can let you experiment with W-cycles, etc.  The default setting is MG V-cycle::

   #define SOLVER_NUM_MG     1

4. Tolerance for declaring a solver hang: If the difference of the last residual and the new residual is smaller than the SOLVER_HANG defined (last residual - new residial < SOLVER_HANG), then declare the solve "hung" and exit the solver.  The default setting is::

   #define SOLVER_HANG      1.e-14

5. Residual norm type: The solver tries to reduce the residual by some factor (the tolerance) until the residual norm is less than the tolerance times a convergence metric. The metric can be 0: maxnorm, 1: L1 norm, 2: L2 norm.  The current default setting is::
   
   #define SOLVER_NORM_TYPE (0)

   By default, the convergence metric is the initial residual based on the initial guess, but you can also set that yourself in situations where your initial residual is already quite small (for instance in cases where you have a really good initial guess).

6. Minimum and maximum iteration numbers: The current settings are at least 2 and maximum 50 iterations::

   #define SOLVER_MAX_ITER  (50)
   #define SOLVER_MIN_ITER  (2)


There had been major upgrade to the Chombo ellitpic solver by the Chombo group in the past and the Poisson solver now is working with the latest elliptic solver in Chombo3.2/lib/src/AMRElliptic in the Chombo 3.2 release.

To use this gravity solver, one simply needs to set::
  
  #define SELF_GRAVITY               YES

in definitions.h file.

This self-gravity solver computes the gravitational potential for the gas component only.  The gravitational acceleration to the gas component is computed in add_body_force.c.  Users can modify add_body_force.c or body_force.c to add different external forces to the system.  Also note that the gravity for sink particles is separately computed.  See Sink/Star particles module for information on how the gravity is computed for particles.  The self-gravity solver currently works with periodic and outflow boundary conditions.  The users need to know that for outflow boundaries, the gravitational potential is fixed as 0 at the physical boundaries.

A uniform gas cloud gravitationally collapse test is included for testing the Poisson solver. See Truelove et al. (1997) on the details of the test.
