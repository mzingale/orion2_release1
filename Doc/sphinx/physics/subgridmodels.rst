.. highlight:: rest

.. _ssec-subgridmodels:

Subgrid Feedback Modules for Star Particles
===========================================

ORION2 contains three subgrid feedback models for star particles used to model stellar radiation, collimated outflows, and isotropic stellar winds. These modules are coupled to star particles and add radiation energy, mass, kinetic and thermal energies, and momentum to the computational grid. 

Stellar Radiation
^^^^^^^^^^^^^^^^^
Each star particle is coupled to a protostellar evolution model, as described in Appendix B of `Offner, Klein, McKee, & Krumholz (2009, ApJ, 703, 131) <https://ui.adsabs.harvard.edu/abs/2009ApJ...703..131O/abstract>`_ that returns the protostellar radius and luminosity. The luminosity is equal to :math:`L_{\rm int}+L_{\rm acc}+L_{\rm disk}` where :math:`L_{\rm int}` is the luminosity leaving the stellar interior, :math:`L_{\rm acc}` is the luminosity radiated outward at the accretion shock, and :math:`L_{\rm disk}` is the luminosity released by material in traversing the inner disk. These latter quantities are given by 

.. math:: L_{\rm acc} = f_{\rm acc} f_{\rm k} \frac{GM_{\rm \star} \dot{M}}{r} \\
		  L_{\rm disk} = (1 - f_{\rm k}) \frac{GM_{\rm \star} \dot{M}}{r}

where :math:`f_{\rm k}=0.5` is the standard value for an :math:`\alpha` disk,  and :math:`f_{\rm acc} = 0.5` is the fraction of the accretion power that is radiated away.

The current public release does not include radiative transfer, so the luminosity is not used. In the private version, and in future public releases, the luminosity is used as a source term for radiative transfer.


Collimated Outflows
^^^^^^^^^^^^^^^^^^^
Each accreting star particle is able to launch collimated protostellar outflows following the subgrid module described in `Cunningham, Klein, Krumholz, & McKee (2011, ApJ, 740, 107) <https://ui.adsabs.harvard.edu/abs/2011ApJ...740..107C/abstract>`_. This subgrid prescription calculates the outflow mass-loss rate :math:`\dot{M}_{\rm o}=f_w \dot{M}_{\rm acc}` at a fraction (:math:`f_w`) of the accretion rate (:math:`\dot{M}_{\rm acc}`). The outflows are launched along the star's angular momentum axis at a fraction (:math:`f_k`), where  of the star's Keplarian velocity given by :math:`v_k=\sqrt{G M_{\rm \star}/R_{\rm star}}` where :math:`M_{\rm \star}` and :math:`R_{\rm \star}` are the star particle's mass and radius, respectively. Here the outflow velocity is defined as :math:`v_{\rm o}=f_k v_k`The outflows are injected in the nearest 8 cells to the star with the weighting kernal :math:`W_o(\theta_c, \mathbf{x}-\mathbf{x_i})` that is parameterized by the collimation angle, :math:`\theta_c`. The rates of outflow momentum, kinetic energy, and thermal energy added to cell `i` with volume :math:`V_i` is

.. math:: \dot{p} = \dot{M}_{\rm{o},i} v_{\rm o} \\
	 	  \dot{E}_{\rm KE} = \frac{1}{2}\dot{M}_{\rm{o},i} v_{\rm o}^2 \\
	 	  \dot{E}_{\rm Th} = \frac{\rho_{\rm o}k_{\rm B} T_{\rm o}}{\mu (\gamma -1)}.

Here :math:`\rho_{\rm{w}, i}=\dot{M}_{\rm{w}, i}/V_{i}`, :math:`\mu=2.12\times10^{-24} \, \rm{g}`, and :math:`\gamma` is the adiabatic index set by the user at compile time usually taken to be 5/3.

Isotropic Stellar Winds
^^^^^^^^^^^^^^^^^^^^^^^
Similar to the collimated outflow module the user can also select to inject radiatively driven isotropic stellar winds. In this module the mass-loss rate and wind speed follow the `Vink, de Koter, & Lamers (2001, A&A, 369, 574) <https://ui.adsabs.harvard.edu/abs/2001A%26A...369..574V/abstract>`_ prescriptions for stars with :math:`T_{\rm eff} > 12.5` kK as described in `Offner & Arce (2015, ApJ, 811, 146) <https://ui.adsabs.harvard.edu/abs/2015ApJ...811..146O/abstract>`_. This subgrid prescription calculates the wind mass-loss rate, :math:`\dot{M}_{\rm w}`, and wind speed from the stellar properties and injects the wind within a sphere of 8 cells centered on the star. Here, the mass injected into cell `i` is :math:`\dot{M}_{\rm{w},i} = \dot{M}_{\rm w}/N_{\rm cell}` where :math:`N_{\rm cell}` is the total number of cells within the selected region. The material is injected with a constant wind velocity that is proportional to it's escape speed multiplied by a factor of 1.3 (2.6) for stars below (above) the bi-stability jump as described in Vink, de Koter, & Lamers (2001, A&A, 369, 574) and temperature :math:`T_{\rm w}=10^4` K assuming that the wind is ionized. The rates of wind momentum, kinetic energy, and thermal energy added to cell `i` with volume :math:`V_i` is 

.. math:: \dot{p} = \dot{M}_{\rm{w},i} v_w \\
	 	  \dot{E}_{\rm KE} = \frac{1}{2}\dot{M}_{\rm{w},i} v_w^2 \\
	 	  \dot{E}_{\rm Th} = \frac{\rho_{\rm w}k_{\rm B} T_{\rm w}}{\mu (\gamma -1)}.

Here :math:`\rho_{\rm{w}, i}=\dot{M}_{\rm{w}, i}/V_{i}`, :math:`\mu=2.12\times10^{-24} \, \rm{g}`, and :math:`\gamma` is the adiabatic index set by the user at compile time usually taken to be 5/3.

