.. highlight:: rest

.. _ssec-driving:

Turbulence Driving
===================

Turbulence Driving is a routine used to drive turbulence, typically on large scales, during the simulation. This routine can be run prior to running a gravitatioanlly collapse simulation or throughout. 

The driving routines follow the prescription of `Dubinski et al. (1995,ApJ, 448, 226) <https://ui.adsabs.harvard.edu/abs/1995ApJ...448..226D/>`_. This method drive the gas in Fourier space by using a (user-provided) power spectrum over a range of wavenumbers. The user must also provide the ratio of compressive perturbations to divergence-free (solenoidal) perturbations.

For turbulence driving, you must first generate a perturbation file using the python script perturbation.py. See::
 
    python $ORION2_DIR/Tools/Python/perturbation.py --help 

for detailed documentation on this tool. The minimum and maximum wavenumber, powerlaw slope, and data cube size can be chosen on the command line. The code will save the desired zdrv.hdf5 to the current path. At the time of this writing, this code is only serial so it cannot produce very large driving cubes (unless your machine has an exorbitant amount of memory).

Turbulence Driving is switched on in the code during setup::

    TURBULENCE_DRIVING -> {NO, YES, HDF5, INITIAL_ONLY}

The options are the following:

  * ``NO``: No driving is done 
  * ``YES``: Looks for a legacy Zeus-style file “zdatXXYYZZ.dat” HDF4 formatted files. That I'm aware of we do not currently have a generator code that can produce data cubes in this forma, but PS has some legacy files in this format.
  * ``HDF5``: Looks for a zdrv.hdf5 file, which is generated using the above perturbation.py tool.
  * ``INITIAL``: Seeds your simulation with initial turbulence (using zdrv.hdf5?) but the turbulence is not pumped after the first timestep. 

The most common choices are ``NO`` and ``HDF5``. For the latter, Orion2 looks for pertx, perty and pertz fields in the zdrv.hdf5 file. The perturbation fields must be the same size and shape as the root-level domain for the ORION2 run for driven turbulence. If un-driven turbulence is used (ie just an initial spectrum of motions), the perturbation field may be an arbitrary size and the code will interpolated up or down as needed to initialize the t = 0 AMR hierarchy. 

Run-time turbulence driving parameters for ORION2 are read from the orion2.ini file to the Pluto-style “ user aux array” as::
 
    vinit         266183.  # initial mass weighted RMS (3d) velocity
    ldrv          -1       # inject time-dependent energy to maintain mass weighted RMS velocity = vinit

These parameters may be accessed in C/C++ routines as::

    aux[vinit], aux[ldrv]

vinit is the target mass-weighted RMS turbulent initial velocity in code units. If ldrv > = 0 then ldrv specifies turbulent luminosity (energy injection rate) in code units. If the RMS velocity is lower than vinit, the code injects turbulent energy at this rate, otherwise no energy is injected. If ldrv < 0. then the code will add just enough turbulent energy each time-step to drive the mass-weighted RMS turbulent initial velocity back up to vinit. If the turbulent velocity is already > vinit, on a given timestep then no energy will be injected.

Note: To reiterate, vinit is the 3D velocity. For typical Milky Way clouds, this is somewhere around 6.6*c_s, depending on the size of your simulation domain. This is not to be confused with the velocity used in calculating, say, the virial parameter, which is the total 1D dispersion. They are related by vinit =~ sqrt(3)*\sigma_{1D}.

Turbulence driving is typically done at low wavenumber, with the cascade eventually providing the motions on small scales. On high-mass core problems, however, you have many levels of refinement at the beginning of the simulation and would generally like to seed those levels with small-scale velocity perturbations. If you set the TURBULENCE_DRIVING parameter to INITIAL_ONLY, the code will read the perturbations in from the zdrv.hdf5 file, and either average or interpolate them down to fit them on all the grids at all the levels. The aux[ldrv] parameter is ignored in this mode; no pumping is ever done after the simulation begins.


Example of generating turbulent initial conditions for star formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Suppose you have a 4 parsec-cubed box that you wish to initialize with turbulent initial conditions. Your setup may be similar to the following: 

  1. Using the scaling relations in the appendix of `McKee et al. (2010, ApJ, 720, 1612) <https://ui.adsabs.harvard.edu/abs/2010ApJ...720.1612M/>`_, you determine that your initial average gas density should be 5.09e-21 g/cm3 and that the large-scale Mach number should be approximately 6.6 for a cloud with a virial parameter of unity. You first set up init.c so that your gas is stationary and uniform at this density. 
  2. You plan on using a base grid of 512^3, so you generate a perturbation file on a 512^3 grid with a flat power-spectrum over the first and second wavenumbers (large-scale), using a compressive to solonoidal perturbation ratio of 2:1. 
  3. With the zdrv.hdf5 file generated, you run Orion2 for two gas crossing times with driving turned on and gravity, sink particles, AMR, and radiation turned off. At the end of the simulation, a turbulent power spectrum has been generated. This last output is your initial conditions.
  4. Without continued driving, you turn driving off and turn the relevant physics on before recompiling Orion2. You then commence the simulation for a free-fall time (or whatever). 
 

