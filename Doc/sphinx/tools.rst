.. highlight:: rest

.. _sec-tools:

Tools
=======

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tools/visualization
   tools/checkpoint



Here we describe the primary physics modules in the orion2 code, with explanations for the various compile-time and run-time (given in ``orion2.ini``) parameters that affect their behavior. The modules described in this page are:

* ``add_tracer_checkpoint.py`` : Adding a tracer field to the state vector.
* ``add_radiation_energy_density_checkpoint.py.py`` : Adding a diffuse radiation field as a component to the state vector.
* ``coarsen_checkpoint.py`` : Coarsening the checkpoint file so that the basegrid resolution is lower by some integer factor.
* ``add_level_checkpoint.py`` : Adding to a checkpoint file another checkpoint file's base grid as higher-level AMR data. 
