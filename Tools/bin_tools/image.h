#include <png.h>

typedef struct Colormap{
  unsigned char r[256], g[256], b[256];
} Colormap;

typedef struct Image{
  Colormap colormap;
  unsigned char **image;
  char name[32];
  int nx, ny;
  int get_max_scale; /* -- if NO, the maximum scale must given -- */
  int get_min_scale; /* -- if NO, the minimum scale must given -- */
} Image;


void set_color_map(Colormap *, char *);
void create_image (Image *, int, int, Colormap *, int);
void put_data (double **, double, double, Image *, Colormap *);
void write_png(unsigned char **, int, int, char *);
void fill(unsigned char **, int, int, int, int, Colormap *, int);
void swap_image(unsigned char **, int, int);
