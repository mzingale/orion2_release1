#include "data.h"
#include "SwapEndian.h"

typedef struct PLUTO_GRID{
  int     nx, ny, nz;
  double *xl, *xc, *xr;
  double *yl, *yc, *yr;
  double *zl, *zc, *zr;
  double *dx, *dy, *dz;
} PlGrid;

typedef struct SCALAR{
  char name[32];
  char filename[64];
  long int offset;
  int nx,ny,nz;
  int precision;
  int scalar;
} Scalar;


static void READ_PLUTO_GRID (PlGrid *Q);


/* *************************************************************** */
int main(int argc, char *argv[]) 
/* 
 *
 * PURPOSE
 *
 *  Produce a VTK data file out of binary files.
 *
 * USAGE
 *
 *    [bin2vtk] [opt]
 * 
 *
 * LAST MODIFIED: 
 *
 *   26 April 2007 by A. Mignone, e-mail: mignone@to.astro.it 
 *
 ***************************************************************** */
{
  int   i, j, k, n, nrec;
  int   beg_rec, end_rec;
  int   sap_endian;
  int   float_input, struct_grid, swap_endian;
  char  filename[64];
  int   nscl, nvec, ncomp, icomp;
  char   geometry[64];
  double xdbl, vec_dbl[3];
  float  x1, x2, x3;
  float  xflt, vec_flt[3];
  FILE  *fout, *fin;
  PlGrid grid;
  fpos_t *fpos;
  Scalar s[64];

  printf ("bin2vtk - version 0.1\n");
  printf ("Last modified Wed May 30 2007\n");
  printf ("Copyright(C) 2006,2007 by A. Mignone (mignone@to.astro.it)\n\n");
/*
  if (argc < 2) Print_Usage();

  if (scalars[0] == NULL) {
    for (k = 0; k < 64; k++){
      scalars[k] = (char *) malloc ((size_t) 128*sizeof (char));
      vectors[k] = (char *) malloc ((size_t) 128*sizeof (char));
    }
  }
*/
/* -- Defaults -- */

  float_input   = 0;
  beg_rec       = -1;
  end_rec       = -1;
  swap_endian  = IsLittleEndian();

  sprintf (geometry,"cartesian");
/*
  Parse_cmdln_Options(argc, argv, &nscl, scalars, &nvec, &ncomp, vectors, 
                      &beg_rec, &end_rec, geometry,  
                      &swap_endian, &float_input);
*/

nscl  = 8;
nvec  = 0;
ncomp = 0;
beg_rec = 10;
end_rec = 10;

  READ_PLUTO_GRID (&grid);

/* ----------------------------------------------------
    For cartesian geometry use "STRUCTURED_POINTS" 
    instead of "STRUCTURED_GRID" which yields larger
    file sizes.
   ---------------------------------------------------- */

  if (!strcmp(geometry,"cartesian")) struct_grid = 0;
  else struct_grid = 1;

  for (nrec = beg_rec; nrec <= end_rec; nrec++){

  /* ----------------------------------------------------------
              Open VTK file for writing 
     ---------------------------------------------------------- */

    sprintf (filename,"data.%04d.vtk",nrec);
    printf ("Writing VTK file %s...\n",filename);
    fout = fopen(filename,"w");

    /* -- Header section -- */
  
    fprintf(fout,"# vtk DataFile Version 2.0\n"); 
    fprintf(fout,"Structured grid\n");
    fprintf(fout,"BINARY\n");

    if (struct_grid){
      fprintf(fout,"DATASET STRUCTURED_GRID\n");
      fprintf(fout,"DIMENSIONS %d %d %d\n", grid.nx, grid.ny, grid.nz);
      fprintf(fout,"POINTS %d float\n", (grid.nx)*(grid.ny)*(grid.nz));
    }else{
      fprintf(fout,"DATASET STRUCTURED_POINTS\n");
      fprintf(fout,"DIMENSIONS %d %d %d\n", grid.nx+1, grid.ny+1, grid.nz+1);
      fprintf(fout,"ORIGIN %e %e %e \n", grid.xl[0], grid.yl[0], grid.zl[0]);
      fprintf(fout,"SPACING %e %e %e \n", grid.dx[0], grid.dy[0], grid.dz[0]);
      fprintf(fout,"CELL_DATA %d \n", (grid.nx)*(grid.ny)*(grid.nz));
    }
  
    /* -- grid section -- */

    if (struct_grid){
      for (k = 0; k < grid.nz; k++){
      for (j = 0; j < grid.ny; j++){
      for (i = 0; i < grid.nx; i++){
        x1 = grid.xc[i];
        x2 = grid.yc[j];
        x3 = grid.zc[k];

        if (!strcmp(geometry,"cartesian")){

          vec_flt[0] = x1;
          vec_flt[1] = x2;
          vec_flt[2] = x3;

        }else if (!strcmp(geometry,"polar")){
 
          vec_flt[0] = x1*cos(x2);
          vec_flt[1] = x1*sin(x2);
          vec_flt[2] = x3;

        }else if (!strcmp(geometry,"spherical")){

          vec_flt[0] = x1*sin(x2)*cos(x3);
          vec_flt[1] = x1*sin(x2)*cos(x3);
          vec_flt[2] = x1*cos(x2);

        }else{
 
          printf (" ! Geometry %s unknown\n",geometry);
          exit(1);
 
        }
      
        if (swap_endian){
          SWAP_FLOAT(vec_flt[0]);
          SWAP_FLOAT(vec_flt[1]);
          SWAP_FLOAT(vec_flt[2]);
        }

        fwrite (vec_flt, sizeof(float), 3, fout);
      }}}
      fprintf(fout,"POINT_DATA %d \n", (grid.nx)*(grid.ny)*(grid.nz));             
    }
/*    fprintf (fout,"\n");  */

  /* ----------------------------------------------------------
            Scalars: open files for reading 
     ---------------------------------------------------------- */




{
 FILE *fdbl;
 int add_label, single_file = -1;
 char *str, sline[512];
 char *label[64];

 fdbl = fopen("dbl.out","r");
 fgets (sline, 512, fdbl);
 str = strtok (sline," ");
 add_label = n = 0;

 while (str != NULL){
//   printf ("----> %s\n",str);
   str = strtok(NULL," ");
   
   if (add_label) {
     if (strlen (str) <= 1) break;
     sprintf (s[n].name, "%s", str);
     s[n].offset = n*grid.nx*grid.ny*grid.nz*sizeof(double);
     if (single_file == 1){
       sprintf (s[n].filename,"data.%04d.dbl",nrec);
     }
     n++;
   }

   if (strcmp(str,"single_file") == 0)    single_file = 1;
   if (strcmp(str,"multiple_files") == 0) single_file = 0;
    
   if (strcmp(str,"little") == 0) add_label = 1;
      
 }
 fclose(fdbl);
 nscl = n;
 
 for (n = 0; n < nscl; n++){
   printf ("%d  %s  %s\n",n,s[n].name, s[n].filename);
 }

}




    for (n = 0; n < nscl; n++){

      fprintf(fout,"SCALARS %d float\n", n);
      fprintf(fout,"LOOKUP_TABLE default\n");

//      sprintf (s[n].filename, "data.%04d.dbl", nrec);
//      s[n].offset = n*grid.nx*grid.ny*grid.nz*sizeof(double);

      fin = fopen(s[n].filename,"r");
      if (fin  == NULL){
        printf (" ! cannot open %s for reading\n",s[n].filename);
        exit(1);
      }

      fseek (fin, s[n].offset, SEEK_SET);

      for (k = 0; k < grid.nz; k++){
      for (j = 0; j < grid.ny; j++){
      for (i = 0; i < grid.nx; i++){

        fread  (&xdbl, sizeof(double), 1, fin);
        xflt = (float)xdbl;

        if (feof(fin)){
          printf (" ! end of file reached\n");
          exit(1);
        }

        if (swap_endian){
          SWAP_FLOAT(xflt);
        }
        fwrite (&xflt, sizeof(float), 1, fout);
      }}}
      fprintf (fout,"\n");
      fclose(fin);
    }

  }
}

/* ******************************************************************** */
void READ_PLUTO_GRID (PlGrid *grid)
/*
 *
 * 
 ***************************************************************** */
{
  int    nv, ii, jj, kk, ip;
  double dx, xl, xr, xc;
  FILE *gpnt, *fpnt;
    
/* --------------------------------------
        open grid file pointer
   -------------------------------------- */

  gpnt = fopen ("grid.out", "r");
  
  if (gpnt == NULL){
    printf (" ! grid.out cannot be found\n");
    exit(1);
  }

  fscanf (gpnt,"%d \n",&(grid->nx));
  grid->xr = vector(grid->nx);
  grid->xl = vector(grid->nx);
  grid->xc = vector(grid->nx);
  grid->dx = vector(grid->nx);
  for (ii = 0; ii < grid->nx; ii++){
    fscanf(gpnt,"%d  %lf %lf %lf %lf\n",
           &ip, &xl, &xc, &xr, &dx);
    grid->xl[ii] = xl;
    grid->xc[ii] = xc;
    grid->xr[ii] = xr;
    grid->dx[ii] = dx;
  }

  fscanf (gpnt,"%d \n",&(grid->ny));
  grid->yr = vector(grid->ny);
  grid->yl = vector(grid->ny);
  grid->yc = vector(grid->ny);
  grid->dy = vector(grid->ny);
  for (jj = 0; jj < grid->ny; jj++){
    fscanf(gpnt,"%d  %lf %lf %lf %lf\n",
           &ip, &xl, &xc, &xr, &dx);
    grid->yl[jj] = xl;
    grid->yc[jj] = xc;
    grid->yr[jj] = xr;
    grid->dy[jj] = dx;
  }

  fscanf (gpnt,"%d \n",&(grid->nz));
  grid->zr = vector(grid->nz);
  grid->zl = vector(grid->nz);
  grid->zc = vector(grid->nz);
  grid->dz = vector(grid->nz);
  for (kk = 0; kk < grid->nz; kk++){
    fscanf(gpnt,"%d  %lf %lf %lf %lf\n",
           &ip, &xl, &xc, &xr, &dx);
    grid->zl[kk] = xl;
    grid->zc[kk] = xc;
    grid->zr[kk] = xr;
    grid->dz[kk] = dx;
  }
        
  printf ("Grid: %d X %d X %d points\n",grid->nx,grid->ny,grid->nz);  
}

