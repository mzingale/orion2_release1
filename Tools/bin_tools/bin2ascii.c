#include "data.h"

void Print_Usage ();
void Parse_cmdln_Options (int argc, char *argv[], Data *, char *);

/* *************************************************************** */
int main(int argc, char *argv[]) 
/* 
 *
 * PURPOSE
 *
 *  Produce an ascii file from a binary data file.
 *  For 3D binary data only a 2D slice can be produced.
 *
 * USAGE
 *
 *    [bin2ascii] file_name [opt]
 * 
 *
 * TODO: set the swap_endian flag
 * 
 * LAST MODIFIED: 
 *
 *   3 Jan 2006 by A. Mignone, e-mail: mignone@to.astro.it 
 *
 ***************************************************************** */
{
  int    ii, jj, nrec;
  char   filename[64], basename[64];
  Data   Q;
  FILE   *fout;

  if (argc <= 2) {
    Print_Usage();
    exit(1);
  }  

  Q.file = argv[1];

/* ----------------------------
    set some default values
   ---------------------------- */

  Q.beg_rec  = -1;
  Q.end_rec  = -1;
  Q.nx       = -1;
  Q.ny       = -1;
  Q.nz       = -1;
  Q.is_float    = NO;
  Q.swap_endian = NO;
   
  sprintf (basename,"%s",Q.file);

  Parse_cmdln_Options (argc, argv, &Q, basename);
  Q.slice_coord--;  /* -- now switch to C array conventions --*/

/* ---------------------------------------------------------
                        Allocate memory 
   --------------------------------------------------------- */

  Q.data = matrix(Q.nrow, Q.ncol);

  for (nrec = Q.beg_rec; nrec <= Q.end_rec; nrec++){
    
    printf ("\n");
    sprintf (filename,"%s.%04d.ascii",basename,nrec);
    Read_Binary (&Q, nrec, Q.is_float, Q.swap_endian);

    printf (" > record # %d\n",nrec);
    printf (" > writing ascii file %s ...\n",filename);
    fflush(stdout);
    fout = fopen(filename,"w");
    for (jj = 0; jj < Q.nrow; jj++){
      for (ii = 0; ii < Q.ncol; ii++){
        fprintf (fout,"%12.6e\n",Q.data[jj][ii]);
      }
      fprintf (fout,"\n");
    }
    fclose(fout);
  }

  free_matrix (Q.data);
}

/* ****************************************************************************  */
void Print_Usage ()
/*
 *
 *
 *
 *
 *
 ****************************************************************************** */
{
  printf ("\nUsage: bin2ascii file -size <nx>x<ny>x<nz> [options]\n\n");
  printf (" file                       the name of a valid binary data file;\n");
  printf (" -size, -s <nx>x<ny>x<nz>   specifies the number of points of the binary \n");
  printf ("                            data. If the array is two dimensional, only \n");
  printf ("                            <nx>x<ny> may be given;\n\n");
  printf ("[options] are:\n\n");
  printf (" -float, -f              use this option if data has single precision.\n");
  printf ("                         Default is double precision;\n");
  printf (" -rec, -r <beg>-<end>    specifies the initial and final record numbers\n");
  printf ("                         in a binary file. Only one field may be given.\n");
  printf ("                         Positive record numbers are counted since the\n");
  printf ("                         beginning of file; negative number from the end.\n");
  printf ("                         Default is last record;\n");
  printf (" -swap-endian            swap endianity. This switch must be used when\n");
  printf ("                         the byte ordering is reversed.\n");
  printf (" -icut <int>             take a 2-D slice cut in the YZ plane at i = <int>,\n");
  printf ("                         where i is the coordinate orthogonal to the plane.\n");
  printf ("                         This option is useful when the array is three\n");
  printf ("                         dimensional;\n");
  printf (" -jcut <int>             take a 2-D slice cut in the XZ plane at j = <int>;\n");
  printf (" -kcut <int>             take a 2-D slice cut in the XY plane at k = <int>;\n");
  printf ("                         This is the default\n");
  printf (" -output, -o <name>      specify the base output file name. The ascii file\n");
  printf ("                         will be named name.rec.ascii, where rec is a 4-digit\n");
  printf ("                         (zero-padded) record number.\n");
}

/* ***************************************************************************** */
void Parse_cmdln_Options (int argc, char *argv[], Data *Q, char *fname)
/*
 *
 *
 *
 *
 *
 ****************************************************************************** */
{
  int ii;
  char *indx_i, *indx_j, *indx_k;

  for (ii = 2; ii < argc; ii++){

    if (!strcmp(argv[ii], "--help") ||
        !strcmp(argv[ii], "-help")){

      Print_Usage();
      exit(1);

    }else if (!strcmp(argv[ii], "-size") || !strcmp(argv[ii],"-s")){

      ii++;
      Q->nx = atoi(strtok(argv[ii],"x"));
      Q->ny = atoi(strtok(NULL,"x"));

      if ( (indx_i = strtok(NULL,"x")) == NULL){
        Q->nz = 1;
      }else{
        Q->nz = atoi(indx_i);
      }

      /* ----------------------------
          assign image size defaults 
         ---------------------------- */

      Q->ncol = Q->nx;
      Q->nrow = Q->ny;
      Q->slice_plane = XY_PLANE;
      Q->slice_coord = 1;

    }else if (!strcmp(argv[ii],"-float") || !strcmp(argv[ii],"-f")){

      Q->is_float = YES;

    }else if (!strcmp(argv[ii], "-rec") || !strcmp(argv[ii],"-r")){

      Q->beg_rec = atoi(strtok(argv[++ii],"-"));
      if ((indx_i = strtok(NULL,"-")) == NULL){
        Q->end_rec = Q->beg_rec;
      }else{
        Q->end_rec = atoi(indx_i);
      }

    }else if (!strcmp(argv[ii], "-swap-endian")){

       Q->swap_endian = YES;
       printf (" > Endianity is swapped\n");

    }else if (!strcmp(argv[ii], "-kcut")){

      Q->slice_plane = XY_PLANE;
      Q->slice_coord = atoi(argv[++ii]);
      Q->ncol = Q->nx;
      Q->nrow = Q->ny;
      if (Q->slice_coord <= 0 || Q->slice_coord > Q->nz){
        printf (" ! slice coordinate must be in [1, %d]\n", Q->nz);
        exit(1);
      }

    }else if (!strcmp(argv[ii], "-jcut")){

      Q->slice_plane = XZ_PLANE;
      Q->slice_coord = atoi(argv[++ii]);
      Q->ncol = Q->nx;
      Q->nrow = Q->nz;
      if (Q->slice_coord <= 0 || Q->slice_coord > Q->ny){
        printf (" ! slice coordinate must be in [1, %d]\n", Q->ny);
        exit(1);
      }
    }else if (!strcmp(argv[ii], "-icut")){

      Q->slice_plane = YZ_PLANE;
      Q->slice_coord = atoi(argv[++ii]);
      Q->ncol = Q->ny;
      Q->nrow = Q->nz;
      if (Q->slice_coord <= 0 || Q->slice_coord > Q->nx){
        printf (" ! slice coordinate must be in [1, %d]\n", Q->nx);
        exit(1);
      }
 
    }else if (!strcmp(argv[ii],"-output") || !strcmp(argv[ii],"-o")){
      
      sprintf (fname,"%s",argv[++ii]);

    }else{

      printf (" ! Unknown option %s\n",argv[ii]);
      exit(1);

    }
  }

/* ----------------------------
     check if size has been 
     specified 
   ---------------------------- */

  if (Q->nx == -1 || Q->ny == -1){
    printf ("you must specify -size <nx>x<ny>x<nz>\n");
    exit(1); 
  }

/* ----------------------------
     print some nice output
   ---------------------------- */

  printf (" > precision is ");
  if (Q->is_float){
    printf ("single\n");
  }else{
    printf ("double\n");
  }
  
  if (Q->slice_plane == XY_PLANE){
    if (Q->slice_coord > Q->nz) Q->slice_coord = Q->nz;
    printf (" > XY Slice, at k = %d\n", Q->slice_coord);
  }else if (Q->slice_plane == XZ_PLANE) {
    if (Q->slice_coord > Q->ny) Q->slice_coord = Q->ny;
    printf (" > XZ Slice, at j = %d\n",Q->slice_coord);
  }else if (Q->slice_plane == YZ_PLANE) {
    if (Q->slice_coord > Q->nx) Q->slice_coord = Q->nx;
    printf (" > YZ Slice, at i = %d\n",Q->slice_coord);
  }
}

/* ############################################################# */
void LINEAR_TRANSFORM (double **q, int nx, int ny)
/* 
 #
 #
 #
 #
 ############################################################### */
{
  return;
}

/* ############################################################# */
void LOG_TRANSFORM (double **q, int nx, int ny)
/* 
 #
 #
 #
 #
 ############################################################### */
{
  int ii,jj;

  for (jj = 0; jj < ny; jj++){
  for (ii = 0; ii < nx; ii++){
    q[jj][ii] = log10(q[jj][ii]);
  }}

}

