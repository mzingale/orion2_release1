#include<stdio.h>
#include<stdlib.h>
#include "image.h"

/* ############################################################ */
void write_png(unsigned char **image, int nx, int ny, char *filename_) 
/* 
 #
 #
 #
 #
 #
 #
 ############################################################## */
{
  FILE            *fp;
  png_structp     png_ptr;
  png_infop       info_ptr;
  int backgroundcolour_;
  int bit_depth_;
  int colortype_;
  int compressionlevel_;
  int  indx;
  double filegamma_;

  swap_image (image, nx, ny);

  compressionlevel_ = 6;
  backgroundcolour_ = 0;
  bit_depth_ = 16;
  filegamma_ = 0.;
  colortype_ = 2.; 

  fp = fopen(filename_, "wb");
  if(fp == NULL){
    printf(" ! error opening file in writing data\n");
    exit(1);
  }

  printf (" > Writing %s ... \n", filename_);
  
  png_ptr  = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  info_ptr = png_create_info_struct(png_ptr);
  png_init_io(png_ptr, fp);

   /*   if(compressionlevel_ != -2){ */
        png_set_compression_level(png_ptr, compressionlevel_);
   /* }
   else
     {
        png_set_compression_level(png_ptr, PNGWRITER_DEFAULT_COMPRESSION);
   }*/

  png_set_IHDR(png_ptr, info_ptr, nx, ny,
               bit_depth_, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
               PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

  if(filegamma_ < 1.0e-1){
    filegamma_ = 0.7;
  }

  png_set_gAMA(png_ptr, info_ptr, filegamma_);
  
   /*
   time_t          gmt;
   png_time        mod_time;
   png_text        text_ptr[5];
   time(&gmt);
   png_convert_from_time_t(&mod_time, gmt);
   png_set_tIME(png_ptr, info_ptr, &mod_time);
   */

   png_write_info(png_ptr, info_ptr);
   png_write_image(png_ptr, image); 
   png_write_end(png_ptr, info_ptr);
   png_destroy_write_struct(&png_ptr, &info_ptr);
   fclose(fp);
}

/* ************************************************************* */
void put_data (double **data, double max_data, double min_data,
               Image *image, Colormap *colormap)
/* 
 * 
 * 
 * 
 *************************************************************** */
{
  int  ii, jj, tempindex, indx;
  double scrh;

  for(ii = 0; ii < image->nx; ii++){
  for(jj = 0; jj < image->ny; jj++){

    scrh = (data[jj][ii] - min_data)/(max_data - min_data);     

    indx = (int)(255.0*scrh);
    if (indx > 255) indx = 255;
    if (indx < 0  ) indx = 0;
    
    tempindex = 6*ii;

    image->image[jj][tempindex]   = colormap->r[indx];      /* -- red -- */
    image->image[jj][tempindex+1] = 0;
    image->image[jj][tempindex+2] = colormap->g[indx];  /* -- green -- */
    image->image[jj][tempindex+3] = 0;
    image->image[jj][tempindex+4] = colormap->b[indx];     /* -- blue -- */
    image->image[jj][tempindex+5] = 0;
  }}
}

/* ***************************************************************** */
void fill(unsigned char **img, int i0, int j0, int i1, int j1, 
          Colormap *colormap, int indx)
/* 
 *
 *
 *
 *
 *
 ******************************************************************* */
{
  int ii, jj, tmp;

  for(ii = i0; ii <= i1; ii++){
  for(jj = j0; jj <= j1; jj++){

    tmp = 6*ii;

    img[jj][tmp]     = colormap->r[indx];      /* -- red -- */
    img[jj][tmp + 1] = 0;
    img[jj][tmp + 2] = colormap->g[indx];     /* -- green -- */
    img[jj][tmp + 3] = 0;
    img[jj][tmp + 4] = colormap->b[indx];     /* -- blue -- */
    img[jj][tmp + 5] = 0;
  }}
}

/* ************************************************************* */
void swap_image(unsigned char **image, int nx, int ny)
/* 
 *
 *
 *
 *
 *
 ******************************************************************* */
{
  int ii,jj;
  unsigned char scrh;

  for (jj = 0; jj < ny/2; jj++){
  for (ii = 0; ii < 6*nx; ii++){
    scrh = image[jj][ii];
    image[jj][ii] = image[ny - 1 - jj][ii];
    image[ny - 1 - jj][ii] = scrh;
  }}
}


