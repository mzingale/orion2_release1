#include "data.h"
#include "image.h"

void Print_Usage ();
void Parse_cmdln_Options (int argc, char *argv[], Data *, Image *);
void LINEAR_TRANSFORM (double **, int, int);
void LOG_TRANSFORM (double **, int, int);

/* *************************************************************** */
int main(int argc, char *argv[]) 
/* 
 *
 * PURPOSE
 *
 *  Produce a png image from a binary data file.
 *
 * USAGE
 *
 *    [bin2png] file_name [opt]
 * 
 *
 * 
 * LAST MODIFIED: 
 *
 *   3 Jan 2006 by A. Mignone, e-mail: mignone@to.astro.it 
 *
 ***************************************************************** */
{
  int    ii, jj, nrec;
  char   filename[64];
  Data   Q;
  Image  image;

  printf ("bin2png - version 1.0\n");
  printf ("Last modified Wed May 30 2007\n");
  printf ("Copyright(C) 2006,2007 by A. Mignone (mignone@to.astro.it)\n\n");

  if (argc <= 2) {
    Print_Usage();
    exit(1);
  }  

  Q.file = argv[1];

/* ----------------------------
    set some default values
   ---------------------------- */

  image.get_max_scale = YES;
  image.get_min_scale = YES;

  set_color_map(&image.colormap, "bw");
  Q.max_data = 0.0;
  Q.min_data = 0.0;
  Q.beg_rec  = -1;
  Q.end_rec  = -1;
  Q.nx       = -1;
  Q.ny       = -1;
  Q.nz       = -1;
  Q.is_float    = NO;
  Q.swap_endian = NO;
  Q.transform   = LINEAR_TRANSFORM;
   
  sprintf (image.name,"%s",Q.file);

  Parse_cmdln_Options (argc, argv, &Q, &image);
  Q.slice_coord--;  /* -- now switch to C array conventions --*/

/* ---------------------------------------------------------
                        Allocate memory 
   --------------------------------------------------------- */

  Q.data = matrix(Q.nrow, Q.ncol);

  for (nrec = Q.beg_rec; nrec <= Q.end_rec; nrec++){
    
    printf ("\n");
    if (Q.beg_rec == Q.end_rec){ /*  -- do not put record number
                                        if first and last record 
                                        are the same              -- */ 
      sprintf (filename,"%s.png",image.name);
    }else{
      sprintf (filename,"%s.%04d.png",image.name,nrec);
    }

    Read_Binary (&Q, nrec, Q.is_float, Q.swap_endian);
    Q.transform(Q.data, Q.ncol, Q.nrow);

 /* ------------------------------------------
       get max and min for scaling purposes
    ------------------------------------------ */
 
    if (image.get_max_scale){
      Q.max_data = -1.e38;
      for (jj = 0; jj < Q.nrow; jj++){
      for (ii = 0; ii < Q.ncol; ii++){
        if (Q.data[jj][ii] > Q.max_data) Q.max_data = Q.data[jj][ii];
      }}
    }

    if (image.get_min_scale){
      Q.min_data =  1.e38;
      for (jj = 0; jj < Q.nrow; jj++){
      for (ii = 0; ii < Q.ncol; ii++){
        if (Q.data[jj][ii] < Q.min_data) Q.min_data = Q.data[jj][ii];
      }}
    }

    printf (" > record # %d\n",nrec);
    printf (" > Max: %12.6e, Min: %12.6e\n",Q.max_data,Q.min_data);
  
  /* -----------------------------------------
       allocate memory for an image and set 
       its background 
     ----------------------------------------- */

    image.image = (png_bytepp)malloc(image.ny*sizeof(png_bytep));
    for (jj = 0; jj < image.ny; jj++) {
      image.image[jj] = (png_bytep)malloc(6*image.nx*sizeof(png_byte));
    }

    fill (image.image, 0, 0, image.nx - 1, image.ny - 1, &image.colormap, 255);
    put_data(Q.data, Q.max_data, Q.min_data, &image, &image.colormap);
    write_png (image.image, image.nx, image.ny, filename);
  }

  free_matrix (Q.data);
}

/* ****************************************************************************  */
void Print_Usage ()
/*
 *
 *
 *
 *
 *
 ****************************************************************************** */
{
  printf ("Usage: bin2png file -size <nx>x<ny>x<nz> [options]\n\n");
  printf (" file                       the name of a valid binary data file;\n");
  printf (" -size, -s <nx>x<ny>x<nz>   specifies the number of points of the binary \n");
  printf ("                            data. If the array is two dimensional, only \n");
  printf ("                            <nx>x<ny> may be given;\n\n");
  printf ("[options] are:\n\n");
  printf (" -float, -f              use this option if data has single precision.\n");
  printf ("                         Default is double precision;\n");
  printf (" -rec, -r <beg>-<end>    specifies the initial and final record numbers\n");
  printf ("                         in a binary file. Only one field may be given.\n");
  printf ("                         Positive record numbers are counted since the\n");
  printf ("                         beginning of file; negative number from the end.\n");
  printf ("                         Default is last record;\n");
  printf (" -swap-endian            swap endianity. This switch must be used when\n");
  printf ("                         the byte ordering is reversed.\n");
  printf (" -icut <int>             take a 2-D slice cut in the YZ plane at i = <int>,\n");
  printf ("                         where i is the coordinate orthogonal to the plane.\n");
  printf ("                         This option is useful when the array is three\n");
  printf ("                         dimensional;\n");
  printf (" -jcut <int>             take a 2-D slice cut in the XZ plane at j = <int>;\n");
  printf (" -kcut <int>             take a 2-D slice cut in the XY plane at k = <int>;\n");
  printf ("                         This is the default\n");
  printf (" -log                    take the logarithm of the data;\n");
  printf (" -colormap, -c <string>  change the colormap. <string> may be:\n\n");
  printf ("                           bw      (BLACK and WHITE)\n");
  printf ("                           blue    (BLUE-WHITE)\n");
  printf ("                           red     (RED)\n");
  printf ("                           bgry    (BLUE-GREEN-RED-YELLOW)\n");
  printf ("                           gmm2    ( )\n");
  printf ("                           green   (GREEN-WHITE)\n");
  printf ("                           br      (BLUE-RED)\n\n");
  printf (" -max <float>            sets the highest colormap value to <float>;\n");
  printf (" -min <float>            sets the lowest colormap value to <float>;\n");
  printf (" -output, -o <name>      specify the base output file name. If more than one\n");
  printf ("                         record is specified (e.g. -r 2-4), the output png file\n");
  printf ("                         will be named name.rec.png, where rec is a 4-digit\n");
  printf ("                         (zero-padded) record number. For a single record\n");
  printf ("                         name.png will be used\n");

}

/* ***************************************************************************** */
void Parse_cmdln_Options (int argc, char *argv[], Data *Q, Image *image)
/*
 *
 *
 *
 *
 *
 ****************************************************************************** */
{
  int ii;
  char *indx_i, *indx_j, *indx_k;

  for (ii = 2; ii < argc; ii++){

    if (!strcmp(argv[ii], "--help") ||
        !strcmp(argv[ii], "-help")){

      Print_Usage();
      exit(1);

    }else if (!strcmp(argv[ii], "-size") || !strcmp(argv[ii],"-s")){

      ii++;
      Q->nx = atoi(strtok(argv[ii],"x"));
      Q->ny = atoi(strtok(NULL,"x"));

      if ( (indx_i = strtok(NULL,"x")) == NULL){
        Q->nz = 1;
      }else{
        Q->nz = atoi(indx_i);
      }

      /* ----------------------------
          assign image size defaults 
         ---------------------------- */

      Q->ncol = Q->nx;
      Q->nrow = Q->ny;
      Q->slice_plane = XY_PLANE;
      Q->slice_coord = 1;

    }else if (!strcmp(argv[ii],"-float") || !strcmp(argv[ii],"-f")){

      Q->is_float = YES;

    }else if (!strcmp(argv[ii], "-rec") || !strcmp(argv[ii],"-r")){

      Q->beg_rec = atoi(strtok(argv[++ii],"-"));
      if ((indx_i = strtok(NULL,"-")) == NULL){
        Q->end_rec = Q->beg_rec;
      }else{
        Q->end_rec = atoi(indx_i);
      }

    }else if (!strcmp(argv[ii], "-swap-endian")){

       Q->swap_endian = YES;
       printf (" > Endianity is swapped\n");

    }else if (!strcmp(argv[ii], "-kcut")){

      Q->slice_plane = XY_PLANE;
      Q->slice_coord = atoi(argv[++ii]);
      Q->ncol = Q->nx;
      Q->nrow = Q->ny;
      if (Q->slice_coord <= 0 || Q->slice_coord > Q->nz){
        printf (" ! slice coordinate must be in [1, %d]\n", Q->nz);
        exit(1);
      }

    }else if (!strcmp(argv[ii], "-jcut")){

      Q->slice_plane = XZ_PLANE;
      Q->slice_coord = atoi(argv[++ii]);
      Q->ncol = Q->nx;
      Q->nrow = Q->nz;
      if (Q->slice_coord <= 0 || Q->slice_coord > Q->ny){
        printf (" ! slice coordinate must be in [1, %d]\n", Q->ny);
        exit(1);
      }
    }else if (!strcmp(argv[ii], "-icut")){

      Q->slice_plane = YZ_PLANE;
      Q->slice_coord = atoi(argv[++ii]);
      Q->ncol = Q->ny;
      Q->nrow = Q->nz;
      if (Q->slice_coord <= 0 || Q->slice_coord > Q->nx){
        printf (" ! slice coordinate must be in [1, %d]\n", Q->nx);
        exit(1);
      }
 
    }else if (!strcmp(argv[ii],"-log")){

      Q->transform = LOG_TRANSFORM;

    }else if (!strcmp(argv[ii],"-colormap") || !strcmp(argv[ii],"-c")){

      set_color_map(&(image->colormap), argv[++ii]);

    }else if (!strcmp(argv[ii],"-max")){

      Q->max_data = atof(argv[++ii]);
      image->get_max_scale = NO;
    
    }else if (!strcmp(argv[ii],"-min")){

      Q->min_data = atof(argv[++ii]);
      image->get_min_scale = NO;
    
    }else if (!strcmp(argv[ii],"-output") || !strcmp(argv[ii],"-o")){
      
      sprintf (image->name,"%s",argv[++ii]);

    }else{

      printf (" ! Unknown option %s\n",argv[ii]);
      exit(1);

    }
  }

/* ----------------------------
     check if size has been 
     specified 
   ---------------------------- */

  if (Q->nx == -1 || Q->ny == -1){
    printf ("you must specify -size <nx>x<ny>x<nz>\n");
    exit(1); 
  }

  image->nx = Q->ncol;
  image->ny = Q->nrow;

/* ----------------------------
     print some nice output
   ---------------------------- */

  printf (" > Image has size %d x %d\n",image->nx,image->ny);
  printf (" > precision is ");
  if (Q->is_float){
    printf ("single\n");
  }else{
    printf ("double\n");
  }
  
  if (Q->slice_plane == XY_PLANE){
    if (Q->slice_coord > Q->nz) Q->slice_coord = Q->nz;
    printf (" > XY Slice, at k = %d\n", Q->slice_coord);

  }else if (Q->slice_plane == XZ_PLANE) {
    if (Q->slice_coord > Q->ny) Q->slice_coord = Q->ny;
    printf (" > XZ Slice, at j = %d\n",Q->slice_coord);
  }else if (Q->slice_plane == YZ_PLANE) {
    if (Q->slice_coord > Q->nx) Q->slice_coord = Q->nx;
    printf (" > YZ Slice, at i = %d\n",Q->slice_coord);
  }
  
  if (Q->transform == LOG_TRANSFORM){
    printf (" > Log transform\n");
  }      
}

/* ############################################################# */
void LINEAR_TRANSFORM (double **q, int nx, int ny)
/* 
 #
 #
 #
 #
 ############################################################### */
{
  return;
}

/* ############################################################# */
void LOG_TRANSFORM (double **q, int nx, int ny)
/* 
 #
 #
 #
 #
 ############################################################### */
{
  int ii,jj;

  for (jj = 0; jj < ny; jj++){
  for (ii = 0; ii < nx; ii++){
    q[jj][ii] = log10(q[jj][ii]);
  }}

}

