pro readHDF,filename,pdom,boxes,vars,dx

 checkfile,filename

 ifil = H5F_OPEN(filename)
 igen = H5G_OPEN(ifil,'/')

 inlev = H5A_OPEN_NAME(igen,'num_levels')
 nlev = H5A_READ(inlev)
 H5A_CLOSE,inlev

 icomp = H5A_OPEN_NAME(igen,'num_components')
 ncomp = H5A_READ (icomp)
 H5A_CLOSE,icomp

 lev = strarr(nlev)
 for i=0,nlev-1 do lev[i] = 'level_'+strcompress(string(i),/REMOVE_ALL)

 scomp = strarr(ncomp)
 comp = strarr(ncomp)

for i=0,ncomp-1 do begin

 scomp[i] = 'component_'+strcompress(string(i),/REMOVE_ALL)
 icomp = H5A_OPEN_NAME(igen,scomp[i])
 comp[i] = H5A_READ(icomp)
 H5A_CLOSE,icomp

endfor

 Str = 'Refinement level [0-'+strcompress(string(nlev-1),/REMOVE_ALL)+']:'
 Read,PROMPT=Str,ll,FORMAT='(I)'

 ilev = H5G_OPEN(ifil,lev[ll])

 iprobdom = H5A_OPEN_NAME(ilev,'prob_domain')
 pdom = H5A_READ(iprobdom)
 H5A_CLOSE,iprobdom

 idx = H5A_OPEN_NAME(ilev,'dx')
 dx = H5A_READ(idx)
 H5A_CLOSE,idx

 idata = H5D_OPEN(ilev,'data:datatype=0')
 data = H5D_READ(idata)
 H5D_CLOSE,idata

 iboxes = H5D_OPEN(ilev,'boxes')
 boxes = H5D_READ(iboxes)
 H5D_CLOSE,iboxes

 ilo = pdom.lo_i
 jlo = pdom.lo_j
 ihi = pdom.hi_i
 jhi = pdom.hi_j

 nx = ihi-ilo+1
 ny = jhi-jlo+1

 vars = fltarr(nx,ny,ncomp)

 nbox = n_elements(boxes.lo_i)
 ncount = long(0)

 print,'Variables [0-'+strcompress(string(ncomp-1),/REMOVE_ALL)+']:',comp
 print,'Resolution: '+strcompress(string(nx),/REMOVE_ALL)+' x '+strcompress(string(ny),/REMOVE_ALL)
 print,'Number of boxes: '+strcompress(string(nbox),/REMOVE_ALL)

for ib = 0,nbox-1 do begin

 illoc = boxes[ib].lo_i
 ihloc = boxes[ib].hi_i
 jlloc = boxes[ib].lo_j
 jhloc = boxes[ib].hi_j

for ic =0,ncomp-1 do begin
 for j = jlloc,jhloc do begin
  for i = illoc,ihloc do begin

  vars[i,j,ic] = data[ncount]

  ncount = ncount + long(1)

  endfor
 endfor
endfor

endfor

 H5G_CLOSE,igen
 H5G_CLOSE,ilev
 H5F_CLOSE,ifil

return
end
