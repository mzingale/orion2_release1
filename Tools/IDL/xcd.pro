;+
; NAME:
;   xcd
;
; PURPOSE:
;   Change current directory via mouse.
;
;   Two lists are displayed side by side.  The one on the left shows
;   directories.  Click on a directory to cd there.  The list
;   on the right shows files to help you see where you are.
;   (Selecting from the list on the right does not do anything.)
; CATEGORY:
;   Utility.
; CALLING SEQUENCE:
;   xcd
; INPUTS:
;   None.
; KEYWORD PARAMETERS:
;   Debug: If set, run in debug mode.
; OUTPUTS:
;   None.
; SIDE EFFECTS:
;   Your current directory can be changed.
; RESTRICTIONS:
;   Windows, OpenVMS & unix platforms only.  Originally written on Windows95.
;   Should work on other Windows platforms, but I (Paul) havn't tried it.
;   Unix functionality written on HP and SGI machines.
;
; PROCEDURE:
;   Xcd uses a IDLexDirChanger object.
;
; MODIFICATION HISTORY:
;   Paul C. Sorenson, July 1997. paulcs@netcom.com.
;        Written with IDL 5.0.
;   Jim Pendleton, July 1997. jimp@rsinc.com
;        Modified for compatability with OpenVMS as a basis for
;        platform independent code
;   Paul C. Sorenson, July 13, 1997.  Changes so that DirListing class
;        methods do not return pointers to data members.  (Better
;        object-oriented design that way.)
;   Karsten Rodenacker, July 16, 1997. Changes using my routine GET_DEVICES
;        and additions for unix machines
;   Paul C. Sorenson. July 22, 1997.  Hardcode drive "A:" to always appear
;        when running on Windows.
;   Paul C. Sorenson. Nov 8, 1997.  Avoid testing for the existence of "A:"
;        on Windows platform.  Doing so was slow, and unnessesary due to
;        July 22 modification.
;   Paul C. Sorenson. December 10, 1997.  Make xcd be a widget program with
;        a state rather than an object.
;   Paul C. Sorenson, 1999-Feb2000.  Encapsulate directory changing
;        functionality into IDLexDirChanger object and use an instance
;        of that.
;-
;----------------------------------------------------------------------
pro xcd_event, event
compile_opt hidden
end
;----------------------------------------------------------------------
pro xcd_cleanup, wid
compile_opt hidden

cd, current=current & print, 'Current directory: ' + current ; A nicety.
widget_control, wid, get_uvalue=oDirChanger
obj_destroy, oDirChanger
end
;----------------------------------------------------------------------
pro xcd, debug=debug
on_error, ([2, 0])[keyword_set(debug)]
tlb = widget_base(title='xcd') ; top-level base.
;
;Create an IDLexDirChanger widget object in top-level base.
;
oDirChanger = obj_new('IDLexDirChanger', tlb, debug=debug)
if oDirChanger eq obj_new() then $
   message, 'failed to create IDLexDirChanger object.'
;
;Center and realize our tlb.
;
widget_control, tlb, map=0
widget_control, tlb, /realize
tlb_geometry = widget_info(tlb, /geometry)
device, get_screen_size=scrsz
widget_control, tlb, $
   tlb_set_xoffset=(0 > (scrsz(0) - tlb_geometry.scr_xsize) / 2), $
   tlb_set_yoffset=(0 > (scrsz(1) - tlb_geometry.scr_ysize) / 2)
widget_control, tlb, map=1
;
widget_control, tlb, set_uvalue=oDirChanger
;
;Register with xmanager.
;
xmanager, 'xcd', tlb, /no_block, cleanup='xcd_cleanup'
end


