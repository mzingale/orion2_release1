function IDLexDirChanger::Rewrite, event
compile_opt hidden

self.oDirListing->GetProperty, $
   drive=drive, $
   path=path, $
   subdir_names=subdir_names, $
   file_names=file_names

normal_result = create_struct( $
   self->IDLexWidget::Rewrite(event), $
   'drive', drive, $
   'path', path $
   )
case 1 of
   keyword_set(self.return_subdirnames) and $
   keyword_set(self.return_filenames): $
      return, create_struct( $
         normal_result, $
         'subdirnames', subdir_names, $
         'filenames', file_names $
         )
   keyword_set(self.return_subdirnames): $
      return, create_struct( $
         normal_result, $
         'subdirnames', subdir_names $
         )
   keyword_set(self.return_filenames): $
      return, create_struct( $
         normal_result, $
         'filenames', file_names $
         )
   else: $
      return, normal_result
   endcase
end
;----------------------------------------------------------------------
pro IDLexDirChanger::RakeHeap
compile_opt hidden

ptr_free, self.pDriveNames
obj_destroy, self.oDirListing
end
;----------------------------------------------------------------------
pro IDLexDirChanger::cleanup
compile_opt hidden

self->IDLexDirChanger::RakeHeap
self->IDLexWidget::cleanup
end
;----------------------------------------------------------------------
pro IDLexDirChanger::GetProperty, value=value, _ref_extra=e

self.oDirListing->GetProperty, drive=drive, path=path
value = strcompress(drive + path, /remove_all)

self->IDLexWidget::GetProperty, _extra=e
end
;----------------------------------------------------------------------
pro IDLexDirChanger::SetProperty, value=value, _extra=e

if n_elements(value) gt 0 then $
   self->Update, value

self->IDLexWidget::SetProperty, _extra=e
end
;----------------------------------------------------------------------
function IDLexDirChanger::HandleEvent, event

catch, error_stat
if error_stat ne 0 then begin
   catch, /cancel
   void = dialog_message(!err_string, /error, dialog_parent=event.handler)
   self->Update ; Try again, this time without "cd".
   return, self->Rewrite(event)
   end

case event.id of
   self.wDirList: begin
      self.oDirListing->GetProperty, $
         path=path, $
         drive=drive, $
         subdir_names=subdir_names
;
;     Construct full (if possible) pathname, and cd to it.  (Using
;     a full, rather than relative pathname here makes self impervious
;     to directory changes made by other IDL programs or from the
;     command line.)
;
      case !version.os_family of
         'Windows' : begin
            if rstrpos(path, '\') ne (strlen(path) - 1) then $
               path = path + '\'
            cd, drive + path + subdir_names[event.index]
            end
         'vms' : begin
            subdir = subdir_names[event.index]
            if (subdir ne '[-]' and subdir ne 'sys$login') then begin
               rightbracket = rstrpos(path, ']')
               leftbracket = strpos(path, '[')
               path = strmid(path, leftbracket + 1, rightbracket - $
                  leftbracket - 1)
               newdir = drive $
                  + '[' + path + '.' $
                  + subdir + ']'
               end $
            else begin
               newdir = subdir
               end
            cd, newdir
            end
         'unix' : begin
            if rstrpos(path, '/') ne (strlen(path) - 1) then $
               path = path + '/'
            cd, path + subdir_names[event.index]
            end
         else:
         endcase
;
      self->Update
      return, self->Rewrite(event)
      end
   self.wDriveList: begin
      widget_control, /hourglass
      case !version.os_family of
         'Windows' : cd, (*self.pDriveNames)[event.index]
         'vms' : cd, (*self.pDriveNames)[event.index] + '[000000]'
         'unix' : cd, (*self.pDriveNames)[event.index]
         else:
         endcase
      self->Update
      return, self->Rewrite(event)
      end
   else: begin
      end
   endcase

end
;----------------------------------------------------------------------
pro IDLexDirChanger::Update, location
;
;Procedure IDLexDirChanger::UPDATE: set self's widgets and state values to
;  reflect the current directory, or LOCATION directory if
;  supplied.
;
widget_control, /hourglass

obj_destroy, self.oDirListing
self.oDirListing = obj_new('IDLexDirListing', location)
self.oDirListing->GetProperty, $
   drive=drive, $
   path=path, $
   subdir_names=subdir_names, $
   file_names=file_names

widget_control, self.wDriveList, set_droplist_select=$
   (where(*self.pDriveNames eq drive))[0]
widget_control, self.wLabel, set_value=path
widget_control, self.wDirList, set_value=subdir_names
widget_control, self.wFileList, set_value=file_names
end
;----------------------------------------------------------------------
function IDLexDirChanger::init, wParent, $
   value=value, $
   debug=debug, $
   _extra=e

catch, error_status
if error_status ne 0 then begin
   catch, /cancel
   printf, $
      -2, $
      !error_state.msg_prefix, $
      !error_state.msg, $
      !error_state.sys_msg
   self->IDLexDirChanger::RakeHeap
   return, 0
   end
if keyword_set(debug) then $
   catch, /cancel

;
;Get all existing drives.
;
widget_control, /hourglass
case !version.os_family of
   'MacOS': begin
      message, 'IDLexDirChanger has not been implemented for Mac OS.'
      end
   'Windows': begin
;
;     Assume that drive "A:" is a removable (e.g. floppy) drive.
;     If no disk in in the removable drive, we still want the drive
;     to appear in wDriveList.  If you have other removable drives,
;     you may want to modify the following code to treat them
;     in a similar fashion.
;
      drives = string(bindgen(1,25) + (byte('B'))[0]) + ':' ; ['B:',...'Z:']
      indx = where(direxist(drives))
      if indx[0] ne -1 then $
         self.pDriveNames = ptr_new(['A:', drives[indx]]) $
      else $
         self.pDriveNames = ptr_new(['A:'])
      end
   else: begin
      self.pDriveNames = ptr_new(get_devices())
      end
   endcase
;
;Create widgets.
;
if not self->IDLexWidget::init(wParent, _extra=e) then $
   message, 'Failed to initialize IDLexWidget part of self.'

catch, error_status
if error_status ne 0 then begin
   catch, /cancel
   printf, $
      -2, $
      !error_state.msg_prefix, $
      !error_state.msg, $
      !error_state.sys_msg
   self->IDLexDirChanger::RakeHeap
   self->IDLexWidget::cleanup
   return, 0
   end
if keyword_set(debug) then $
   catch, /cancel

if n_elements(uvalue) gt 0 then $
   widget_control, self.wIDBase, set_uvalue=uvalue

self.wBase = widget_base(self.wIDBase, /column)

wReadoutBase = widget_base(self.wBase, /row)
self.wDriveList = widget_droplist(wReadoutBase, value=*self.pDriveNames)
self.wLabel = widget_label(wReadoutBase, /dynamic_resize)

wListBase = widget_base(self.wBase, /row)
ysize = 20 ; Looks good on my (Paul's) monitor.
self.wDirList = widget_list(wListBase, ysize=ysize)
self.wFileList = widget_list(wListBase, ysize=ysize)
;
;Update self to reflect the appropriate directory.
;
self->Update, value
;
;Store a reference to self, and associate event handler.
;
widget_control, self.wBase, $
   set_uvalue=self, $
   event_func='IDLexWidget__HandleEvent'
;
return, 1 ; success.
end
;----------------------------------------------------------------------
pro IDLexDirChanger__define
void = {IDLexDirChanger, $
   inherits IDLexWidget, $
   wBase:       0L, $      ; contains all subsequent widgets listed here
   wDriveList:  0L, $      ; droplist of available drives.
   wLabel:      0L, $      ; shows name of current directory
   wDirList:    0L, $      ; shows sub-directories in current directory
   wFileList:   0L, $      ; shows files in current directory
   pDriveNames: ptr_new(),$; String array.  e.g. ['A', 'C:', 'D:', etc.]
   oDirListing: obj_new(),$; listing of current directory, $
   return_subdirnames: 0b,$; boolean.  1=include subdirnames in events.
   return_filenames: 0b $  ; boolean.  1=include filenames in events.
   }
end

