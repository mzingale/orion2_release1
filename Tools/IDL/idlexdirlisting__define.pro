function IDLexDirListing::init, location
;
;Purpose: construct listing of LOCATION's contents.
;INPUT:
;  LOCATION (optional): string indicating the directory we want listing
;                       of. default is current directory.
;
catch, error_stat
if error_stat ne 0 then begin
   catch, /cancel
   print, !err_string
   return, 0
   end
;
;Store name of location.
;
if n_elements(location) gt 0 then $
   pushd, location
cd, current=current
case !version.os_family of
   'Windows' : begin
      self.drive = strupcase(strmid(current, 0, 2))
      self.path = strmid(current, 2, strlen(current))
      end
   'vms' : begin
      colon = rstrpos(current, ':')
      self.drive = strmid(current, 0, colon + 1)
      rightbracket = rstrpos(current, ']')
      self.path = strmid(current, colon + 1, rightbracket - colon)
      end
   'unix' : begin
      self.drive = strmid(current, 0, rstrpos(current, '/'))
      self.path = current
      end
   else :
   endcase
;
;Obtain listing of location's contents.
;
case !version.os_family of
   'Windows' : listing = findfile()
   'vms' : listing = findfile()
   'unix' : listing = findfile('-Fa')
   else :
   endcase
if n_elements(location) gt 0 then $
   popd
;
;Divide into direcory-only & file-only listings.
;
flags = bytarr(n_elements(listing))
case !version.os_family of
   'Windows' : begin
      for i=0,n_elements(listing)-1 do begin
         if rstrpos(listing[i], '\') eq (strlen(listing[i]) - 1) then $
            flags[i] = 1b
         end
      end
   'vms' : begin
      for i=0,n_elements(listing)-1 do begin
         dotdir = strpos(listing[i], '.DIR;')
         if dotdir ne -1 then begin
            flags[i] = 1b
            rightbracket = rstrpos(listing[i], ']')
            listing[i] = strmid(listing[i], rightbracket + 1, $
               dotdir - rightbracket - 1)
            end
         end
      end
   'unix' : begin
      for i=0,n_elements(listing)-1 do begin
         dotdir = strpos(listing[i], '/',1)
         if dotdir ne -1 then begin
            flags[i] = 1b
            rightbracket = strpos(listing[i], '/', dotdir + 1)
            listing[i] = strmid(listing[i], rightbracket + 1, $
               dotdir - rightbracket - 1)
            end
         end
      end
   else :
   endcase

dirs_indx = where(flags, dir_count)
files_indx = where(flags eq 0b, file_count)

if dir_count gt 0 then begin
   dirs = listing[dirs_indx]
   dirs = dirs[sort(strupcase(dirs))]
   case !version.os_family of
      'Windows' : begin
         current_dir_indx = where((dirs eq '.\'), count)
         if count gt 0 then begin
            other_dir_indx = where((dirs ne '.\'), count)
            if count gt 0 then $
               dirs = dirs[[current_dir_indx, other_dir_indx]]
            end
         end
      'vms' :
      'unix' : begin
         current_dir_indx = where((dirs eq '.'), count)
         if count gt 0 then begin
            other_dir_indx = where((dirs ne '.'), count)
            if count gt 0 then $
               dirs = dirs[[current_dir_indx, other_dir_indx]]
            end
         end
      else :
      endcase
   if (!version.os_family eq 'vms') then $
      dirs = ['[-]', 'sys$login', dirs]
   end $
else begin
   if (!version.os_family eq 'vms') then begin
      dirs = ['[-]', 'sys$login']
      end $
   else begin
      dirs = ''
      end
   end

if file_count gt 0 then begin
   files = listing[files_indx]
   case !version.os_family of
      'Windows':
      'vms': begin
          for i = 0l, n_elements(files) - 1 do begin
             rightbracket = rstrpos(files[i], ']')
             files[i] = strmid(files[i], rightbracket + 1, $
                strlen(files[i]))
             end
          end
      'unix':
      endcase
   files = files[sort(strupcase(files))]
   end $
else begin
   files = ''
   end
;
;Store pointers to resulting string arrays.
;
self.pSubdirNames = ptr_new(dirs, /no_copy)
self.pFileNames = ptr_new(files, /no_copy)
return, 1 ; Success.
end
;----------------------------------------------------------------------
pro IDLexDirListing::cleanup
ptr_free, self.pSubdirNames
ptr_free, self.pFileNames
end
;----------------------------------------------------------------------
pro IDLexDirListing::GetProperty, $
    subdir_names=subdir_names, $
    file_names=file_names, $
    path=path, $
    drive=drive

subdir_names = *self.pSubdirNames
file_names = *self.pFileNames
path = self.path
drive = self.drive
end
;----------------------------------------------------------------------
pro IDLexDirListing__define
void = {IDLexDirListing, $
   drive: '',               $ ; e.g. 'C:'
   path: '',                $ ; location.  e.g. '\foo\bar'
   pSubdirNames: ptr_new(), $ ; string array of sub-directory names
   pFileNames:   ptr_new()  $ ; string array of file names
   }
end

