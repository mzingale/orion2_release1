pro cvel, img, vx, vy, x,y, $
             SAMPLE_X=sample_x,SAMPLE_Y=sample_y,$
             VCOLOR=vcolor,$
             _EXTRA=extra


scrh = size(img)
nx = scrh(1)
ny = scrh(2)

if (NOT KEYWORD_SET(vcolor)) then vcolor = 0

xpos = x#sin(y)
ypos = x#cos(y)
contour, img, x,y,xstyle=1,ystyle=1,$
         nlev=40, _EXTRA=extra,color=190, title='Lorentz Force, t = 1.7 sec',$
         xtitle = '!4h!3',ytitle='r',chars=1.5

NCELL = 1

if KEYWORD_SET (sample_x OR sample_y) then begin
  NCELL_X = sample_x
  NCELL_Y = sample_y
  u  = congrid(vx,nx/NCELL_X,ny/NCELL_Y)
  v  = congrid(vy,nx/NCELL_X,ny/NCELL_Y)
  xp = congrid(x,nx/NCELL_X)
  yp = congrid(y,ny/NCELL_Y)

  velovect,u,v,xp,yp,/overplot,$
           color=vcolor,length=1.5,missing=80
endif else begin
  velovect,vx,vy,x,y,/overplot,$
           color=vcolor
endelse




end
