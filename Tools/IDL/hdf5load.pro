COMMON PLUTO_VAR, vars
COMMON PLUTO_GRID, x1,x2,x3

;+
;
; NAME:      hdf5load
;
; AUTHOR:    Andrea Mignone
;
; REQUIRES:  HDF5 support.
;
; PURPOSE:   - read a HDF5 file and store its content on the variables vars;
;
;
; SYNTAX:    hdf5load, filename[, var=var]
;
; KEYWORD & PARAMETERS:
;
;           var = the variable number (starts from 1)
;
;
;
;  EXAMPLES:
;
;      pload,10
;
;
;
; LAST MODIFIED
;
;   March 30, 2008 by A. Mignone (mignone@to.astro.it)
;
;+

PRO hdf5load, filename, var=var


COMMON PLUTO_VAR
COMMON PLUTO_GRID

checkfile,filename



rat = 1
ifil = H5F_OPEN(filename)
igen = H5G_OPEN(ifil,'/')

inlev = H5A_OPEN_NAME(igen,'num_levels')
nlev = H5A_READ(inlev)
H5A_CLOSE,inlev

icomp = H5A_OPEN_NAME(igen,'num_components')
ncomp = H5A_READ (icomp)
H5A_CLOSE,icomp

lev = strarr(nlev)
for i=0,nlev-1 do lev[i] = 'level_'+strcompress(string(i),/REMOVE_ALL)

freb = intarr(nlev)

FOR nl = nlev-1,0,-1 DO BEGIN

 ilev = H5G_OPEN(ifil,lev[nl])

 IF (nl EQ nlev-1) THEN BEGIN
   freb[nl] = 1
   iprobdom = H5A_OPEN_NAME(ilev,'prob_domain')
   pdom = H5A_READ(iprobdom)
   H5A_CLOSE,iprobdom
   ilo = pdom.lo_i
   jlo = pdom.lo_j
   ihi = pdom.hi_i
   jhi = pdom.hi_j
   nx = ihi-ilo+1
   ny = jhi-jlo+1
   idx = H5A_OPEN_NAME(ilev,'dx')
   dx = H5A_READ(idx)
   H5A_CLOSE,idx
 ENDIF ELSE BEGIN
  irat = H5A_OPEN_NAME(ilev,'ref_ratio')
  rat = H5A_READ(irat)
  H5A_CLOSE,irat
  freb[nl] = freb[nl+1]*rat
 ENDELSE

 H5G_CLOSE,ilev

ENDFOR

vars = dblarr(nx,ny,ncomp)
x1 = (findgen(nx)+0.5)*dx
x2 = (findgen(ny)+0.5)*dx

; --------------------------------------------
;     select variables to load 
; --------------------------------------------

loadvar = intarr(ncomp)
loadvar(*) = 1

IF (KEYWORD_SET(var)) THEN BEGIN
  FOR ic = 0, ncomp-1 DO BEGIN
    loadvar(ic) = ic EQ (var-1)
  ENDFOR
ENDIF

print,ncomp
print,loadvar

; ----------------------------------------------
;   read variables
; ----------------------------------------------

FOR nl = 0, NLEV - 1 DO BEGIN

  xbox = rebin(x1,nx/rat^(NLEV - 1 - nl))-10.;

  ilev = H5G_OPEN(ifil,lev[nl])

  idata = H5D_OPEN(ilev,'data:datatype=0')
  data  = H5D_READ(idata)
  H5D_CLOSE,idata

  iboxes = H5D_OPEN(ilev,'boxes')
  boxes  = H5D_READ(iboxes)
  H5D_CLOSE,iboxes

  nbox = n_elements(boxes.lo_i)
  ncount = long(0)

  FOR ib = 0,nbox-1 DO BEGIN

    illoc = boxes[ib].lo_i
    ihloc = boxes[ib].hi_i
    jlloc = boxes[ib].lo_j
    jhloc = boxes[ib].hi_j

    FOR ic = 0, ncomp-1 DO BEGIN
    FOR j  = jlloc,jhloc DO BEGIN
    FOR i  = illoc,ihloc DO BEGIN

      IF (loadvar(ic)) THEN BEGIN
      
        FOR jsub = 0,freb[nl]-1 DO BEGIN
        FOR isub = 0,freb[nl]-1 DO BEGIN
          vars[i*freb[nl]+isub,j*freb[nl]+jsub,ic] = data[ncount]
         ;vars[i*freb[nl]+isub,j*freb[nl]+jsub,ic] = data[ncount]/xbox[i]

        ENDFOR
        ENDFOR
      ENDIF

      ncount = ncount + long(1)

    ENDFOR
    ENDFOR
    ENDFOR

  ENDFOR

 H5G_CLOSE,ilev

ENDFOR

H5G_CLOSE,igen
H5F_CLOSE,ifil

END
