; ----------------------------------
;
;  Make documentation
;
; ----------------------------------

sources = ["colorbar.pro","curl.pro",$
           "display.pro","div.pro","div3d.pro",$
           "extrema.pro", $
           "field_line.pro", "fourier.pro",$
           "get_frame.pro", "grad.pro", $
           "load_pluto_data.pro",$
           "mirror.pro", $
           "pload.pro", "polar.pro", "put_eps.pro",$
           "regrid.pro", $
           "set_multi_plot_pos.pro","shockfind.pro",$
           "tot_int.pro",$
           "vecfield.pro",$
           "xcd.pro"]

MK_HTML_HELP,sources,"idl_tools.html",/strict,$
             title="PLUTO IDL Tools"


END