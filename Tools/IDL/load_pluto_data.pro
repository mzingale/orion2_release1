;+
;
; NAME:       load_pluto_data
;
; AUTHOR:     A. Mignone
;
; DATE:       May 09, 2006
;
; SYNOPSIS:   load_pluto_data
;
; DESCRIPTION:  load grid and geometry information;
;-
PRO load_pluto_data, verbose=verbose

 COMMON PLUTO_GRID
 COMMON PLUTO_VAR
 COMMON PLUTO_RUN

;+++++++++++++++++++++++++++++++++++++++++++++++++++++
;  initialize some variables
;+++++++++++++++++++++++++++++++++++++++++++++++++++++

 n1 = 0
 n2 = 0
 n3 = 0
 nlast = 0
 i = 0
 j = 0
 GRID_UNIT = 98
 DEF_UNIT  = 97

 close,/all

;+++++++++++++++++++++++++++++++++++++++++++++++++++++
;  if dir is not set, use current working directory
;+++++++++++++++++++++++++++++++++++++++++++++++++++++

 cd,CURRENT=this_dir
 dir = this_dir+'/'

 IF (KEYWORD_SET (verbose)) THEN BEGIN
   print," "
   print," > Directory: "+dir
 ENDIF

;+++++++++++++++++++++++++++++++++++++++
;        read grid files
;+++++++++++++++++++++++++++++++++++++++

 openr,GRID_UNIT,dir+'grid.out',ERROR=err
 IF (NOT (err EQ 0)) THEN BEGIN
   print,"file "+dir+"grid.out not found"
   return
 ENDIF

 readf, GRID_UNIT, n1
 x1  = fltarr(n1)
 dx1 = fltarr(n1)
 for i = 0,n1-1 do begin
   readf, GRID_UNIT, j,xl_0,xc_0,xr_0,dx_0
   x1(i)  = xc_0
   dx1(i) = dx_0
 endfor

 readf, GRID_UNIT, n2
 x2  = fltarr(n2)
 dx2 = fltarr(n2)
 for i = 0,n2-1 do begin
   readf, GRID_UNIT,j,xl_0,xc_0,xr_0,dx_0
   x2(i)  = xc_0
   dx2(i) = dx_0
 endfor

 readf, GRID_UNIT, n3
 x3  = fltarr(n3)
 dx3 = fltarr(n3)
 for i = 0,n3-1 do begin
   readf, GRID_UNIT,j,xl_0,xc_0,xr_0,dx_0
   x3(i)  = xc_0
   dx3(i) = dx_0
 endfor

 IF (KEYWORD_SET (verbose)) THEN BEGIN
   print," > Grid sizes:  "
   print," "
   print,"      n1 = ",string(n1,format='(i4)'),$
         "  ;   x1 = [",string(x1(0)    - 0.5*dx1(0),format='(f8.4)'),$
              " ,",     string(x1(n1-1) + 0.5*dx1(n1-1),format='(f8.4)'),"]"

   print,"      n2 = ",string(n2,format='(i4)'),$
         "  ;   x2 = [",string(x2(0)    - 0.5*dx2(0),format='(f8.4)'),$
              " ,",     string(x2(n2-1) + 0.5*dx2(n2-1),format='(f8.4)'),"]"

   print,"      n3 = ",string(n3,format='(i4)'),$
         "  ;   x3 = [",string(x3(0)    - 0.5*dx3(0),format='(f8.4)'),$
              " ,",     string(x3(n3-1) + 0.5*dx3(n3-1),format='(f8.4)'),"]"
   print,"  "
 ENDIF

;++++++++++++++++++++++++++++++++++++++++
; Read definitions.h if present
;++++++++++++++++++++++++++++++++++++++++

 openr,DEF_UNIT,dir+'definitions.h',ERROR=err

 IF (NOT (err EQ 0)) THEN BEGIN
   print," > Can not find definitions.h, skipping geometry definition..."
 ENDIF ELSE BEGIN
   str = ' '
   FOR i = 0,30 DO BEGIN
     readf,DEF_UNIT,format='(A)',str
     str_el = strsplit(str,' ',/extract)
     IF (str_el(1) EQ 'GEOMETRY') THEN GOTO, DEF_GEOMETRY
  ENDFOR

;++++++++++++++++++++++++++++++++++++++
;  define geometry
;++++++++++++++++++++++++++++++++++++++

 DEF_GEOMETRY:
   IF (str_el(2) EQ 'CARTESIAN')   THEN BEGIN
     geometry = 'cartesian'
     x = x1
     y = x2
     z = x3
     IF (KEYWORD_SET (verbose)) THEN BEGIN
       print," > Geometry:    Cartesian (x = x1; y = x2; z = x3)"
     ENDIF
   ENDIF

   IF (str_el(2) EQ 'CYLINDRICAL') THEN BEGIN
     geometry = 'cylindrical'
     r   = x1
     z   = x2
     phi = x3
     IF (KEYWORD_SET (verbose)) THEN BEGIN
       print," > Geometry:    Cylindrical (r = x1; z = x2; phi = x3)"
     ENDIF
   ENDIF

   IF (str_el(2) EQ 'SPHERICAL') THEN BEGIN
     geometry = 'spherical'
     r     = x1
     theta = x2

     xpos = fltarr(n1,n2)  ; define projection on cartesian coordinates
     ypos = fltarr(n1,n2)

     for i = 0,n1-1 do begin
     for j = 0,n2-1 do begin
       xpos(i,j) = (x1(i)-0.0*dx1(i))*sin(x2(j)-0.0*dx2(j))
       ypos(i,j) = (x1(i)-0.0*dx1(i))*cos(x2(j)-0.0*dx2(j))
     endfor
     endfor

     IF (KEYWORD_SET (verbose)) THEN BEGIN
       print," > Geometry:    Spherical (xpos = r*sin(theta); ypos = r*cos(theta))"
     ENDIF
   ENDIF

   IF (str_el(2) EQ 'POLAR') THEN BEGIN
     geometry = 'polar'
     r   = x1
     phi = x2
     xpos = fltarr(n1,n2)  ; define projection on cartesian coordinates
     ypos = fltarr(n1,n2)

     for i = 0,n1-1 do begin
     for j = 0,n2-1 do begin


       xpos(i,j) = x1(i)*cos(x2(j))
       ypos(i,j) = x1(i)*sin(x2(j))
     endfor
     endfor

     print," > Geometry:    Polar (xpos = r*cos(phi); ypos = r*sin(phi))"
   ENDIF

 ENDELSE

 close,GRID_UNIT
 close,DEF_UNIT
end
