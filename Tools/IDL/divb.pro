

bx = b1f(nout)
by = b2f(nout)
divb = fltarr(n1,n2)

for i = 0, n1-1 do begin
for j = 0, n2-1 do begin
  divb(i,j) =   (bx(i+1,j) - bx(i,j))/dx1(i) $
              + (by(i,j+1) - by(i,j))/dx2(j)
endfor
endfor

print,max(abs(divb))
end

