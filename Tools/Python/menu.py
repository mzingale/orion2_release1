import curses
import sys
import os
import sys
import string
import time

######################################################
def dir_browse(entries, title):
#
#
######################################################
#
# Initialize curses
#
# set  tset -e ^? for erase if it doesnt work
#

  stdscr = curses.initscr()
  curses.noecho()
  curses.cbreak()
  stdscr.clear()
  stdscr.keypad(1)
  (y1,x1) = stdscr.getmaxyx()

#  stdscr.border('+')

#
# Write menu-title
#

  stdscr.addstr (0,0, ">> "+title+" <<", curses.A_UNDERLINE)

#
# Re-set  attributes to default
#

  n_entries = len(entries)
  attrib = []
  for entry in entries:
     attrib.append(0)

  attrib[0] = 1

#
# Set screen positions and origin
#

  y0   = 4
  x0   = 4
  ypos     = 0
  not_done = 1

  while not_done:

#
# Display menu
#

    n = 0
    for entry in entries:
      if attrib[n] == 0:
        if n == 0:                    # first entry 
          stdscr.addstr(n+y0,x0,entry)
          stdscr.addstr(n+y0,1,'   ')
        elif n == n_entries-1:        # last entry
          stdscr.addstr(n+y0+1,x0,entry)
          stdscr.addstr(n+y0+1,1,'    ')
        else:                         # all other entries
          stdscr.addstr(n+y0,x0,entry)
          stdscr.addstr(n+y0,x0+len(entry),'    ')

      if attrib[n] == 1:    
        if n == 0:
          stdscr.addstr(n+y0,1,'<- ')
          stdscr.addstr(n+y0,x0,entry,curses.A_REVERSE)
        elif n == n_entries-1:
          stdscr.addstr(n+y0+1,1,'    ')
          stdscr.addstr(n+y0+1,x0,entry,curses.A_REVERSE)
        else:
          stdscr.addstr(n+y0,x0+len(entry),' -> ')
          stdscr.addstr(n+y0,x0,entry,curses.A_REVERSE)

      n = n+1


    stdscr.addstr (y1-3,1,
                  '|Up/Down: move|Enter: select dir|Left/Right: Up/Down 1 level|q:quit|',
                   curses.A_REVERSE)

#
# User  Input
#

    c = stdscr.getch()

    if c == ord('q'):
      not_done = 0
      curses.nocbreak()
      curses.echo()
      curses.endwin()
      sys.exit()

    if (c == curses.KEY_UP or c == ord('u')):   #ord('u'):  # UP
      attrib[ypos] = 0
      ypos = max(ypos - 1,0)
      attrib[ypos] = 1


    if (c == curses.KEY_DOWN or c == ord('d')): #ord('d'):    #  DOWN
      attrib[ypos] = 0
      ypos = min(ypos + 1,n_entries-1)
      attrib[ypos] = 1

    if (c == curses.KEY_RIGHT  or c == ord('i') or c == ord('d')):
      curses.nocbreak()
      curses.echo()
      curses.endwin()
      return ['DOWN_ONE_LEVEL',ypos]

    if (ypos == 0 and (c == curses.KEY_LEFT  or c == ord('u'))):
      curses.nocbreak()
      curses.echo()
      curses.endwin()
      return ['UP_ONE_LEVEL',ypos]


    if (c == 10 or c == curses.KEY_EXIT): #ord('s'):  #  ENTER
      curses.nocbreak()
      curses.echo()
      curses.endwin()
      return ['SELECT_THIS_DIR',ypos]


######################################################
def select0(entries, title, extra):
#
#
######################################################
#
# Initialize curses
#
# set  tset -e ^? for erase if it doesnt work
#

  stdscr = curses.initscr()
  curses.noecho()
  curses.cbreak()
  stdscr.clear()
  stdscr.keypad(1)
  (y1,x1) = stdscr.getmaxyx()

#  stdscr.border('+')

#
# Write menu-title
#

#  stdscr.addstr (0,0,'>> Python setup (08.2006):')
  stdscr.addstr (0,0, ">> "+title+" <<", curses.A_UNDERLINE)
  y0_inc = 0
  if (extra != ''):
    stdscr.addstr (2,0, extra, curses.A_UNDERLINE)
    y0_inc = 3

  stdscr.addstr (y1-3,1,'|Up/Down Arrow Keys: move|Enter: make a selection|q: quit|',
                 curses.A_REVERSE)

#
# Re-set  attributes to default
#

  n_entries = len(entries) 
  attrib = []
  for entry in entries:
     attrib.append(0)

  attrib[0] = 1

# 
# Set screen positions and origin
#

  y0   = 3 + y0_inc
  x0   = 1
  ypos     = 0
  not_done = 1

  while not_done:

#
# Display menu
#

    n = 0
    for entry in entries:
      if attrib[n] == 0:
        stdscr.addstr(n+y0,x0,entry)

      if attrib[n] == 1: 
        stdscr.addstr(n+y0,x0,entry,curses.A_REVERSE)

      n = n+1

#
# User  Input
#

    c = stdscr.getch()

    if c == ord('q'): 
      not_done = 0
      curses.nocbreak()
      curses.echo()
      curses.endwin()
      sys.exit()

    if (c == curses.KEY_UP or c == ord('u')):   #ord('u'):  # UP
      attrib[ypos] = 0
      ypos = max(ypos - 1,0)
      attrib[ypos] = 1
      
      
    if (c == curses.KEY_DOWN or c == ord('d')): #ord('d'):    #  DOWN
      attrib[ypos] = 0
      ypos = min(ypos + 1,n_entries-1)
      attrib[ypos] = 1

    if (c == 10 or c == curses.KEY_EXIT): #ord('s'):  #  ENTER
      curses.nocbreak()
      curses.echo()
      curses.endwin()
      return entries[ypos]


######################################################
def select(entries, options, default, title):
#
#
######################################################

#
# Initialize curses
#
# set  tset -e ^? for erase if it doesnt work
#

  stdscr = curses.initscr()
  curses.noecho()
  curses.cbreak()
  stdscr.clear()
  stdscr.keypad(1)
  (y1,x1) = stdscr.getmaxyx()


#
# Write menu-title
#

  stdscr.addstr (0,0, ">> "+title+" <<", curses.A_UNDERLINE)
  stdscr.addstr (y1-3,1,'|Right/Left Arrow Keys: change|Enter: when done|q: quit|',
                 curses.A_REVERSE)


#
# Re-set  attributes to default
#

  n_entries = len(entries) 
  attrib = []
  for entry in entries:
     attrib.append(0)

  attrib[0] = 1

# 
# Set screen positions and origin
#

  y0   = 3
  x0   = 1
  ypos     = 0
  not_done = 1

  while not_done:

#
# Display menu
#

    n = 0
    for entry in entries:
      if attrib[n] == 0:
        stdscr.addstr(n+y0,x0,entry)
        stdscr.addstr(n+y0,x0+30,default[n])
        stdscr.clrtoeol()

      if attrib[n] == 1: 
        stdscr.addstr(n+y0,x0,entry,curses.A_REVERSE)
        stdscr.addstr(n+y0,x0+30,default[n], curses.A_UNDERLINE)
        stdscr.clrtoeol()

      n = n+1

#
# User  Input
#

    c = stdscr.getch()

    if c == ord('q'): 
      not_done = 0
      curses.nocbreak()
      curses.echo()
      curses.endwin()
      sys.exit()

    if (c == curses.KEY_RIGHT or c == ord('r')):   #ord('l'):  # RIGHT
      try:
        iopt = options[ypos].index(default[ypos])
      except:
        iopt = -1

      iopt = iopt + 1
      if iopt > len(options[ypos]) - 1:
         iopt = 0

      default[ypos] = options[ypos][iopt]

    if (c == curses.KEY_LEFT or c == ord('l')):   #ord('u'):  # LEFT
      iopt = options[ypos].index(default[ypos])
      iopt = iopt - 1
      if iopt == -1:
         iopt = len(options[ypos]) - 1

      default[ypos] = options[ypos][iopt]

    if (c == curses.KEY_UP or c == ord('u')):   #ord('u'):  # UP
      attrib[ypos] = 0
      ypos = max(ypos - 1,0)
      attrib[ypos] = 1
      
      
    if (c == curses.KEY_DOWN or c == ord('d')): #ord('d'):    #  DOWN
      attrib[ypos] = 0
      ypos = min(ypos + 1,n_entries-1)
      attrib[ypos] = 1

    if (c == 10 or c == curses.KEY_EXIT): #ord('s'):  #  ENTER
      curses.nocbreak()
      curses.echo()
      curses.endwin()
      return entries[ypos]

#    if (c == 127 or c == 8 or c == curses.KEY_BACKSPACE): 
#      curses.nocbreak()
#      curses.echo()
#      curses.endwin()
#      return
      

#################################################################
def put(num, names, title):
#
#
######################################################

#
# Initialize curses
#

  stdscr = curses.initscr()
  curses.noecho()
  curses.cbreak()
  stdscr.clear()
  stdscr.keypad(1)
  (y1,x1) = stdscr.getmaxyx()

#
# Write menu-title
#

  stdscr.addstr (0,0, ">> "+title+" <<", curses.A_UNDERLINE)
  stdscr.addstr (y1-3,1,'|Right Arrow: insert a new name|Enter: when done|q: quit|')

#
# Re-set  attributes to default
#
 
  attrib = []
  for n in range(num):
     attrib.append(0)

  attrib[0] = 1

# 
# Set screen positions and origin
#

  y0   = 3
  x0   = 1
  ypos      = 0
  not_done = 1

  while not_done:

#
# Display menu
#

    for n in range(num):
      if attrib[n] == 0:  
        stdscr.addstr(n+y0,x0,repr(n))
        stdscr.addstr(n+y0,x0+30,names[n])
        stdscr.clrtoeol()

      if attrib[n] == 1: 
        stdscr.addstr(n+y0,x0,repr(n),curses.A_REVERSE)
        stdscr.addstr(n+y0,x0+30,names[n], curses.A_UNDERLINE)
        stdscr.clrtoeol()

#
# User  Input
#

    c = stdscr.getch()

    if c == ord('q'): 
      not_done = 0
      curses.nocbreak()
      curses.echo()
      curses.endwin()
      sys.exit()

    if (c == curses.KEY_RIGHT  or c == ord('i')): 
      curses.echo()
      stdscr.addstr(ypos+y0,x0+30,'NAME > ',curses.A_UNDERLINE)
      new_name = stdscr.getstr()
      names.pop(ypos)
      names.insert(ypos,new_name)
      curses.noecho()
      stdscr.clrtoeol()
      
    if (c == curses.KEY_UP or c == ord('u')):   #ord('u'):  # UP
      attrib[ypos] = 0
      ypos = max(ypos - 1,0)
      attrib[ypos] = 1
      
      
    if (c == curses.KEY_DOWN or c == ord('d')): #ord('d'):    #  DOWN
      attrib[ypos] = 0
      ypos = min(ypos + 1,num-1)
      attrib[ypos] = 1

    if (c == 127 or c == 10 or c == curses.KEY_ENTER): #ord('s'):  #  ENTER
      curses.nocbreak()
      curses.echo()
      curses.endwin()
      return 


######################################################
def PROMPT_MESSAGE (strng):
#
#
######################################################
#
# Initialize curses
#
# set  tset -e ^? for erase if it doesnt work
#

  stdscr = curses.initscr()
  curses.noecho()
  curses.cbreak()
  stdscr.clear()
  stdscr.keypad(1)

#
# Write menu-title
#

  stdscr.addstr (1,1,strng,curses.A_BLINK)
  c = stdscr.getch()

  curses.nocbreak()
  curses.echo()
  curses.endwin()
