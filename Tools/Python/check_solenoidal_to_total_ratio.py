import h5py
import numpy as np
import numpy.fft
from math import *

f = h5py.File('zdrv.hdf5', 'r')
pertx = f['pertx'][:]
perty = f['perty'][:]
pertz = f['pertz'][:]
f.close()

n = pertx.shape
dtype = np.float
fx = np.real(np.fft.fftn(pertx))
fy = np.real(np.fft.fftn(perty))
fz = np.real(np.fft.fftn(pertz))
kx = np.zeros(n, dtype=dtype)
ky = np.zeros(n, dtype=dtype)
kz = np.zeros(n, dtype=dtype)
# perform fft k-ordering convention shifts
for j in range(0,n[1]):
    for k in range(0,n[2]):
        kx[:,j,k] = n[0]*np.fft.fftfreq(n[0])
for i in range(0,n[0]):
    for k in range(0,n[2]):
        ky[i,:,k] = n[1]*np.fft.fftfreq(n[1])
for i in range(0,n[0]):
    for j in range(0,n[1]):
        kz[i,j,:] = n[2]*np.fft.fftfreq(n[2])
k2 = kx**2+ky**2+kz**2
# comprssive part
fxc = np.zeros(n, dtype=dtype)
fyc = np.zeros(n, dtype=dtype)
fzc = np.zeros(n, dtype=dtype)
fxc = kx*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
fyc = ky*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
fzc = kz*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
# solenoidal part
fxs = np.zeros(n, dtype=dtype)
fys = np.zeros(n, dtype=dtype)
fzs = np.zeros(n, dtype=dtype)
fxs = fx - kx*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
fys = fy - ky*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
fzs = fz - kz*(kx*fx+ky*fy+kz*fz)/np.maximum(k2,1e-16)
normC = np.sqrt(np.sum(fxc**2 + fyc**2 + fzc**2)/np.product(n))
normS = np.sqrt(np.sum(fxs**2 + fys**2 + fzs**2)/np.product(n))
print 'solenoidal-to-total forcing ratio = ', normS/(normS+normC)
