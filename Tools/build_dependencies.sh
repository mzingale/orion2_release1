#!/bin/bash

## Automated script to install dependencies for ORION2 on ubuntu
## (This should *not* be modified for other systems,
##  since this script is used by Bitbucket CI to automatically run
##  the test suite.)

git clone https://github.com/spack/spack.git
. spack/share/spack/setup-env.sh
spack external find

# install openmpi v3 (ubuntu installes v4, which does not work)
spack install openmpi@3.1.6

# install hypre 2.11.2 (hypre provides no backward compatibility guarantees)
spack install hypre@2.11.2^openmpi@3.1.6

spack load openmpi@3.1.6
spack load hypre

# build Chombo
cd Lib/Chombo3.2/lib
cp mk/local/Make.defs.ubuntu mk/Make.defs.local
make lib
